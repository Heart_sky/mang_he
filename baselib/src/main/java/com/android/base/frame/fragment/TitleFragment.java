package com.android.base.frame.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.base.R;
import com.android.base.frame.title.ETitleTheme;
import com.android.base.frame.title.ETitleType;
import com.android.base.frame.title.TitleBar;
import com.android.base.tools.StatusBarUtil;

import static com.android.base.frame.title.ETitleType.NO_TITLE;
import static com.android.base.frame.title.ETitleType.OVERLAP_TITLE;
import static com.android.base.frame.title.ETitleType.SIMPLE_TITLE;

/**
 * Created by Sammie on 2017/9/7.
 */

public abstract class TitleFragment extends StatedFragment {

    private TitleBar titleBar;
    private View layoutView; //内容View

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = super.onCreateView(inflater, container, savedInstanceState);
        StatusBarUtil.darkMode(getActivity());
        switch (showToolBarType()) {
            case NO_TITLE:
                //不显示toolbar(全屏显示)
                if (layoutView != null) {
                    mView = layoutView;
                }
                break;
            case SIMPLE_TITLE:
                setSimpleTitle(container);
                break;
            case OVERLAP_TITLE:  //显示透明toolbar
                setOverlap(container);
                break;
        }
        return mView;
    }


    /**
     * 设置普通标题
     */
    private void setSimpleTitle(View rootView) {
        //默认显示不透明titlebar
        View parent = LayoutInflater.from(mContext).inflate(R.layout.layout_title_simple, (ViewGroup) rootView, false);
        titleBar = (TitleBar) parent.findViewById(R.id.titleBar);
        View child = null;
        if (layoutView == null) {
            child = LayoutInflater.from(mContext).inflate(getLayoutId(), (ViewGroup) parent, false);
        } else {
            child = layoutView;
        }
        ((ViewGroup) parent).addView(child);
        titleBar.setStatuBarHeight(StatusBarUtil.getStatusBarHeight(mContext));
        child.setPadding(child.getPaddingLeft(),
                child.getPaddingTop(),
                child.getPaddingRight(),
                child.getPaddingBottom());
        mView = parent;
    }

    /**
     * 默认透明背景，黑色文字
     */
    private void setOverlap(View rootView) {
        View parent = LayoutInflater.from(mContext).inflate(R.layout.layout_title_simple, (ViewGroup) rootView, false);
        titleBar = (TitleBar) parent.findViewById(R.id.titleBar);
        View child = null;
        if (layoutView == null) {
            child = LayoutInflater.from(mContext).inflate(getLayoutId(), (ViewGroup) parent, false);
        } else {
            child = layoutView;
        }
        ((ViewGroup) parent).addView(child, 0);
        mView = parent;
        titleBar.setDefaultTheme(ETitleTheme.DARK_TRANSPARENT);
    }


    protected TitleBar getTitleBar() {
        if (titleBar == null) {
            Log.e("TitleBar", "TitleBar没有初始化，请选择TitleBar模式");
            return null;
        }
        return titleBar;
    }

    /**
     * 控制Activity是否显示toolbar,重写该方法可修改
     */
    protected ETitleType showToolBarType() {
        return ETitleType.SIMPLE_TITLE;
    }

    /**
     * 可以对getLayoutID的内容包装后统一返回
     *
     * @param layoutView
     */
    protected void setLayoutView(View layoutView) {
        this.layoutView = layoutView;
    }

}
