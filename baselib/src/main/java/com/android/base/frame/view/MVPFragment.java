package com.android.base.frame.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;

import com.android.base.frame.fragment.LazyFragment;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.tools.TUtil;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Sammie on 2017/9/19.
 */

public abstract class MVPFragment<T extends XPresenter> extends LazyFragment {

    private T presenter;

    protected void open(Class clazz) {
        open(clazz, null);
    }

    protected void open(Class clazz, Map<String, Object> object) {
        startActivity(getParamIntent(clazz,object));
    }
    protected void open(Class clazz, Map<String, Object> object,int reqCode) {
       startActivityForResult(getParamIntent(clazz,object),reqCode);
    }

    private Intent getParamIntent( Class clazz, Map<String, Object> object){
        Intent intent = new Intent(getActivity(), clazz);
        if (object != null) {
            for (Map.Entry<String, Object> entry : object.entrySet()) {
                if (entry.getValue() instanceof String){
                    intent.putExtra(entry.getKey(),(String)entry.getValue());
                }else if(entry.getValue() instanceof Integer){
                    intent.putExtra(entry.getKey(),(Integer)entry.getValue());
                }else if(entry.getValue() instanceof Float){
                    intent.putExtra(entry.getKey(),(Float)entry.getValue());
                }else if(entry.getValue() instanceof Long){
                    intent.putExtra(entry.getKey(),(Long)entry.getValue());
                }else if(entry.getValue() instanceof Boolean){
                    intent.putExtra(entry.getKey(),(Boolean)entry.getValue());
                }else if(entry.getValue() instanceof Double){
                    intent.putExtra(entry.getKey(),(Double) entry.getValue());
                }else if(entry.getValue() instanceof Serializable){
                    intent.putExtra(entry.getKey(),(Serializable)entry.getValue());
                }
            }
        }
        return intent;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        presenter = TUtil.getT(MVPFragment.this, 0);
        if (presenter != null) {
            presenter.attachView(this);
        }
        super.onViewCreated(view, savedInstanceState);
//        initData(savedInstanceState, view);
    }

    protected T getP() {
        return presenter;
    }


}
