package com.android.base.frame.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;

import com.android.base.frame.presenter.IView;
import com.android.base.tools.ActivityManager;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.trello.rxlifecycle3.components.support.RxAppCompatActivity;

/**
 * 最顶层父Activity
 * Created by Sammie on 2017/4/30.
 */
public abstract class BaseActivity extends RxAppCompatActivity implements IView {

    //不推荐使用静态
    protected BaseActivity mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mContext = this;
        super.onCreate(savedInstanceState);
        ActivityManager.addActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManager.finishActivity(this);
    }

    protected abstract void initData(@androidx.annotation.Nullable Bundle savedInstanceState);

    private KProgressHUD loadingDialog;

    public void showLoadingDialog() {
        try {
            loadingDialog = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setDetailsLabel("Downloading data")
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();

        }catch (Exception e){}
    }

    public void showLoadingDialog(String label) {
        try {
            loadingDialog = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(label)
//                .setDetailsLabel("Downloading data")
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();

        }catch (Exception e){}
    }

    public void hideLoadingDialog() {
        try {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
        }catch (Exception e){}
    }




}
