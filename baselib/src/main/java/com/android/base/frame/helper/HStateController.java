package com.android.base.frame.helper;

import android.view.View;

import com.android.base.R;
import com.android.base.frame.view.XStateController;

/**
 * Created by Sammie on 2017/7/12.
 * 状态控制器
 */

public class HStateController {
    /**
     * 初始化，设置基本状态
     */
    public static void init(XStateController view){
        XStateController contentLayout =  view;
        if(contentLayout.getEmptyView()== null) {
            contentLayout.emptyView(View.inflate(view.getContext(), R.layout.status_empty, null));
        }
        if(contentLayout.getLoadingView()== null){
            contentLayout.loadingView(View.inflate(view.getContext(), R.layout.status_loading,null));
        }
        if(contentLayout.getErrorView()== null){
            contentLayout.errorView(View.inflate(view.getContext(), R.layout.status_erro,null));
        }
    }
}
