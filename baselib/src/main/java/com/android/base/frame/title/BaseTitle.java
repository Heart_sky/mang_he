package com.android.base.frame.title;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.base.R;


/**
 * Created by Sammie on 2017/8/17.
 * Title基本布局加载
 */
class BaseTitle extends LinearLayout {

    protected Context context;
    protected LinearLayout rootView; //根布局

    protected View statusBarView;//状态栏
    protected RelativeLayout statusRoot;//状态栏根View

    protected RelativeLayout titleRootView;//titleBarRootView
    protected RelativeLayout leftRoot;//左侧根布局
    protected RelativeLayout centerRoot;//中间根布局
    protected RelativeLayout rightRoot;//右侧根布局

    protected TextView leftTextView;//左侧
    protected TextView centerTextView;//中间
    protected TextView rightTextView;//右侧

    protected View lineTitle;
    protected int floatZ = 0; //悬浮的高度默认悬浮4dp

    protected String TAG = "TitleBar";

    protected BaseTitle(Context _context, @Nullable AttributeSet attrs) {
        super(_context, attrs);
        this.context = _context;
        initView();
    }

    /**
     * 初始化布局文件
     */
    private void initView() {
        View tempView = LayoutInflater.from(context).inflate(R.layout.title_tool_bar, this, true);
        if(tempView != null) {
            rootView = (LinearLayout) tempView;

            statusRoot = findViewById(R.id.rl_status);
            statusBarView = findViewById(R.id.status_bar);

            titleRootView = findViewById(R.id.fl_title_bar);
            leftRoot = findViewById(R.id.rl_left_toolbar);
            centerRoot =  findViewById(R.id.rl_center_toolbar);
            rightRoot = findViewById(R.id.rl_right_toolbar);
            leftTextView = findViewById(R.id.tv_left);
            centerTextView = findViewById(R.id.tv_center);
            rightTextView = findViewById(R.id.tv_right);

            lineTitle = findViewById(R.id.title_line);
        }
    }

}
