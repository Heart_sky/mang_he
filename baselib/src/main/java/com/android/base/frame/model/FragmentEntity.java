package com.android.base.frame.model;

import android.os.Bundle;

public class FragmentEntity {
    public Class clazz;
    public Bundle data;

    public FragmentEntity(Class clazz, Bundle data) {
        this.clazz = clazz;
        this.data = data;
    }
}
