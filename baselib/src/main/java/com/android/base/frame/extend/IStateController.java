package com.android.base.frame.extend;

import com.android.base.frame.view.XStateController;

/**
 * Created by Sammie on 2017/7/12.
 * 界面状态控制器
 */

public interface IStateController<T extends XStateController> {
    T getStateView();
}
