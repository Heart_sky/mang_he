package com.android.base.frame.title;

import android.content.Context;
import android.os.Build;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import com.android.base.R;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.StatusBarUtil;

/**
 * Created by Sammie on 2017/9/1.
 * 设置titleBar的主题,高度效果
 */
public class TitleThemeHelper<T extends TitleThemeHelper> extends TitleContentHelper<TitleThemeHelper> {

    private T child;


    protected TitleThemeHelper(Context _context, @Nullable AttributeSet attrs) {
        super(_context, attrs);
        child = (T) this;
        init();
    }

    /**
     * 初始化各个控件的默认状态
     */
    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //设置悬浮titleBar
            rootView.setTranslationZ(DisplayUtil.dip2px(context, floatZ));
            setStatuBarHeight(StatusBarUtil.getStatusBarHeight(context));
        }
        //设置状态栏颜色
        setDefaultTheme(ETitleTheme.DARK_TRANSPARENT);
    }

    /**
     * -------------------------------------toolbar默认主题样式组合---------------------------------------------
     **/
    public T setDefaultTheme(ETitleTheme type) {
        switch (type) {
            case LIGHT_PRIMARY:
                setTitleAndStatusBgColor(ContextCompat.getColor(context, R.color.colorPrimary));
                setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                setFontColor(ContextCompat.getColor(context, R.color.white));
                break;
            case LIGHT_TRANSPARENT:
                setTitleAndStatusBgColor(ContextCompat.getColor(context, R.color.transparent));
                setTitleBarColor(ContextCompat.getColor(context, R.color.transparent));
                setStatusBarColor(ContextCompat.getColor(context, R.color.transparent));
                setFontColor(ContextCompat.getColor(context, R.color.white));
                break;
            case DARK_PRIMARY:
                setTitleAndStatusBgColor(ContextCompat.getColor(context, R.color.colorPrimary));
                setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                setFontColor(ContextCompat.getColor(context, R.color.colorFont33));
                break;
            case DARK_TRANSPARENT:
                setTitleAndStatusBgColor(ContextCompat.getColor(context, R.color.transparent));
                setTitleBarColor(ContextCompat.getColor(context, R.color.transparent));
                setStatusBarColor(ContextCompat.getColor(context, R.color.transparent));
                setFontColor(ContextCompat.getColor(context, R.color.colorFont33));
                break;
        }
        return child;
    }

    /**
     * -------------------------------------设置toolbar颜色---------------------------------------------
     **/

    public T setCenterFontColor(@ColorInt int textColor) {
        centerTextView.setTextColor(textColor);
        return child;
    }

    public T setRightFontColor(@ColorInt int textColor) {
        rightTextView.setTextColor(textColor);
        return child;
    }

    public T setLeftFontColor(@ColorInt int textColor) {
        leftTextView.setTextColor(textColor);
        return child;
    }

    public T setFontColor(@ColorInt int textColor) {
        leftTextView.setTextColor(textColor);
        centerTextView.setTextColor(textColor);
        rightTextView.setTextColor(textColor);
        return child;
    }

    public T setTitleBarColor(int backgroundColor) {
        if (titleRootView != null && backgroundColor != -1 && backgroundColor != 0) {
            titleRootView.setBackgroundColor(ContextCompat.getColor(context, backgroundColor));
        }
        return child;
    }

    public T setStatusBarColor(int backgroundColor) {
        if (statusBarView != null && backgroundColor != -1 && backgroundColor != 0) {
            statusBarView.setBackgroundColor(ContextCompat.getColor(context, backgroundColor));
        }
        return child;
    }

    public T setTitleAndStatusBgColor(int backgroundColor) {
        setTitleBarColor(backgroundColor);
        setStatusBarColor(backgroundColor);
        return child;
    }

    //TODO 标题背景可以设置图片

    /**
     * -------------------------------------设置toolbar高度---------------------------------------------
     **/
    public T setStatuBarHeight(int height) {
        //版本判断，5.0以上有效
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Log.e(TAG, "系统版本不支持StatueBar");
            return child;
        }
        if (height >= 0 && statusBarView != null) {
            statusBarView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
        }
        return child;
    }

    public T setTitleBarHeight(int height) {
        if (height >= 0 && titleRootView != null) {
            titleRootView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
        }
        return child;
    }

    public int getStatusBarHeight() {
        int h = 0;
        if (statusBarView != null) {
            h = statusBarView.getLayoutParams().height;
        }
        return h;
    }

    public int getTitleBarHeight() {
        int h = 0;
        if (titleRootView != null) {
            h = titleRootView.getLayoutParams().height;
        }
        return h;
    }
}
