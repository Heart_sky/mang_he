package com.android.base.frame.presenter;

import androidx.annotation.LayoutRes;

/**
 * Created by Sammie on 2017/7/11.
 * 该接口主要用于约束Activity和Fragment的基本方法
 */
public interface IView {
    int getLayoutId();
}
