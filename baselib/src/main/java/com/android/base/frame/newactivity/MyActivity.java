package com.android.base.frame.newactivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.android.base.R;
import com.android.base.frame.newactivity.action.TitleBarAction;
import com.android.base.frame.presenter.XPresenter;
import com.gyf.immersionbar.ImmersionBar;
import com.hjq.bar.TitleBar;

import java.io.Serializable;
import java.util.Map;

import okhttp3.Call;

/**
 *    time   : 2018/10/18
 *    desc   : 项目中的 Activity 基类
 */
public abstract class MyActivity<T extends XPresenter> extends BaseActivity<T>
        implements TitleBarAction {

    /** 标题栏对象 */
    private TitleBar mTitleBar;
    /** 状态栏沉浸 */
    private ImmersionBar mImmersionBar;

    @Override
    protected void initLayout() {
        super.initLayout();

        if (getTitleBar() != null) {
            getTitleBar().setOnTitleBarListener(this);
        }

        // 初始化沉浸式状态栏
        if (isStatusBarEnabled()) {
            getStatusBarConfig().init();

            // 设置标题栏沉浸
            if (getTitleBar() != null) {
                ImmersionBar.setTitleBar(this, getTitleBar());
            }
        }
    }

    /**
     * 是否使用沉浸式状态栏
     */
    protected boolean isStatusBarEnabled() {
        return true;
    }

    /**
     * 状态栏字体深色模式
     */
    protected boolean isStatusBarDarkFont() {
        return true;
    }

    /**
     * 初始化沉浸式状态栏
     */
    @NonNull
    protected ImmersionBar createStatusBarConfig() {
        return ImmersionBar.with(this)
                // 默认状态栏字体颜色为黑色
                .statusBarDarkFont(isStatusBarDarkFont());
    }

    /**
     * 获取状态栏沉浸的配置对象
     */
    @NonNull
    public ImmersionBar getStatusBarConfig() {
        if (mImmersionBar == null) {
            mImmersionBar = createStatusBarConfig();
        }
        return mImmersionBar;
    }

    /**
     * 设置标题栏的标题
     */
    @Override
    public void setTitle(@StringRes int id) {
        setTitle(getString(id));
    }

    /**
     * 设置标题栏的标题
     */
    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        if (getTitleBar() != null) {
            getTitleBar().setTitle(title);
        }
    }

    @Override
    @Nullable
    public TitleBar getTitleBar() {
        if (mTitleBar == null) {
            mTitleBar = obtainTitleBar(getContentView());
        }
        return mTitleBar;
    }

    @Override
    public void onLeftClick(View v) {
        onBackPressed();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
        overridePendingTransition(R.anim.right_in_activity, R.anim.right_out_activity);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.left_in_activity, R.anim.left_out_activity);
    }
    protected void open(Class clazz) {
        open(clazz, null);
    }

    protected void open(Class clazz, Map<String, Object> object) {
        startActivity(getParamIntent(clazz,object));
    }
    protected void open(Class clazz, int reqCode) {
        startActivityForResult(new Intent(this, clazz),reqCode);
    }
    protected void open(Class clazz, Map<String, Object> object,int reqCode) {
        startActivityForResult(getParamIntent(clazz,object),reqCode);
    }

    private Intent getParamIntent(Class clazz, Map<String, Object> object){
        Intent intent = new Intent(this, clazz);
        if (object != null) {
            for (Map.Entry<String, Object> entry : object.entrySet()) {
                if (entry.getValue() instanceof String){
                    intent.putExtra(entry.getKey(),(String)entry.getValue());
                }else if(entry.getValue() instanceof Integer){
                    intent.putExtra(entry.getKey(),(Integer)entry.getValue());
                }else if(entry.getValue() instanceof Float){
                    intent.putExtra(entry.getKey(),(Float)entry.getValue());
                }else if(entry.getValue() instanceof Long){
                    intent.putExtra(entry.getKey(),(Long)entry.getValue());
                }else if(entry.getValue() instanceof Boolean){
                    intent.putExtra(entry.getKey(),(Boolean)entry.getValue());
                }else if(entry.getValue() instanceof Serializable){
                    intent.putExtra(entry.getKey(),(Serializable)entry.getValue());
                }
            }
        }
        return intent;
    }
}