package com.android.base.frame.newactivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;

import com.android.base.frame.newactivity.action.ActivityAction;
import com.android.base.frame.presenter.IView;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.tools.ActivityManager;
import com.android.base.tools.TUtil;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.trello.rxlifecycle3.components.support.RxAppCompatActivity;

import java.util.Random;

/**
 *    time   : 2020/12/31
 *    desc   : Activity 基类
 */
public abstract class BaseActivity<T extends XPresenter> extends RxAppCompatActivity
        implements ActivityAction, IView {
    private T presenter;
    //不推荐使用静态
    protected BaseActivity mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mContext = this;
        presenter = TUtil.getT(this, 0);
        if (presenter != null) {
            presenter.attachView(this);
        }

        super.onCreate(savedInstanceState);
        ActivityManager.addActivity(this);
        initActivity(savedInstanceState);
    }

    protected void initActivity(Bundle savedInstanceState) {
        initLayout();
        initData(savedInstanceState);
    }

    public abstract void initData(@androidx.annotation.Nullable Bundle savedInstanceState);

    /**
     * 获取业务类
     */
    public T getP() {
        return presenter;
    }

    /**
     * 初始化布局
     */
    protected void initLayout() {
        if(getLayoutId() > 0) {
            setContentView(getLayoutId());
            initSoftKeyboard();
        }
    }

    /**
     * 初始化软键盘
     */
    protected void initSoftKeyboard() {
        // 点击外部隐藏软键盘，提升用户体验
        getContentView().setOnClickListener(v -> hideSoftKeyboard());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManager.finishActivity(this);
    }

    @Override
    public void finish() {
        hideSoftKeyboard();
        super.finish();
    }

    /**
     * 如果当前的 Activity（singleTop 启动模式） 被复用时会回调
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // 设置为当前的 Intent，避免 Activity 被杀死后重启 Intent 还是最原先的那个
        setIntent(intent);
    }

    /**
     * 和 setContentView 对应的方法
     */
    public ViewGroup getContentView() {
        return findViewById(Window.ID_ANDROID_CONTENT);
    }

    @Override
    public Context getContext() {
        return this;
    }

    /**
     * startActivityForResult 方法优化
     */
    private OnActivityCallback mActivityCallback;
    private int mActivityRequestCode;

    public void startActivityForResult(Class<? extends Activity> clazz, OnActivityCallback callback) {
        startActivityForResult(new Intent(this, clazz), null, callback);
    }

    public void startActivityForResult(Intent intent, OnActivityCallback callback) {
        startActivityForResult(intent, null, callback);
    }

    public void startActivityForResult(Intent intent, @Nullable Bundle options, OnActivityCallback callback) {
        // 回调还没有结束，所以不能再次调用此方法，这个方法只适合一对一回调，其他需求请使用原生的方法实现
        if (mActivityCallback == null) {
            mActivityCallback = callback;
            // 随机生成请求码，这个请求码必须在 2 的 16 次幂以内，也就是 0 - 65535
            mActivityRequestCode = new Random().nextInt((int) Math.pow(2, 16));
            startActivityForResult(intent, mActivityRequestCode, options);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (mActivityCallback != null && mActivityRequestCode == requestCode) {
            mActivityCallback.onActivityResult(resultCode, data);
            mActivityCallback = null;
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        hideSoftKeyboard();
        // 查看源码得知 startActivity 最终也会调用 startActivityForResult
        super.startActivityForResult(intent, requestCode, options);
    }

    /**
     * 隐藏软键盘
     */
    private void hideSoftKeyboard() {
        // 隐藏软键盘，避免软键盘引发的内存泄露
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (manager != null && manager.isActive(view)) {
                manager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    private KProgressHUD loadingDialog;

    public void showLoadingDialog() {
        try {
            loadingDialog = KProgressHUD.create(this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setDetailsLabel("Downloading data")
                    .setCancellable(true)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show();

        }catch (Exception e){}
    }

    public void hideLoadingDialog() {
        try {
            if (loadingDialog != null && loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }
        }catch (Exception e){}
    }

    public interface OnActivityCallback {

        /**
         * 结果回调
         *
         * @param resultCode        结果码
         * @param data              数据
         */
        void onActivityResult(int resultCode, @Nullable Intent data);
    }
}