package com.android.base.frame.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import androidx.core.content.ContextCompat;
import androidx.annotation.Nullable;
import com.android.base.R;
import com.android.base.frame.activity.ExtraActivity;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.tools.TUtil;
import com.githang.statusbar.StatusBarCompat;

import java.io.Serializable;
import java.util.Map;

/**
 * 功能：界面跳转，值传递，初始化Presenter
 * Created by Sammie on 2017/9/19.
 */

public abstract class MVPActivity<T extends XPresenter> extends ExtraActivity {
    private T presenter;

    protected void open(Class clazz) {
        open(clazz, null);
    }

    protected void open(Class clazz, Map<String, Object> object) {
        startActivity(getParamIntent(clazz,object));
    }
    protected void open(Class clazz, int reqCode) {
        startActivityForResult(new Intent(this, clazz),reqCode);
    }
    protected void open(Class clazz, Map<String, Object> object,int reqCode) {
        startActivityForResult(getParamIntent(clazz,object),reqCode);
    }

    private Intent getParamIntent(Class clazz, Map<String, Object> object){
        Intent intent = new Intent(this, clazz);
        if (object != null) {
            for (Map.Entry<String, Object> entry : object.entrySet()) {
                if (entry.getValue() instanceof String){
                    intent.putExtra(entry.getKey(),(String)entry.getValue());
                }else if(entry.getValue() instanceof Integer){
                    intent.putExtra(entry.getKey(),(Integer)entry.getValue());
                }else if(entry.getValue() instanceof Float){
                    intent.putExtra(entry.getKey(),(Float)entry.getValue());
                }else if(entry.getValue() instanceof Long){
                    intent.putExtra(entry.getKey(),(Long)entry.getValue());
                }else if(entry.getValue() instanceof Boolean){
                    intent.putExtra(entry.getKey(),(Boolean)entry.getValue());
                }else if(entry.getValue() instanceof Serializable){
                    intent.putExtra(entry.getKey(),(Serializable)entry.getValue());
                }
            }
        }
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        presenter = TUtil.getT(MVPActivity.this, 0);
        if (presenter != null) {
            presenter.attachView(this);
        }
        super.onCreate(savedInstanceState);

        StatusBarCompat.setStatusBarColor(this, ContextCompat.getColor(this, R.color.white));
        initData(savedInstanceState);
    }

    public T getP() {
        return presenter;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.fontScale != 1)//非默认值
            getResources();
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public Resources getResources() {
        // 字体大小不跟随系统
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }
}
