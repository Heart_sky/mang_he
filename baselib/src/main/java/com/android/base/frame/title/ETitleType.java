package com.android.base.frame.title;

/**
 * Created by Sammie on 2017/7/13.
 */
public enum ETitleType {
    NO_TITLE,                 //1 不显示Title
    SIMPLE_TITLE,             //2 title上，内容下
    OVERLAP_TITLE,            //3 title和内容重叠
}
