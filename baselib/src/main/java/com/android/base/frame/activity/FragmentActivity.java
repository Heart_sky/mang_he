package com.android.base.frame.activity;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import com.android.base.R;
import com.android.base.frame.fragment.LazyFragment;
import com.android.base.frame.model.FragmentEntity;
import com.android.base.frame.title.ETitleType;
import com.githang.statusbar.StatusBarCompat;

import java.util.ArrayList;
import java.util.List;

public abstract class FragmentActivity extends ExtraActivity {

    @IdRes
    private int container; //Fragment需要放置的容器
    private Class[] clazzs;
    private FragmentEntity[] fragmentEntityList;
    private int showIndex = 0; //需要默认显示Fragment下标,默认显示第一页
    private List<Fragment> fragments;  //用于放置Fragment

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarCompat.setStatusBarColor(this, ContextCompat.getColor(this, R.color.white));
        if (fragments == null) fragments = new ArrayList<>();
        if (clazzs == null || clazzs.length == 0) {

        } else if (savedInstanceState != null) {
            // 解决重叠问题
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            for (int i = 0; i < clazzs.length; i++) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(clazzs[i].getName());
                fragments.add(fragment);
                if (i == showIndex)
                    ft.show(fragment);
                else
                    ft.hide(fragment);
            }
            ft.commit();
        }
        initData(savedInstanceState);
    }

    @Override
    public ETitleType showToolBarType() {
        return ETitleType.NO_TITLE;
    }

    /**
     * 根据下标，设置要展示的Fragment
     * ####该方法必须在Activity初始化完成后调用
     * @param newIndex 要显示的Fragment的下标
     */
    protected void showFragment(int newIndex) {
        if (showIndex == newIndex) return;
        if (fragments == null) fragments = new ArrayList<>();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.show(fragments.get(newIndex))
                .hide(fragments.get(showIndex))
                .commitAllowingStateLoss();
        fragments.get(newIndex).setUserVisibleHint(true);
        showIndex = newIndex;
//        if(showIndex == 4){
//            StatusBarCompat.setTranslucent(getWindow(),true);
//        }else{
//            StatusBarCompat.setLightStatusBar(getWindow(),true);
//        }
    }

    /**
     * 把Fragment添加到FragmentActivity中
     *
     * @param container   需要添加的布局ID
     * @param fragmentClass Fragment的Class
     */
    protected void addFragment(@IdRes int container, Class... fragmentClass) {
        addFragment(container, 0, fragmentClass);
    }

    /**
     * 把Fragment添加到FragmentActivity中
     *
     * @param container   需要添加的布局ID
     * @param fragmentEntities Fragment带参数
     */
    protected void addFragment(@IdRes int container, FragmentEntity... fragmentEntities) {
        addFragment(container, 0, fragmentEntities);
    }

    protected Fragment getCurrentFragment(){
        return fragments.get(showIndex);
    }
    protected Fragment getFragment(int position){
        return fragments.get(position);
    }
    /**
     * 把Fragment添加到FragmentActivity中
     *
     * @param showIndex 默认显示的页数
     */
    protected void addFragment(@IdRes int container, int showIndex, Class... fragmentClass) {
        showIndex = showIndex >= fragmentClass.length ? 0 : showIndex;
        this.container = container;
        this.clazzs = fragmentClass;
        this.showIndex = showIndex;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        for (int i = 0; i < clazzs.length; i++) {
            LazyFragment fragment = (LazyFragment) LazyFragment.newInstance(clazzs[i]);
            fragments.add(fragment);
            ft.add(container, fragment, clazzs[i].getName());
            if (i == showIndex) {
                ft.show(fragment);
                fragment.setUserVisibleHint(true);
            } else {
                ft.hide(fragment);
                fragment.setUserVisibleHint(false);
            }
        }
        ft.commit();
    }

    /**
     * 把Fragment添加到FragmentActivity中
     *
     * @param showIndex 默认显示的页数
     */
    protected void addFragment(@IdRes int container, int showIndex, FragmentEntity... fragmentEntities) {
        showIndex = showIndex >= fragmentEntities.length ? 0 : showIndex;
        this.container = container;
        this.fragmentEntityList = fragmentEntities;
        this.showIndex = showIndex;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        for (int i = 0; i < fragmentEntityList.length; i++) {
            FragmentEntity entity = fragmentEntities[i];
            LazyFragment fragment = (LazyFragment) LazyFragment.newInstance(entity.clazz, entity.data);
            fragments.add(fragment);
            ft.add(container, fragment, entity.clazz.getName());
            if (i == showIndex) {
                ft.show(fragment);
                fragment.setUserVisibleHint(true);
            } else {
                ft.hide(fragment);
                fragment.setUserVisibleHint(false);
            }
        }
        ft.commit();
    }

    @Override
    public Resources getResources() {
        // 字体大小不跟随系统
        Resources res = super.getResources();
        Configuration config = new Configuration();
        config.setToDefaults();
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }
}