package com.android.base.frame.helper;

import android.content.Context;

import com.android.base.frame.extend.IRefresh;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.DefaultRefreshFooterCreator;
import com.scwang.smartrefresh.layout.api.DefaultRefreshHeaderCreator;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

/**
 * Created by Sammie on 2017/7/12.
 * 初始化下拉刷新控件
 * 参考地址：https://github.com/scwang90/SmartRefreshLayout
 */
public class HRefresh {
    /**
     * 静态代码块只执行一次
     */
    static {
        //设置全局的Header构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator(new DefaultRefreshHeaderCreator() {
            @Override
            public RefreshHeader createRefreshHeader(Context context, RefreshLayout layout) {
                ClassicsHeader header = new ClassicsHeader(context);
                return header;//指定为经典Header，默认是 贝塞尔雷达Header
            }
        });
        //设置全局的Footer构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator(new DefaultRefreshFooterCreator() {
            @Override
            public RefreshFooter createRefreshFooter(Context context, RefreshLayout layout) {
                ClassicsFooter footer = new ClassicsFooter(context);
                return footer;//指定为经典Footer，默认是 BallPulseFooter
            }
        });
    }

    public static void setRefreshLayout(RefreshLayout view, final IRefresh iRefresh) {
        if(view != null) {
            RefreshLayout refreshLayout = view;
            //初始化一些默认设置(内容区域是否可以跟随下拉等连动)
            refreshLayout.setEnableHeaderTranslationContent(true);
            refreshLayout.setEnableFooterTranslationContent(true);
            //第一个颜色刷新头部主题色,
            refreshLayout.setPrimaryColorsId(android.R.color.white);//全局设置主题颜色
            refreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(RefreshLayout refreshlayout) {
                    refreshlayout.finishRefresh(1000);
                    iRefresh.onRefresh();
                }
            });
            refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(RefreshLayout refreshLayout) {
                    refreshLayout.finishLoadMore(1000);
                    iRefresh.onLoad();
                }
            });
        }
    }

}
