package com.android.base.frame.presenter;

/**
 * Created by Sammie on 2017/9/19.
 */
public class XPresenter<V extends IView> {

    private V view;

    public void attachView(V view) {
        this.view = view;
    }

    public V getV() {
        return view;
    }
}
