package com.android.base.frame.extend;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;

/**
 * Created by Sammie on 2017/7/12.
 * <p>
 * 接口说明： 用于刷新的接口
 */

public interface IRefresh {
    //初始化刷新控件
    SmartRefreshLayout getRefreshView();

    //下拉刷新
    void onRefresh();

    //上拉加载
    void onLoad();
}
