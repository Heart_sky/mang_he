package com.android.base;

/**
 * Created by wanglei on 2016/12/4.
 */

public class XConfig {

    /******************************* 缓存相关设置 (缓存父目录为Android/data)************************************/
    // #netcache  网络缓存文件名
    public static final String NET_CACHE = "NetCache";
    public static final long MAX_DIR_SIZE = Runtime.getRuntime().maxMemory() / 8;

}
