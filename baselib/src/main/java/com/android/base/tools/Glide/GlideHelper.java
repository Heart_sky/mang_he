package com.android.base.tools.Glide;

import android.content.Context;
import android.widget.ImageView;
import com.android.base.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

/**
 * Created by YuanYe on 2017/7/14.
 * 建议所有使用图片加载的地方都通过这样的方法来实现图片加载
 */
public class GlideHelper {

    public static void load(Context context, String load, int holderRes, ImageView imgView) {
        Glide.with(context.getApplicationContext())
                .load(load)
                .error(holderRes)
                .placeholder(holderRes)
                .into(imgView);

    }

    /**
     * 简单加载图片
     * @param context
     * @param load
     * @param imgView
     */
    public static void load(Context context, String load, ImageView imgView) {
        Glide.with(context.getApplicationContext())
                .load(load)
                .crossFade()
                .into(imgView);

    }


    /**
     * 简单加载图片,不缓存
     * @param context
     * @param load
     * @param imgView
     */
    public static void loadNoCache(Context context, String load, ImageView imgView) {
        Glide.with(context.getApplicationContext())
                .load(load)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)//禁用磁盘缓存
                .skipMemoryCache(true)//跳过内存缓存
                .into(imgView);

    }


    /**
     * 简单加载图片
     * @param context
     * @param res
     * @param imgView
     */
    public static void load(Context context, int res, ImageView imgView) {
        Glide.with(context.getApplicationContext())
                .load(res)
                .crossFade()
                .into(imgView);

    }


    /**
     * 加载圆角图片
     * @param context
     * @param load
     * @param imgView
     * @param conner
     */
    public static void loadRoundTrans(Context context, String load, ImageView imgView, int conner) {
        Glide.with(context.getApplicationContext())
                .load(load)
                .crossFade()
                .transform(new GlideRoundTransform(context, conner))
                .error(R.drawable.holder_pic)
                .placeholder(R.drawable.holder_pic)
                .into(imgView);

    }
    /**
     * 加载圆角图片
     * @param context
     * @param load
     * @param imgView
     * @param conner
     */
    public static void loadCropRoundTrans(Context context, String load, ImageView imgView, int conner) {
        Glide.with(context.getApplicationContext())
                .load(load)
                .crossFade()
                .centerCrop()
                .transform(new GlideRoundTransform(context, conner))
                .into(imgView);

    }

    /**
     * 加载图片，带加载中和错误图片
     * @param context
     * @param load
     * @param imgview
     */
    public static void loadWithHolderErr(Context context, String load, ImageView imgview) {
        Glide.with(context.getApplicationContext())
                .load(load)
                .crossFade()
                .error(R.drawable.holder_pic)
                .placeholder(R.drawable.holder_pic)
                .into(imgview);

    }

    /**
     * 加载图片，带错误图片
     * @param context
     * @param load
     * @param imgview
     */
    public static void loadWithErr(Context context, String load, ImageView imgview) {
        Glide.with(context.getApplicationContext())
                .load(load)
                .crossFade()
                .error(R.drawable.holder_pic)
                .into(imgview);

    }

    /**
     * 加载图片，Crop样式
     * @param mcontext
     * @param load
     * @param imgview
     */
    public static void loadWithCrop(Context mcontext, String load, ImageView imgview) {
        Glide.with(mcontext.getApplicationContext())
                .load(load)
                .centerCrop()
                .error(R.drawable.holder_pic)
                .into(imgview);

    }

    /**
     * 加载图片，Crop样式，error图片
     * @param mcontext
     * @param load
     * @param imgview
     */
    public static void loadWithErrCrop(Context mcontext, String load, ImageView imgview) {
        Glide.with(mcontext.getApplicationContext())
                .load(load)
                .centerCrop()
                .error(R.drawable.holder_pic)
                .into(imgview);

    }

    //Glide加载图片
    public static void loadCircle(Context mcontext, String load, ImageView imgview) {
        RequestManager glideRequest = Glide.with(mcontext.getApplicationContext());
        glideRequest.load(load)
                .transform(new CircleTransform(mcontext))
                .error(R.drawable.holder_pic).placeholder(R.drawable.avatar)
                .into(imgview);

    }

    //Glide加载图片
    public static void loadAvatar(Context mcontext, String load, ImageView imgview) {
        RequestManager glideRequest = Glide.with(mcontext.getApplicationContext());
        glideRequest.load(load)
                .transform(new CircleTransform(mcontext))
                .diskCacheStrategy(DiskCacheStrategy.NONE)//禁用磁盘缓存
                .skipMemoryCache(true)//跳过内存缓存
                .error(R.drawable.avatar).placeholder(R.drawable.avatar)
                .into(imgview);

    }
}
