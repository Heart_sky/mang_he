package com.android.base.tools;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * 日期时间
 *
 * @author zwj
 * create at 2018/11/3 16:31
 */
public class DateTimeUtil {
    private static SimpleDateFormat m = new SimpleDateFormat("MM", Locale.getDefault());
    private static SimpleDateFormat d = new SimpleDateFormat("dd", Locale.getDefault());
    private static SimpleDateFormat md = new SimpleDateFormat("MM-dd", Locale.getDefault());
    private static SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static SimpleDateFormat ymdDot = new SimpleDateFormat("yyyy.MM.dd", Locale.getDefault());
    private static SimpleDateFormat ymdhms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private static SimpleDateFormat ymdhmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
    private static SimpleDateFormat ymdhm = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
    private static SimpleDateFormat hm = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private static SimpleDateFormat mdhm = new SimpleDateFormat("MM月dd日 HH:mm", Locale.getDefault());
    private static SimpleDateFormat mdhmLink = new SimpleDateFormat("MM-dd HH:mm", Locale.getDefault());
    private static SimpleDateFormat my = new SimpleDateFormat("MM月 yyyy", Locale.getDefault());


    /**
     * 年月日[2015-07-28]
     *
     * @param timeInMills
     * @return
     */
    public static String getYmd(long timeInMills) {
        return ymd.format(new java.util.Date(timeInMills));
    }

    /**
     * 年月日[2015.07.28]
     *
     * @param timeInMills
     * @return
     */
    public static String getYmdDot(long timeInMills) {
        return ymdDot.format(new java.util.Date(timeInMills));
    }

    public static String getYmdhms(long timeInMills) {
        return ymdhms.format(new java.util.Date(timeInMills));
    }

    public static String getYmdhmsS(long timeInMills) {
        return ymdhmss.format(new java.util.Date(timeInMills));
    }

    public static String getYmdhm(long timeInMills) {
        return ymdhm.format(new java.util.Date(timeInMills));
    }

    public static String getHm(long timeInMills) {
        return hm.format(new java.util.Date(timeInMills));
    }

    public static String getMd(long timeInMills) {
        return md.format(new java.util.Date(timeInMills));
    }

    public static String getMdhm(long timeInMills) {
        return mdhm.format(new java.util.Date(timeInMills));
    }

    public static String getMdhmLink(long timeInMills) {
        return mdhmLink.format(new java.util.Date(timeInMills));
    }

    public static String getM(long timeInMills) {
        return m.format(new java.util.Date(timeInMills));
    }

    public static String getD(long timeInMills) {
        return d.format(new java.util.Date(timeInMills));
    }

    public static String getMy(long timeInMills) {
        return my.format(new java.util.Date(timeInMills));
    }

    /**
     * 是否是今天
     *
     * @param timeInMills
     * @return
     */
    public static boolean isToday(long timeInMills) {
        String dest = getYmd(timeInMills);
        String now = getYmd(Calendar.getInstance().getTimeInMillis());
        return dest.equals(now);
    }

    /**
     * 是否是同一天
     *
     * @param aMills
     * @param bMills
     * @return
     */
    public static boolean isSameDay(long aMills, long bMills) {
        String aDay = getYmd(aMills);
        String bDay = getYmd(bMills);
        return aDay.equals(bDay);
    }

    /**
     * 获取年份
     *
     * @param mills
     * @return
     */
    public static int getYear(long mills) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);
        return calendar.get(Calendar.YEAR);
    }

    /**
     * 获取月份
     *
     * @param mills
     * @return
     */
    public static int getMonth(long mills) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);
        return calendar.get(Calendar.MONTH) + 1;
    }


    /**
     * 获取月份的天数
     *
     * @param mills
     * @return
     */
    public static int getDaysInMonth(long mills) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);

        switch (month) {
            case Calendar.JANUARY:
            case Calendar.MARCH:
            case Calendar.MAY:
            case Calendar.JULY:
            case Calendar.AUGUST:
            case Calendar.OCTOBER:
            case Calendar.DECEMBER:
                return 31;
            case Calendar.APRIL:
            case Calendar.JUNE:
            case Calendar.SEPTEMBER:
            case Calendar.NOVEMBER:
                return 30;
            case Calendar.FEBRUARY:
                return (year % 4 == 0) ? 29 : 28;
            default:
                throw new IllegalArgumentException("Invalid Month");
        }
    }


    /**
     * 获取星期,0-周日,1-周一，2-周二，3-周三，4-周四，5-周五，6-周六
     *
     * @param mills
     * @return
     */
    public static int getWeekIndex(long mills) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);

        return calendar.get(Calendar.DAY_OF_WEEK) - 1;
    }

    public static String getWeekStr(long mills){
        return weekStr[getWeekIndex(mills)];
    }

    public static String[] weekStr = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};

    /**
     * 获取当月第一天的时间（毫秒值）
     *
     * @param mills
     * @return
     */
    public static long getFirstOfMonth(long mills) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        return calendar.getTimeInMillis();
    }


    /**
     * Date对象转String
     *
     * @param date Date对象
     * @param formatString 如:yyyy-MM-dd HH:mm:ss,sss
     * @return
     */
    public static String dateToString(Date date, String formatString) {
        String retStr = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(formatString);
            retStr = sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retStr;
    }

    /**
     * 获取当前时间字符串
     *
     * @param formatString 如:yyyy-MM-dd HH:mm:ss,sss
     * @return
     */
    public static String getCurrentDateTimeString(String formatString) {
        SimpleDateFormat timeFormater = new SimpleDateFormat(formatString);// 日志内容的时间格式
        return timeFormater.format(new Date().getTime());
    }

    /**
     * 获取当前时间的Date对象
     *
     * @return
     */
    public static Date getCurrentDateObject() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * 長類型時間轉字符時間
     *
     * @param longDate
     * @param formatString
     * @return
     */
    public static String longToString(long longDate, String formatString) {
        Date date = new Date(longDate*1000);
        return dateToString(date, formatString);
    }

    public static Map<String, Long> getTimeSpace(long startTime, long endTime) {
        Map<String, Long> map = new HashMap<String, Long>();
        long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
        long nh = 1000 * 60 * 60;// 一小时的毫秒数
        long nm = 1000 * 60;// 一分钟的毫秒数
        long ns = 1000;// 一秒钟的毫秒数
        long diff = endTime - startTime;
        long day = diff / nd;// 计算差多少天
        long hour = diff % nd / nh;// 计算差多少小时
        long min = diff % nd % nh / nm;// 计算差多少分钟
        long sec = diff % nd % nh % nm / ns;// 计算差多少秒
        map.put("Day", day);
        map.put("Hour", hour);
        map.put("Min", min);
        map.put("Sec", sec);
        return map;
    }

    public static String addYear(long mill, int add){
        Calendar curr = Calendar.getInstance();
        curr.setTimeInMillis(mill*1000);
        curr.add(Calendar.YEAR, add);
        Date date=curr.getTime();
        return dateToString(date, "yyyy.MM.dd");
    }
}
