package com.android.base.tools;

import android.app.Activity;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.DisplayMetrics;

public class ScreenUtil {
	public static int getScreenHeight(Context context){
		DisplayMetrics dm = new DisplayMetrics();  
		((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);  
        return dm.heightPixels;
	}
	public static int getScreenWidth(Context context){
		DisplayMetrics dm = new DisplayMetrics();  
		((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);  
        return dm.widthPixels;
	}
	
	public static WakeLock mWakeLock = null;
	public static void toWakeScreen(Context context){
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "SimpleTimer");
		mWakeLock.setReferenceCounted(false);
        mWakeLock.acquire();//这里唤醒锁，用这种方式要记得在适当的地方关闭锁，
	}
	public static void toReleaseWakeScreen(){
		try {
			if(null != mWakeLock){
				mWakeLock.release();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
