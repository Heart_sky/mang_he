package com.android.base.tools;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;

/*
 *@ProjectName: AndroidCommon
 *@FileName: PathUtil.java
 *@CreateTime: 2014-2-8 下午2:36:57
 *@Author: Sammie.Zhang
 *@Description:
 */
public class PathUtil {
	/**
	 * 創建文件夾
	 * 
	 * @param path
	 */
	public static void makeDirs(String path) {
		File file = new File(path);
		if (!file.exists() && !file.isFile()) {
			file.mkdirs();
		}
	}

	/**
	 * 获取包含后缀名的文件名称
	 * 
	 * @param url
	 * @return
	 */
	public static String getHttpFileName(String url) {
		File file = new File(url);
		return file.getName();
	}

	/**
	 * 创建新文件
	 * 
	 * @param fullPath
	 */
	public static File createNewFile(String fullPath) {
		File file = new File(fullPath);
		String dir = fullPath.substring(0, fullPath.lastIndexOf("/") + 1);
		File tempFile = new File(dir);
		if (!tempFile.exists()) {
			tempFile.mkdirs();
		}
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return file;
	}

	/**
	 * 获取后缀名
	 * 
	 * @param allname
	 * @return
	 */
	public static String getExtName(String allname) {
		if ((allname != null) && (allname.length() > 0)) {
			int i = allname.lastIndexOf('.');
			if ((i > -1) && (i < (allname.length() - 1))) {
				return ("." + allname.substring(i + 1));
			}
		}
		return null;
	}

	/**
	 * 检查是否为图片地址
	 * 
	 * @param path
	 * @return
	 */
	public static boolean checkPhoto(String path) {
		try {
			String prefix = path.substring(path.lastIndexOf(".") + 1);
			prefix = prefix.toLowerCase();
			if (prefix.equals("jpg") || prefix.equals("png") || prefix.equals("jpeg") || prefix.equals("bnp") || prefix.equals("gif")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static void saveMyBitmap(File bitName, Bitmap mBitmap) {
		FileOutputStream fOut = null;
		try {
			fOut = new FileOutputStream(bitName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			mBitmap = Bitmap.createScaledBitmap(mBitmap, 200, 200, true);
			mBitmap.compress(Bitmap.CompressFormat.JPEG, 68, fOut);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			fOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void deleteFile(String filePath) {
		try {
			File file = new File(filePath);
			if (file.isFile() && file.exists()) {
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 产出文件夹
	 * 
	 * @param folderPath
	 *            文件夹完整绝对路径
	 */
	public static void delFolder(String folderPath) {
		try {
			delAllFile(folderPath); // 删除完里面所有内容
			File myFilePath = new File(folderPath.toString());
			myFilePath.delete(); // 删除空文件夹
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除指定文件夹下的所有文件
	 * 
	 * @param path
	 *            文件夹完整绝对路径
	 * @return
	 */
	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists()) {
			return flag;
		}
		if (!file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件
				delFolder(path + "/" + tempList[i]);// 再删除空文件夹
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * 获取单个文件的MD5值！

	 * @param file
	 * @return
	 */

	public static String getFileMD5(File file) {
		if (!file.isFile()) {
			return null;
		}
		MessageDigest digest = null;
		FileInputStream in = null;
		byte buffer[] = new byte[1024];
		int len;
		try {
			digest = MessageDigest.getInstance("MD5");
			in = new FileInputStream(file);
			while ((len = in.read(buffer, 0, 1024)) != -1) {
				digest.update(buffer, 0, len);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		BigInteger bigInt = new BigInteger(1, digest.digest());
		return bigInt.toString(16);
	}
}
