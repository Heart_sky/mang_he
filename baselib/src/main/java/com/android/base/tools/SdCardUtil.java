package com.android.base.tools;

import android.content.Context;
import android.os.Environment;

public class SdCardUtil {

    /**
     * 获取内存储路径
     *
     * @return
     */
    public static String getRootPath() {
        return Environment.getExternalStorageDirectory().getPath();
    }

    /**
     * 获取缓存路径
     */
    public static String getCachePath(Context context) {
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return cachePath;
    }


}
