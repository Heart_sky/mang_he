package com.android.base.tools.permission;

import java.util.List;

/**
 * 权限请求结果监听器
 */
public interface OnPermissionResultListener {
    /**
     * 权限被授予
     *
     * @param permissions 授权的权限列表
     */
    void onGranted(List<String> permissions);

    /**
     * 权限被拒绝
     *
     * @param permissions 拒绝的权限列表
     */
    default void onDenied(List<String> permissions) {
    }
}
