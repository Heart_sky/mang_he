package com.android.base.tools.permission;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import com.android.base.R;
import com.android.base.tools.ToastUtil;
import com.longsh.optionframelibrary.OptionMaterialDialog;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.RequestExecutor;
import com.yanzhenjie.permission.runtime.Permission;

import java.util.List;

/**
 * 动态权限申请工具类
 */
public class PermissionUtil {
    private PermissionUtil() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }



    /**
     * 从Activity中发起权限申请
     *
     * @param activity    Activity
     * @param listener    监听对象
     * @param permissions 权限列表
     */
    public static void requestPermissions(@NonNull Activity activity,
                                          OnPermissionResultListener listener,
                                          String... permissions) {
        if (permissions.length == 0) {
            return;
        }
        AndPermission.with(activity)
                .runtime()
                .permission(permissions.clone())
                .onGranted(permissions12 -> {
                    if (listener != null) {
                        listener.onGranted(permissions12);
                    }
                })
                .onDenied(permissions13 -> {
                    if (AndPermission.hasAlwaysDeniedPermission(activity, permissions13)) {
                        String permissionStrings = TextUtils.join(", ",
                                Permission.transformText(activity, permissions));
                        ToastUtil.showShort(activity,
                                activity.getString(R.string.setting_permission_warning,
                                        permissionStrings));
                    }
                    if (listener != null) {
                        listener.onDenied(permissions13);
                    }
                })
                .rationale(PermissionUtil::showPermissionRationale)
                .start();
    }


    /**
     * 从任意Context中发起权限申请
     *
     * @param context     Context
     * @param listener    监听对象
     * @param permissions 权限列表
     */
    public static void requestPermissions(@NonNull Context context,
                                          OnPermissionResultListener listener,
                                          String... permissions) {
        if (permissions.length == 0) {
            return;
        }
        AndPermission.with(context)
                .runtime()
                .permission(permissions.clone())
                .onGranted(permissions12 -> {
                    if (listener != null) {
                        listener.onGranted(permissions12);
                    }
                })
                .onDenied(permissions13 -> {
                    if (AndPermission.hasAlwaysDeniedPermission(context, permissions13)) {
                        String permissionStrings = TextUtils.join(", ", permissions);
                        ToastUtil.showShort(context,
                                context.getString(R.string.setting_permission_warning,
                                        permissionStrings));
                    }
                    if (listener != null) {
                        listener.onDenied(permissions13);
                    }
                })
                .rationale(PermissionUtil::showPermissionRationale)
                .start();
    }

    /**
     * 从Fragment中发起权限申请
     *
     * @param fragment    fragment
     * @param listener    监听对象
     * @param permissions 权限列表
     */
    public static void requestPermissions(@NonNull Fragment fragment,
                                          OnPermissionResultListener listener,
                                          String... permissions) {
        if (permissions.length == 0) {
            return;
        }
        AndPermission.with(fragment)
                .runtime()
                .permission(permissions.clone())
                .onGranted(permissions12 -> {
                    if (listener != null) {
                        listener.onGranted(permissions12);
                    }
                })
                .onDenied(permissions13 -> {
                    if (AndPermission.hasAlwaysDeniedPermission(fragment, permissions13)) {
                        String permissionStrings = TextUtils.join(", ",
                                Permission.transformText(fragment.getActivity(), permissions));
                        ToastUtil.showShort(fragment.getActivity(),
                                fragment.getActivity().getString(R.string.setting_permission_warning,
                                        permissionStrings));
                    }
                    if (listener != null) {
                        listener.onDenied(permissions13);
                    }
                })
                .rationale(PermissionUtil::showPermissionRationale)
                .start();
    }


    /**
     * 当用户点击拒绝之后再次申请权限时进行解释说明的对话框操作
     */
    private static void showPermissionRationale(Context context,
                                                List<String> permissions,
                                                RequestExecutor executor) {
        String permissionStrings = TextUtils.join(", ", Permission.transformText(context, permissions));
        final OptionMaterialDialog mMaterialDialog = new OptionMaterialDialog(context);
        mMaterialDialog.setTitle(context.getString(R.string.setting_permissions_ask_for))
                .setMessage(context.getString(R.string.setting_permission_request_again, permissionStrings))
                .setPositiveButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        executor.execute();
                    }
                })
                .setNegativeButton("取消",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                                executor.cancel();
                            }
                        })
                .show();
    }
}
