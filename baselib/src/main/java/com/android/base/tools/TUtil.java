package com.android.base.tools;

import java.lang.reflect.ParameterizedType;

/**
 * Created by hpw on 16/10/28.
 */
public class TUtil {

    //反射泛型生成对象
    public static <T> T getT(Object o, int i) {
        try {
            return ((Class<T>) ((ParameterizedType) (o.getClass().getGenericSuperclass())).getActualTypeArguments()[i])
                    .newInstance();
        } catch (Exception e) {

        }
        return null;
    }

    public static Class<?> forName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {

        }
        return null;
    }
}

