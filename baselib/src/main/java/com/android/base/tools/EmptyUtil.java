package com.android.base.tools;

import android.text.TextUtils;

import java.util.List;

/**
* 判空
* @author zwj
* create at 2018/11/3 16:20
*/
public class EmptyUtil {
    public static boolean check(Object obj) {
        return obj == null;
    }

    public static boolean check(List list) {
        return list == null || list.isEmpty();
    }

    public static boolean check(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean check(String str) {
        return TextUtils.isEmpty(str);
    }
}
