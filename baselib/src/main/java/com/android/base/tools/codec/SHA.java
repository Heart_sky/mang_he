package com.android.base.tools.codec;


import java.security.MessageDigest;

public class SHA {

    public static byte[] encrypt(byte[] data) throws Exception {
        MessageDigest sha = MessageDigest.getInstance(Algorithm.SHA.getType());
        sha.update(data);
        return sha.digest();
    }

}