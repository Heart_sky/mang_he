package com.android.base.view.recycler.listener;

import android.view.View;

public interface OnItemLongClickListener<T> {
    void onLongClick(View view, T item);
}
