package com.android.base.view.recycler.listener;

import android.view.View;

public interface OnItemClickListener<T> {
    void onClick(View view, T item);
}
