package com.android.base.view.recycler;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class GridItemSpaceDecoration extends RecyclerView.ItemDecoration {
    private int space;
    private int lineSize;

    public GridItemSpaceDecoration(int space, int lineSize) {
        this.space = space;
        this.lineSize = lineSize;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //不是第一个的格子都设一个左边和底部的间距
        outRect.left = space;
        outRect.bottom = space;
        //由于每行都有lineSize个，所以第一个都是lineSize的倍数，把左边距设为0
        if (parent.getChildLayoutPosition(view) % lineSize == 0) {
            outRect.left = 0;
        }
    }

}