package com.android.base.okhttp.okUtil.base;

import java.io.Serializable;

/**
 * Created by Sammie on 2017/10/18.
 * <p>
 * NetBean: 为适应多格式json解析，而优化代码
 * NetBean中的内容为json返回中的公共部分。
 */
public class NetBean<T> implements Serializable {
    public int code;//访问接口返回状态  0--访问成功
    public int time;
    public String msg;//访问成功或者失败的接口表描述
    public T data;//服务器实际返回的数据

    public String url;
}
