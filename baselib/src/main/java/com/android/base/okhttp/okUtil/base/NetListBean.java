package com.android.base.okhttp.okUtil.base;

/**
 * Created by Sammie on 2017/10/18.
 * <p>
 * NetBean: 为适应多格式json解析，而优化代码
 * NetBean中的内容为json返回中的公共部分。
 */
public class NetListBean {
    String recode = "";//访问接口返回状态  1--访问成功   0--访问失败
    String msg = "";//访问成功或者失败的接口表描述
    String status = "";

    public String getRecode() {
        return recode;
    }

    public void setRecode(String recode) {
        this.recode = recode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
