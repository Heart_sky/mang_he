package com.android.base.okhttp;

import android.content.Context;

import com.android.base.XConfig;
import com.android.base.okhttp.okUtil.RxClientBuilder;
import com.android.base.tools.SdCardUtil;

import java.io.File;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CookieJar;
import okhttp3.OkHttpClient;

/**
 * Created by Sammie on 2017/7/19.
 * 本类设计说明：根据ClientBuilder构建Client初始化，
 * 根据CacheInterceptor实现OKHttp缓存,
 * 本类主要用于创建OKHttpClient对象。
 */
public class RxHttpClient {

    private OkHttpClient client = null;
    private RxClientBuilder config;
    private Context mContext;

    private final long CONNECTTIMEOUT = 10 * 1000l; //链接超时，单位：毫秒
    private final long READTIMEOUT = 10 * 1000l;//读取超时， 单位：毫秒

    public RxHttpClient(Context context) {
        this.mContext = context;
        this.config = RxClientBuilder.create()
                .cookie(null)
                .connectTimeout(CONNECTTIMEOUT)
                .readTimeoutMills(READTIMEOUT)
                .isReConnection(false)
                .isCommonHead(true)
                .build();
    }

    /**
     * 传入设置参数
     *
     * @param _config
     */
    public RxHttpClient(Context context,RxClientBuilder _config) {
        this.mContext = context;
        if (_config == null) {
            //默认设置
            config = RxClientBuilder.create()
                    .cookie(null)
                    .connectTimeout(CONNECTTIMEOUT)
                    .readTimeoutMills(READTIMEOUT)
                    .isReConnection(false)
                    .isCommonHead(false)
                    .build();
        } else {
            this.config = _config;
        }
    }

    public OkHttpClient getClient() {
        if (client == null) {
            //设置缓存路径
            File cacheFile = new File(SdCardUtil.getCachePath(mContext), XConfig.NET_CACHE);
            if (cacheFile.getParentFile().exists())
                cacheFile.mkdirs();
            //设置缓存大小(当先线程的八分之一)
            Cache cache = new Cache(cacheFile, XConfig.MAX_DIR_SIZE);


            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            //失败后是否重新连接
            builder.retryOnConnectionFailure(config.isReConnection());
            //连接超时
            builder.connectTimeout(config.getConnectTimeout() != 0 ? config.getConnectTimeout() : CONNECTTIMEOUT, TimeUnit.MILLISECONDS);
            //读取超时
            builder.readTimeout(config.getReadTimeoutMills() != 0 ? config.getReadTimeoutMills() : READTIMEOUT, TimeUnit.MILLISECONDS);
            //设置cookie，保存cookie,读取cookie
            CookieJar cookieJar = config.getCookie();
            if (cookieJar != null) {
                builder.cookieJar(cookieJar);
            }
//            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor(new HttpLogger());
//            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            //设置缓存拦截器，实现网络缓存(有网络的时候不缓存，没有网络的时候缓存)
            builder.addInterceptor(new CacheInterceptor(mContext))
                    .addNetworkInterceptor(new CacheInterceptor(mContext))
                   // .addNetworkInterceptor(logInterceptor)
                    .cache(cache);
            //不走代理
            builder.proxy(Proxy.NO_PROXY);
            //添加请求头
            client = builder.build();

        }
        return client;
    }

}
