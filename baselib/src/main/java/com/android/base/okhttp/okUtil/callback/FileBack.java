package com.android.base.okhttp.okUtil.callback;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import androidx.annotation.NonNull;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Sammie on 2017/9/13.
 * 文件下载
 */
public abstract class FileBack implements Callback {

    private String mSaveDir = "/Download"; //下载文件保存目录

    protected Context mContext; //在ParamsBuilder中传递过来，不为空(用于统一处理提示、dialog等)

    /**
     * @param saveDir 下载文件保存的路径
     */
    public FileBack(String saveDir) {
        mSaveDir = saveDir;
    }


    @Override
    public void onFailure(Call call, final IOException ioE) {
        Observable.create(new ObservableOnSubscribe<IOException>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<IOException> e) throws Exception {
                e.onNext(ioE);
            }
        }).observeOn(AndroidSchedulers.mainThread()) //指定 Subscriber 的回调发生在主线程
                .subscribe(new Consumer<IOException>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull IOException response) throws Exception {
                        onDownloadFailed(response);
                    }
                });
    }

    @Override
    public void onResponse(Call call, Response response) {
        InputStream is = null;
        byte[] buf = new byte[2048];
        int len = 0;
        FileOutputStream fos = null;
        try {
            double current = 0;
            double total = response.body().contentLength();
            String fileName = getNameFromUrl(response.request().url().url().getPath()); //文件名
            is = response.body().byteStream();
            File file = new File(mSaveDir, fileName);
            fos = new FileOutputStream(file);
            while ((len = is.read(buf)) != -1) {
                current += len;
                fos.write(buf, 0, len);
                int progress = (int) (current * 1f / total * 100);
                System.out.println("xxx--"+progress+"-"+current +"/"+total);
                Observable.create(new ObservableOnSubscribe<Integer>() {
                    @Override
                    public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<Integer> e) throws Exception {
                        e.onNext(progress);
                    }
                }).observeOn(AndroidSchedulers.mainThread()) //指定 Subscriber 的回调发生在主线程
                        .subscribe(new Consumer<Integer>() {
                            @Override
                            public void accept(@io.reactivex.annotations.NonNull Integer progress) throws Exception {
                                // 下载中
                                onDownloading(progress);
                            }
                        });
            }
            fos.flush();
            //如果下载文件成功，第一个参数为文件的绝对路径
            Observable.create(new ObservableOnSubscribe<Integer>() {
                @Override
                public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<Integer> e) throws Exception {
                    e.onNext(0);
                }
            }).observeOn(AndroidSchedulers.mainThread()) //指定 Subscriber 的回调发生在主线程
                    .subscribe(new Consumer<Integer>() {
                        @Override
                        public void accept(@io.reactivex.annotations.NonNull Integer response) throws Exception {
                            // 下载完成
                            onDownloadSuccess(mSaveDir + File.separator + fileName);
                        }
                    });
        } catch (final Exception e1) {
            Observable.create(new ObservableOnSubscribe<Exception>() {
                @Override
                public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<Exception> e) throws Exception {
                    e.onNext(e1);
                }
            }).observeOn(AndroidSchedulers.mainThread()) //指定 Subscriber 的回调发生在主线程
                    .subscribe(new Consumer<Exception>() {
                        @Override
                        public void accept(@io.reactivex.annotations.NonNull Exception response) throws Exception {
                            onDownloadFailed(response);
                        }
                    });
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (fos != null) fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param url
     * @return 从下载连接中解析出文件名
     */
    @NonNull
    private String getNameFromUrl(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

    /**
     * 下载成功
     */
    public abstract void onDownloadSuccess(String fileDir);

    /**
     * @param progress 下载进度
     */
    public abstract void onDownloading(int progress);

    /**
     * 下载失败
     */
    public abstract void onDownloadFailed(Exception e);

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        this.mContext = context;
    }
}
