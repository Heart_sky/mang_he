package com.android.base.okhttp.okUtil;

import android.content.Context;


import com.android.base.tools.EmptyUtil;

import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by Sammie on 2017/9/26.
 * 用于构造OKUtil头部
 */
class HeadBuilder<T extends HeadBuilder> {

    protected Request.Builder requestBuilder;
    protected OkHttpClient client;
    protected Context mContext;

    public HeadBuilder(Context context, Request.Builder request, OkHttpClient _client) {
        this.requestBuilder = request;
        this.client = _client;
        this.mContext = context;
    }


    /**
     * ****************************addHead请求封装****************************************
     */
    public T addHead(Map<String, String> params) {
        if (EmptyUtil.check(params)) throw new NullPointerException("参数：params == null");
        for (Map.Entry<String, String> entry : params.entrySet()) {
            requestBuilder.addHeader(entry.getKey(), entry.getValue());
        }
        return (T)this;
    }

    public T addHead(String key, String value) {
        if (EmptyUtil.check(key)) throw new NullPointerException("参数：params.key == null");
        requestBuilder.addHeader(key, value);
        return (T)this;
    }

}
