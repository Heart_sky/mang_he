package com.android.base.okhttp.okUtil;

/**
 * Created by Sammie on 2017/9/8.
 */

import android.content.Context;

import com.android.base.tools.EmptyUtil;

import java.io.File;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * 用于构建get(),post()等参数方法
 */
public class ParamsBuild {

    private Request.Builder requestBuilder;
    private OkHttpClient client;
    private Context mContext;


    public ParamsBuild(Context context, Request.Builder request, OkHttpClient _client) {
        this.requestBuilder = request;
        this.client = _client;
        this.mContext = context;
        builder = new FormBody.Builder();
    }

    /**
     * ****************************post请求封装****************************************
     */
    private FormBody.Builder builder;

    public PostBuilder post(Map<String, String> params) {
        if (EmptyUtil.check(params)) throw new NullPointerException("参数：params == null");
        //设置参数
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.add(entry.getKey(), entry.getValue());
        }
        return new PostBuilder(mContext, requestBuilder, client, builder);
    }

    public PostBuilder post(String key, String value) {
        if (EmptyUtil.check(key)) throw new NullPointerException("参数：params.key == null");
        builder.add(key, value);
        return new PostBuilder(mContext, requestBuilder, client, builder);
    }

    /**
     * ****************************get请求封装****************************************
     */
    public Execute get() {
        return new Execute(mContext, requestBuilder.get(), client);
    }

    /**
     * ****************************上传文件请求封装****************************************
     */
    public UploadFileBuilder uploadFile(String key, String fileUrl) {
        File file = new File(fileUrl);
        if (file == null) {
            new Throwable("上传文件不存在。。。");
        }
        MultipartBody.Builder multipartBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart(key, file.getName(),RequestBody.create(MediaType.parse(UploadFileBuilder.getMimeType(fileUrl)), file));
        return new UploadFileBuilder(mContext, requestBuilder, client, multipartBody);
    }

    /**
     * ****************************上传文件请求封装****************************************
     */
    public UploadFileBuilder uploadFiles(String key, String[] fileUrlList) {
        UploadFileBuilder uploadFileBuilder = new UploadFileBuilder(mContext, requestBuilder, client, new MultipartBody.Builder().setType(MultipartBody.FORM));
        if (fileUrlList != null) {
            for (String fileUrl : fileUrlList) {
                uploadFileBuilder.put(key, fileUrl);
            }
        }
        //上传文件
        return uploadFileBuilder;
    }

    /**
     * ****************************Json上传****************************************
     */
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public ParamsBuild putJson(String json) {
        RequestBody requestBody = RequestBody.create(JSON, json);
        requestBuilder.post(requestBody);
        return this;
    }
}