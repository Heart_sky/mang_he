package com.android.base.okhttp.okUtil;

import android.content.Context;

import com.android.base.tools.EmptyUtil;

import java.util.Map;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by Sammie on 2017/9/26.
 * 用于构造Post所需的所有参数
 */
public class PostBuilder extends HeadBuilder<PostBuilder> {

    private FormBody.Builder builder;

    public PostBuilder(Context context, Request.Builder request, OkHttpClient _client, FormBody.Builder builder) {
        super(context, request, _client);
        this.builder = builder;
    }

    public PostBuilder post(Map<String, String> params) {
        if (EmptyUtil.check(params)) throw new NullPointerException("参数：params == null");
        //设置参数
        for (Map.Entry<String, String> entry : params.entrySet()) {
            builder.add(entry.getKey(), entry.getValue());
        }
        return this;
    }

    public PostBuilder post(String key, String value) {
        if(null == value){
            return this;
        }
        if (EmptyUtil.check(key)) throw new NullPointerException("参数：params.key == null");
        builder.add(key, value);
        return this;
    }

    public Execute build() {
        builder.add("app_name", "meng_xiang");
        return new Execute(mContext, requestBuilder.post(builder.build()), client);
    }
}
