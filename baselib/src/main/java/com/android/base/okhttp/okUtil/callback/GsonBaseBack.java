package com.android.base.okhttp.okUtil.callback;

import android.content.Context;

import com.android.base.tools.TUtil;
import com.google.gson.Gson;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.orhanobut.logger.Logger;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Sammie on 2017/8/4.
 * RxCallBack---用于处理OKHttpUtil返回
 * Gson处理返回--使用RxJava切换处理方法到主线程
 * 支持的json说明：
 * 1、当setUseNetBean（）为空时，T代表完整Json的实体对象
 */
public abstract class GsonBaseBack<T> implements Callback {

    private final String TAG= "NetworkRes---";
    protected Context mContext; //在ParamsBuilder中传递过来，不为空

    public Context getContext() {
        if (mContext == null) {
            onFailure(new Exception("Context为空,请检查ParamsBuild中Context是否为空。"));
        }
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }
    /**
     * 子线程失败方法
     *
     * @param call
     */
    @Override
    public void onFailure(Call call, final IOException ioE) {
        Observable.create(new ObservableOnSubscribe<IOException>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<IOException> e) throws Exception {
                e.onNext(ioE);
            }
        }).observeOn(AndroidSchedulers.mainThread()) //指定 Subscriber 的回调发生在主线程
                .subscribe(new Consumer<IOException>() {
                    @Override
                    public void accept(@NonNull IOException response) throws Exception {
                        onFailure(response);
                    }
                });
    }

    /**
     * 子线程成功方法
     *
     * @param call
     * @param response
     * @throws IOException
     */
    @Override
    public void onResponse(final Call call, final Response response) throws IOException {
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<Object> e) throws Exception {
                /**
                 * TODO 把返回结果当做标准Json统一处理,如果返回结果不是标准json,
                 * TODO 请自行重写该方法。(response.body()只能调用一次)
                 */
                if (TUtil.getT(GsonBaseBack.this, 0) == null) {
                    //不存在泛型的情况,直接返回json
                    e.onNext(response.body().string());
                } else {
                    String json = parseJsonBefore(response.body().string());
                    Object entity = parseJson(response.request().url().toString(), json);
                    e.onNext(entity);
                }
            }
        }).observeOn(AndroidSchedulers.mainThread()) //指定 Subscriber 的回调发生在主线程
                .subscribe(new Observer<Object>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Object response) {
                        parseJsonAfter(call, response);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        if (e instanceof Exception) {
                            onFailure((Exception) e);
                            GsonBaseBack.this.onComplete();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 自定义解析json
     *
     * @return 传入泛型
     */
    protected Object parseJson(String url, String json) {
        T entity = (T) new Gson().fromJson(json, TUtil.getT(GsonBaseBack.this, 0).getClass());
        return entity;
    }

    /**
     * 主线程成功方法，返回 String
     */
    public void onSuccess(Call call, String json) {
    }

    /**
     * 主线程成功方法,返回 对象
     */
    public void onSuccess(Call call, T obj) {
    }

    /**
     * 主线程成功方法,返回
     */
    public void onSuccess(Call call, List<T> list) {
    }

    /**
     * 主线程处理异常方法
     */
    public void onFailure(Exception e) {

    }

    /**
     * 完成
     */
    public void onComplete() {

    }

    /**
     * 解析Json之前调用，根据需求统一处理Json
     * 执行在子线程(例如 json unicode转utf-8)
     *
     * @return 处理后的Json数据
     */
    public String parseJsonBefore(String json) {
        return json;
    }

    /**
     * 解析Json完成后调用，根据需求处理返回
     * 执行在主线程，可以重新统一对数据处理
     */
    protected void parseJsonAfter(Call call, @NonNull Object response) {
        //返回主线程
        if (response instanceof String) {
            onSuccess(call, (String) response);
        } else {
            onSuccess(call, (T) response);
        }
    }

    /**
     * 解码 Unicode
     *
     * @param str
     * @return
     */
    private static String decodeUnicode(String str) {
        Charset set = Charset.forName("UTF-16");
        Pattern p = Pattern.compile("\\\\u([0-9a-fA-F]{4})");
        Matcher m = p.matcher(str);
        int start = 0;
        int start2 = 0;
        StringBuffer sb = new StringBuffer();
        while (m.find(start)) {
            start2 = m.start();
            if (start2 > start) {
                String seg = str.substring(start, start2);
                sb.append(seg);
            }
            String code = m.group(1);
            int i = Integer.valueOf(code, 16);
            byte[] bb = new byte[4];
            bb[0] = (byte) ((i >> 8) & 0xFF);
            bb[1] = (byte) (i & 0xFF);
            ByteBuffer b = ByteBuffer.wrap(bb);
            sb.append(String.valueOf(set.decode(b)).trim());
            start = m.end();
        }
        start2 = str.length();
        if (start2 > start) {
            String seg = str.substring(start, start2);
            sb.append(seg);
        }
        return sb.toString();
    }
}