package com.android.base.thread;

import java.io.Serializable;


/**
 *@Project Name:	AndroidCommon
 *@Copyright:		
 *@Version:			1.0.0.1
 *@File_Name:		IDataAction.java	
 *@AuthorName		ZhangWeiJun
 *@CreateDate:		2014-1-22
 *@ModifyHistory:
 */
public interface ICommonAction extends Serializable {
	void action();
}
