package com.android.base.thread;

import java.io.Serializable;


/**
 *@Project Name:	AndroidCommon
 *@Copyright:		
 *@Version:			1.0.0.1
 *@File_Name:		IDataAction.java	
 *@AuthorName		ZhangWeiJun
 *@CreateDate:		2014-1-22
 *@ModifyHistory:
 */
public interface IThreadAction<T> extends Serializable {
	T doInBackground();
	void returnForeground(T obj);
	void cancel();//这个方法中可做一些取消后的处理，如断开网络
}
