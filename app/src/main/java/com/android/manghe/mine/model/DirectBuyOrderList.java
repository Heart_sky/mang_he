package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

/**
 * Created by lnest-php on 2017/9/19.
 */

public class DirectBuyOrderList extends NetBean<DirectBuyOrderList.DataBean> {

    /**
     * data : {"list_total":11,"list":[{"id":"15","mid":"4","order_sn":"2017092115503550100981","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505980235","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"11","mid":"4","order_sn":"2017092115444410097100","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979884","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"10","mid":"4","order_sn":"2017092115441499100509","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979854","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"9","mid":"4","order_sn":"2017092115405550494855","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979655","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"62","pay_name":"支付宝APP","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"8","mid":"4","order_sn":"2017092115394910249100","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979589","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"7","mid":"4","order_sn":"2017092115272799521005","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505978847","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"6","mid":"4","order_sn":"2017092115211456545510","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505978474","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"5","mid":"4","order_sn":"2017092115124650575353","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505977966","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"4","mid":"4","order_sn":"2017092115084898995155","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505977728","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"3","mid":"4","order_sn":"2017092114571898102100","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505977038","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"2","mid":"4","order_sn":"2017092114544548102995","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505976885","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]}]}
     * flag : true
     * code : 0
     * msg : 操作成功
     * time : 1505980750
     */
    public static class DataBean {
        /**
         * list_total : 11
         * list : [{"id":"15","mid":"4","order_sn":"2017092115503550100981","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505980235","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"11","mid":"4","order_sn":"2017092115444410097100","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979884","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"10","mid":"4","order_sn":"2017092115441499100509","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979854","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"9","mid":"4","order_sn":"2017092115405550494855","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979655","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"62","pay_name":"支付宝APP","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"8","mid":"4","order_sn":"2017092115394910249100","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505979589","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"7","mid":"4","order_sn":"2017092115272799521005","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505978847","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"6","mid":"4","order_sn":"2017092115211456545510","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505978474","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"5","mid":"4","order_sn":"2017092115124650575353","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505977966","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"4","mid":"4","order_sn":"2017092115084898995155","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505977728","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"3","mid":"4","order_sn":"2017092114571898102100","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505977038","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]},{"id":"2","mid":"4","order_sn":"2017092114544548102995","status_pay":"0","status_shipping":"0","status_order":"0","c_time":"1505976885","pay_time":"0","fright_name":"顺丰速递","order_amount":"0.10","goods_amount":"0.10","shipping_fee":"0.00","pay_fee":"0.00","amount":"0.10","pay_id":"0","pay_name":"","address_id":"2","address":"宇宙中心五道口","area":"北京  东城区","mobile":"13512345677","name":"海飞飞","ship_time":"0","score_amount":"1","is_rate":"0","status_name":"未付款,未发货","status_id":100,"goods_item":[{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]}]
         */

        public int list_total;
        public List<ListBean> list;


        public static class ListBean {
            /**
             * id : 15
             * mid : 4
             * order_sn : 2017092115503550100981
             * status_pay : 0
             * status_shipping : 0
             * status_order : 0
             * c_time : 1505980235
             * pay_time : 0
             * fright_name : 顺丰速递
             * order_amount : 0.10
             * goods_amount : 0.10
             * shipping_fee : 0.00
             * pay_fee : 0.00
             * amount : 0.10
             * pay_id : 0
             * pay_name :
             * address_id : 2
             * address : 宇宙中心五道口
             * area : 北京  东城区
             * mobile : 13512345677
             * name : 海飞飞
             * ship_time : 0
             * score_amount : 1
             * is_rate : 0
             * status_name : 未付款,未发货
             * status_id : 100
             * goods_item : [{"good_id":"5","goods_spec":"","buy_num":"1","sell_price":"0.10","score_price":"1","name":"爱之小屋洗漱套装测试用","thumb":"http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg"}]
             */

            public String id;
            public String mid;
            public String order_sn;
            public String status_pay;
            public String status_shipping;
            public String status_order;
            public String c_time;
            public String pay_time;
            public String fright_name;
            public String order_amount;
            public String goods_amount;
            public String shipping_fee;
            public String pay_fee;
            public String amount;
            public String pay_id;
            public String pay_name;
            public String address_id;
            public String address;
            public String area;
            public String mobile;
            public String name;
            public String ship_time;
            public String score_amount;
            public String is_rate;
            public String status_name;
            public int status_id;
            public String is_exempt; //0原价 1免单
            public float distance_amount;
            public List<GoodsItemBean> goods_item;


            public static class GoodsItemBean {
                /**
                 * good_id : 5
                 * goods_spec :
                 * buy_num : 1
                 * sell_price : 0.10
                 * score_price : 1
                 * name : 爱之小屋洗漱套装测试用
                 * thumb : http://jm.lnest.cc/demo/1/images/gallery/1/2/39_src.jpg
                 */

                public String good_id;
                public String goods_spec;
                public String buy_num;
                public String sell_price;
                public String score_price;
                public String name;
                public String thumb;
            }
        }
    }
}
