package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.CommonProblemActivity;
import com.android.manghe.mine.model.CustomerRes;

import java.util.HashMap;

import okhttp3.Call;

public class PCommonProblem extends XPresenter<CommonProblemActivity> {


    public void getData() {
        HashMap<String, String> data = new HashMap<>();
        data.put("cat", "1");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBER_CUSTOMER, data, headMap)
                .get()
                .execute(new BaseBack<CustomerRes>() {

                    @Override
                    public void onSuccess(Call call, CustomerRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0 && res.catlist != null) {
                            getV().updateView(res.catlist);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
