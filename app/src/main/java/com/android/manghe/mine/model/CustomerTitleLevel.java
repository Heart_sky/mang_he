package com.android.manghe.mine.model;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;

public class CustomerTitleLevel extends AbstractExpandableItem<CustomerContentLevel> implements MultiItemEntity {

    public String title;

    public CustomerTitleLevel(String title) {
        this.title = title;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}
