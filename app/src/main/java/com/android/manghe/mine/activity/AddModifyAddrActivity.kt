package com.android.manghe.mine.activity

import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.text.TextUtils
import android.view.View
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import com.android.manghe.mine.model.AddressInfo
import com.android.manghe.mine.model.AreaInfo
import com.android.manghe.mine.presenter.PAddModifyAddr
import kotlinx.android.synthetic.main.activity_add_addr.*


class AddModifyAddrActivity : MVPActivity<PAddModifyAddr>() {

    private var addrInfo: AddressInfo? = null
    private var zoneId = "";

    override fun getLayoutId(): Int {
        return R.layout.activity_add_addr
    }

    override fun initData(savedInstanceState: Bundle?) {
        val tempParam = intent.getSerializableExtra("AddressInfo");
        if (tempParam != null) {
            addrInfo = tempParam as AddressInfo
        }
        titleBar.setToolbar(if (addrInfo == null) "添加收货地址" else "修改收货地址").setLeftIcon(R.mipmap.icon_back_black)
            .setTitleAndStatusBgColor(R.color.white)
        if (addrInfo != null) {
            //添加
            etName.setText(addrInfo!!.name)
            etTel.setText(addrInfo!!.mobile)
            tvArea.setText(addrInfo!!.area)
            etAddr.setText(addrInfo!!.address)
            zoneId = addrInfo!!.zone

            layoutDefaultAddr.visibility = if(TextUtils.equals("1", addrInfo!!.is_default)) View.GONE else View.VISIBLE
        }

        tvArea.setOnClickListener {
            //选择区域
            tvArea.text = ""
            zoneId = ""
            showLoadingDialog()
            p.getArea("1")
        }

        btnSave.setOnClickListener {
            val name = etName.text.toString().trim()
            val zoneStr = tvArea.text.toString().trim()
            val addr = etAddr.text.toString().trim()
            val tel = etTel.text.toString().trim()
            if (TextUtils.isEmpty(name)) {
                ToastUtil.showShort(this@AddModifyAddrActivity, "请输入收货人姓名")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(tel)) {
                ToastUtil.showShort(this@AddModifyAddrActivity, "请输入手机号码")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(zoneStr)) {
                ToastUtil.showShort(this@AddModifyAddrActivity, "请选择所在地区")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(addr)) {
                ToastUtil.showShort(this@AddModifyAddrActivity, "请输入详细地址")
                return@setOnClickListener
            }

            showLoadingDialog()
            if (addrInfo != null) {
                //修改地址
                p.modifyAddr(addrInfo!!.id, name, zoneId, addr, tel, switchDefaultAddr.isChecked)
            } else {
                //添加地址
                p.addAddr(name, zoneId, addr, tel, switchDefaultAddr.isChecked)
            }
        }
    }

    fun showAreaList(areaList: List<AreaInfo>) {
        var items = arrayOfNulls<String>(areaList.size)
        for ((index, area) in areaList.withIndex()) {
            items[index] = area.name
        }
        val listDialog = AlertDialog.Builder(this)
        listDialog.setItems(items, DialogInterface.OnClickListener { dialog, which ->
            val areaInfo = areaList[which]
            zoneId = areaInfo.id
            tvArea.append(areaInfo.name + " ")
            if (TextUtils.equals("1", areaInfo.child)) {
                //包含子级
                showLoadingDialog()
                p.getArea(areaInfo.id)
            }
        })
        listDialog.show()
    }
}