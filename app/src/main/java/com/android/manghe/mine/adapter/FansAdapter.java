package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.FansRes;

import java.util.List;

public class FansAdapter extends BaseRecyclerViewAdapter<FansRes.DataBean.ListBean, FansAdapter.CoinDetialHolder> {
    private Context mContext;

    public FansAdapter(Context context, List<FansRes.DataBean.ListBean> list) {
        super(list);
        mContext = context;
    }

    public void update(List<FansRes.DataBean.ListBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(CoinDetialHolder holder, FansRes.DataBean.ListBean item) {
        GlideHelper.loadAvatar(mContext,item.photo, holder.getView(R.id.ivHead));
        holder.setText(R.id.tvName, item.username);
        holder.setText(R.id.tvTime, item.c_time);

    }

    @Override
    public CoinDetialHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new CoinDetialHolder(inflateItemView(viewGroup, R.layout.item_fans));
    }

    public class CoinDetialHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public CoinDetialHolder(View itemView) {
            super(itemView);
        }
    }
}
