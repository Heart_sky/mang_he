package com.android.manghe.mine.adapter;

import android.content.Context;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.android.manghe.R;
import com.android.manghe.mine.model.FreightInfoRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/13 17:35
 * desc   :
 */
public class WuLiuAdapter extends BaseQuickAdapter<FreightInfoRes.LogisticBean, BaseViewHolder> {


    private Context mContext;

    public WuLiuAdapter(Context context, @Nullable List<FreightInfoRes.LogisticBean> data) {
        super(R.layout.item_wu_liu, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, FreightInfoRes.LogisticBean item) {
        helper.setText(R.id.item_express_time, item.getTime());
        helper.setText(R.id.item_express_desc, item.getContext());
        RelativeLayout mLayout = helper.itemView.findViewById(R.id.id_re);
        
    }
}
