package com.android.manghe.mine.adapter;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.mine.model.IntegralModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/30 16:12
 * desc   :
 */
public class DeliverIntegralAdapter extends BaseQuickAdapter<IntegralModel.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;

    public DeliverIntegralAdapter(Context context, @Nullable List<IntegralModel.DataBean.ListBean> data) {
        super(R.layout.item_goods, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, IntegralModel.DataBean.ListBean model) {
        GlideHelper.loadWithHolderErr(mContext, model.thumb, helper.getView(R.id.ivPic));
        helper.setText(R.id.tvGoodName, model.name);
        TextView tvScore = helper.itemView.findViewById(R.id.tvCount);
        tvScore.setText("积分 "+model.score);
        tvScore.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        tvScore.setTextSize(14f);


    }
}
