package com.android.manghe.mine.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.FansInCommissionRes;

import java.util.List;

public class CommissionDetailAdapter extends BaseRecyclerViewAdapter<FansInCommissionRes.DataBean.ListBean, CommissionDetailAdapter.CommissionDetialHolder> {
    public CommissionDetailAdapter(List<FansInCommissionRes.DataBean.ListBean> list) {
        super(list);
    }

    public void update(List<FansInCommissionRes.DataBean.ListBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(CommissionDetialHolder holder, FansInCommissionRes.DataBean.ListBean item) {
        holder.setText(R.id.tvTitle, item.remark);
        holder.setText(R.id.tvTime, item.c_time);
        holder.setText(R.id.tvCoin, "+"+item.amount);
    }

    @Override
    public CommissionDetialHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new CommissionDetialHolder(inflateItemView(viewGroup, R.layout.item_commission_detail));
    }

    public class CommissionDetialHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public CommissionDetialHolder(View itemView) {
            super(itemView);
        }
    }
}
