package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.PackageUtil;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.SettingActivity;
import com.android.manghe.mine.model.AppVersionRes;

import java.util.HashMap;

import okhttp3.Call;

public class PSetting extends XPresenter<SettingActivity> {

    public void checkVersion() {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MAIN_UPDATE_VERSION)
                .get()
                .execute(new BaseBack<AppVersionRes>() {

                    @Override
                    public void onSuccess(Call call, AppVersionRes res) {
                        if (res.code == 0) {
                            if (res.data != null) {
                                if(PackageUtil.getVersionCode(getV()) >= Integer.parseInt(res.data.name)){
                                    getV().alreadyLastedVersion();
                                }else{
                                    //提示升级
                                    getV().showUpdateDialog(res.data.version,res.data.content,res.data.down_url);
                                }
                            } else {
                                getV().showGetUpdateFailMsg(res.msg);
                            }
                        } else {
                            getV().showGetUpdateFailMsg(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().showGetUpdateFailMsg(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }


    public void cancellationAccount(){
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MemberDestroy, headMap)
                .post(new HashMap<>()).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean netBean) {
                        if (netBean != null && netBean.code == 0) {
                            getV().doSignOut();
                        }else{
                            ToastUtil.showLong(getV(), "注销失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        getV().hideLoadingDialog();
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
