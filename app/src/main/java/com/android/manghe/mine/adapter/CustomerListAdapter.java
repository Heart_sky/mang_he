package com.android.manghe.mine.adapter;

import android.os.Build;
import android.text.Html;
import android.view.View;
import com.android.manghe.R;
import com.android.manghe.mine.model.CustomerContentLevel;
import com.android.manghe.mine.model.CustomerTitleLevel;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

public class CustomerListAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    public CustomerListAdapter(List<MultiItemEntity> data) {
        super(data);
        addItemType(0, R.layout.item_customer_title);
        addItemType(1, R.layout.item_customer_content);
    }

    @Override
    protected void convert(final BaseViewHolder holder, final MultiItemEntity item) {
        switch (holder.getItemViewType()) {
            case 0:
                final CustomerTitleLevel titleLevel = (CustomerTitleLevel) item;
                holder.setText(R.id.tvTitle, titleLevel.title)
                        .setImageResource(R.id.ivArrow, titleLevel.isExpanded() ? R.mipmap.arrow_down : R.mipmap.arrow_right);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = holder.getAdapterPosition();
                        if (titleLevel.isExpanded()) {
                            collapse(pos);
                        } else {
                            expand(pos);
                        }
                    }
                });
                break;
            case 1:
                final CustomerContentLevel contentLevel = (CustomerContentLevel) item;
                holder.setText(R.id.tvContent, Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?Html.fromHtml(contentLevel.content, Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(contentLevel.content) );
                break;
            default:
                break;
        }
    }
}
