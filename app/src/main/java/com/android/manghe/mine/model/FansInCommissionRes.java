package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class FansInCommissionRes extends NetBean<FansInCommissionRes.DataBean> {


    public static class DataBean {
        public List<ListBean> list;

        public static class ListBean {
            public String amount;
            public String remark;
            public String c_time;
        }
    }
}
