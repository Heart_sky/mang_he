package com.android.manghe.mine.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.ShareActivity;

import java.util.HashMap;

import okhttp3.Call;

public class PShare extends XPresenter<ShareActivity> {


    public void sendCode(String code){
        HashMap<String, String> headMap = new HashMap<String, String>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.SetInvite, headMap)
                .post("ivt_id", TextUtils.isEmpty(code)? "" :code).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null) {
                            if(res.code == 0) {
                                getV().sendCodeOk();
                                ToastUtil.showShort(getV(), "提交邀请码成功");
                            }else{
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        }else{
                            ToastUtil.showShort(getV(), "提交邀请码失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        ToastUtil.showShort(getV(), "提交邀请码失败");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
