package com.android.manghe.mine.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.ToastUtil;
import com.android.base.view.roundview.RoundTextView;
import com.android.manghe.R;
import com.android.manghe.mine.adapter.SignAdapter;
import com.android.manghe.mine.adapter.SignDayAdapter;
import com.android.manghe.mine.model.SignInfoModel;
import com.android.manghe.mine.model.SignModel;
import com.android.manghe.mine.presenter.PSignBak;
import com.android.manghe.view.dialog.NewTipDialog;
import com.android.manghe.view.popWindow.SignSuccessPopWindow;

import java.util.ArrayList;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/20 14:46
 * desc   : 签到页面改版
 */
public class SignActivityBak extends MVPActivity<PSignBak> {

    private RoundTextView tvRules;
    private TextView tvTotalMoney, tvContinuitySign, tvTomorrowMoney;
    private RecyclerView mRecyclerView, mDayRecyclerView;
    private SignAdapter mAdapter;
    private SignDayAdapter mDayAdapter;
    private TextView mBtn;

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("签到领金币").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setBackgroundColor(ContextCompat.getColor(this, R.color.white));

        tvRules = findViewById(R.id.tvSignRule);
        tvTotalMoney = findViewById(R.id.tvTotalMoney);
        tvContinuitySign = findViewById(R.id.tvContinuitySign);
        mRecyclerView = findViewById(R.id.id_recycler_view);
        mDayRecyclerView = findViewById(R.id.recyclerView);
        mBtn = findViewById(R.id.btnSign);
        tvTomorrowMoney = findViewById(R.id.tvTomorrowSing);

        initRecyclerView();
        setListener();
        showLoadingDialog();
        getP().getSignInfo();
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new SignAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 7);
        mDayRecyclerView.setLayoutManager(gridLayoutManager);
        mDayAdapter = new SignDayAdapter(this, new ArrayList<>());
        mDayRecyclerView.setAdapter(mDayAdapter);


    }


    private void setListener() {
        tvRules.setOnClickListener(v -> {
            open(SignRuleActivity.class);
        });

        mAdapter.setListener(new SignAdapter.ISignListener() {
            @Override
            public void onClick(int position) {
                showLoadingDialog();
                getP().signTakeBonus(position + "");
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_sing_bak;
    }

    public void onClickSign(View view) {
        getP().sign();

    }

    public void showData(SignInfoModel.DataBean sinInfoModel) {
        tvTotalMoney.setText(sinInfoModel.total + "");
        tvContinuitySign.setText(sinInfoModel.continuous_days + "");
        mAdapter.updateList(getP().getSigDesignList(this, sinInfoModel));
        mDayAdapter.replaceData(getP().getSignDayList(this, sinInfoModel));
        tvTomorrowMoney.setText(mDayAdapter.getSingDesignInfo(sinInfoModel.continuous_days).getValue());

    }

    public void showSignOk(SignModel.DataBean dataBean) {
        SignSuccessPopWindow popWindow = new SignSuccessPopWindow(this, dataBean.bonus);
        popWindow.showPopWindow(mBtn);
        getP().getSignInfo();
    }

    public void showSignTakeBoundsOK() {
        getP().getSignInfo();
    }

    public void showSignFail(String award_type,String message) {
        NewTipDialog tipDialog = new NewTipDialog(this, message);
        tipDialog.show();
        tipDialog.setListener(new NewTipDialog.ITipDialogListener() {
            @Override
            public void clickLeft() {

            }

            @Override
            public void clickRight() {

            }
        });
    }


    public void getSignInfoFail() {
        ToastUtil.showLong(this, "获取信息失败，请重试！");
    }
}
