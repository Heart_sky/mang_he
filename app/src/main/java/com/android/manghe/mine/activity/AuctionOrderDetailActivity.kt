package com.android.manghe.mine.activity

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.DisplayUtil
import com.android.base.tools.Glide.GlideHelper
import com.android.base.tools.ToastUtil
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.common.activity.PaySuccessActivity
import com.android.manghe.common.model.PaymentTypeRes
import com.android.manghe.config.events.RefreshOrdersEvent
import com.android.manghe.coupon.adapter.PaymentAdapter
import com.android.manghe.mine.model.MyOrderDetailRes
import com.android.manghe.mine.presenter.PAuctionOrderDetail
import com.android.manghe.orderpay.model.AuctionOrderInfoRes
import com.android.manghe.view.AutoHeightLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.eightbitlab.rxbus.Bus
import com.eightbitlab.rxbus.registerInBus
import kotlinx.android.synthetic.main.activity_market_order_detail.*

/**
 * 竞拍订单详情
 * @author ZhangWeiJun
 * @date 2019/6/14
 */

class AuctionOrderDetailActivity : MVPActivity<PAuctionOrderDetail>() {
    private var mPaymentAdapter : PaymentAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_market_order_detail
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("订单详情").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)
        p.orderId = intent.getStringExtra("orderId")
        paymentRecyclerView.layoutManager = AutoHeightLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mPaymentAdapter = PaymentAdapter(this, ArrayList<PaymentTypeRes.DataBean.PaymentBean>())
        mPaymentAdapter!!.hideDesc()
        paymentRecyclerView.addItemDecoration(RecyclerViewDivider(this, LinearLayoutManager.HORIZONTAL))
        paymentRecyclerView.adapter = mPaymentAdapter
        mPaymentAdapter?.setOnItemClickListener { adapter, view, position ->
            val item = adapter.data[position] as PaymentTypeRes.DataBean.PaymentBean
            mPaymentAdapter?.selectedItem(item)
            p.setPayType(item)
        }


        p.getOrderDetail(true)
        btn.setOnClickListener {
            when (p.orderDetail.order_code) {
                4 -> {
                    //确认收货
                    AlertDialog.Builder(this)
                        .setTitle("是否确认收到货物？")
                        .setPositiveButton(
                            "确定"
                        ) { _, _ ->
                            p.confirmGetGood()
                        }
                        .setNegativeButton("取消") { dialog, _ ->
                            dialog.dismiss()
                        }
                        .create().show()
                }
                5 -> {
                    //晒单
                    AuctionShowOrderActivity.showActivity(this,p.orderId, p.orderDetail.auction_id, p.orderDetail.thumb,p.orderDetail.title)
                }
            }
        }
        Bus.observe<RefreshOrdersEvent>().subscribe {
            p.getOrderDetail(false)
        }.registerInBus(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        Bus.unregister(this)
    }

    fun update(data: AuctionOrderInfoRes.DataBean) {
        //地址
        tvAddrName.text = data.receive_name
        tvAddrTel.text = data.mobile
        tvAddrDetail.text = data.area +" "+ data.address
        tvOrderSN.text = data.order_sn

        //商品信息
//        rvGood.layoutManager = AutoHeightLayoutManager(this)
//        val adapter = GoodListAdapter(this, data.)
//        rvGood.adapter = adapter

        //金额
//        tvPrice.text = data.price.toString()
        tvResultPay.text = "￥${data.price}"

        //物流信息
        tvLogistics.text = data.express_name
        tvRemark.text = data.remark.toString()

        //状态
        tvStatus.text = data.order_status
        btn.visibility = View.VISIBLE
        //按钮
        when (data.order_code) {
            3 -> {
                btn.visibility = View.GONE
            }
            4 -> {
                btn.text = "确认收货"
            }
            5 -> {
                btn.text = "立即晒单"
            }
            7 -> {
                btn.visibility = View.GONE
            }
        }
    }


    fun updatePayments(paymentList : List<PaymentTypeRes.DataBean.PaymentBean>){
        mPaymentAdapter?.let {
            it.update(paymentList)
        }
        try{
            val lp = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,paymentList.size * DisplayUtil.dip2px(this, 60f))
            lp.topMargin = DisplayUtil.dip2px(this, 20f)
            lp.bottomMargin = DisplayUtil.dip2px(this, 20f)
            paymentRecyclerView.layoutParams = lp
        }catch (e:java.lang.Exception){
            e.printStackTrace()
        }
    }


    fun payFail(){
        ToastUtil.showShort(this, "支付失败")
    }

    fun paySuccess(){
        finish()
        open(PaySuccessActivity::class.java)
    }

    class GoodListAdapter(context: Context, data: List<MyOrderDetailRes.DataBean.GoodsBean>) :
        BaseQuickAdapter<MyOrderDetailRes.DataBean.GoodsBean, BaseViewHolder>(R.layout.item_order_detail_good, data) {
        var context: Context? = null

        init {
            this.context = context
        }

        override fun convert(
            helper: BaseViewHolder,
            item: MyOrderDetailRes.DataBean.GoodsBean
        ) {
            GlideHelper.loadRoundTrans(context, item.thumb, helper.getView(R.id.ivPic), 6)
            helper.setText(R.id.tvTitle, item.goods_name)
            helper.setText(R.id.tvAmount, item.buy_num)
        }

    }
}