package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/30 10:05
 * desc   :
 */
public class IntegralModel extends NetBean<IntegralModel.DataBean> {


    /**
     * data : {"list":[{"id":"2142","name":"THERMOS膳魔师焖烧罐470ml高真空不锈钢保温饭盒保温桶SK-3000 PK 【京东专供】","price":"678.00","score":"23","sell":"6","thumb":"https://www.mxmd88.com/upload/1/images/211128/f959b194a75e4cfb.jpg"},{"id":"2141","name":"诺基亚（NOKIA）105 新 黑色 直板按键 移动2G手机 老人老年手机 学生备用功能机 超长待机","price":"300.00","score":"66","sell":"56","thumb":"https://www.mxmd88.com/upload/1/images/211128/7db17147d2938085.jpg"},{"id":"2140","name":"红牡丹 骨瓷餐具套装陶瓷碗碟盘套装碗具6-8人用碗筷套装风华正茂 28件 方形配置","price":"479.00","score":"56","sell":"45","thumb":"https://www.mxmd88.com/upload/1/images/211128/e8d2f516bc868e6e.jpg"},{"id":"2139","name":"炊大皇 炒锅 304不锈钢炒菜锅 不粘锅 平底炒锅 可用不锈钢锅铲  可立盖 燃气煤气灶电磁炉通用","price":"200.00","score":"25","sell":"12","thumb":"https://www.mxmd88.com/upload/1/images/211128/4847e27b6dd83d25.jpg"},{"id":"2138","name":"测试1","price":"60.00","score":"10","sell":"10","thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png"}],"list_total":5}
     * flag : true
     */

    public static class DataBean implements Serializable {
        /**
         * list : [{"id":"2142","name":"THERMOS膳魔师焖烧罐470ml高真空不锈钢保温饭盒保温桶SK-3000 PK 【京东专供】","price":"678.00","score":"23","sell":"6","thumb":"https://www.mxmd88.com/upload/1/images/211128/f959b194a75e4cfb.jpg"},{"id":"2141","name":"诺基亚（NOKIA）105 新 黑色 直板按键 移动2G手机 老人老年手机 学生备用功能机 超长待机","price":"300.00","score":"66","sell":"56","thumb":"https://www.mxmd88.com/upload/1/images/211128/7db17147d2938085.jpg"},{"id":"2140","name":"红牡丹 骨瓷餐具套装陶瓷碗碟盘套装碗具6-8人用碗筷套装风华正茂 28件 方形配置","price":"479.00","score":"56","sell":"45","thumb":"https://www.mxmd88.com/upload/1/images/211128/e8d2f516bc868e6e.jpg"},{"id":"2139","name":"炊大皇 炒锅 304不锈钢炒菜锅 不粘锅 平底炒锅 可用不锈钢锅铲  可立盖 燃气煤气灶电磁炉通用","price":"200.00","score":"25","sell":"12","thumb":"https://www.mxmd88.com/upload/1/images/211128/4847e27b6dd83d25.jpg"},{"id":"2138","name":"测试1","price":"60.00","score":"10","sell":"10","thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png"}]
         * list_total : 5
         */

        public int list_total;
        public List<ListBean> list;
        public String remain_score;

        public static class ListBean implements Serializable {
            /**
             * id : 2142
             * name : THERMOS膳魔师焖烧罐470ml高真空不锈钢保温饭盒保温桶SK-3000 PK 【京东专供】
             * price : 678.00
             * score : 23
             * sell : 6
             * thumb : https://www.mxmd88.com/upload/1/images/211128/f959b194a75e4cfb.jpg
             */

            public String id;
            public String name;
            public String price;
            public String score;
            public String sell;
            public String thumb;
        }
    }
}
