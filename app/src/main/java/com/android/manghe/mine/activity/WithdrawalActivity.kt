package com.android.manghe.mine.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.MediaUtil
import com.android.base.tools.ToastUtil
import com.android.base.tools.permission.OnPermissionResultListener
import com.android.base.tools.permission.PermissionUtil
import com.android.manghe.R
import com.android.manghe.mine.presenter.PWithdrawal
import com.android.manghe.view.crop.ImageCropHelper
import com.android.manghe.view.multiselectorview.MultiImageSelectorView
import com.isseiaoki.simplecropview.CropImageView
import com.yanzhenjie.permission.runtime.Permission
import kotlinx.android.synthetic.main.activity_withdrawal.*
import kotlinx.android.synthetic.main.activity_withdrawal.layoutPicTip
import kotlinx.android.synthetic.main.activity_withdrawal.multiImageSelectorView
import me.nereo.multi_image_selector.MultiImageSelector
import me.nereo.multi_image_selector.MultiImageSelectorActivity

/**
 * 佣金提现
 */
class WithdrawalActivity :MVPActivity<PWithdrawal>(), OnPermissionResultListener, MultiImageSelectorView.OnItemClickListener{
    private val CODE_IMAGE_SELECT = 100
    private val CODE_IMAGE_CROP = 101

    override fun getLayoutId():Int = R.layout.activity_withdrawal
    override fun initData(savedInstanceState: Bundle?) {
        p.commission = intent.getFloatExtra("commission", 0f)
        initView()
        initListener()

    }

    private fun initView(){
        titleBar.setToolbar("提现").setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black)
        tvTotalMoney.text = "全部余额：${p.commission}元"
        multiImageSelectorView.setImageMaxCount(1)
        multiImageSelectorView.setShowAddImageOnMaximum(false)
    }

    private fun initListener(){
        multiImageSelectorView.setOnItemClickListener(this)
        tvALL.setOnClickListener {
            //全部
            etMoney.setText("${p.commission}")
        }
        btnWithdraw.setOnClickListener {
            //提现
            val name = etName.text.toString().trim()
            val account = etAccount.text.toString().trim()
            val tempMoney = etMoney.text.toString().trim()
            var money = 0f
            if(name.isEmpty()){
                ToastUtil.showLong(this, "请输入姓名")
                return@setOnClickListener
            }
            if(account.isEmpty()){
                ToastUtil.showLong(this, "请输入支付宝账号")
                return@setOnClickListener
            }
            if(tempMoney.isEmpty()){
                ToastUtil.showLong(this, "请输入提现金额")
                return@setOnClickListener
            }else{
                money = tempMoney.toFloat()
                if(money <= 0){
                    ToastUtil.showLong(this, "请输入正确的金额")
                    return@setOnClickListener
                }
            }
            if(p.picList.size==0){
                ToastUtil.showLong(this,"请上传收款码")
                return@setOnClickListener
            }
            p.submit(name, account, money)
        }
        layoutPicTip.setOnClickListener {
            //添加第一个图片
            PermissionUtil.requestPermissions(
                this, this,
                Permission.WRITE_EXTERNAL_STORAGE, Permission.CAMERA
            )
        }
    }

    override fun onGranted(permissions: MutableList<String>?) {
        MultiImageSelector.create()
            .showCamera(true) // show camera or not. true by default
            .count(1) // max select image size, 9 by default. used width #.multi()
            .single() // multi mode, default mode;
            .origin(p.picList) // original select data set, used width #.multi()
            .start(this, CODE_IMAGE_SELECT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == CODE_IMAGE_SELECT){
                val arrayList = data?.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT)
                if (arrayList != null && !arrayList.isEmpty()) {
                    ImageCropHelper.startCropFree(this, arrayList.get(0),
                        ImageCropHelper.ImageCropParamsBuilder
                            .create()
                            .compressQuality(90)
                            .cropMode(CropImageView.CropMode.FREE)
                            .outputMaxSize(1280, 1280)
                            .build(), CODE_IMAGE_CROP)
                }

            }else if(requestCode == CODE_IMAGE_CROP){
                val resultUri = ImageCropHelper.onActivityResult(resultCode, data)
                if (resultUri != null) {
                    val imagePath = MediaUtil.getPath(this, resultUri)
                    p.picList.add(imagePath)
                    layoutPicTip.visibility = View.GONE
                    multiImageSelectorView.visibility = View.VISIBLE
                    multiImageSelectorView.setList(p.picList)
                }
            }
        }
    }



    override fun onDelete(view: View?, position: Int) {
        if (position >= 0 && position < p.picList.size) {
            p.picList.removeAt(position)
            multiImageSelectorView.setList(p.picList)
        }
    }

    override fun onAdd(view: View?) {
        PermissionUtil.requestPermissions(
            this, this,
            Permission.WRITE_EXTERNAL_STORAGE, Permission.CAMERA
        )
    }
    override fun onItemClick(view: View?, position: Int) {
    }
    override fun onReachTheMaximum() {
    }
}