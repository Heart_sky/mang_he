package com.android.manghe.mine.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.SignWithdrawalActivity;
import com.android.manghe.mine.model.UploadFileRes;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Call;

public class PSignWithdrawal extends XPresenter<SignWithdrawalActivity> {

    public int memberLevel;
    public float totalMoney;
    private String qrCodeUrl;
    private ArrayList<String> mPicLocalPathList = new ArrayList<>();

    public void setPicList(ArrayList<String> picPath) {
        mPicLocalPathList = picPath;
    }

    public ArrayList<String> getPicList() {
        return mPicLocalPathList;
    }

    public void submit(String name, String account, float money) {
        getV().showLoadingDialog();
        if (mPicLocalPathList.size() == 0) {
            //文字
            withdrawal(name, account, money, null);
        } else {
            //文字+图片
            uploadFile(name, account, money, mPicLocalPathList.get(0));
        }
    }

    private void uploadFile(String name, String account, float money, String path) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBER_UPLOAD)
                .uploadFile("upFile", path)
                .addHead("UID", UserHolder.getUID(getV()))
                .addHead("TOKEN", UserHolder.getUserInfo(getV()).TOKEN)
                .build()
                .execute(new BaseBack<UploadFileRes>() {
                    @Override
                    public void onSuccess(Call call, UploadFileRes res) {
                        super.onSuccess(call, res);
                        if (res.code == 0 && !TextUtils.isEmpty(res.data.toString())) {
                            qrCodeUrl = res.data.toString();
                            withdrawal(name, account, money, qrCodeUrl);
                        }else{
                            qrCodeUrl = "";
                            getV().hideLoadingDialog();
                            ToastUtil.showLong(getV(), "提现失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        qrCodeUrl= "";
                        getV().hideLoadingDialog();
                        ToastUtil.showLong(getV(), "提现失败");
                    }
                });

    }


    public void withdrawal(String name, String account, float money, String qrCodeUrl) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.SignWithdraw, headMap)
                .post("name",name)
                .post("account",account)
                .post("money", money+"")
                .post("qrcode", qrCodeUrl)
                .build().execute(new BaseBack<NetBean>() {

            @Override
            public void onSuccess(Call call, NetBean bean) {
                getV().hideLoadingDialog();
                if (bean.code == 0) {
                    ToastUtil.showLong(getV(), "提现成功，请等待后台审核");
                    getV().finish();
                }else{
                    ToastUtil.showLong(getV(), bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().hideLoadingDialog();
                ToastUtil.showLong(getV(), "提现失败");
            }
        });
    }
}
