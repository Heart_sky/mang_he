package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.ScoreDetailList;

import java.util.List;

public class ScoreDetailAdapter extends BaseRecyclerViewAdapter<ScoreDetailList.ListBean, ScoreDetailAdapter.ScoreDetialHolder> {
    private Context mContext;
    private int mListType;

    public ScoreDetailAdapter(Context context, List<ScoreDetailList.ListBean> list, int listType) {
        super(list);
        mContext = context;
        mListType = listType;
    }

    public void update(List<ScoreDetailList.ListBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(ScoreDetialHolder holder, ScoreDetailList.ListBean item) {
        holder.setText(R.id.tvTitle, item.remark);
        holder.setText(R.id.tvTime, item.c_time);
        if(mListType == 1){
            holder.setText(R.id.tvCoin, "+"+item.amount);
        }else if(mListType == 2){
            holder.setText(R.id.tvCoin, item.amount);
        }
    }

    @Override
    public ScoreDetialHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new ScoreDetialHolder(inflateItemView(viewGroup, R.layout.item_coin_detail));
    }

    public class ScoreDetialHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public ScoreDetialHolder(View itemView) {
            super(itemView);
        }
    }
}
