package com.android.manghe.mine.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.MediaUtil
import com.android.base.tools.ToastUtil
import com.android.base.tools.permission.OnPermissionResultListener
import com.android.base.tools.permission.PermissionUtil
import com.android.manghe.R
import com.android.manghe.mine.presenter.PFeedback
import com.android.manghe.view.crop.ImageCropHelper
import com.android.manghe.view.multiselectorview.MultiImageSelectorView
import com.isseiaoki.simplecropview.CropImageView
import com.yanzhenjie.permission.runtime.Permission
import kotlinx.android.synthetic.main.activity_feed_back.*
import me.nereo.multi_image_selector.MultiImageSelector
import me.nereo.multi_image_selector.MultiImageSelectorActivity


class FeedbackActivity : MVPActivity<PFeedback>(), OnPermissionResultListener, MultiImageSelectorView.OnItemClickListener {


    private val CODE_IMAGE_SELECT = 100
    private val CODE_IMAGE_CROP = 101

    override fun getLayoutId(): Int {
        return R.layout.activity_feed_back
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("投诉建议").setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black).setRightText("反馈记录"){
            open(FeedbackListActivity::class.java)
        }
        multiImageSelectorView.setShowAddImageOnMaximum(false)
        initListener()
    }

    override fun onResume() {
        super.onResume()
        p.getFeedBackMsgCount()
    }

    private fun initListener() {
        multiImageSelectorView.setOnItemClickListener(this)
        layoutPicTip.setOnClickListener {
            //添加第一个图片
            PermissionUtil.requestPermissions(
                this, this,
                Permission.WRITE_EXTERNAL_STORAGE, Permission.CAMERA
            )
        }
        btnSubmit.setOnClickListener {
            val content = etContent.text.toString().trim()
            if(TextUtils.isEmpty(content)){
                ToastUtil.showShort(this@FeedbackActivity, "请输入投诉或建议内容")
                return@setOnClickListener
            }
            val tel = etTel.text.toString().trim()
            if(TextUtils.isEmpty(tel)){
                ToastUtil.showShort(this@FeedbackActivity, "请输入联系电话")
                return@setOnClickListener
            }
            showLoadingDialog()
            p.submit(content,tel)
        }
    }

    fun submitOk(text: String?){
        ToastUtil.showShort(this, text)
        finish()
    }
    fun submitFail(text : String?){
        ToastUtil.showShort(this, if(TextUtils.isEmpty(text)) "提交失败" else text)
    }

    override fun onGranted(permissions: MutableList<String>?) {
        MultiImageSelector.create()
            .showCamera(true) // show camera or not. true by default
            .count(3) // max select image size, 9 by default. used width #.multi()
            .single() // multi mode, default mode;
            .origin(p.picList) // original select data set, used width #.multi()
            .start(this, CODE_IMAGE_SELECT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == CODE_IMAGE_SELECT){
                val arrayList = data?.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT)
                if (arrayList != null && !arrayList.isEmpty()) {
                    ImageCropHelper.startCropFree(this, arrayList.get(0),
                        ImageCropHelper.ImageCropParamsBuilder
                        .create()
                        .compressQuality(90)
                        .cropMode(CropImageView.CropMode.FREE)
                        .outputMaxSize(1280, 1280)
                        .build(), CODE_IMAGE_CROP)
                }

            }else if(requestCode == CODE_IMAGE_CROP){
                val resultUri = ImageCropHelper.onActivityResult(resultCode, data)
                if (resultUri != null) {
                    val imagePath = MediaUtil.getPath(this, resultUri)
                    p.picList.add(imagePath)
                    layoutPicTip.visibility = View.GONE
                    multiImageSelectorView.visibility = View.VISIBLE
                    multiImageSelectorView.setList(p.picList)
                }
            }
        }
    }



    override fun onDelete(view: View?, position: Int) {
        if (position >= 0 && position < p.picList.size) {
            p.picList.removeAt(position)
            multiImageSelectorView.setList(p.picList)
        }
    }

    override fun onAdd(view: View?) {
        PermissionUtil.requestPermissions(
            this, this,
            Permission.WRITE_EXTERNAL_STORAGE, Permission.CAMERA
        )
    }
    override fun onItemClick(view: View?, position: Int) {
    }
    override fun onReachTheMaximum() {
    }

    fun setFeedbackDot(isShowDot : Boolean){
        titleBar.setRightTextIcon("反馈记录", if(isShowDot) R.mipmap.dot else R.mipmap.tran)
    }
}