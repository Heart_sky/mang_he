package com.android.manghe.mine.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.mine.model.OrderDetailModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/02 14:01
 * desc   :
 */
public class OrderDetailAdapter extends BaseQuickAdapter<OrderDetailModel.DataBean, BaseViewHolder> {


    private Context mContext;

    public OrderDetailAdapter(Context context, @Nullable List<OrderDetailModel.DataBean> data) {
        super(R.layout.item_order_detail_good, data);
        this.mContext = context;
    }

    public void updateList(List<OrderDetailModel.DataBean> list){
        replaceData(list);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrderDetailModel.DataBean item) {
        GlideHelper.loadRoundTrans(mContext, item.goods_thumb, helper.getView(R.id.ivPic), 6);
        helper.setText(R.id.tvTitle, item.goods_name);
        helper.setText(R.id.tvAmount, item.buy_num);
        helper.setText(R.id.tvSellPrice, item.score_price);
    }


}
