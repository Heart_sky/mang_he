package com.android.manghe.mine.presenter;

import android.content.Context;
import android.content.Intent;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.common.webview.ERefreshWebType;
import com.android.manghe.common.webview.WebViewActivity;
import com.android.manghe.common.webview.WebViewParameter;
import com.android.manghe.config.Config;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshMemberLevelEvent;
import com.android.manghe.mine.fragment.MineFragment;
import com.android.manghe.mine.model.FeedbackInfoRes;
import com.android.manghe.mine.model.InviteStatisticsRes;
import com.android.manghe.mine.model.MemberIndexRes;
import com.android.manghe.mine.model.MsgCountRes;
import com.android.manghe.mine.model.UserDetailRes;
import com.android.manghe.mine.model.UserRes;
import com.eightbitlab.rxbus.Bus;

import java.util.HashMap;

import okhttp3.Call;

public class PMine extends XPresenter<MineFragment> {

    public UserDetailRes.UserDetail userDetail;
    public MemberIndexRes.DataBean.MemberBean memberBean;

    public void getUserDetail(String userId, String token) {
        try {
            HashMap<String, String> data = new HashMap<>();
            data.put("UID", userId);
            data.put("TOKEN", token);
            new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERORDINGDANGNUM, data)
                    .get()
                    .execute(new BaseBack<UserDetailRes>() {

                        @Override
                        public void onSuccess(Call call, UserDetailRes userDetailRes) {
                            if (userDetailRes != null && userDetailRes.data != null) {
                                userDetail = userDetailRes.data;
                                getV().updateUserDetail(userDetail);
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            super.onFailure(e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    public void getMsgCount(String userId, String token) {
        try {
            HashMap<String, String> data = new HashMap<>();
            data.put("UID", userId);
            data.put("TOKEN", token);
            new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERMESSAGENUM, data)
                    .get()
                    .execute(new BaseBack<MsgCountRes>() {

                        @Override
                        public void onSuccess(Call call, MsgCountRes msgCountRes) {
                            if (msgCountRes.code == 0 && msgCountRes.data != null) {
                                getV().updateMsgDot((msgCountRes.data.message_unread_num + msgCountRes.data.notice_unread_num) > 0);
                            } else {
                                getV().updateMsgDot(false);
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            super.onFailure(e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    public void getFeedBackMsgCount(String userId, String token) {
        try {
            HashMap<String, String> data = new HashMap<>();
            data.put("page", "1");
            HashMap<String, String> headMap = new HashMap<>();
            headMap.put("UID", userId);
            headMap.put("TOKEN", token);
            new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.FEED_BACK_LIST, data, headMap)
                    .get()
                    .execute(new BaseBack<FeedbackInfoRes>() {

                        @Override
                        public void onSuccess(Call call, FeedbackInfoRes res) {
                            if (res != null && res.code == 0) {
                                getV().setFeedbackDot(res.unread_count > 0);
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            super.onFailure(e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }


    /**
     * 获取会员
     *
     * @param userId
     * @param token
     */
    public void getMemberInfo(String userId, String token) {
        HashMap<String, String> data = new HashMap<>();
        data.put("UID", userId);
        data.put("TOKEN", token);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERINDEX, data)
                .get()
                .execute(new BaseBack<MemberIndexRes>() {

                    @Override
                    public void onSuccess(Call call, MemberIndexRes res) {
                        if (res.code == 0 && res.data != null) {
                            memberBean = res.data.member;
                            //  getV().updateMemberInfo(memberBean);
                            Bus.INSTANCE.send(new RefreshMemberLevelEvent(memberBean.level));
                        }
                    }
                });
    }

    public void getInviteStatistics(String userId, String token) {
        try {
            HashMap<String, String> data = new HashMap<>();
            data.put("UID", userId);
            data.put("TOKEN", token);
            new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBER_INVITE_STATISTICS, data)
                    .get()
                    .execute(new BaseBack<InviteStatisticsRes>() {

                        @Override
                        public void onSuccess(Call call, InviteStatisticsRes res) {
                            if (res.code == 0 && res.data != null) {
                                getV().updateInviteStatistics(res.data);
                            }
                        }
                    });
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }


    public void set53(Context context, UserRes.UserInfo userInfo) {
        String url = Config.Box_53_URL + Config.Box_53_DEVEICE
                + String.format(Config.BOX_53_ID, userInfo.UID)
                + String.format(Config.BOX_53_NAME, userInfo.UID);
        WebViewParameter parameters = new WebViewParameter.WebParameterBuilder()
                .setUrl(url)
                .setShowProgress(true)
                .setCanHistoryGoBackOrForward(true)
                .setReloadable(true)
                .setShowWebPageTitle(true)
                .setReloadType(ERefreshWebType.ClickRefresh)
                .build();
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra("parameters", parameters);
        context.startActivity(intent);
    }
}
