package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeedbackInfoRes extends NetBean<FeedbackInfoRes.DataBean> {

    public int unread_count;
    public List<FeedbackInfoRes.ListBean> list;

    public static class DataBean {

    }

    public static class ListBean {
        @SerializedName("id")
        public String id;
        @SerializedName("status")
        public String status;
        @SerializedName("userid")
        public String userid;
        @SerializedName("username")
        public String username;
        @SerializedName("url")
        public String url;
        @SerializedName("listorder")
        public String listorder;
        @SerializedName("createtime")
        public String createtime;
        @SerializedName("updatetime")
        public String updatetime;
        @SerializedName("lang")
        public String lang;
        @SerializedName("content")
        public String content;
        @SerializedName("ip")
        public String ip;
        @SerializedName("mid")
        public String mid;
        @SerializedName("mobile")
        public String mobile;
        @SerializedName("imgone")
        public String imgone;
        @SerializedName("imgtwo")
        public String imgtwo;
        @SerializedName("imgthree")
        public String imgthree;
        @SerializedName("replycontent")
        public String replycontent;
        @SerializedName("reply_time")
        public String replyTime;
        @SerializedName("is_read")
        public boolean isRead;
    }
}
