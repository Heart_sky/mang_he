package com.android.manghe.mine.activity

import android.os.Bundle
import android.text.TextUtils
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import com.android.manghe.mine.presenter.PModifyPsw
import kotlinx.android.synthetic.main.activity_modify_psw.*

class ModifyPswActivity :MVPActivity<PModifyPsw>() {

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("重置密码").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)
        initListener()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_modify_psw
    }


    private fun initListener(){
        btnSave.setOnClickListener {
            val oldPsw = etOldPsw.text.toString()
            val newPsw = etNewPsw.text.toString()
            val againPsw = etNewPswAgain.text.toString()
            if(oldPsw.isEmpty()){
                ToastUtil.showShort(this@ModifyPswActivity, "请输入旧密码")
                return@setOnClickListener
            }
            if(!newPsw.equals(againPsw)){
                ToastUtil.showShort(this@ModifyPswActivity, "两次输入新密码不一致")
                return@setOnClickListener
            }
            showLoadingDialog()
            p.modify(oldPsw, newPsw)
        }
    }
    fun closeLoadingDialog() {
        hideLoadingDialog()
    }

    fun modifyOk() {
        ToastUtil.showShort(this, "修改成功")
        finish()
    }

    fun modifyFail(text: String?) {
        ToastUtil.showShort(this, if (TextUtils.isEmpty(text)) "修改失败" else text)
    }
}