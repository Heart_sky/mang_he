package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.common.model.SimpleResult;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.FeedbackListActivity;
import com.android.manghe.mine.model.FeedbackInfoRes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PFeedbackList extends XPresenter<FeedbackListActivity> {
    private int mCurrentPage = 1;
    public final List<FeedbackInfoRes.ListBean> mDataList = new ArrayList<>();

    public void getData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mDataList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.FEED_BACK_LIST, data, headMap)
                .get()
                .execute(new BaseBack<FeedbackInfoRes>() {

                    @Override
                    public void onSuccess(Call call, FeedbackInfoRes res) {
                        if (res != null && res.code == 0 && res.list != null) {
                            mDataList.addAll(res.list);
                            getV().setCanLoadMore(res.list.size() == 10);
                            getV().update();
                            if (mDataList.size() == 10) {
                                mCurrentPage++;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void readAll(){
        HashMap<String, String> data = new HashMap<>();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.READ_ALL_FEED_BACK, data, headMap)
                .get()
                .execute(new BaseBack<SimpleResult>() {

                    @Override
                    public void onSuccess(Call call, SimpleResult res) {

                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
