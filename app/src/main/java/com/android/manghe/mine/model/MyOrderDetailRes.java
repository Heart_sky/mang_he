package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

/**
 * Created by lnest-php on 2017/9/13.
 */

public class MyOrderDetailRes extends NetBean<MyOrderDetailRes.DataBean> {

    public static class DataBean {
        /**
         * id : 542
         * mid : 106171
         * order_sn : 2019061314434310053495
         * status_common : 0
         * extension_code : 0
         * status_lottery : 0
         * status_pay : 0
         * status_shipping : 0
         * status_order : 0
         * c_time : 1560408223
         * pay_time : 0
         * order_amount : 2.00
         * goods_amount : 2.00
         * shipping_fee : 0.00
         * pay_fee : 0.00
         * amount : 2.00
         * pay_id : 61
         * pay_name : 微信支付（推荐）
         * address_id : 41493
         * address : 就是记得记得接
         * area : 福建  南平
         * mobile : 18158289898
         * express : 10
         * express_num :
         * name : 栗经伟
         * ship_time : 0
         * score_amount : 10
         * is_rate : 0
         * coupon_favourable_price : 0.00
         * order_tip : 新年匠心独具
         * express_name : 顺丰速递
         * goods : [{"id":"542","mid":"106171","order_id":"542","good_id":"114","goods_name":"测试","goods_spec":"","spec":"","buy_num":"1","cost_price":"10.00","sell_price":"2.00","score_price":"10","c_time":"1560408223","type":"1","obj_id":"0","goods_info":null,"extension_id":"0","share_id":"0","verify_code_id":"0","team_num":"0","express_id":null,"goods_id":"114","goods_market_price":"10.00","goods_discount_type":"0","goods_discount_amount":"0.00","goods_sid":"0","thumb":"[{\"path\":\"\\/upload\\/1\\/images\\/gallery\\/9\\/e\\/13299_src.jpg\",\"title\":\"\"}]","thumbs":"[{\"path\":\"\\/upload\\/1\\/images\\/gallery\\/9\\/e\\/13299_src.jpg\",\"title\":\"\"}]","img_src":"https://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg","img_cover":"https://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg"}]
         * status_name : 未付款,未发货
         * order_code : 商城
         * status_id : 100
         * m_mid : 106171
         * m_username : 18158289899
         * m_mobile : 18158289899
         * m_status : 1
         * express_id : 10
         * express_pinyin : shunfeng
         * express_status : 1
         * express_free : 1
         */

        public String id;
        public String mid;
        public String order_sn;
        public String status_common;
        public String extension_code;
        public String status_lottery;
        public String status_pay;
        public String status_shipping;
        public String status_order;
        public String c_time;
        public String pay_time;
        public String order_amount;
        public String goods_amount;
        public String shipping_fee;
        public String pay_fee;
        public String amount;
        public String pay_id;
        public String pay_name;
        public String address_id;
        public String address;
        public String area;
        public String mobile;
        public String express;
        public String express_num;
        public String name;
        public String ship_time;
        public String score_amount;
        public String is_rate;
        public String coupon_favourable_price;
        public String order_tip;
        public String express_name;
        public String status_name;
        public String order_code;
        public int status_id;
        public String m_mid;
        public String m_username;
        public String m_mobile;
        public String m_status;
        public String express_id;
        public String express_pinyin;
        public String express_status;
        public String express_free;
        public List<GoodsBean> goods;

        public static class GoodsBean {
            /**
             * id : 542
             * mid : 106171
             * order_id : 542
             * good_id : 114
             * goods_name : 测试
             * goods_spec :
             * spec :
             * buy_num : 1
             * cost_price : 10.00
             * sell_price : 2.00
             * score_price : 10
             * c_time : 1560408223
             * type : 1
             * obj_id : 0
             * goods_info : null
             * extension_id : 0
             * share_id : 0
             * verify_code_id : 0
             * team_num : 0
             * express_id : null
             * goods_id : 114
             * goods_market_price : 10.00
             * goods_discount_type : 0
             * goods_discount_amount : 0.00
             * goods_sid : 0
             * thumb : [{"path":"\/upload\/1\/images\/gallery\/9\/e\/13299_src.jpg","title":""}]
             * thumbs : [{"path":"\/upload\/1\/images\/gallery\/9\/e\/13299_src.jpg","title":""}]
             * img_src : https://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg
             * img_cover : https://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg
             */

            public String id;
            public String mid;
            public String order_id;
            public String good_id;
            public String goods_name;
            public String goods_spec;
            public String spec;
            public String buy_num;
            public String cost_price;
            public String sell_price;
            public String score_price;
            public String c_time;
            public String type;
            public String obj_id;
            public Object goods_info;
            public String extension_id;
            public String share_id;
            public String verify_code_id;
            public String team_num;
            public Object express_id;
            public String goods_id;
            public String goods_market_price;
            public String goods_discount_type;
            public String goods_discount_amount;
            public String goods_sid;
            public String thumb;
            public String thumbs;
            public String img_src;
            public String img_cover;
        }
    }
}
