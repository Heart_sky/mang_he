package com.android.manghe.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.title.ETitleType;
import com.android.base.frame.view.MVPFragment;
import com.android.base.tools.ToastUtil;
import com.android.manghe.R;
import com.android.manghe.box.model.MessageEvent;
import com.android.manghe.config.Config;
import com.android.manghe.config.events.RefreshMemberInfoEvent;
import com.android.manghe.mine.adapter.AllStateOrderAdapter;
import com.android.manghe.mine.model.AllStateOrderModel;
import com.android.manghe.mine.presenter.PTakeOver;
import com.android.manghe.view.dialog.NewTipDialog;
import com.eightbitlab.rxbus.Bus;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import static com.android.base.frame.title.ETitleType.NO_TITLE;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/01 14:56
 * desc   : 待收货页面
 */
public class TakeOverFragment extends MVPFragment<PTakeOver> implements IRefresh {


    private RecyclerView mRecyclerView;
    private AllStateOrderAdapter mAdapter;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_take_over;
    }

    @Override
    protected ETitleType showToolBarType() {
        return NO_TITLE;
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return mView.findViewById(R.id.refreshLayout);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().register(this);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        initRecyclerView();
        showLoadingDialog();
        getP().getAllOrderList(true);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new AllStateOrderAdapter(getContext(), new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setListener(new AllStateOrderAdapter.IOrderListener() {
            @Override
            public void onTakeOver(AllStateOrderModel.DataBean.ListBean item) {
                NewTipDialog newTipDialog = new NewTipDialog(getContext(), "是否确定收货吗？");
                newTipDialog.show();
                newTipDialog.setListener(new NewTipDialog.ITipDialogListener() {
                    @Override
                    public void clickLeft() {

                    }

                    @Override
                    public void clickRight() {
                        showLoadingDialog();
                        getP().ConfirmReceive(item
                                .order_sn);

                    }
                });
            }
        });
    }

    @Override
    public void onRefresh() {
        getP().getAllOrderList(true);
    }

    @Override
    public void onLoad() {
        getP().getAllOrderList(false);
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState, View parent) {

    }


    public void updateList(List<AllStateOrderModel.DataBean.ListBean> listBeans) {
        mAdapter.replaceData(listBeans);

    }

    public void receiveOk() {
        EventBus.getDefault().post(new MessageEvent(Config.EVENT_BUS_GET_ALL_ORDER));
    }

    public void showErrorTip() {
        ToastUtil.showLong(mContext, "获取订单失败，请重试");
    }

    public void showConfirmReceiveErrorTip() {
        ToastUtil.showLong(mContext, "确认收货失败，请重试");
    }


    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Bus.INSTANCE.send(new RefreshMemberInfoEvent());
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getEventType() == Config.EVENT_BUS_GET_ALL_ORDER) {
            onRefresh();
        }
    }

}
