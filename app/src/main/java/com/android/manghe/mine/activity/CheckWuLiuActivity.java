package com.android.manghe.mine.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.mine.adapter.WuLiuAdapter;
import com.android.manghe.mine.model.AllStateOrderModel;
import com.android.manghe.mine.model.FreightInfoRes;
import com.android.manghe.mine.presenter.PCheckWuLiu;
import com.android.manghe.view.dialog.NewTipDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/10 15:04
 * desc   :
 */
public class CheckWuLiuActivity extends MVPActivity<PCheckWuLiu> {


    private RecyclerView mRecyclerView;
    private AllStateOrderModel.DataBean.ListBean mCurrentAllStateOrderModel;
    private boolean mIsAuction;
    private ImageView mImgSrc;
    private TextView mTvTitle, mTvPrice, mTvCount, mTvExpressName, mTvExpressNum;
    private WuLiuAdapter mAdapter;


    @Override
    public int getLayoutId() {
        return R.layout.activity_check_wu_liu;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("物流信息").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine()
                .setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mRecyclerView = findViewById(R.id.id_recycler_view);
        mImgSrc = findViewById(R.id.ivPic);
        mTvTitle = findViewById(R.id.tvTitle);
        mTvPrice = findViewById(R.id.id_tv_price);
        mTvCount = findViewById(R.id.id_tv_count);
        mTvExpressName = findViewById(R.id.wuliu_kuaidiname);
        mTvExpressNum = findViewById(R.id.wuliu_kuaidinumber);

        mCurrentAllStateOrderModel = (AllStateOrderModel.DataBean.ListBean) getIntent().getSerializableExtra("AllStateOrderModel");
        mIsAuction = getIntent().getBooleanExtra("isAuction", false);

        if (mCurrentAllStateOrderModel != null) {
            GlideHelper.loadRoundTrans(mContext, mCurrentAllStateOrderModel.goods_thumb, mImgSrc, 6);
            mTvTitle.setText(mCurrentAllStateOrderModel.goods_name);
            mTvPrice.setText(mCurrentAllStateOrderModel.sell_price);
            mTvCount.setText("x" + mCurrentAllStateOrderModel.buy_num);
        }
        initRecyclerView();


        showLoadingDialog();
        getP().loadData(mIsAuction, mCurrentAllStateOrderModel.order_sn);
    }

    public void onCopy(View view) {

    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new WuLiuAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
    }


    public void update(List<FreightInfoRes.LogisticBean> list) {
        mAdapter.replaceData(list);
    }

    public void showTopMsg(String kuaidiName, String num, String phone) {
        mTvExpressName.setText(kuaidiName);
        mTvExpressNum.setText(num);
    }

    public void noFreightMsg() {
        NewTipDialog mDialog = new NewTipDialog(this, "暂无物流信息");
        mDialog.show();
        mDialog.setSureMode();
        mDialog.setListener(new NewTipDialog.ITipDialogListener() {
            @Override
            public void clickLeft() {

            }

            @Override
            public void clickRight() {
                finish();
            }
        });

    }

}
