package com.android.manghe.mine.activity

import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.view.MVPActivity
import com.android.base.frame.view.XStateController
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.common.model.BaseBack
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.mine.adapter.CommissionDetailAdapter
import com.android.manghe.mine.model.FansInCommissionRes
import com.android.manghe.mine.presenter.PCommissionDetail
import com.android.manghe.view.VerticalDividerItemDecoration
import com.githang.statusbar.StatusBarCompat
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_commission_detail.*
import okhttp3.Call
import java.util.*

class CommissionDetailActivity: MVPActivity<PCommissionDetail>(),IRefresh, IStateController<XStateController> {
    override fun getStateView(): XStateController {
        return findViewById(R.id.xStateController)
    }
    private var mCurrentPage = 1
    private var mAuctionList = arrayListOf<FansInCommissionRes.DataBean.ListBean>()
    private var mAdapter: CommissionDetailAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_commission_detail
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar!!.setToolbar("我的佣金", R.color.white)
            .setLeftIcon(R.mipmap.icon_back_white)
            .hideBottomLine()
            .setBackgroundColor(Color.parseColor("#5FC88C"))
        StatusBarCompat.setStatusBarColor(this, Color.parseColor("#5FC88C"))
        tvTotal.text = intent.getStringExtra("totalCommission")
        mAdapter = CommissionDetailAdapter(arrayListOf<FansInCommissionRes.DataBean.ListBean>())
        recyclerView.layoutManager =
            LinearLayoutManager(this)
        recyclerView.adapter = mAdapter
        recyclerView.addItemDecoration(VerticalDividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        loadData(true)
    }

    private fun loadData(isRefresh: Boolean) {
        if (isRefresh) {
            mCurrentPage = 1
            mAuctionList.clear()
        }
        val data = HashMap<String, String>()
        data["page"] = mCurrentPage.toString() + ""
        data["size"] = "10"
        val headMap = HashMap<String, String>()
        headMap["UID"] = UserHolder.getUID(this)
        headMap["TOKEN"] = UserHolder.getUserInfo(this)!!.TOKEN
        OKHttpUtil(this).urlByHeadData(
            ConstantsUrl.domain + ConstantsUrl.GetInviteCommissionLog,
            data,
            headMap
        )
            .get()
            .execute(object : BaseBack<FansInCommissionRes>() {

                override fun onSuccess(call: Call, res: FansInCommissionRes?) {
                    if (res != null && res.code == 0 && res.data != null) {
                        mAuctionList.addAll(res.data.list)
                        refreshLayout.isEnableLoadMore = res.data.list.size == 10
                        mAdapter?.update(mAuctionList)

                        if (mAuctionList.isEmpty()) {
                            xStateController.showEmpty()
                        } else {
                            xStateController.showContent()
                        }

                        if (mAuctionList.size == 10) {
                            mCurrentPage++
                        }
                    }
                }
            })
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        loadData(true)
    }

    override fun onLoad() {
        loadData(false)
    }
}
