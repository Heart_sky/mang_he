package com.android.manghe.mine.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.frame.view.XStateController
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.mine.adapter.FansAdapter
import com.android.manghe.mine.model.FansRes
import com.android.manghe.mine.presenter.POneLevelFans
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_level_fans.*

class OneLevelFansFragment : MVPFragment<POneLevelFans>() , IRefresh , IStateController<XStateController> {
    override fun getStateView(): XStateController {
        return mView.findViewById(R.id.xStateController)
    }
    private var mAdapter: FansAdapter? = null
    override fun showToolBarType(): ETitleType {
        return ETitleType.OVERLAP_TITLE
    }
    override fun getLayoutId(): Int {
        return R.layout.fragment_level_fans
    }
    override fun initData(savedInstanceState: Bundle?, parent: View?) {
        mAdapter = FansAdapter(activity, arrayListOf())
        val linearManager =
            LinearLayoutManager(activity)
        recyclerView.layoutManager = linearManager
        recyclerView.addItemDecoration(RecyclerViewDivider(mContext, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = mAdapter
        mAdapter!!.setOnItemClickListener{
                _,position->

        }
        p.setLevel("1")
        p.getData(true)
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getData(true)
    }

    override fun onLoad() {
        p.getData(false)
    }

    fun update(fansList: List<FansRes.DataBean.ListBean>){
        mAdapter?.let {
            it.update(fansList)

            if (fansList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }

}