package com.android.manghe.mine.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.frame.view.XStateController
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.mine.adapter.MsgAdapter
import com.android.manghe.mine.model.MessageIndexData
import com.android.manghe.mine.presenter.PMsg
import com.android.manghe.view.dialog.TipDialog
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_level_fans.recyclerView
import kotlinx.android.synthetic.main.fragment_level_fans.refreshLayout
import kotlinx.android.synthetic.main.fragment_msg.*
import kotlinx.android.synthetic.main.no_data.*

class MsgFragment : MVPFragment<PMsg>(), IRefresh, IStateController<XStateController> {


    override fun getStateView(): XStateController {
        return mView.findViewById(R.id.xStateController)
    }

    private var mAdapter: MsgAdapter? = null
    override fun showToolBarType(): ETitleType {
        return ETitleType.OVERLAP_TITLE
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_msg
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = MsgAdapter(activity, arrayListOf())
        val linearManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = linearManager
        recyclerView.addItemDecoration(RecyclerViewDivider(mContext, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = mAdapter
        mAdapter!!.setOnItemClickListener { _, item ->
            TipDialog(activity!!, item.message_title, item.message_content).show()
            p.setRead(item.message_id)
        }
        tvNoData.text = "暂无消息"

        p.getData(true)
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getData(true)
    }

    override fun onLoad() {
        p.getData(false)
    }

    fun update(msgList: List<MessageIndexData.ListBean>) {
        mAdapter?.let {
            it.update(msgList)
            if (msgList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }

}