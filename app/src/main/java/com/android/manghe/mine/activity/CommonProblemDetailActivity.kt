package com.android.manghe.mine.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.mine.adapter.CustomerListAdapter
import com.android.manghe.mine.model.CustomerContentLevel
import com.android.manghe.mine.model.CustomerRes
import com.android.manghe.mine.model.CustomerTitleLevel
import com.android.manghe.mine.presenter.PCommonProblemDetail
import com.chad.library.adapter.base.entity.MultiItemEntity
import kotlinx.android.synthetic.main.activity_common_problem_detail.*

class CommonProblemDetailActivity  : MVPActivity<PCommonProblemDetail>(){

    var adapter : CustomerListAdapter? = null
    var data: CustomerRes.CatlistBean? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_common_problem_detail
    }
    override fun initData(savedInstanceState: Bundle?) {
        data = intent.getSerializableExtra("problems") as CustomerRes.CatlistBean
        data?.let {
            titleBar.setToolbar(it.catname).setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black)
            val itemEntityList = java.util.ArrayList<MultiItemEntity>()
            for (data in it.list) {
                val titleLevel = CustomerTitleLevel(data.title)
                titleLevel.addSubItem(CustomerContentLevel(data.content))
                itemEntityList.add(titleLevel)
            }
            adapter = CustomerListAdapter(itemEntityList)
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = adapter
        }
    }
}