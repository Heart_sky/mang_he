package com.android.manghe.mine.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.SPUtil
import com.android.manghe.R
import com.android.manghe.common.webview.ERefreshWebType
import com.android.manghe.common.webview.WebViewActivity
import com.android.manghe.common.webview.WebViewParameter
import kotlinx.android.synthetic.main.activity_setting_permission.*


class SettingPermissionActivity: MVPActivity<XPresenter<IView>>(),View.OnClickListener {
    var  isGxhOn = false
    var  isCopyOn = false
    override fun getLayoutId(): Int {
        return R.layout.activity_setting_permission
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("隐私设置").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)
        layoutVisitCameraAgreement1.setOnClickListener(this)
        layoutVisitCameraAgreement2.setOnClickListener(this)
        layoutVisitCameraAgreement3.setOnClickListener(this)
        toSetting1.setOnClickListener(this)
        toSetting2.setOnClickListener(this)
        toSetting3.setOnClickListener(this)

        isGxhOn = SPUtil.get(this, "gxh", true) as Boolean
        ivSwitchGxh.setImageResource(if(isGxhOn) R.mipmap.off else R.mipmap.on)
        ivSwitchGxh.setOnClickListener {
            isGxhOn = !isGxhOn
            SPUtil.put(this, "gxh", isGxhOn)
            ivSwitchGxh.setImageResource(if(isGxhOn) R.mipmap.off else R.mipmap.on)
        }

        isCopyOn = SPUtil.get(this, "copy", true) as Boolean
        ivSwitchCopy.setImageResource(if(isCopyOn) R.mipmap.off else R.mipmap.on)
        ivSwitchCopy.setOnClickListener {
            isCopyOn = !isCopyOn
            SPUtil.put(this, "copy", isCopyOn)
            ivSwitchCopy.setImageResource(if(isCopyOn) R.mipmap.off else R.mipmap.on)
        }
    }

    override fun onClick(p0: View?) {
        p0?.let {
            if(it == layoutVisitCameraAgreement1 || it == layoutVisitCameraAgreement2 || it == layoutVisitCameraAgreement3){
                WebViewActivity.showActivity(
                    mContext, WebViewParameter.WebParameterBuilder()
                        .setUrl("https://www.manghe98.com/protocol/privacy.html")
                        .setShowProgress(true)
                        .setCanHistoryGoBackOrForward(true)
                        .setReloadable(true)
                        .setReloadType(ERefreshWebType.ClickRefresh)
                        .setCustomTitle("隐私政策")
                        .build()
                )
            }else if (it == toSetting1 || it == toSetting2|| it == toSetting3){
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                intent.data = Uri.fromParts("package", packageName, null)
                startActivity(intent)
            }
        }
    }


}