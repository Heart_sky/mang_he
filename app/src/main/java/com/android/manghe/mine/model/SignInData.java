package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class SignInData extends NetBean<SignInData.DataBean> {
    public static class DataBean {
        public String score;
        public String date;
        public String continue_day;
    }
}
