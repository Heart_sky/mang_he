package com.android.manghe.mine.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.StatusBarUtil;
import com.android.manghe.R;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.adapter.CoinAndIntegralDetailAdapter;
import com.android.manghe.mine.adapter.CommonPagerAdapter;
import com.android.manghe.mine.model.CoinAndIntegralModel;
import com.android.manghe.mine.presenter.PCoinAndIntegralDetail;
import com.githang.statusbar.StatusBarCompat;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/14 16:10
 * desc   : 币和积分详细明细
 */
public class CoinAndIntegralDetailActivity extends MVPActivity<PCoinAndIntegralDetail> implements IRefresh {


    private TextView mTvShouRu, mTvZhiChu, mTVDefinite;
    private ViewPager mViewpager;
    private View mTvLine1, mTvLine2;
    private List<View> mViews;
    private RecyclerView mRecyclerViewShouRu, mRecyclerViewZhiChu;
    private CoinAndIntegralDetailAdapter mShouRuAdapter, mZhiChuAdapter;
    private CommonPagerAdapter mMyPageAdapter;
    private int mPosition;
    private int mStatus = 1;//默认支出


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        StatusBarCompat.setStatusBarColor(this, ContextCompat.getColor(this, R.color.yellow400));
        StatusBarUtil.darkMode(this, true);
        getTitleBar().setToolbar("收支明细", R.color.white).setLeftIcon(R.mipmap.icon_back_white).hideBottomLine()
                .setBackgroundColor(ContextCompat.getColor(this, R.color.yellow400));

        mTvShouRu = findViewById(R.id.id_tv_shouru);
        mTvZhiChu = findViewById(R.id.id_tv_zhichu);
        mTvLine1 = findViewById(R.id.id_line1);
        mTvLine2 = findViewById(R.id.id_line2);
        mViewpager = findViewById(R.id.id_view_pager);
        mTVDefinite = findViewById(R.id.id_tv_definite);

        mPosition = getIntent().getIntExtra("position", 1);
        mTVDefinite.setText(getIntent().getStringExtra("definite"));
        initPager();
        setListener();

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_coin_and_integral_detail;
    }

    private void initPager() {

        mViews = new ArrayList<>();
        mRecyclerViewZhiChu = new RecyclerView(this);
        mRecyclerViewZhiChu.setLayoutManager(new LinearLayoutManager(this));
        mZhiChuAdapter = new CoinAndIntegralDetailAdapter(this, new ArrayList<>(), true);
        mRecyclerViewZhiChu.setAdapter(mZhiChuAdapter);

        mRecyclerViewShouRu = new RecyclerView(this);
        mRecyclerViewShouRu.setLayoutManager(new LinearLayoutManager(this));
        mShouRuAdapter = new CoinAndIntegralDetailAdapter(this, new ArrayList<>(), false);
        mRecyclerViewShouRu.setAdapter(mShouRuAdapter);

        mViews.add(mRecyclerViewZhiChu);
        mViews.add(mRecyclerViewShouRu);
        mMyPageAdapter = new CommonPagerAdapter(mViews);
        mViewpager.setAdapter(mMyPageAdapter);

        showLoadingDialog();
        showLine(true);
        getDate(true);
    }

    private void setListener() {

        mTvZhiChu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLine(true);
            }
        });
        mTvShouRu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLine(false);
            }
        });

        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mStatus = 1;
                        showLine(true);
                        getDate(true);
                        break;
                    case 1:
                        mStatus = 2;
                        showLine(false);
                        getDate(true);
                        break;
                    default:
                        break;

                }

            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });
    }


    private void showLine(boolean tag) {
        mViewpager.setCurrentItem(tag ? 0 : 1);
        mTvLine1.setVisibility(tag ? View.VISIBLE : View.GONE);
        mTvLine2.setVisibility(!tag ? View.VISIBLE : View.GONE);
        mTvZhiChu.setTextColor(ContextCompat.getColor(this, tag ? R.color.yellow500 : R.color.colorFont33));
        mTvShouRu.setTextColor(ContextCompat.getColor(this, !tag ? R.color.yellow500 : R.color.colorFont33));

    }


    @Override
    public SmartRefreshLayout getRefreshView() {
        return findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        getDate(true);
    }

    private void getDate(boolean isReFresh) {
        switch (mPosition) {
            case 0:
                //获取开盒币
                getP().setUrl(ConstantsUrl.BOx_COIN_LOG);
                getP().setType(0);
                getP().setStatus(mStatus);
                getP().getCoinList(isReFresh);
                break;
            case 1:
                //获取赠送币
                getP().setUrl(ConstantsUrl.BOx_COIN_LOG);
                getP().setType(1);
                getP().setStatus(mStatus);
                getP().getCoinList(isReFresh);
                break;
            case 2:
                //获取积分
                getP().setUrl(ConstantsUrl.BOx_INTEGRAL_LOG);
                getP().setStatus(mStatus);
                getP().getIntegralList(isReFresh);
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoad() {
        getDate(false);
    }


    public void updateList(List<CoinAndIntegralModel.DataBean.ListBean> listBeans, int code) {
        if (code == 1) {
            //支出
            mZhiChuAdapter.replaceData(listBeans);
        } else if (code == 2) {
            mShouRuAdapter.replaceData(listBeans);
        }

    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }


}
