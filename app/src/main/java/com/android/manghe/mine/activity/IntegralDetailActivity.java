package com.android.manghe.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.market.model.MarketGoodDetailRes;
import com.android.manghe.mine.adapter.IntegralUltraPagerAdapter;
import com.android.manghe.mine.model.IntegralModel;
import com.android.manghe.mine.model.UserRes;
import com.android.manghe.mine.presenter.PIntegralDetail;
import com.android.manghe.user.activity.LoginActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.squareup.picasso.Picasso;
import com.tmall.ultraviewpager.UltraViewPager;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/26 15:20
 * desc   : 积分详情页面
 */
public class IntegralDetailActivity extends MVPActivity<PIntegralDetail> implements IRefresh {

    private UltraViewPager mUltraViewPager;
    private TextView mTvPrice, mTvIntegral, mTvTitle;
    private IntegralModel.DataBean.ListBean mCurrentIntegralModel;
    private String mGoodId;
    private IntegralUltraPagerAdapter mAdapter;
    private LinearLayout mLayoutDetailPics;
    private TextView mTvNoDetail;

    @Override
    public int getLayoutId() {
        return R.layout.activity_integral_detail;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("商品详情").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().
                setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mUltraViewPager = findViewById(R.id.ultraViewPager);
        mTvPrice = findViewById(R.id.tvPrice);
        mTvIntegral = findViewById(R.id.tvIntegral);
        mTvTitle = findViewById(R.id.tvTitle);
        mLayoutDetailPics = findViewById(R.id.layoutDetailPics);
        mTvNoDetail = findViewById(R.id.tvNoDetail);


        mCurrentIntegralModel = (IntegralModel.DataBean.ListBean) getIntent().getSerializableExtra("IntegralModel");
        if (null != mCurrentIntegralModel) {
            mTvPrice.setText("￥" + mCurrentIntegralModel.price);
            mTvIntegral.setText(mCurrentIntegralModel.score);
            mTvTitle.setText(mCurrentIntegralModel.name);
            mGoodId = mCurrentIntegralModel.id;
            getP().loadData(mGoodId);
        }
    }


    public void updateView(MarketGoodDetailRes.DataBean data) {
        mAdapter = new IntegralUltraPagerAdapter(mContext, data.thumbs);
        mUltraViewPager.setAdapter(mAdapter);
        if (data.thumbs.size() > 1) {
            mUltraViewPager.initIndicator()
                    .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                    .setFocusColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                    .setNormalColor(ContextCompat.getColor(mContext, R.color.backgroundGray))
                    .setRadius(DisplayUtil.dip2px(mContext, 6f))
                    .setMargin(0, 0, 0, DisplayUtil.dip2px(mContext, 10f))
                    .setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)
                    .build();
            mUltraViewPager.setInfiniteLoop(true);
            mUltraViewPager.setAutoScroll(5000);
        } else {
            mUltraViewPager.disableIndicator();
            mUltraViewPager.setInfiniteLoop(false);
        }

        showWeb(data.content);

    }

    private void showWeb(String htmlString) {
        if (TextUtils.isEmpty(htmlString)) {
            mTvNoDetail.setVisibility(View.VISIBLE);
            return;
        }
        List<String> picUrlList = getP().htmlConvertPicList(htmlString);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (picUrlList.size() > 0) {
            mLayoutDetailPics.removeAllViews();
            for (String str : picUrlList) {
                ImageView imageView = new ImageView(IntegralDetailActivity.this);
                imageView.setAdjustViewBounds(true);
                imageView.setLayoutParams(lp);
                mLayoutDetailPics.addView(imageView);

                Picasso.get().load(str)
                        .fit()
                        .into(imageView);
                GlideHelper.load(IntegralDetailActivity.this, str, imageView);
            }

        }
    }

    public void tvService(View view) {
        UserRes.UserInfo userInfo = UserHolder.getUserInfo(this);
        if (userInfo == null) {
            open(LoginActivity.class);
        } else {
//            HashMap<String, String> map = new HashMap<>();
//            map.put("mid", userInfo.UID);
//            map.put("nickName", userInfo.nickname);
//            map.put("avatar", userInfo.avatar);
//            MQIntentBuilder mqIntentBuilder = new MQIntentBuilder(this);
//            mqIntentBuilder.setClientInfo(map);
//            startActivity(mqIntentBuilder.build());
            getP().set53(mContext, userInfo);
        }
    }

    public void onClickChange(View view) {
        if (UserHolder.getUserInfo(this) == null) {
            open(LoginActivity.class);
        } else {
            Intent intent = new Intent(this, DeliverIntegralActivity.class);
            intent.putExtra("IntegralModel", mCurrentIntegralModel);
            startActivity(intent);
        }
    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        getP().loadData(mGoodId);
    }

    @Override
    public void onLoad() {
    }
}
