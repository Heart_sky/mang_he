package com.android.manghe.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.ToastUtil;
import com.android.manghe.R;
import com.android.manghe.box.model.MessageEvent;
import com.android.manghe.config.Config;
import com.android.manghe.mine.adapter.DeliverIntegralAdapter;
import com.android.manghe.mine.model.AddressInfo;
import com.android.manghe.mine.model.IntegralModel;
import com.android.manghe.mine.presenter.PDeliverIntegral;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/30 15:35
 * desc   : 积分兑换发货页面
 */
public class DeliverIntegralActivity extends MVPActivity<PDeliverIntegral> implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private DeliverIntegralAdapter mAdapter;
    private IntegralModel.DataBean.ListBean mCurrentIntegralModel;

    private TextView mTvTotalScore, mTvName, mTvTelNum, mTvAddress;
    private TextView mBtnQuantityMinus, mTvQuantity, mBtnQuantityPlus;
    private LinearLayout mLayoutAddAddress;
    private RelativeLayout mLayoutEditAddress;
    private int ReqCode_SelectAddress = 1000;
    List<IntegralModel.DataBean.ListBean> mList = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_deliver_integral;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("积分兑换").hideBottomLine()
                .setLeftIcon(R.mipmap.icon_back_black)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mRecyclerView = findViewById(R.id.id_recycler_view);


        mTvTotalScore = findViewById(R.id.tvTotalScore);
        mTvName = findViewById(R.id.id_tv_name);
        mTvTelNum = findViewById(R.id.id_tv_tle_num);
        mTvAddress = findViewById(R.id.id_tv_address);
        mLayoutAddAddress = findViewById(R.id.ly_add_address);
        mLayoutEditAddress = findViewById(R.id.ly_edit_address);
        mBtnQuantityMinus = findViewById(R.id.btnQuantityMinus);
        mTvQuantity = findViewById(R.id.tvQuantity);
        mBtnQuantityPlus = findViewById(R.id.btnQuantityPlus);

        mCurrentIntegralModel = (IntegralModel.DataBean.ListBean) getIntent().getSerializableExtra("IntegralModel");

        if (mCurrentIntegralModel != null) {
            mList.add(mCurrentIntegralModel);
            mTvTotalScore.setText(mCurrentIntegralModel.score);
        }
        mBtnQuantityPlus.setOnClickListener(this);
        mBtnQuantityMinus.setOnClickListener(this);
        initRecyclerView();
        getP().getAddressList();
    }


    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new DeliverIntegralAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);


    }

    public void onClickChooseAddress(View view) {
        Intent intent = new Intent(mContext, AddressListActivity.class);
        intent.putExtra("isToSelect", true);
        startActivityForResult(intent, ReqCode_SelectAddress);
    }

    public void onClickAddAddress(View view) {
        Intent intent = new Intent(mContext, AddressListActivity.class);
        intent.putExtra("isToSelect", true);
        startActivityForResult(intent, ReqCode_SelectAddress);
    }


    public void onClickSureSent(View view) {
        if (getP().currentAddress != null) {
            showLoadingDialog();
            getP().setIntegralSent(mCurrentIntegralModel.id, getP().currentAddress.id, getSelectedQuantity(mTvQuantity));
        } else {
            ToastUtil.showLong(mContext, "请填写收货地址");
        }
    }

    public void showAddress(AddressInfo addressInfo) {
        getP().setCurrentAddress(addressInfo);
        if (getP().currentAddress != null) {
            mLayoutAddAddress.setVisibility(View.GONE);
            mLayoutEditAddress.setVisibility(View.VISIBLE);
            mTvName.setText(getP().currentAddress.name);
            mTvAddress.setText(getP().currentAddress.address);
            mTvTelNum.setText(getP().currentAddress.mobile);
        } else {
            mLayoutAddAddress.setVisibility(View.VISIBLE);
            mLayoutEditAddress.setVisibility(View.GONE);
        }
    }


    public void setGoodsSuccess() {
        ToastUtil.showLong(mContext, "恭喜您兑换成功");
        finish();
        EventBus.getDefault().post(new MessageEvent(Config.EVENT_BUS_GET_ORDER));
        Intent intent = new Intent(mContext, MyOrderActivity.class);
        startActivity(intent);
    }

    public void showExchangeErrorTip() {
        ToastUtil.showLong(mContext, "兑换失败，请重试");
    }

    @Override
    public void onClick(View v) {
        int quantityInt = getSelectedQuantity(mTvQuantity);
        switch (v.getId()) {
            case R.id.btnQuantityMinus:
                if (quantityInt > 1) {
                    String newQuantity = String.valueOf(quantityInt - 1);
                    mTvQuantity.setText(newQuantity);
                    mTvTotalScore.setText(Integer.parseInt(newQuantity) * Integer.parseInt(mCurrentIntegralModel.score) + "");
                }
                break;

            case R.id.btnQuantityPlus:
                //加
                if (quantityInt <= 999) {
                    String newQuantity = String.valueOf(quantityInt + 1);
                    mTvQuantity.setText(newQuantity);
                    mTvTotalScore.setText(Integer.parseInt(newQuantity) * Integer.parseInt(mCurrentIntegralModel.score) + "");
                }
                break;
            default:
                break;
        }
    }

    public int getSelectedQuantity(TextView tvQyt) {
        return Integer.parseInt(tvQyt.getText().toString());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ReqCode_SelectAddress) {
                getP().setCurrentAddress((AddressInfo) data.getSerializableExtra("addressInfo"));
                if (getP().currentAddress != null) {
                    mLayoutAddAddress.setVisibility(View.GONE);
                    mLayoutEditAddress.setVisibility(View.VISIBLE);
                    mTvName.setText(getP().currentAddress.name);
                    mTvAddress.setText(getP().currentAddress.address);
                    mTvTelNum.setText(getP().currentAddress.mobile);
                } else {
                    mLayoutAddAddress.setVisibility(View.VISIBLE);
                    mLayoutEditAddress.setVisibility(View.GONE);
                }
            }
        }
    }


}
