package com.android.manghe.mine.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RelativeLayout
import com.android.base.frame.activity.FragmentActivity
import com.android.base.tools.DisplayUtil
import com.android.base.tools.StatusBarUtil
import com.android.manghe.R
import com.android.manghe.main.view.TabEntity
import com.android.manghe.mine.fragment.*
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_my_order.*

class MyOrderActivity : FragmentActivity() {

    companion object {
        fun showActivity(context: Context) {
            val intent = Intent(context, MyOrderActivity::class.java)
            context.startActivity(intent)
        }
    }

    private val mTabEntities = ArrayList<CustomTabEntity>()

    //private val mTitles = arrayOf("全部", "待付款", "待发货", "待收货", "已完成") //"退款/售后"
    private val mTitles = arrayOf("全部", "待发货", "待收货", "已完成") //"待付款","退款/售后"

    override fun getLayoutId(): Int {
        return R.layout.activity_my_order
    }

    override fun initData(savedInstanceState: Bundle?) {
        val lp = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            DisplayUtil.dip2px(this, 48f)
        )
        lp.topMargin = StatusBarUtil.getStatusBarHeight(this)
        layoutTitleBar.layoutParams = lp

        ivBack.setOnClickListener { finish() }
        addFragment(
            R.id.container,
            AllOrdersFragment::class.java,
            //ToPayOrderFragment::class.java,
            DeliverFragment::class.java,
            TakeOverFragment::class.java,
            FinishFragment::class.java
        )
        for (i in mTitles.indices) {
            mTabEntities.add(TabEntity(mTitles[i], 0, 0))
        }
        tabLayout.setTabData(mTabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabReselect(position: Int) {
            }

            override fun onTabSelect(position: Int) {
                showFragment(position)
            }
        })
        val initPagePosition = intent.getIntExtra("position", 0)
        tabLayout.currentTab = initPagePosition
        showFragment(initPagePosition)
    }
}