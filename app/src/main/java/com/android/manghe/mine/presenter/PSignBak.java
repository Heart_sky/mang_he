package com.android.manghe.mine.presenter;

import android.content.Context;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.SignActivityBak;
import com.android.manghe.mine.model.SignDesignInfo;
import com.android.manghe.mine.model.SignInfoModel;
import com.android.manghe.mine.model.SignModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/20 16:51
 * desc   :
 */
public class PSignBak extends XPresenter<SignActivityBak> {

    public SignInfoModel.DataBean currentSignInfo;
    public int memberLevel;


    /**
     * 获取签到信息
     */
    public void getSignInfo() {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.BOx_SIGN_INFO, headMap)
                .get()
                .execute(new BaseBack<SignInfoModel>() {

                    @Override
                    public void onSuccess(Call call, SignInfoModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0 && res.data != null) {
                            currentSignInfo = res.data;
                            getV().showData(res.data);
                        } else {
                            getV().getSignInfoFail();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().getSignInfoFail();
                    }
                });
    }

    public void sign() {
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.BOx_SIGN, headMap)
                .get()
                .execute(new BaseBack<SignModel>() {

                    @Override
                    public void onSuccess(Call call, SignModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                           getV().showSignOk(res.data);
                        } else {
                            ToastUtil.showShort(getV(), res.msg);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        ToastUtil.showShort(getV(), "签到失败");
                    }

                });
    }


    public List<SignDesignInfo> getSigDesignList(Context context, SignInfoModel.DataBean currentSignInfo) {
        List<SignDesignInfo> list = new ArrayList<>();
        list.add(new SignDesignInfo(R.mipmap.sign_1, "完成1次一发入魂", "今日购买1个商品盒子",
                currentSignInfo.list.today_buy1_award.amount, currentSignInfo.list.today_buy1_award.is_complete));
        list.add(new SignDesignInfo(R.mipmap.sign_2, "完成1次五连不重", "今日购买1个商品盒子",
                currentSignInfo.list.today_buy5_award.amount, currentSignInfo.list.today_buy5_award.is_complete));
        list.add(new SignDesignInfo(R.mipmap.sign_3, "积分置换1次商品", "今日置换一次商品",
                currentSignInfo.list.score_exchange.amount, currentSignInfo.list.score_exchange.is_complete));
        list.add(new SignDesignInfo(R.mipmap.sign_4, String.format(context.getString(R.string.sing_five),
                currentSignInfo.list.full100_award.today_buy_total), "今日购买100个商品盒子",
                currentSignInfo.list.full100_award.amount, currentSignInfo.list.full100_award.is_complete));

        return list;
    }

    public List<SignDesignInfo> getSignDayList(Context context, SignInfoModel.DataBean currentSignInfo) {
        List<SignDesignInfo> list = new ArrayList<>();
        list.add(new SignDesignInfo(currentSignInfo.list.bonus_1.is_open, currentSignInfo.list.bonus_1.value, "第1天"));
        list.add(new SignDesignInfo(currentSignInfo.list.bonus_2.is_open, currentSignInfo.list.bonus_2.value, "第2天"));
        list.add(new SignDesignInfo(currentSignInfo.list.bonus_3.is_open, currentSignInfo.list.bonus_3.value, "第3天"));
        list.add(new SignDesignInfo(currentSignInfo.list.bonus_4.is_open, currentSignInfo.list.bonus_4.value, "第4天"));
        list.add(new SignDesignInfo(currentSignInfo.list.bonus_5.is_open, currentSignInfo.list.bonus_5.value, "第5天"));
        list.add(new SignDesignInfo(currentSignInfo.list.bonus_6.is_open, currentSignInfo.list.bonus_6.value, "第6天"));
        list.add(new SignDesignInfo(currentSignInfo.list.bonus_7.is_open, currentSignInfo.list.bonus_7.value, "第7天"));

        return list;

    }


    /**
     * 领取奖励
     *
     * @param award_type
     */
    public void signTakeBonus(String award_type) {
        HashMap<String, String> data = new HashMap<>();
        data.put("award_type", award_type);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.BOx_SIGN_TAKE_BONUS, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            getV().showSignTakeBoundsOK();
                        } else {
                            getV().showSignFail(award_type,res.msg);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showSignFail(award_type,e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }
}
