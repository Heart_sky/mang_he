package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.CoinDetailRes;

import java.util.List;

public class CoinDetailAdapter extends BaseRecyclerViewAdapter<CoinDetailRes.CoinDetail, CoinDetailAdapter.CoinDetialHolder> {
    private Context mContext;
    private int mListType = 1;

    public CoinDetailAdapter(Context context, List<CoinDetailRes.CoinDetail> list, int listType) {
        super(list);
        mContext = context;
        mListType = listType;
    }

    public void update(List<CoinDetailRes.CoinDetail> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(CoinDetialHolder holder, CoinDetailRes.CoinDetail item) {
        holder.setText(R.id.tvTitle, item.remark);
        holder.setText(R.id.tvTime, item.c_time);
        if(mListType == 1){
            if(item.type.equals("0")){
                holder.setText(R.id.tvCoin, item.amount+"抵用金");
            }else{
                holder.setText(R.id.tvCoin, item.amount+"抵用金");
            }
        }else if(mListType == 2){
            if(item.type.equals("0")){
                holder.setText(R.id.tvCoin, "+"+item.amount+"抵用金");
            }else{
                holder.setText(R.id.tvCoin, "+"+item.amount+"抵用金");
            }
        }

    }

    @Override
    public CoinDetialHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new CoinDetialHolder(inflateItemView(viewGroup, R.layout.item_coin_detail));
    }

    public class CoinDetialHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public CoinDetialHolder(View itemView) {
            super(itemView);
        }
    }
}
