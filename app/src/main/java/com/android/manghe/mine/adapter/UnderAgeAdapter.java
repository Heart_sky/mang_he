package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.mine.model.UnderAgeModel;
import com.android.manghe.view.GridAuctionSpacesItemDecoration;
import com.android.manghe.view.preview.PreviewViewPagerActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/29 14:15
 * desc   :
 */
public class UnderAgeAdapter extends BaseQuickAdapter<UnderAgeModel.DataBean.ListBean, BaseViewHolder> {


    private Context mContext;
    private View emptyView;

    public UnderAgeAdapter(Context context, @Nullable List<UnderAgeModel.DataBean.ListBean> data) {
        super(R.layout.item_under_age, data);
        this.mContext = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    @Override
    protected void convert(BaseViewHolder helper, UnderAgeModel.DataBean.ListBean item) {

        helper.setGone(R.id.layoutGuanFang, item.reply_content != null);
        helper.setGone(R.id.divider, item.reply_content != null);

        helper.setText(R.id.tvReplyTime, item.reply_time != null ? item.reply_time : "");
        helper.setText(R.id.tvReplyContent, item.reply_content != null ? item.reply_content : "");

        helper.setText(R.id.tvCreateTime, item.c_time);
        helper.setText(R.id.tvContent, item.content);


        RecyclerView mRecyclerView = helper.itemView.findViewById(R.id.id_recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(mContext, 4);
        mRecyclerView.addItemDecoration(new GridAuctionSpacesItemDecoration(DisplayUtil.dip2px(mContext, 8f)));
        mRecyclerView.setLayoutManager(layoutManager);
        ImgAdapter imgAdapter = new ImgAdapter(item.prove_imgs);
        mRecyclerView.setAdapter(imgAdapter);


    }

    private static class ImgAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

        public ImgAdapter(@Nullable List<String> data) {
            super(R.layout.item_img, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, String item) {
            GlideHelper.loadRoundTrans(mContext, item, helper.getView(R.id.id_img_src), 6);
            helper.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreviewViewPagerActivity.showActivity(mContext, getData(), helper.getAdapterPosition());
                }
            });
        }
    }
}
