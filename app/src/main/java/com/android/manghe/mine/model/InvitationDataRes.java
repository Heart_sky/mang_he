package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class InvitationDataRes extends NetBean<InvitationDataRes.DataBean> {

    /**
     * data : {"comment":{"site_name":"港湾有巢趣买","text":"趣买一分钱，当两份钱花。真实惠，衣食住行全都能，省钱赚券。","url":"http://jm.lnest.cc/x?inviter_id=4","pic":"http://jm.lnest.cc/upload/1/images/qrcode/qr4.png"},"explain":"<div>\r\n\t邀请好友注册并且好友成功购券，赠送赠豆，邀请越多，获赠赠豆越多哦！\r\n<\/div>"}
     * flag : true
     * code : 0
     * msg : 操作成功
     * time : 1506568950
     */
    public static class DataBean {
        /**
         * comment : {"site_name":"港湾有巢趣买","text":"趣买一分钱，当两份钱花。真实惠，衣食住行全都能，省钱赚券。","url":"http://jm.lnest.cc/member/index?inviter_id=4","pic":"http://jm.lnest.cc/upload/1/images/qrcode/qr4.png"}
         * explain : <div>
         邀请好友注册并且好友成功购券，赠送赠币，邀请越多，获赠赠币越多哦！
         </div>
         */

        public CommentBean comment;
        public String explain;
        public String mid;
        public InviteInfo invite_info;

        public static class CommentBean {
            /**
             * site_name : 港湾有巢趣买
             * text : 趣买一分钱，当两份钱花。真实惠，衣食住行全都能，省钱赚券。
             * url : http://jm.lnest.cc/member/index?inviter_id=4
             * pic : http://jm.lnest.cc/upload/1/images/qrcode/qr4.png
             */

            public String site_name;
            public String text;
            public String url;
            public String pic;

        }
        public static class InviteInfo{
            public String nickname;
        }
    }
}
