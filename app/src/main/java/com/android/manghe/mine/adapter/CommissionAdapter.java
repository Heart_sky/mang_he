package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.android.manghe.R;
import com.android.manghe.mine.model.FansInCommissionRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/23 11:12
 * desc   :
 */
public class CommissionAdapter extends BaseQuickAdapter<FansInCommissionRes.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;
    private View emptyView;

    public CommissionAdapter(Context context, @Nullable List<FansInCommissionRes.DataBean.ListBean> data) {
        super(R.layout.item_commission, data);
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void updateList(List<FansInCommissionRes.DataBean.ListBean> listBeans) {
        replaceData(listBeans);
    }

    @Override
    protected void convert(BaseViewHolder helper, FansInCommissionRes.DataBean.ListBean item) {
        helper.setText(R.id.id_tv_name, item.remark);
        helper.setText(R.id.id_tv_time, item.c_time);
        helper.setText(R.id.id_tv_coin, item.amount);
    }
}
