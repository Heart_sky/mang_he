package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.index.model.MainTopBannerBean;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/13 16:44
 * desc   :
 */
public class IntegralCoverPagerAdapter extends PagerAdapter {

    List<MainTopBannerBean> mBannerList=new ArrayList<>();
    private Context mContext;

    public IntegralCoverPagerAdapter(Context context,  List<MainTopBannerBean> bannerList) {
        this.mContext = context;
        this.mBannerList = bannerList;
    }

    @Override
    public int getCount() {
        if (mBannerList == null) {
            return 0;
        }
        return mBannerList.size();

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }



    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_interal_cover, null);
        ImageView imgView = view.findViewById(R.id.id_img_src);
        GlideHelper.loadWithHolderErr(mContext, mBannerList.get(position).src, imgView);
        container.addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
