package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.fragment.TakeOverFragment;
import com.android.manghe.mine.model.AllStateOrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/01 15:01
 * desc   :
 */
public class PTakeOver  extends XPresenter<TakeOverFragment> {



    private int currentPage = 1;
    private final String PageSize = "10";
    private String shipping_status = "1";
    private List<AllStateOrderModel.DataBean.ListBean> allStateOrderList = new ArrayList<>();


    public void getAllOrderList(boolean isRefresh) {
        //获取精选
        if (isRefresh) {
            currentPage = 1;
            allStateOrderList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        data.put("shipping_status", shipping_status);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getContext()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getContext()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_GET_ALL_ORDERS, data, headMap)
                .get()
                .execute(new BaseBack<AllStateOrderModel>() {
                    @Override
                    public void onSuccess(Call call, AllStateOrderModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                allStateOrderList.addAll(res.data.list);
                                getV().setCanLoadMore(allStateOrderList.size() != res.data.list_total);
                                getV().updateList(allStateOrderList);
                                if (allStateOrderList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateList(allStateOrderList);
                            }
                        } else {
                            getV().showErrorTip();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showErrorTip();
                    }

                });
    }

    /**
     * 确认收货
     *
     * @param order_sn
     */
    public void ConfirmReceive(String order_sn) {
        HashMap<String, String> data = new HashMap<>();
        data.put("order_sn", order_sn);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.BOx_CONFIRM_RECEIVE, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            getV().receiveOk();
                        } else {
                            getV().showConfirmReceiveErrorTip();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showConfirmReceiveErrorTip();
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }
}
