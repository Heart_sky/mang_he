package com.android.manghe.mine.activity

import android.os.Bundle
import com.android.base.frame.activity.FragmentActivity
import com.android.base.frame.title.ETitleType
import com.android.manghe.R
import com.android.manghe.main.view.TabEntity
import com.android.manghe.mine.fragment.OneLevelFansFragment
import com.android.manghe.mine.fragment.TwoLevelFansFragment
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_my_team.*

class MyTeamActivity : FragmentActivity() {
    private val mTabEntities = ArrayList<CustomTabEntity>()
    private val mTitles = arrayOf("一级粉丝", "二级粉丝")
    override fun getLayoutId(): Int {
        return R.layout.activity_my_team
    }

    override fun showToolBarType(): ETitleType {
        return ETitleType.SIMPLE_TITLE
    }
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("我的团队").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)
        addFragment(
            R.id.container,
            OneLevelFansFragment::class.java,
            TwoLevelFansFragment::class.java
        )
        for (i in mTitles.indices) {
            mTabEntities.add(TabEntity(mTitles[i], 0, 0))
        }
        tabLayout.setTabData(mTabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabReselect(position: Int) {
            }

            override fun onTabSelect(position: Int) {
                showFragment(position)
            }
        })

    }

}