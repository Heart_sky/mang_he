package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.AddressListActivity;
import com.android.manghe.mine.model.AddressInfo;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PAddressList extends XPresenter<AddressListActivity> {
    public List<AddressInfo> mAddressList = new ArrayList<>();
    public void getAddressList(){
        mAddressList.clear();
        HashMap<String, String> data = new HashMap<>();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERADDRESSLIST,data, headMap)
                .get()
                .execute(new GsonBaseBack<AddressInfo>() {

                    @Override
                    public void onSuccess(Call call, AddressInfo addressInfo) {
                        if (addressInfo!= null && addressInfo.code == 0 && addressInfo.data!=null) {
                            mAddressList.addAll(addressInfo.data);
                        }
                        getV().update(mAddressList);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void deleteAddress(String id){
        HashMap<String, String> data = new HashMap<>();
        data.put("id", id);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERADDRESSDETELE, data, headMap)
                .get()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null) {
                            if(res.code == 0) {
                                ToastUtil.showShort(getV(), "删除成功");
                                getAddressList();
                            }else{
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        }else{
                            ToastUtil.showShort(getV(), "删除失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        ToastUtil.showShort(getV(), "删除失败");
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
