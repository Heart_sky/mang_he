package com.android.manghe.mine.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.base.frame.view.MVPActivity;
import com.android.base.view.viewpager.CustomViewPager;
import com.android.base.view.viewpager.CustomViewPagerAdapter;
import com.android.manghe.R;
import com.android.manghe.main.view.TabEntity;
import com.android.manghe.mine.fragment.CommissionFragment;
import com.android.manghe.mine.fragment.InviteFragment;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/19 16:57
 * desc   :  邀请页面
 */
public class InviteActivity extends MVPActivity {

    private List<CustomTabEntity> mTabEntities;
    private CommonTabLayout mCommonTabLayout;
    private CustomViewPager mCustomViewPager;
    private CommissionFragment mCommissionFragment;
    private InviteFragment mInviteFragment;
    private CustomViewPagerAdapter mAdapter;
    private String[] mTitles = {"邀请记录", "佣金记录"};


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("邀请记录").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine()
                .setBackgroundColor(ContextCompat.getColor(this, R.color.white));

        mCommonTabLayout = findViewById(R.id.tabLayout);
        mCustomViewPager = findViewById(R.id.viewPager);
        mCommissionFragment = new CommissionFragment();
        mInviteFragment = new InviteFragment();
        mTabEntities = new ArrayList<>();
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(mInviteFragment);
        fragments.add(mCommissionFragment);
        mAdapter = new CustomViewPagerAdapter((ArrayList<Fragment>) fragments, getSupportFragmentManager());

        mCustomViewPager.setAdapter(mAdapter);
        mCustomViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCommonTabLayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        for (String str : mTitles) {
            mTabEntities.add(new TabEntity(str, 0, 0));
        }
        mCommonTabLayout.setTabData((ArrayList<CustomTabEntity>) mTabEntities);
        mCommonTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mCustomViewPager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_invite;
    }
}
