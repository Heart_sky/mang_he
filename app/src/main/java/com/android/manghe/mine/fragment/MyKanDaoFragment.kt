package com.android.manghe.mine.fragment

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.frame.view.XStateController
import com.android.base.tools.Glide.GlideHelper
import com.android.manghe.R
import com.android.manghe.config.events.RefreshOrdersEvent
import com.android.manghe.mine.activity.AuctionOrderDetailActivity
import com.android.manghe.mine.activity.CheckFreightActivity
import com.android.manghe.mine.model.MyKanRes
import com.android.manghe.mine.presenter.PMyKanDao
import com.android.manghe.orderpay.activity.AuctionOrderPayActivity
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.eightbitlab.rxbus.Bus
import com.eightbitlab.rxbus.registerInBus
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_my_kan.*
import java.math.RoundingMode
import java.text.DecimalFormat


class MyKanDaoFragment : MVPFragment<PMyKanDao>(), IRefresh , IStateController<XStateController> {
    override fun getStateView(): XStateController {
        return mView.findViewById(R.id.xStateController)
    }
    private var mAdapter: MyKanAdapter? = null

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_my_kan
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = MyKanAdapter(mContext, R.layout.item_my_kan_dao, arrayListOf())
        recyclerView.layoutManager =
            LinearLayoutManager(mContext)
        recyclerView.adapter = mAdapter
        p.setType(1)
        showLoadingDialog()
        p.loadData(true)

        mAdapter!!.setOnItemClickListener { adapter, _, position ->
            val orderInfo = adapter.data[position] as MyKanRes.MyKan.ListBean
            when (orderInfo.order_code) {
                1,2 -> {
                    //进入支付页面
                    open(
                        AuctionOrderPayActivity::class.java,
                        hashMapOf<String, Any>("orderId" to orderInfo.id)
                    )
                }
                3 , 4 , 5 , 7-> {
                    //进入订单详情
                    open(
                        AuctionOrderDetailActivity::class.java,
                        hashMapOf<String, Any>("orderId" to orderInfo.id)
                    )
                }
            }
        }
        Bus.observe<RefreshOrdersEvent>().subscribe {
            p.loadData(true)
        }.registerInBus(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        Bus.unregister(this)
    }

    override fun onRefresh() {
        p.loadData(true)
    }

    override fun onLoad() {
        p.loadData(false)
    }

    fun update(dataList: List<MyKanRes.MyKan.ListBean>) {
        mAdapter?.let {
            it.replaceData(dataList)
            if (dataList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }


    companion object {
        class MyKanAdapter(context: Context, layoutResId: Int, data: List<MyKanRes.MyKan.ListBean>) :
            BaseQuickAdapter<MyKanRes.MyKan.ListBean, BaseViewHolder>(layoutResId, data) {
            val df = DecimalFormat("0.00")
            var context: Context? = null

            init {
                this.context = context
                df.roundingMode = RoundingMode.FLOOR
            }

            override fun convert(helper: BaseViewHolder, item: MyKanRes.MyKan.ListBean) {
                helper.setText(R.id.tvPeopleGot, "用户 " + item.username + " 已抢到")
                helper.setText(R.id.tvTitle, item.title)
                helper.setText(R.id.tvMarketPrice, "￥" + item.market_price)
                helper.setText(R.id.tvPrice, "￥" + item.price)
                helper.setText(R.id.tvTimes, "${item.mid_count}次")
                val percent =
                    java.lang.Float.parseFloat(item.price) / java.lang.Float.parseFloat(item.market_price) * 100
                helper.setText(R.id.tvPercent, df.format((100 - percent).toDouble()))
                GlideHelper.loadAvatar(context, item.photo, helper.getView(R.id.ivAvatar))
                GlideHelper.loadRoundTrans(context, item.thumb, helper.getView(R.id.ivPic), 6)


                helper.setVisible(R.id.tvToGet, item.order_code == 1)

                helper.setGone(
                    R.id.tvToGet,
                    item.order_code == 1 || item.order_code == 2 || item.order_code == 4 || item.order_code == 5
                )
                helper.setGone(R.id.checkFreight, item.order_code == 4)
                when (item.order_code) {
                    0->{
                        helper.setText(R.id.tvStatus, "状态未知, 检查中")
                    }
                    1 -> {
                        helper.setText(R.id.tvToGet, "去领取")
                        helper.setText(R.id.tvStatus, "待领取")
                    }
                    2 -> {
                        helper.setText(R.id.tvToGet, "去支付")
                        helper.setText(R.id.tvStatus, "待支付")
                    }
                    3 -> {
                        helper.setText(R.id.tvStatus, "待发货")
                    }
                    4 -> {
                        helper.setText(R.id.tvToGet, "确认收货")
                        helper.setText(R.id.tvStatus, "待收货")
                    }
                    5 -> {
                        helper.setText(R.id.tvToGet, "去晒单")
                        helper.setText(R.id.tvStatus, "待晒单")
                    }
                    6->{
                        helper.setText(R.id.tvStatus, "已关闭")
                    }
                    7 -> {
                        helper.setText(R.id.tvStatus, "已收货,已评论")
                    }
                    8->{
                        helper.setText(R.id.tvStatus, "发货超过14天,已自动收货")
                    }
                    9->{
                        helper.setText(R.id.tvStatus, "领取超时,已视为放弃")
                    }
                    else -> {
                        helper.setText(R.id.tvStatus, "")
                        helper.setText(R.id.tvToGet, "")
                    }
                }
                helper.getView<TextView>(R.id.checkFreight).setOnClickListener {
                    //查看物流
                    CheckFreightActivity.showActivity(context!!, item.order_sn, true)
                }
            }
        }
    }
}