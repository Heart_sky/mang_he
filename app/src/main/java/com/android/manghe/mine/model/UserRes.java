package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

public class UserRes extends NetBean<UserRes.UserInfo> implements Serializable {
    /**
     * UID : 1
     * UNAME : test01
     * UPSW : f9c2d2e2f0ed651159307e0782166bfd
     * avatar : http://192.168.1.47//upload/photo.gif
     */
    public class UserInfo implements Serializable{
        public String UID;
        public String UNAME;
        public String UPSW;
        public String avatar;
        public String TOKEN;
        public String nickname;
        public String sex;
        public String mobile;
        public String photo;
    }
}
