package com.android.manghe.mine.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;
import com.android.manghe.mine.adapter.UnderAgeAdapter;
import com.android.manghe.mine.model.UnderAgeModel;
import com.android.manghe.mine.presenter.PUnderAgeFeedBack;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/29 11:06
 * desc   : 未年成反馈记录
 */
public class UnderAgeFeedBackActivity extends MVPActivity<PUnderAgeFeedBack> implements IRefresh {

    private RecyclerView mRecyclerView;
    private UnderAgeAdapter mAdapter;

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("反馈记录").setLeftIcon(R.mipmap.icon_back_black)
                .hideBottomLine().setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
        mRecyclerView = findViewById(R.id.id_recyclerView);

        initRecyclerView();
        showLoadingDialog();
        getP().getUnderAgeList(true);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new UnderAgeAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_under_age_feed_back;
    }


    public void updateList(List<UnderAgeModel.DataBean.ListBean> listBeans) {
        mAdapter.replaceData(listBeans);

    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return findViewById(R.id.id_refreshLayout);
    }


    @Override
    public void onRefresh() {
        getP().getUnderAgeList(true);
    }

    @Override
    public void onLoad() {
        getP().getUnderAgeList(false);
    }
}
