package com.android.manghe.mine.activity

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.mine.presenter.PSignRule
import kotlinx.android.synthetic.main.activity_article.*

class SignRuleActivity : MVPActivity<PSignRule>() {
    companion object {
        fun showActivity(context: Context) {
            val intent = Intent(context, SignRuleActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_article
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("签到规则").setLeftIcon(R.mipmap.icon_back_black)
            .setTitleAndStatusBgColor(R.color.white)
        p.getRule()
    }

    fun showText(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.text = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        } else {
            tvContent.text = Html.fromHtml(text)
        }
    }

}