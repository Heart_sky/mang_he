package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SandPayModel extends NetBean<SandPayModel.Data> {


    /**
     * data : {"paycode":{"version":"1.0","mer_no":"6888806041519","mer_key":"Z+fNwoemytdPBis4iHS+9ax80f10NZjWm3I90H/jWzqxRPMEWPVjc13D7r6MBS9138gGHsDpQPU=","mer_order_no":"20220115115421_326","create_time":"20220115115421","expire_time":"20220115125421","order_amt":"0.10","notify_url":"https: //www.mxmd88.com/welcome/apprespond/sandpay","return_url":"","create_ip":"121_5_113_157","goods_name":"芒铺开盒币","store_id":"000000","product_code":"02020004,             02030001","clear_cycle":"0","pay_extra":"","accsplit_flag":"NO","jump_scheme":"sandcash: //scpay","sign_type":"MD5","sign":"accsplit_flag=NO&create_ip=121_5_113_157&create_time=20220115115421&mer_key=Z+fNwoemytdPBis4iHS+9ax80f10NZjWm3I90H/jWzqxRPMEWPVjc13D7r6MBS9138gGHsDpQPU=&mer_no=6888806041519&mer_order_no=20220115115421_326¬ify_url=https: //www.mxmd88.com/welcome/apprespond/sandpay&order_amt=0.10&sign_type=MD5&store_id=000000&version=1.0&key=rY6sef01dRKYAoZoKgWd7i2zV56dwZNLFckzpH1KoSdrJx3WNsDko7CSSEyqwE8g7e/D2W7Y/CZ9LPMpYDST//aqS+9v427du7QhuaYg3ZhJEg81wSSzBKaITmYsCtImul7EaHVR6aBixuz+UKFTiw=="},"order":{"bid":"0","voucher_id":"94","mid":911767,"title":"芒铺开盒币","thumb":"https: //www.mxmd88.com/upload/1/images/gallery/i/d/29174_src.jpg","summary":"玩转芒铺赢好礼！","content":"芒铺开盒币","mobile":" ","c_time":1642218861,"start_time":"0","end_time":"0","pay_id":70,"pay_time":0,"count":1,"cost_price":"30.00","cost_amount":30,"price":"0.10","amount":0.1,"coin":"30","free_coin":"1","coin_total":30,"status":1,"favourable_value":0,"order_id":279,"log_id":326,"out_trade_no":"20220115115421_326","order_sn":"20220115115421_326","goods_name":"芒铺开盒币","order_amount":0.1,"items":[{"service_identify":"10000001","subject":"芒铺开盒币","product_type":"3","goods_count":1}]}}
     * flag : true
     */
    public static class Data {
        /**
         * paycode : {"version":"1.0","mer_no":"6888806041519","mer_key":"Z+fNwoemytdPBis4iHS+9ax80f10NZjWm3I90H/jWzqxRPMEWPVjc13D7r6MBS9138gGHsDpQPU=","mer_order_no":"20220115115421_326","create_time":"20220115115421","expire_time":"20220115125421","order_amt":"0.10","notify_url":"https: //www.mxmd88.com/welcome/apprespond/sandpay","return_url":"","create_ip":"121_5_113_157","goods_name":"芒铺开盒币","store_id":"000000","product_code":"02020004,             02030001","clear_cycle":"0","pay_extra":"","accsplit_flag":"NO","jump_scheme":"sandcash: //scpay","sign_type":"MD5","sign":"accsplit_flag=NO&create_ip=121_5_113_157&create_time=20220115115421&mer_key=Z+fNwoemytdPBis4iHS+9ax80f10NZjWm3I90H/jWzqxRPMEWPVjc13D7r6MBS9138gGHsDpQPU=&mer_no=6888806041519&mer_order_no=20220115115421_326¬ify_url=https: //www.mxmd88.com/welcome/apprespond/sandpay&order_amt=0.10&sign_type=MD5&store_id=000000&version=1.0&key=rY6sef01dRKYAoZoKgWd7i2zV56dwZNLFckzpH1KoSdrJx3WNsDko7CSSEyqwE8g7e/D2W7Y/CZ9LPMpYDST//aqS+9v427du7QhuaYg3ZhJEg81wSSzBKaITmYsCtImul7EaHVR6aBixuz+UKFTiw=="}
         * order : {"bid":"0","voucher_id":"94","mid":911767,"title":"芒铺开盒币","thumb":"https: //www.mxmd88.com/upload/1/images/gallery/i/d/29174_src.jpg","summary":"玩转芒铺赢好礼！","content":"芒铺开盒币","mobile":" ","c_time":1642218861,"start_time":"0","end_time":"0","pay_id":70,"pay_time":0,"count":1,"cost_price":"30.00","cost_amount":30,"price":"0.10","amount":0.1,"coin":"30","free_coin":"1","coin_total":30,"status":1,"favourable_value":0,"order_id":279,"log_id":326,"out_trade_no":"20220115115421_326","order_sn":"20220115115421_326","goods_name":"芒铺开盒币","order_amount":0.1,"items":[{"service_identify":"10000001","subject":"芒铺开盒币","product_type":"3","goods_count":1}]}
         */

        @SerializedName("paycode")
        public Paycode paycode;
        @SerializedName("order")
        public Order order;

        public static class Paycode {
            /**
             * version : 1.0
             * mer_no : 6888806041519
             * mer_key : Z+fNwoemytdPBis4iHS+9ax80f10NZjWm3I90H/jWzqxRPMEWPVjc13D7r6MBS9138gGHsDpQPU=
             * mer_order_no : 20220115115421_326
             * create_time : 20220115115421
             * expire_time : 20220115125421
             * order_amt : 0.10
             * notify_url : https: //www.mxmd88.com/welcome/apprespond/sandpay
             * return_url :
             * create_ip : 121_5_113_157
             * goods_name : 芒铺开盒币
             * store_id : 000000
             * product_code : 02020004,             02030001
             * clear_cycle : 0
             * pay_extra :
             * accsplit_flag : NO
             * jump_scheme : sandcash: //scpay
             * sign_type : MD5
             * sign : accsplit_flag=NO&create_ip=121_5_113_157&create_time=20220115115421&mer_key=Z+fNwoemytdPBis4iHS+9ax80f10NZjWm3I90H/jWzqxRPMEWPVjc13D7r6MBS9138gGHsDpQPU=&mer_no=6888806041519&mer_order_no=20220115115421_326¬ify_url=https: //www.mxmd88.com/welcome/apprespond/sandpay&order_amt=0.10&sign_type=MD5&store_id=000000&version=1.0&key=rY6sef01dRKYAoZoKgWd7i2zV56dwZNLFckzpH1KoSdrJx3WNsDko7CSSEyqwE8g7e/D2W7Y/CZ9LPMpYDST//aqS+9v427du7QhuaYg3ZhJEg81wSSzBKaITmYsCtImul7EaHVR6aBixuz+UKFTiw==
             */

            @SerializedName("version")
            public String version;
            @SerializedName("mer_no")
            public String merNo;
            @SerializedName("mer_key")
            public String merKey;
            @SerializedName("mer_order_no")
            public String merOrderNo;
            @SerializedName("create_time")
            public String createTime;
            @SerializedName("expire_time")
            public String expireTime;
            @SerializedName("order_amt")
            public String orderAmt;
            @SerializedName("notify_url")
            public String notifyUrl;
            @SerializedName("return_url")
            public String returnUrl;
            @SerializedName("create_ip")
            public String createIp;
            @SerializedName("goods_name")
            public String goodsName;
            @SerializedName("store_id")
            public String storeId;
            @SerializedName("product_code")
            public String productCode;
            @SerializedName("clear_cycle")
            public String clearCycle;
            @SerializedName("pay_extra")
            public String payExtra;
            @SerializedName("accsplit_flag")
            public String accsplitFlag;
            @SerializedName("jump_scheme")
            public String jumpScheme;
            @SerializedName("sign_type")
            public String signType;
            @SerializedName("sign")
            public String sign;
        }

        public static class Order {
            /**
             * bid : 0
             * voucher_id : 94
             * mid : 911767
             * title : 芒铺开盒币
             * thumb : https: //www.mxmd88.com/upload/1/images/gallery/i/d/29174_src.jpg
             * summary : 玩转芒铺赢好礼！
             * content : 芒铺开盒币
             * mobile :
             * c_time : 1642218861
             * start_time : 0
             * end_time : 0
             * pay_id : 70
             * pay_time : 0
             * count : 1
             * cost_price : 30.00
             * cost_amount : 30
             * price : 0.10
             * amount : 0.1
             * coin : 30
             * free_coin : 1
             * coin_total : 30
             * status : 1
             * favourable_value : 0
             * order_id : 279
             * log_id : 326order_id
             * out_trade_no : 20220115115421_326
             * order_sn : 20220115115421_326
             * goods_name : 芒铺开盒币
             * order_amount : 0.1
             * items : [{"service_identify":"10000001","subject":"芒铺开盒币","product_type":"3","goods_count":1}]
             */

            @SerializedName("bid")
            public String bid;
            @SerializedName("voucher_id")
            public String voucherId;
            @SerializedName("mid")
            public Integer mid;
            @SerializedName("title")
            public String title;
            @SerializedName("thumb")
            public String thumb;
            @SerializedName("summary")
            public String summary;
            @SerializedName("content")
            public String content;
            @SerializedName("mobile")
            public String mobile;
            @SerializedName("c_time")
            public Integer cTime;
            @SerializedName("start_time")
            public String startTime;
            @SerializedName("end_time")
            public String endTime;
            @SerializedName("pay_id")
            public Integer payId;
            @SerializedName("pay_time")
            public Integer payTime;
            @SerializedName("count")
            public Integer count;
            @SerializedName("cost_price")
            public String costPrice;
            @SerializedName("cost_amount")
            public Integer costAmount;
            @SerializedName("price")
            public String price;
            @SerializedName("amount")
            public Double amount;
            @SerializedName("coin")
            public String coin;
            @SerializedName("free_coin")
            public String freeCoin;
            @SerializedName("coin_total")
            public Integer coinTotal;
            @SerializedName("status")
            public Integer status;
            @SerializedName("favourable_value")
            public Integer favourableValue;
            @SerializedName("order_id")
            public String orderId;
            @SerializedName("log_id")
            public Integer logId;
            @SerializedName("out_trade_no")
            public String outTradeNo;
            @SerializedName("order_sn")
            public String orderSn;
            @SerializedName("goods_name")
            public String goodsName;
            @SerializedName("order_amount")
            public Double orderAmount;
            @SerializedName("items")
            public List<Items> items;

            public static class Items {
                /**
                 * service_identify : 10000001
                 * subject : 芒铺开盒币
                 * product_type : 3
                 * goods_count : 1
                 */

                @SerializedName("service_identify")
                public String serviceIdentify;
                @SerializedName("subject")
                public String subject;
                @SerializedName("product_type")
                public String productType;
                @SerializedName("goods_count")
                public Integer goodsCount;
            }
        }
    }
}
