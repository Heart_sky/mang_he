package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

public class CustomerRes extends NetBean implements Serializable {

    /**
     * cat : {"id":"33","catname":"常见问题","catdir":"faq","parentdir":"","parentid":"0","moduleid":"2","module":"article","arrparentid":"0","arrchildid":"33,41,39,42,37,38","type":"0","title":"","keywords":"","description":"","listorder":"50","ishtml":"0","ismenu":"1","hits":"0","thumb":"","child":"1","url":"/content/index/33","template_list":"list_help","template_show":"show_help","pagesize":"10","readgroup":"0","listtype":"0","lang":"1","urlruleid":"0","issystem":"1"}
     * catlist : [{"id":"37","catname":"注册登录","list":[{"id":"46","title":"如何注册？","content":"A、打开萌象\r\nB、点底部【我的】，点击左上角【注册】\r\nC、填写手机号码、密码\r\nD、注册成功后，请使用当下注册的方式进行登陆\r\nE、同时也可以使用微信 QQ 微博等方式注册登录平台"},{"id":"47","title":"注册时手机收不到短信怎么办？","content":"A、确认短信是否被手机软件拦截或过滤<br />\r\nB、确认手机是否正常接收短信（信号、欠费、停机等）<br />\r\nC、短信收发过程可能会有延时，请耐心守候<br />\r\nD、仍未收到，请联系在线客服为您服务<br />"},{"id":"51","title":"忘记登录密码怎么办？","content":"可在登陆页面点击【忘记密码】，<span>通过手机短信验证<\/span>进行找回。"}]},{"id":"38","catname":"萌象问题","list":[{"id":"52","title":"名词解释","content":"<span>网服费：指用户参与砍价，每次使用砍币/赠币支付平台网络服务费。<\/span><br />\r\n<span>起砍价价：砍价商品的最高起始价。<\/span><br />\r\n<span>砍价幅度：指用户每次出价参与砍价的减价金额。<\/span><br />\r\n<span>退返比例：按砍价消耗的网络服务费，依照【1:1】的比例赠送商城购物积分。<\/span><br />\r\n<span>砍价倒计时：指每次按照规定的时间【10】秒，如无人出价，则最后一位出价者得。<\/span><br />\r\n<span>积分：指可以用于商城购物时抵用，积分有时效限制，默认周期为【30】天。<\/span><br />\r\n<br />"},{"id":"48","title":"砍价什么流程？","content":"<p>\r\n\t<span>步骤一：免费注册会员。<\/span><br />\r\n<span>步骤二：充值购券获得赠送的砍币。<\/span><br />\r\n<span>步骤三：选择心仪商品出价，等待倒计时结束。<\/span><br />\r\n<span>步骤四：砍价成功，以最终成交价获得商品购买权。<\/span><br />\r\n<span>步骤五：砍价失败，使用商家劵或积分到商城购买商品。<\/span>&nbsp;&nbsp;&nbsp;\r\n<\/p>"},{"id":"54","title":"砍价采用什么规则？","content":"<span>(1) 砍币来源：到充值中心选择金额充值购劵，<\/span><span>充值赠送砍币，<\/span><span>券可以到商城购物时使用。<\/span><br />\r\n<span>(2) 商品砍价起砍价以页面起砍价为准，每一次出价会消耗一定数量的砍币（网络服务费），同时商品价格以减价幅度值递减。<\/span><br />\r\n<span>(3) 在初始倒计时内即可出价，初始倒计时后进入砍价倒计时，当您出价后，该件商品的计时器将被自动重置到砍价倒计时10秒，以便其他用户进行出价砍价。如果没有其他用户对该件商品出价，计时器归零时，您便成功获得得了该商品。<\/span><br />\r\n<span>(4) 若砍价成功，请在3天内以成交价支付购买商品，超过3天未下单，视为放弃，不返积分。<\/span><br />\r\n<span>(5) 若砍价失败，砍价失败没有任何损失，除购物卡和积分可以在商城换购商品外，还<\/span><span>可返还所消耗砍币100%倍率的积分（赠币除外）。<\/span><br />\r\n<span>(6) 平台严禁违规操作，最终解释权归萌象所有。<\/span><br />\r\n<br />"},{"id":"53","title":"如何获得拍币？","content":"<span>登录后，点击首页的\u201c充值\u201d来充值购券，充值券后你可以得到相应的券以及获赠一定数量的砍币。<\/span>"},{"id":"68","title":"1元区和10元区有何区别？","content":"<p>\r\n\t砍价时支付的网服费（砍币）有区别\r\n<\/p>\r\n<p>\r\n\t<p>\r\n\t\t1元区：每次出价消耗1砍币/1赠币。\r\n\t<\/p>\r\n\t<p>\r\n\t\t10元区：每次出价消耗10砍币/10赠币。\r\n\t<\/p>\r\n<\/p>"},{"id":"55","title":"一场砍价什么时候结束？","content":"<span>当没有新的竞争者出价，<\/span><span>倒计时计时器归0<\/span><span>，并显示砍价成功者，就代表该商品这场砍价结束了。<\/span>"},{"id":"56","title":"砍价成功后应该怎么做？","content":"<span>恭喜您成功争取到了该商品的低价购买资格。<\/span><br />\r\n<span>您需要以成交价格，支付该商品，您可以点击\u201c萌象订单\u201d\u2014\u2014\u201c待付款\u201d\u2014\u2014\u201c支付订单\u201d，然后填写您的收货信息，我们会在【3】工作日内给您发货。请您务必在3天内完成支付，否则视为自愿放弃。如果您有什么疑问可以联系我们的客服人员。<\/span><br />"},{"id":"57","title":"砍价会产生什么消耗？","content":"砍价过程中，每次使用砍<span>币<\/span>/赠币支付平台网络服务费后，您可以获得相应一次的砍价出价资格。<br />\r\n一、砍价成功，所消耗砍币不赠送商城购物积分。<br />\r\n二、砍价失败，砍价失败后，平台将跟据您消耗的砍币，以<span>【1:1】<\/span>倍率赠送积分。<br />\r\n三、砍币规则，无论砍币成功或者失败，均不能获得任何赠送或返还。<br />\r\n<br />"},{"id":"74","title":"压秒出价","content":"<div style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t部分用户总会有个认识上的误区，那就是等倒计时将要结束（在倒计时的最后一秒或者0秒）时出价，即压秒出价。\r\n<\/div>\r\n<div style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t<span class=\"STYLE4\">实际上压秒出价是没有任何意义的，因为即使压秒出价成功，倒计时会再次回到10秒，而不是砍价成功。但是这样很可能会错失了出价的机会，导致砍价没能成功。主要原因有以下两点：<\/span>\r\n<\/div>\r\n<div style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t<span class=\"STYLE4\"><span style=\"font-size:10pt;\"><br />\r\n<\/span><\/span>\r\n<\/div>\r\n<div style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t1 因为倒计时是实时刷新请求服务器，如果客户端网络或服务器网络出现不畅或其他网络环境等因素，都可能造成服务器时间与客户端时间有1到2秒的时间差。这种情况下，可能客户端显示倒计时还未结束时，而服务器的倒计时已经结束了。而倒计时又以服务器端为准，这样可能就错过了出价的机会了，有的用户遇到倒计时还有1秒却点击无法出价就是这样的情况。\r\n<\/div>"},{"id":"73","title":"违规认定及处理","content":"<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t对违规情况的认定和处理：\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t1、<span class=\"red\" style=\"font-weight:bold;color:#FF6600;\">关联账户<\/span>：用户注册多个账户，或是拥有对多个账户的操控权。本平台将会通过一系列数据判断账户的关联性，一旦认定，将会从严处理。部分用户以\u201c<span style=\"color:#337FE5;\">帮同事/<\/span><span style=\"color:#337FE5;\">朋友<\/span><span style=\"color:#337FE5;\">砍价<\/span>\u201d或是\u201c在公司/学校注册的，公司共享同一个IP\u201d等托词来辩解，对于这类理由，我们一概不予采信。我们会根据各种数据来判断账户之间的关联性，包括但不限于注册IP、登录IP等。\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t2、<span class=\"red\" style=\"font-weight:bold;color:#FF6600;\">虚假信息<\/span>：用户以虚假或是非本人的身份证信息、手机号码、邮箱等信息完善账户资料。这种情况一旦发现，我们会要求用户提供身份证正反面扫描件、人工拨打手机号码进行验证等方式进行核实。\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t3、\u201c<span class=\"red\" style=\"font-weight:bold;color:#FF6600;\">围砍<\/span>\u201d：围砍是指多个用户通过合作共同竞砍某一个商品，以谋取不正当利益。\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t对于以上违规情况，我们将会视情况严重程度进行以下处理：\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t1、立即锁定所有账户接受调查。\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t2、所有未发货订单立即取消，商品所消耗砍币不返还。\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t3、账户取消晒图赠币、邀请好友奖励赠币等一切福利功能。\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t<span class=\"red\" style=\"font-weight:bold;color:#FF6600;\">处理流程：<\/span>\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t平台客服电话和在线将会暂停对违规用户的服务。\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t<span class=\"red\" style=\"font-weight:bold;color:#FF6600;\">特别说明：<\/span>\r\n<\/p>\r\n<p style=\"color:#333333;font-family:Tahoma;background-color:#FFFFFF;\">\r\n\t1、对于屡次违反上述规定的用户，平台将会在不通知的前提下直接取消一切服务。\r\n<\/p>"},{"id":"69","title":"为什么会出现砍价卡顿现象？","content":"<p>\r\n\t萌象每天都有成千上万的用户参加,我们统计过仅有万分之一的用户会遇到卡顿的情况,究竟这种情况是怎么发生的?又如何避免呢?\r\n<\/p>\r\n<p>\r\n\t请注意以下提到的几点:\r\n<\/p>\r\n<p>\r\n\t1.参与砍价前请检查您手机当前网络是否通畅。一般网络信号不足2格的时候,就容易出现倒计时停顿的现象,这并不是砍价停止,而是您当前网络差页面未能及时刷新的现象。而实际砍价仍在进行中,网络正常的用户的显示也都是同步的。\r\n<\/p>\r\n<p>\r\n\t2.优质的手机配置是顺利砍价的硬性条件配置较低的手机及系统运行速度会相对缓慢,显示也容易出现延缓的情况。一般来讲同样的网络环境下iphone7会比iphone5反应速度更快。所以选择性能较好的手机非常有必要哦\r\n<\/p>\r\n<p>\r\n\t3.网络信号良好的情况下,4G网络会比WiFi网络更稳定因闪趣为减少用户流量和电量的消耗,目前把刷新价格的频率设定为1秒1次,如果砍价中有1秒网络不稳定,就有可能体验到卡顿1秒的情况。当出现卡顿时,可以尝试在4G和WiFi之间切换试试看\r\n<\/p>"}]},{"id":"39","catname":"晒单奖励","list":[{"id":"49","title":"如何晒单赚赠币？","content":"<span>砍价成功的用户，在收到商品后，进入\"萌象订单\u201d\u2014\u201c待晒单\u201d点击晒单按钮进入晒单页面上传图片和发布评论，在经过审核后，最高可获得5个赠币奖励。具体奖励如下：<\/span><br />\r\n<span>(1) 虚拟商品：上传砍价成功页面截图或对应账户收货信息截图，留言超过8个字，即可获得1个赠币奖励！<\/span><br />\r\n<span>(2) 实物商品：上传至少3张商品照片，并留言超过12个字，即可获得3个赠币；真人露脸出镜额外奖励2个赠币。<\/span><br />"},{"id":"59","title":"去哪里查看晒单？","content":"您的晒单信息通过审核后，均在\u201c个人中心\u201d\u2014\u2014\u2014\u201c我的晒单\u201d中显示"},{"id":"60","title":"关于晒单照片使用权？","content":"用户的晒单版权归萌象所有，萌象保留用于自身商业目的的复制、展示和传播权利。萌象在使用过程中将会注意保护用户的隐私和形象，不做任何有损晒图用户利益的行为。若用户不同意此条款，我们建议用户不参与晒图赚赠币的活动。"}]},{"id":"41","catname":"发货说明","list":[{"id":"63","title":"订单支付后什么时候能发货？","content":"(1)虚拟商品24小时内发货，部分商品支持实时发奖。如您在发货前未正确填写收货信息，则发货延期。<br />\r\n(2)实物商品将在下一个工作日内下单，但具体发货时间和物流单号需要依商家而定（一般在7个工作日内发货）。<br />\r\n(3)预售商品发货日需根据实际采购情况而定(一般在90天内发货)<br />\r\n(4)恶意注册用户将取消商品发货<br />"},{"id":"62","title":"长期未收到砍价商品怎么回事呢？","content":"(1)确保收货地址、邮编、电话、Email、地址等各项信息的准确性；<br />\r\n(2)配送过程中请确保您的联系方式畅顺无阻，如果联络您的时间超过7天未得到回复，默认您已经放弃此商品<br />"},{"id":"61","title":"商品可以退换吗？","content":"非质量问题，不在三包范围内，不给予退换货。请尽量亲自签收并当面拆箱验货，如果发现运输途中造成了商品的损坏，请不要签收，可以拒签退回。"}]},{"id":"42","catname":"商城问题","list":[{"id":"64","title":"什么是商城？","content":"<span>商城是购物市场，里面有相当多的物品供您使用购物卡+金额+积分购买，在商城里面用户可以自由的选择使用<\/span><span>购物卡+金额+积分<\/span><span>的组合购任何商品。<\/span>"},{"id":"65","title":"商城兑换商品的积分从哪里获得？","content":"<p>\r\n\t获得积分的路径有两种\r\n<\/p>\r\n<p>\r\n\t1、通过积分签到，每天坚持签到获取相应的积分；\r\n<\/p>\r\n<p>\r\n\t2、砍价返回积分，通过参加砍价来获取积分，砍价未成功返积分。\r\n<\/p>"},{"id":"66","title":"积分或抵用券有效期","content":"积分或抵用券有效期<br />\r\n积分或抵用券时效期：用户虽然获得了积分或商城抵用券但是未使用，<span>【萌象】<\/span>平台将以【30】天为周期，定期对过期的积分进行作废处理。而判断未使用积分失效的起始时间是您未再充值区购买券，或者未进行砍价活动开始计算，至到【30】天依然未有相应购买或者砍价行为。注：充值所购购物卡为永久有效。<br />\r\n<div>\r\n\t<br />\r\n<\/div>"},{"id":"71","title":"商家劵如何使用？","content":"商家劵可以在商城购物时使用，下单时会根据商品的购物卡抵用额自动抵扣<br />\r\n<div>\r\n\t<br />\r\n<\/div>"}]}]
     * business_tel : 400-626-1108
     * worktime : 工作日9:00-18:00
     * contact_qq : 在线客服
     * flag : true
     * data : null
     */

    public CatBean cat;
    public String business_tel;
    public String worktime;
    public String contact_qq;
    public List<CatlistBean> catlist;

    public static class CatBean implements Serializable{
        /**
         * id : 33
         * catname : 常见问题
         * catdir : faq
         * parentdir :
         * parentid : 0
         * moduleid : 2
         * module : article
         * arrparentid : 0
         * arrchildid : 33,41,39,42,37,38
         * type : 0
         * title :
         * keywords :
         * description :
         * listorder : 50
         * ishtml : 0
         * ismenu : 1
         * hits : 0
         * thumb :
         * child : 1
         * url : /content/index/33
         * template_list : list_help
         * template_show : show_help
         * pagesize : 10
         * readgroup : 0
         * listtype : 0
         * lang : 1
         * urlruleid : 0
         * issystem : 1
         */

        public String id;
        public String catname;
        public String catdir;
        public String parentdir;
        public String parentid;
        public String moduleid;
        public String module;
        public String arrparentid;
        public String arrchildid;
        public String type;
        public String title;
        public String keywords;
        public String description;
        public String listorder;
        public String ishtml;
        public String ismenu;
        public String hits;
        public String thumb;
        public String child;
        public String url;
        public String template_list;
        public String template_show;
        public String pagesize;
        public String readgroup;
        public String listtype;
        public String lang;
        public String urlruleid;
        public String issystem;
    }

    public static class CatlistBean  implements Serializable{
        /**
         * id : 37
         * catname : 注册登录
         * list : [{"id":"46","title":"如何注册？","content":"A、打开萌象\r\nB、点底部【我的】，点击左上角【注册】\r\nC、填写手机号码、密码\r\nD、注册成功后，请使用当下注册的方式进行登陆\r\nE、同时也可以使用微信 QQ 微博等方式注册登录平台"},{"id":"47","title":"注册时手机收不到短信怎么办？","content":"A、确认短信是否被手机软件拦截或过滤<br />\r\nB、确认手机是否正常接收短信（信号、欠费、停机等）<br />\r\nC、短信收发过程可能会有延时，请耐心守候<br />\r\nD、仍未收到，请联系在线客服为您服务<br />"},{"id":"51","title":"忘记登录密码怎么办？","content":"可在登陆页面点击【忘记密码】，<span>通过手机短信验证<\/span>进行找回。"}]
         */

        public String id;
        public String catname;
        public List<ListBean> list;

        public static class ListBean  implements Serializable{
            /**
             * id : 46
             * title : 如何注册？
             * content : A、打开萌象
             B、点底部【我的】，点击左上角【注册】
             C、填写手机号码、密码
             D、注册成功后，请使用当下注册的方式进行登陆
             E、同时也可以使用微信 QQ 微博等方式注册登录平台
             */

            public String id;
            public String title;
            public String content;
        }
    }
}
