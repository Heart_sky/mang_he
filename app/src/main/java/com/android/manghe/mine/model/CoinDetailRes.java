package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class CoinDetailRes extends NetBean {
    public int list_total;
    public List<CoinDetail> list;

    public static class CoinDetail {
        /**
         * id : 1437
         * mid : 4
         * amount : 10
         * type : 0
         * remark : 系统调整
         * c_time : 2017-09-30 15:58:33
         */

        public String id;
        public String mid;
        public String amount;
        public String type;
        public String remark;
        public String c_time;
    }
}
