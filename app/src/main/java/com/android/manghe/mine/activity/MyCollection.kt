package com.android.manghe.mine.activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.view.MVPActivity
import com.android.base.frame.view.XStateController
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.detail.activity.GoodDetailActivity
import com.android.manghe.index.model.CollectionRes
import com.android.manghe.mine.adapter.CollectionAdapter
import com.android.manghe.mine.presenter.PMyCollection
import com.android.manghe.user.activity.LoginActivity
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_my_collection.*

/**
 * 我的收藏
 * @author ZhangWeiJun
 * @date 2019/6/30
 */

class MyCollection : MVPActivity<PMyCollection>() , IRefresh , IStateController<XStateController> {
    override fun getStateView(): XStateController {
        return findViewById(R.id.xStateController)
    }
    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    private var mAdapter: CollectionAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_my_collection
    }


    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("我的收藏").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)

        mAdapter = CollectionAdapter(this, arrayListOf(), object : CollectionAdapter.ICollectionListener{
            override fun cancel(id: String?) {
                showLoadingDialog()
                p.cancelCollection(id)
            }
        })
        mAdapter!!.setOnItemClickListener { _, item ->
            if(UserHolder.getUserInfo(this@MyCollection) == null){
                open(LoginActivity::class.java)
            }else {
                val intent = Intent(mContext, GoodDetailActivity::class.java)
                intent.putExtra("auctionId", item.id)
                mContext.startActivity(intent)
            }
        }
        val linearManager =
            LinearLayoutManager(this)
        recyclerView.layoutManager = linearManager
        recyclerView.adapter = mAdapter
        showLoadingDialog()
        p.getData(true)
    }

    override fun onRefresh() {
        p.getData(true)
    }

    override fun onLoad() {
        p.getData(false)
    }


    fun update(hotPaiInfoList: List<CollectionRes.DataBean.ListBean>) {
        mAdapter?.let {
            it.update(hotPaiInfoList)
            if (hotPaiInfoList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
    }



    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }
}