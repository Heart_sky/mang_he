package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.AddModifyAddrActivity;
import com.android.manghe.mine.model.AddAddressRes;
import com.android.manghe.mine.model.AreaInfo;
import okhttp3.Call;

import java.util.HashMap;
import java.util.List;

public class PAddModifyAddr extends XPresenter<AddModifyAddrActivity> {

    public void addAddr(String name, String zone, String addr , String mobile, boolean isDefault){
        HashMap<String, String> data = new HashMap<>();
        data.put("name", name);
        data.put("zone", zone);
        data.put("address", addr);
        data.put("mobile", mobile);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERADDRESSADD, headMap)
                .post(data).build()
                .execute(new BaseBack<AddAddressRes>() {
                    @Override
                    public void onSuccess(Call call, AddAddressRes res) {
                        getV().hideLoadingDialog();
                        if (res != null) {
                            if(res.code == 0) {
                                ToastUtil.showShort(getV(), "添加成功");
                                if(isDefault){
                                    setDefault(res.data.id + "");
                                }else {
                                    getV().finish();
                                }
                            }else{
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        }else{
                            ToastUtil.showShort(getV(), "添加失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        ToastUtil.showShort(getV(), "添加失败");
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    public void modifyAddr(String id, String name, String zone, String addr , String mobile, boolean isDefault){
        HashMap<String, String> data = new HashMap<>();
        data.put("id", id);
        data.put("name", name);
        data.put("zone", zone);
        data.put("address", addr);
        data.put("mobile", mobile);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERADDRESSUPDATE, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null) {
                            if(res.code == 0) {
                                ToastUtil.showShort(getV(), "修改成功");
                                if(isDefault){
                                    setDefault(id);
                                }else {
                                    getV().finish();
                                }
                            }else{
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        }else{
                            ToastUtil.showShort(getV(), "修改失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        ToastUtil.showShort(getV(), "修改失败");
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void setDefault(String id) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("id", id);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERADDRESSDEFAULT, headMap)
                .post(dataMap)
                .build()
                .execute(new BaseBack<NetBean>() {
                    @Override
                    public void onSuccess(Call call, NetBean netBean) {
                        getV().hideLoadingDialog();
                        getV().finish();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().finish();
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }
    public void getArea(String parentId){
        HashMap<String, String> data = new HashMap<>();
        data.put("parentid",parentId);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERGETAREA,data, headMap)
                .get()
                .execute(new BaseBack<AreaInfo>() {

                    @Override
                    public void onSuccess(Call call, List<AreaInfo> areaList) {
                        getV().hideLoadingDialog();
                        if (areaList != null) {
                            getV().showAreaList(areaList);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
