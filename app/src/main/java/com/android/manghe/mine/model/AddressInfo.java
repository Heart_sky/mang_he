package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class AddressInfo extends NetBean<List<AddressInfo>> {


        /**
         * id : 92
         * name : 222222
         * mobile : 222222
         * zone : 500
         * area : 北京 东城区
         * address : 222222
         * is_default : 1
         * city_id : 500
         */

        public String id;
        public String name;
        public String mobile;
        public String zone;
        public String area;
        public String address;
        public String is_default;
        public String city_id;
}
