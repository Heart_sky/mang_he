package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.android.manghe.R;
import com.android.manghe.mine.model.CoinAndIntegralModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/14 16:56
 * desc   :
 */
public class CoinAndIntegralDetailAdapter extends BaseQuickAdapter<CoinAndIntegralModel.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;
    private View emptyView;
    private boolean isZhiChu;


    public CoinAndIntegralDetailAdapter(Context context, @Nullable List<CoinAndIntegralModel.DataBean.ListBean> data, boolean zhichu) {
        super(R.layout.item_coin_and_integral_detail, data);
        mContext = context;
        isZhiChu = zhichu;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    @Override
    protected void convert(BaseViewHolder helper, CoinAndIntegralModel.DataBean.ListBean item) {
        helper.setText(R.id.id_tv_name, item.remark);
        helper.setText(R.id.id_tv_time, item.c_time);
        helper.setText(R.id.id_tv_money, isZhiChu ? item.amount : "+" + item.amount);
    }

}
