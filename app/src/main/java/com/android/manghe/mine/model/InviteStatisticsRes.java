package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class InviteStatisticsRes extends NetBean<InviteStatisticsRes.DataBean> {


    public static class DataBean {
        /**
         * total : null
         * remain : 0.00
         * invite_num : 0
         */

        public String remain;
        public String invite_num;
    }
}
