package com.android.manghe.mine.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.common.model.BaseBack
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.mine.adapter.VoucherAdapter
import com.android.manghe.mine.model.VoucherRes
import com.android.manghe.view.VerticalDividerItemDecoration
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_voucher.*
import kotlinx.android.synthetic.main.fragment_coin_detail_push.*
import okhttp3.Call

class MerchantVoucherFragment : MVPFragment<XPresenter<IView>>(), IRefresh {

    private var mCurrentPage = 1
    private var mDataList = arrayListOf<VoucherRes.DataBean>()
    private var mAdapter: VoucherAdapter? = null
    private var mType = 1
    private var mListener: IMerchantVoucherFragmentListener? = null

    fun setListener(listener: IMerchantVoucherFragmentListener) {
        this.mListener = listener
    }

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        loadData(true)
    }

    override fun onLoad() {
        loadData(false)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_coin_detail_push
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mType = arguments!!.getInt("type")
        mAdapter = VoucherAdapter(activity, arrayListOf<VoucherRes.DataBean>(), mType)
        recyclerView.layoutManager =
            LinearLayoutManager(activity)
        recyclerView.adapter = mAdapter
        recyclerView.addItemDecoration(VerticalDividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        loadData(true)
    }

    private fun loadData(isRefresh: Boolean) {
        if (isRefresh) {
            mCurrentPage = 1
            mDataList.clear()
        }
        val head = hashMapOf("UID" to UserHolder.getUID(activity), "TOKEN" to UserHolder.getUserInfo(activity).TOKEN)
        OKHttpUtil(activity).urlByHeadData(
            ConstantsUrl.domain + (if (mType == 1) ConstantsUrl.VOUCHER_INCOME else ConstantsUrl.VOUCHER_EXPAND)
                    + "?page=" + mCurrentPage.toString(), head
        ).get()
            .execute(object : BaseBack<VoucherRes>() {

                override fun onSuccess(call: Call, bean: VoucherRes?) {
                    if (bean != null && bean.code == 0) {
                        if(mListener != null){
                            mListener!!.showTotalVoucher(bean.remain_coupon_total.toString())
                        }
                        if(!TextUtils.isEmpty(bean.voucher_explain)){
                            tvExplain.text = bean.voucher_explain
                        }
                        if(bean.list != null) {
                            mDataList.addAll(bean.list)
                            mAdapter?.update(mDataList)
                            refreshLayout.isEnableLoadMore = mDataList.size != bean.list_total
                            mCurrentPage++
                            return
                        }
                    }
                    ToastUtil.showShort(activity, "加载失败")
                }

                override fun onFailure(e: Exception) {
                    super.onFailure(e)
                    ToastUtil.showShort(activity, "加载失败")
                }

                override fun onComplete() {

                }
            })

    }

    interface IMerchantVoucherFragmentListener {
        fun showTotalVoucher(total: String?)
    }
}