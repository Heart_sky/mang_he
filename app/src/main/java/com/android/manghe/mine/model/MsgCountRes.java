package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class MsgCountRes extends NetBean<MsgCountRes.DataBean> {
    public static class DataBean {
        public int message_unread_num;
        public int notice_unread_num;
    }
}
