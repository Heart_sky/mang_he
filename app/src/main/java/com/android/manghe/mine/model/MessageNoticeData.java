package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class MessageNoticeData extends NetBean<MessageNoticeData.DataBean> {

    public List<ListBean> list;

    public static class DataBean {

    }

    public static class ListBean {

        /**
         * id : 75
         * catid : 40
         * title : 测试测试测试测试
         * content : 测试测试测试测试
         * createtime : 2019-06-29 11:59:00
         * is_read : 1
         */

        public String id;
        public String catid;
        public String title;
        public String content;
        public String createtime;
        public int is_read;
    }


}
