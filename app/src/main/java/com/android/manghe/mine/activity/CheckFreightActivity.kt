package com.android.manghe.mine.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.mine.adapter.FreightAdapter
import com.android.manghe.mine.fragment.MerchantVoucherFragment
import com.android.manghe.mine.model.FreightInfoRes
import com.android.manghe.mine.presenter.PCheckFreight
import com.android.manghe.user.activity.LoginActivity
import com.android.manghe.view.VerticalDividerItemDecoration
import com.android.manghe.view.dialog.NewTipDialog
import kotlinx.android.synthetic.main.activity_check_freight.*

/**
 * 查看物流
 */
class CheckFreightActivity : MVPActivity<PCheckFreight>() {
    private var mAdapter: FreightAdapter? = null

    companion object {
        fun showActivity(context: Context, orderSn: String, isAuction: Boolean) {
            val intent = Intent(context, CheckFreightActivity::class.java)
            intent.putExtra("orderSn", orderSn)
            intent.putExtra("isAuction", isAuction)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_check_freight
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("物流信息").setLeftIcon(R.mipmap.icon_back_black)
            .setTitleAndStatusBgColor(R.color.white)

        if (UserHolder.getUserInfo(this) == null) {
            finish()
            open(LoginActivity::class.java)
            return
        }

        mAdapter = FreightAdapter(this, arrayListOf<FreightInfoRes.LogisticBean>())
        recyclerView.layoutManager = LinearLayoutManager(
            this
        ) as RecyclerView.LayoutManager?
        recyclerView.adapter = mAdapter
        recyclerView.addItemDecoration(
            VerticalDividerItemDecoration(
                this,
                LinearLayoutManager.VERTICAL
            )
        )

        showLoadingDialog()
        p.loadData(intent.getBooleanExtra("isAuction", false), intent.getStringExtra("orderSn"))
    }


    fun update(freightList: List<FreightInfoRes.LogisticBean>) {
        mAdapter?.let {
            it.update(freightList);
        }
    }

    fun showTopMsg(kuaidiName: String, num: String, phone: String) {
        wuliu_kuaidiname.text = kuaidiName;
        wuliu_kuaidinumber.text = num
        wuliu_kuaidiphone.text = phone
    }

    fun noFreightMsg() {
        val mDialog = NewTipDialog(this, "暂无物流信息")
        mDialog.show()
        mDialog.setSureMode()
        mDialog.setListener(object : NewTipDialog.ITipDialogListener {
            override fun clickRight() {
                finish()
            }

            override fun clickLeft() {

            }
        })


    }

}