package com.android.manghe.mine.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.main.MainActivity;
import com.android.manghe.mine.presenter.PAccountCacel;
import com.android.manghe.view.dialog.NewTipDialog;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/21 09:58
 * desc   : 注销账号
 */
public class AccountCancelActivity extends MVPActivity<PAccountCacel> {

    private TextView mTvDestroy, mTvFinish;

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("账号注销").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));

        mTvDestroy = findViewById(R.id.id_tv_destroy);
        mTvFinish = findViewById(R.id.id_tv_finish);


        mTvFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mTvDestroy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewTipDialog newTipDialog=new NewTipDialog(mContext,"您确定要注销吗？ 注销以后信息将被清空且无法被找回了哦~");
                newTipDialog.show();
                newTipDialog.setTitle("您确定要注销吗？");
                newTipDialog.setLeftText("暂不注销");
                newTipDialog.setRightText("确定注销");
                newTipDialog.setContentSizeColors(13f,R.color.colorFont99);
                newTipDialog.setRightTextColor(R.color.red);
                newTipDialog.setListener(new NewTipDialog.ITipDialogListener() {
                    @Override
                    public void clickLeft() {

                    }

                    @Override
                    public void clickRight() {
                        getP().cancellationAccount();
                    }
                });
                
//                new XPopup.Builder(mContext)
//                        .moveUpToKeyboard(false) //如果不加这个，评论弹窗会移动到软键盘上面
//                        .asCustom(new CancelllationAccountPopupView(mContext, new CancelllationAccountPopupView.ICallBack() {
//                            @Override
//                            public void onConfirm() {
//                                getP().cancellationAccount();
//                            }
//                        }))
//                        .show();

            }
        });
    }


    public void doSignOut() {
        UserHolder.clearInfo(this);
        UMShareAPI.get(mContext).deleteOauth(this, SHARE_MEDIA.WEIXIN, null);
        UMShareAPI.get(mContext).deleteOauth(this, SHARE_MEDIA.QQ, null);
        open(MainActivity.class);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_account_canel;
    }


}
