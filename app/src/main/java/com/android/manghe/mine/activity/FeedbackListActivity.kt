package com.android.manghe.mine.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.view.MVPActivity
import com.android.base.frame.view.XStateController
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.mine.adapter.FeedbackListAdapter
import com.android.manghe.mine.presenter.PFeedbackList
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_feed_back_list.*
import kotlinx.android.synthetic.main.no_data.*

class FeedbackListActivity :MVPActivity<PFeedbackList>(), IRefresh,
    IStateController<XStateController> {
    var mAdapter : FeedbackListAdapter? = null

    override fun getStateView(): XStateController {
        return findViewById(R.id.xStateController)
    }
    override fun getLayoutId(): Int {
        return R.layout.activity_feed_back_list
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("反馈记录").setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black)

        mAdapter = FeedbackListAdapter(this, UserHolder.getUID(this))
        val linearManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearManager
        recyclerView.adapter = mAdapter

        tvNoData.text = "暂无反馈记录"
        p.getData(true)
        p.readAll()
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getData(true)
    }

    override fun onLoad() {
        p.getData(false)
    }

    fun update() {
        mAdapter?.let {
            it.replaceData(p.mDataList)
            if (p.mDataList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }
}