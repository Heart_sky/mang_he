package com.android.manghe.mine.fragment

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.tools.Glide.GlideHelper
import com.android.manghe.R
import com.android.manghe.mine.model.MyKanRes
import com.android.manghe.mine.presenter.PMyKanAll
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_my_kan.*
import java.math.RoundingMode
import java.text.DecimalFormat


class MyKanAllFragment : MVPFragment<PMyKanAll>(), IRefresh {
    private var mAdapter: MyKanAdapter? = null

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }
    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun getLayoutId(): Int {
        return com.android.manghe.R.layout.fragment_my_kan
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = MyKanAdapter(mContext, R.layout.item_my_kan_all, arrayListOf())
        recyclerView.layoutManager =
            LinearLayoutManager(mContext)
        recyclerView.adapter = mAdapter
        p.setType(0)
        p.loadData(true)
    }

    override fun onRefresh() {
        p.loadData(true)
    }

    override fun onLoad() {
        p.loadData(false)
    }

    fun update(dataList: List<MyKanRes.MyKan.ListBean>) {
        mAdapter?.let {
            it.addData(dataList)
        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }


    companion object {
        class MyKanAdapter(context: Context, layoutResId: Int, data: List<MyKanRes.MyKan.ListBean>) :
            BaseQuickAdapter<MyKanRes.MyKan.ListBean, BaseViewHolder>(layoutResId, data) {
            val df = DecimalFormat("0.00")
            var context: Context? = null

            init {
                this.context = context
                df.roundingMode = RoundingMode.FLOOR
            }

            override fun convert(helper: BaseViewHolder, item: MyKanRes.MyKan.ListBean) {
                helper.setText(R.id.tvPeopleGot, "用户 " + item.username + " 已抢到")
                helper.setText(R.id.tvTitle, item.title)
                helper.setText(R.id.tvMarketPrice, "￥" + item.market_price);
                helper.setText(R.id.tvPrice, "￥" + item.price)
                helper.setText(R.id.tvTimes, "${item.auctioning_username}")
                val percent =
                    java.lang.Float.parseFloat(item.price) / java.lang.Float.parseFloat(item.market_price) * 100
                helper.setText(R.id.tvPercent, df.format((100 - percent).toDouble()))
                GlideHelper.loadAvatar(context, item.photo, helper.getView(R.id.ivAvatar))
                GlideHelper.loadRoundTrans(context, item.thumb, helper.getView(R.id.ivPic), 6)
            }
        }
    }
}