package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.PostBuilder;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.ModifyPswActivity;
import com.android.manghe.mine.model.UserRes;
import okhttp3.Call;

import java.util.HashMap;

public class PModifyPsw extends XPresenter<ModifyPswActivity> {
    public void modify(String oldPsw, String newPsw){
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        PostBuilder builder = new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERDATA_EDITPD,headMap)
                .post("oldpass",oldPsw)
                .post("pass1",newPsw)
                .post("pass2",newPsw);
        builder.build().execute(new BaseBack<NetBean>() {

            @Override
            public void onSuccess(Call call, NetBean bean) {
                if (bean.code == 0) {
                    UserRes.UserInfo userInfo = UserHolder.getUserInfo(getV());
                    userInfo.UPSW = newPsw;
                    UserHolder.setUserInfo(getV(), userInfo);
                    getV().modifyOk();
                }else{
                    getV().modifyFail(bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().modifyFail(null);
            }

            @Override
            public void onComplete() {
                getV().closeLoadingDialog();
            }
        });
    }
}
