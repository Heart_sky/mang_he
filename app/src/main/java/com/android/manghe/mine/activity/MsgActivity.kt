package com.android.manghe.mine.activity

import android.os.Bundle
import com.android.base.frame.activity.FragmentActivity
import com.android.base.frame.title.ETitleType
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.config.events.RefreshMsgCountEvent
import com.android.manghe.main.view.TabEntity
import com.android.manghe.mine.fragment.MsgFragment
import com.android.manghe.mine.fragment.MsgSystemFragment
import com.android.manghe.user.activity.LoginActivity
import com.eightbitlab.rxbus.Bus
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_msg.*

class MsgActivity : FragmentActivity() {
    private val mTabEntities = ArrayList<CustomTabEntity>()
    private val mTitles = arrayOf("消息", "公告")
    override fun getLayoutId(): Int {
        return R.layout.activity_msg
    }

    override fun showToolBarType(): ETitleType {
        return ETitleType.SIMPLE_TITLE
    }
    override fun initData(savedInstanceState: Bundle?) {
        if(UserHolder.getUserInfo(this)== null){
            finish()
            LoginActivity.showActivity(this)
            return
        }


        titleBar.setToolbar("消息中心").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)
        addFragment(
            R.id.container,
            MsgFragment::class.java,
            MsgSystemFragment::class.java
        )
        for (i in mTitles.indices) {
            mTabEntities.add(TabEntity(mTitles[i], 0, 0))
        }
        tabLayout.setTabData(mTabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabReselect(position: Int) {
            }

            override fun onTabSelect(position: Int) {
                showFragment(position)
            }
        })

    }

    override fun onStop() {
        super.onStop()
        Bus.send(RefreshMsgCountEvent())
    }

}