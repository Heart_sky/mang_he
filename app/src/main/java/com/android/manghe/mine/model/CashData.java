package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

/**
 * Created by Administrator on 2017/3/14.
 */

public class CashData extends NetBean<CashData.DataBean> {


    /**
     * data : {"list":[{"id":"2","mid":"1","username":"test01","ivt_mid":"2","ivt_username":"test02","order_id":"1191","total":"138.00","commission":"5.00","desc":"邀请会员[test02]下单(订单号2017031471015657、商品ID463),获得5元佣金(分销佣金)","addtime":"2017-03-14 11:03:33"}],"list_total":1,"commission_total":"5.00"}
     * flag : true
     * code : 0
     * msg : 操作成功
     * time : 1489462056
     */

    public static class DataBean {
        /**
         * list : [{"id":"2","mid":"1","username":"test01","ivt_mid":"2","ivt_username":"test02","order_id":"1191","total":"138.00","commission":"5.00","desc":"邀请会员[test02]下单(订单号2017031471015657、商品ID463),获得5元佣金(分销佣金)","addtime":"2017-03-14 11:03:33"}]
         * list_total : 1
         * commission_total : 5.00
         */

        public int list_total;
        public String commission;
        public String commission_total;
        public List<ListBean> list;

        public static class ListBean {
            /**
             * id : 2
             * mid : 1
             * username : test01
             * ivt_mid : 2
             * ivt_username : test02
             * order_id : 1191
             * total : 138.00
             * commission : 5.00
             * desc : 邀请会员[test02]下单(订单号2017031471015657、商品ID463),获得5元佣金(分销佣金)
             * addtime : 2017-03-14 11:03:33
             */

            public String id;
            public String mid;
            public String username;
            public String ivt_mid;
            public String ivt_username;
            public String order_id;
            public String total;
            public String commission;
            public String desc;
            public String addtime;
            public String whereFrom;

        }
    }
}
