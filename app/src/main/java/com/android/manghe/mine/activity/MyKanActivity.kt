package com.android.manghe.mine.activity

import android.os.Bundle
import android.widget.RelativeLayout
import com.android.base.frame.activity.FragmentActivity
import com.android.base.tools.DisplayUtil
import com.android.base.tools.StatusBarUtil
import com.android.manghe.R
import com.android.manghe.main.view.TabEntity
import com.android.manghe.mine.fragment.MyKanDaoFragment
import com.android.manghe.mine.fragment.MyKanNotFragment
import com.android.manghe.mine.fragment.MyKanWillPayFragment
import com.android.manghe.mine.fragment.MyKaningFragment
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_my_kan.*

/**
 * 萌象订单
 * @author ZhangWeiJun
 * @date 2019/6/30
 */

class MyKanActivity : FragmentActivity() {
    private val mTabEntities = ArrayList<CustomTabEntity>()
    private val mTitles = arrayOf("已砍中", "正在砍", "未砍中", "待付款")

    override fun getLayoutId(): Int {
        return R.layout.activity_my_kan
    }

    override fun initData(savedInstanceState: Bundle?) {
        val lp = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, DisplayUtil.dip2px(this, 48f))
        lp.topMargin = StatusBarUtil.getStatusBarHeight(this)
        layoutTitleBar.layoutParams = lp
        ivBack.setOnClickListener { finish() }
        addFragment(
            R.id.container,
            MyKanDaoFragment::class.java,
            MyKaningFragment::class.java,
            MyKanNotFragment::class.java,
            MyKanWillPayFragment::class.java
        )
        for (i in mTitles.indices) {
            mTabEntities.add(TabEntity(mTitles[i], 0, 0))
        }
        tabLayout.setTabData(mTabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabReselect(position: Int) {
            }

            override fun onTabSelect(position: Int) {
                showFragment(position)
            }
        })

        val initPagePosition = intent.getIntExtra("position",0)
        tabLayout.currentTab = initPagePosition
        showFragment(initPagePosition)
    }
}