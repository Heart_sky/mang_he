package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.CheckFreightActivity;
import com.android.manghe.mine.model.FreightInfoRes;

import java.util.HashMap;

import okhttp3.Call;

public class PCheckFreight extends XPresenter<CheckFreightActivity> {


    public void loadData(boolean isAction, String orderSN) {
        HashMap<String, String> data = new HashMap<>();
        data.put("order_sn", orderSN + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + (isAction ? ConstantsUrl.ACTION_ORDERSHIP : ConstantsUrl.BOx_ORDER_SHIP), data, headMap)
                .get()
                .execute(new BaseBack<FreightInfoRes>() {

                    @Override
                    public void onSuccess(Call call, FreightInfoRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (null == res.data || null == res.data.wuliu || res.data.wuliu.size() == 0) {
                                getV().noFreightMsg();
                                return;
                            }
                            getV().showTopMsg(res.data.express, res.data.express_num, res.data.contact_mobile);
                            if (res.data.wuliu != null) {
                                getV().update(res.data.wuliu);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
