package com.android.manghe.mine.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.DeliverIntegralActivity;
import com.android.manghe.mine.model.AddressInfo;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/30 15:55
 * desc   :
 */
public class PDeliverIntegral extends XPresenter<DeliverIntegralActivity> {


    public AddressInfo currentAddress;

    public void setCurrentAddress(AddressInfo currentAddress) {
        this.currentAddress = currentAddress;
    }

    public void setIntegralSent(String good_id, String address_id, int num) {
        HashMap<String, String> data = new HashMap<>();
        data.put("goods_id", good_id);
        data.put("address_id", address_id + "");
        data.put("num", num + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_SCORE_EXCHANGE, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            getV().setGoodsSuccess();
                        } else {
                            getV().showExchangeErrorTip();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showExchangeErrorTip();
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }


    public void getAddressList() {
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERADDRESSLIST, data, headMap)
                .get()
                .execute(new GsonBaseBack<AddressInfo>() {

                    @Override
                    public void onSuccess(Call call, AddressInfo addressInfo) {
                        getV().hideLoadingDialog();
                        if (addressInfo != null && addressInfo.code == 0 && addressInfo.data != null && !addressInfo.data.isEmpty()) {
                            for (AddressInfo addr : addressInfo.data) {
                                if (TextUtils.equals(addr.is_default, "1")) {
                                    getV().showAddress(addr);
                                    return;
                                }
                            }
                            getV().showAddress(addressInfo.data.get(0));
                        } else {
                            getV().showAddress(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showAddress(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }



}
