package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

public class UserDetailRes extends NetBean<UserDetailRes.UserDetail> implements Serializable {


    public class UserDetail implements Serializable {
        public String mid;
        public String password;
        public String pay_password;
        public String salt;
        public String photo;
        public String nickname;
        public String realname;
        public Object idcard;

        public String rank_id;
        public String email;
        public String verify_email;
        public String address;
        public String birthday;
        public String sex;
        public String ip;
        public String lastip;
        public String login_time;
        public String login;
        public String lastlogin;
        public String c_time;
        public Object intro;
        public String qq;
        public String wx;
        public Object mshop_img;
        public Object mshop_name;
        public Object mshop_notice;
        public Object mshop_share;
        public Object good_share;
        public String mobile;
        public String free_coin;
        public String coin;
        public String score;
        public String all_commission;
        public String today_commission;
        public String username;
    }
}
