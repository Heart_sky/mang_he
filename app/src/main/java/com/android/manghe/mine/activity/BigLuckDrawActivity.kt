package com.android.manghe.mine.activity

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import com.android.base.tools.StatusBarUtil
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import com.android.manghe.common.frame.activity.MVPNoTitleActivity
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.mine.presenter.PBigLuckDraw
import com.android.manghe.view.luckdraw.BigLuckPanLayout
import com.android.manghe.zhuli.activity.WebTextActivity
import kotlinx.android.synthetic.main.activity_big_luck_draw.*
import kotlinx.android.synthetic.main.activity_big_luck_draw.go
import kotlinx.android.synthetic.main.activity_big_luck_draw.luckPanLayout

class BigLuckDrawActivity : MVPNoTitleActivity<PBigLuckDraw>() {

    companion object{
        fun showActivity(context : Context){
            context.startActivity(Intent(context, BigLuckDrawActivity::class.java))
        }
    }
    override fun getLayoutId(): Int = R.layout.activity_big_luck_draw


    override fun initData(savedInstanceState: Bundle?) {
        StatusBarUtil.immersive(this)
        tvRule.setOnClickListener {
            //活动规则
            WebTextActivity.showActivity(this, "活动规则", ConstantsUrl.domain + ConstantsUrl.BigLuckDrawRule)
        }
        ivBack.setOnClickListener {
            finish()
        }
        initZhuanPan()
        p.loadData()
        p.loadLuckDrawConfig()
    }

    private fun initZhuanPan(){
        go.setOnClickListener {
            if(p.everyConsume != 0) {
                if (p.remainScore < p.everyConsume) {
                    ToastUtil.showShort(this, "积分余额不足")
                    return@setOnClickListener
                }
                luckPanLayout.rotate(1, 100)
                p.luckDrawConfirm()
            }
        }
        luckPanLayout.animationEndListener = BigLuckPanLayout.AnimationEndListener {
            ToastUtil.showShort(this@BigLuckDrawActivity, "很遗憾！祝您下次好运")
        }
    }

    fun updateScore(everyConsume: Int, remainScore : Int){
        tvPerScore.text = "${everyConsume}积分抽奖一次"
        tvTotalScore.text = "积分剩余 $remainScore"
    }

    fun updateDesc(descHtml :String){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvDesc.text = Html.fromHtml(descHtml, Html.FROM_HTML_MODE_COMPACT)
        } else {
            tvDesc.text = Html.fromHtml(descHtml)
        }
    }


}