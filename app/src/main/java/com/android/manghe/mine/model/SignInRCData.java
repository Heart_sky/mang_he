package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class SignInRCData extends NetBean<SignInRCData.DataBean> {
    public static class DataBean {
        public String mid;
        public String total_left;
        public String total_amount;
        public String total_cost;
        public int signed;
        public String last_sign;
        public String continue_sign;
        public String score_0;
        public String score_1;
        public String score_2;
        public String score_3;
        public String score_4;
        public String score_5;
        public String u_time;
        public String today_sign_score;
        public String rule;
        public String explain;
    }
}
