package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.CoinAndIntegralDetailActivity;
import com.android.manghe.mine.model.CoinAndIntegralModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/14 16:12
 * desc   :
 */
public class PCoinAndIntegralDetail extends XPresenter<CoinAndIntegralDetailActivity> {


    private int currentPage = 1;
    private final String PageSize = "10";
    private int mType;//币种;0.盲盒币;1.赠币;
    private String url;//访问接口
    private int status;///交易类型;1.支出;2.收入;

    private List<CoinAndIntegralModel.DataBean.ListBean> CoinAndIntegralList = new ArrayList<>();

    public void setStatus(int status) {
        this.status = status;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 获取赠币和开盒币
     *
     * @param isRefresh
     */
    public void getCoinList(boolean isRefresh) {
        //获取精选
        if (isRefresh) {
            currentPage = 1;
            CoinAndIntegralList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        data.put("type", mType + "");
        data.put("status", status + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + url, data, headMap)
                .get()
                .execute(new BaseBack<CoinAndIntegralModel>() {
                    @Override
                    public void onSuccess(Call call, CoinAndIntegralModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                CoinAndIntegralList.addAll(res.data.list);
                                getV().setCanLoadMore(CoinAndIntegralList.size() != res.data.list_total);
                                getV().updateList(CoinAndIntegralList,status);
                                if (CoinAndIntegralList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateList(CoinAndIntegralList,status);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }
                });
    }


    /**
     * 获取积分
     *
     * @param isRefresh
     */
    public void getIntegralList(boolean isRefresh) {
        //获取精选
        if (isRefresh) {
            currentPage = 1;
            CoinAndIntegralList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        data.put("status", status + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + url, data, headMap)
                .get()
                .execute(new BaseBack<CoinAndIntegralModel>() {
                    @Override
                    public void onSuccess(Call call, CoinAndIntegralModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                CoinAndIntegralList.addAll(res.data.list);
                                getV().setCanLoadMore(CoinAndIntegralList.size() != res.data.list_total);
                                getV().updateList(CoinAndIntegralList,status);
                                if (CoinAndIntegralList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateList(CoinAndIntegralList,status);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }
                });
    }
}





