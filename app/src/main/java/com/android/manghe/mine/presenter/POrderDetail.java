package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.OrderDetailActivity;
import com.android.manghe.mine.model.OrderDetailModel;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/02 13:46
 * desc   :
 */
public class POrderDetail extends XPresenter<OrderDetailActivity> {


    public OrderDetailModel.DataBean orderDetail;


    public void getOrderDetail(String orderSN) {
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("order_sn", orderSN);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.BOx_ORDER_DETAIL, data, headMap)
                .get()
                .execute(new BaseBack<OrderDetailModel>() {

                    @Override
                    public void onSuccess(Call call, OrderDetailModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0 && res.data != null) {
                            orderDetail = res.data;
                            getV().update(res.data);
                        } else {
                            getV().showErrorTip();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        getV().hideLoadingDialog();
                        super.onFailure(e);
                        getV().showErrorTip();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 确认收货
     *
     * @param order_sn
     */
    public void ConfirmReceive(String order_sn) {
        HashMap<String, String> data = new HashMap<>();
        data.put("order_sn", order_sn);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.BOx_CONFIRM_RECEIVE, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            getV().receiveOk();
                        } else {
                            getV().showConfirmReceiveErrorTip();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showConfirmReceiveErrorTip();
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

}
