package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/01 15:22
 * desc   :
 */
public class AllStateOrderModel extends NetBean<AllStateOrderModel.DataBean> {


    /**
     * data : {"list_total":15,"list":[{"id":"78794","mid":"911730","order_id":"72481","goods_id":"2140","goods_name":"红牡丹 骨瓷餐具套装陶瓷碗碟盘套装碗具6-8人用碗筷套装风华正茂 28件 方形配置","buy_num":"10","sell_price":"479.00","score_price":"56","c_time":"2021-11-30 17:28:22","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021113017282253549910_78794","order_type":"2","goods_thumb":"https://www.mxmd88.com/upload/1/images/211128/e8d2f516bc868e6e.jpg","express":"","expend_score":"560"},{"id":"78793","mid":"911730","order_id":"72480","goods_id":"2142","goods_name":"THERMOS膳魔师焖烧罐470ml高真空不锈钢保温饭盒保温桶SK-3000 PK 【京东专供】","buy_num":"1","sell_price":"678.00","score_price":"23","c_time":"2021-11-30 17:24:14","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021113017241452565053_78793","order_type":"2","goods_thumb":"https://www.mxmd88.com/upload/1/images/211128/f959b194a75e4cfb.jpg","express":"","expend_score":"23"},{"id":"78792","mid":"911730","order_id":"72479","goods_id":"2146","goods_name":"千丝抹茶绿豆饼 整箱500g早餐美食面包糕点心学生宿舍速食充饥小吃休闲零食品【每】 抹茶绿豆饼整箱500g","buy_num":"1","sell_price":"7.80","score_price":"66","c_time":"2021-11-30 17:10:46","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021113017104648575351_78792","order_type":"2","goods_thumb":"https://www.mxmd88.com/upload/1/images/211129/48730a1b19a8b504.jpg","express":"","expend_score":"66"},{"id":"78791","mid":"911730","order_id":"72478","goods_id":"2135","goods_name":"洋河蓝色经典 梦之蓝M6 52度 礼盒装 500ml*2瓶高度白酒 口感绵柔浓香型","buy_num":"1","sell_price":"1606.00","score_price":"0","c_time":"2021-11-29 17:55:59","express_id":"25","express_no":"ytytytyt","shipping_time":"2021-11-30 08:50:21","shipping_status":"1","order_sn":"2021112917555952991005_78791","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/211103/5ab4cb3bN0943eaf5.jpg","express":"圆通速递","expend_score":"0"},{"id":"78790","mid":"911730","order_id":"72478","goods_id":"2133","goods_name":"三只松鼠巨型零食大礼包/内含30包 送礼送女友礼物芒果干网红薯片饼干锅巴辣条肉干肉脯/6斤装","buy_num":"1","sell_price":"228.00","score_price":"0","c_time":"2021-11-29 17:55:59","express_id":"20","express_no":"jdjdjdjdjdjd","shipping_time":"2021-11-30 08:50:11","shipping_status":"1","order_sn":"2021112917555952991005_78790","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/a/29171_src.jpg","express":"京东快递","expend_score":"0"},{"id":"78789","mid":"911730","order_id":"72478","goods_id":"2132","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","buy_num":"1","sell_price":"49.00","score_price":"0","c_time":"2021-11-29 17:55:59","express_id":"10","express_no":"xxxxxxxx","shipping_time":"2021-11-30 08:49:57","shipping_status":"1","order_sn":"2021112917555952991005_78789","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png","express":"顺丰速递","expend_score":"0"},{"id":"78788","mid":"911730","order_id":"72477","goods_id":"2132","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","buy_num":"1","sell_price":"49.00","score_price":"0","c_time":"2021-11-29 17:16:15","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021112917161553102509_78788","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png","express":"","expend_score":"0"},{"id":"78787","mid":"911730","order_id":"72476","goods_id":"2132","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","buy_num":"1","sell_price":"49.00","score_price":"0","c_time":"2021-11-29 16:10:26","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"_78787","order_type":null,"goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png","express":"","expend_score":null},{"id":"78786","mid":"911730","order_id":"72475","goods_id":"2127","goods_name":"MC V-196 电脑音响音箱家用桌面台式机超重低音炮USB影响长条双喇叭笔记本迷你手机小钢炮大音量 黑色经典版（有线）","buy_num":"1","sell_price":"59.00","score_price":"0","c_time":"2021-11-29 16:10:26","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"_78786","order_type":null,"goods_thumb":"https://www.mxmd88.com/upload/1/images/211103/f804b7341b8b9ec4.jpg","express":"","expend_score":null},{"id":"78785","mid":"911730","order_id":"72474","goods_id":"2123","goods_name":"KOMERY数码相机4800万高清像素16倍4K录像复古单反学生数码照相机自拍无线传输 黑色+52MM广角镜 套餐一","buy_num":"1","sell_price":"448.00","score_price":"0","c_time":"2021-11-29 16:09:19","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"_78785","order_type":null,"goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/0/29161_src.jpg","express":"","expend_score":null}]}
     * flag : true
     */
    public static class DataBean implements Serializable {
        /**
         * list_total : 15
         * list : [{"id":"78794","mid":"911730","order_id":"72481","goods_id":"2140","goods_name":"红牡丹 骨瓷餐具套装陶瓷碗碟盘套装碗具6-8人用碗筷套装风华正茂 28件 方形配置","buy_num":"10","sell_price":"479.00","score_price":"56","c_time":"2021-11-30 17:28:22","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021113017282253549910_78794","order_type":"2","goods_thumb":"https://www.mxmd88.com/upload/1/images/211128/e8d2f516bc868e6e.jpg","express":"","expend_score":"560"},{"id":"78793","mid":"911730","order_id":"72480","goods_id":"2142","goods_name":"THERMOS膳魔师焖烧罐470ml高真空不锈钢保温饭盒保温桶SK-3000 PK 【京东专供】","buy_num":"1","sell_price":"678.00","score_price":"23","c_time":"2021-11-30 17:24:14","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021113017241452565053_78793","order_type":"2","goods_thumb":"https://www.mxmd88.com/upload/1/images/211128/f959b194a75e4cfb.jpg","express":"","expend_score":"23"},{"id":"78792","mid":"911730","order_id":"72479","goods_id":"2146","goods_name":"千丝抹茶绿豆饼 整箱500g早餐美食面包糕点心学生宿舍速食充饥小吃休闲零食品【每】 抹茶绿豆饼整箱500g","buy_num":"1","sell_price":"7.80","score_price":"66","c_time":"2021-11-30 17:10:46","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021113017104648575351_78792","order_type":"2","goods_thumb":"https://www.mxmd88.com/upload/1/images/211129/48730a1b19a8b504.jpg","express":"","expend_score":"66"},{"id":"78791","mid":"911730","order_id":"72478","goods_id":"2135","goods_name":"洋河蓝色经典 梦之蓝M6 52度 礼盒装 500ml*2瓶高度白酒 口感绵柔浓香型","buy_num":"1","sell_price":"1606.00","score_price":"0","c_time":"2021-11-29 17:55:59","express_id":"25","express_no":"ytytytyt","shipping_time":"2021-11-30 08:50:21","shipping_status":"1","order_sn":"2021112917555952991005_78791","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/211103/5ab4cb3bN0943eaf5.jpg","express":"圆通速递","expend_score":"0"},{"id":"78790","mid":"911730","order_id":"72478","goods_id":"2133","goods_name":"三只松鼠巨型零食大礼包/内含30包 送礼送女友礼物芒果干网红薯片饼干锅巴辣条肉干肉脯/6斤装","buy_num":"1","sell_price":"228.00","score_price":"0","c_time":"2021-11-29 17:55:59","express_id":"20","express_no":"jdjdjdjdjdjd","shipping_time":"2021-11-30 08:50:11","shipping_status":"1","order_sn":"2021112917555952991005_78790","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/a/29171_src.jpg","express":"京东快递","expend_score":"0"},{"id":"78789","mid":"911730","order_id":"72478","goods_id":"2132","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","buy_num":"1","sell_price":"49.00","score_price":"0","c_time":"2021-11-29 17:55:59","express_id":"10","express_no":"xxxxxxxx","shipping_time":"2021-11-30 08:49:57","shipping_status":"1","order_sn":"2021112917555952991005_78789","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png","express":"顺丰速递","expend_score":"0"},{"id":"78788","mid":"911730","order_id":"72477","goods_id":"2132","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","buy_num":"1","sell_price":"49.00","score_price":"0","c_time":"2021-11-29 17:16:15","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021112917161553102509_78788","order_type":"1","goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png","express":"","expend_score":"0"},{"id":"78787","mid":"911730","order_id":"72476","goods_id":"2132","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","buy_num":"1","sell_price":"49.00","score_price":"0","c_time":"2021-11-29 16:10:26","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"_78787","order_type":null,"goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png","express":"","expend_score":null},{"id":"78786","mid":"911730","order_id":"72475","goods_id":"2127","goods_name":"MC V-196 电脑音响音箱家用桌面台式机超重低音炮USB影响长条双喇叭笔记本迷你手机小钢炮大音量 黑色经典版（有线）","buy_num":"1","sell_price":"59.00","score_price":"0","c_time":"2021-11-29 16:10:26","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"_78786","order_type":null,"goods_thumb":"https://www.mxmd88.com/upload/1/images/211103/f804b7341b8b9ec4.jpg","express":"","expend_score":null},{"id":"78785","mid":"911730","order_id":"72474","goods_id":"2123","goods_name":"KOMERY数码相机4800万高清像素16倍4K录像复古单反学生数码照相机自拍无线传输 黑色+52MM广角镜 套餐一","buy_num":"1","sell_price":"448.00","score_price":"0","c_time":"2021-11-29 16:09:19","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"_78785","order_type":null,"goods_thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/0/29161_src.jpg","express":"","expend_score":null}]
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean implements Serializable {
            /**
             * id : 78794
             * mid : 911730
             * order_id : 72481
             * goods_id : 2140
             * goods_name : 红牡丹 骨瓷餐具套装陶瓷碗碟盘套装碗具6-8人用碗筷套装风华正茂 28件 方形配置
             * buy_num : 10
             * sell_price : 479.00
             * score_price : 56
             * c_time : 2021-11-30 17:28:22
             * express_id : null
             * express_no : null
             * shipping_time :
             * shipping_status : 0
             * order_sn : 2021113017282253549910_78794
             * order_type : 2
             * goods_thumb : https://www.mxmd88.com/upload/1/images/211128/e8d2f516bc868e6e.jpg
             * express :
             * expend_score : 560
             */

            public String id;
            public String mid;
            public String order_id;
            public String goods_id;
            public String goods_name;
            public String buy_num;
            public String sell_price;
            public String score_price;
            public String c_time;
            public Object express_id;
            public Object express_no;
            public String shipping_time;
            public String shipping_status;
            public String order_sn;
            public String order_type;
            public String goods_thumb;
            public String express;
            public String expend_score;
            public String remark;
            public String is_rate;//0.未评价;1.已评价;
        }
    }
}
