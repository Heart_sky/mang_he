package com.android.manghe.mine.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.frame.view.XStateController
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.mine.adapter.MsgNoticeAdapter
import com.android.manghe.mine.model.MessageNoticeData
import com.android.manghe.mine.presenter.PMsgSystem
import com.android.manghe.view.dialog.TipDialog
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_level_fans.recyclerView
import kotlinx.android.synthetic.main.fragment_level_fans.refreshLayout
import kotlinx.android.synthetic.main.fragment_msg.*
import kotlinx.android.synthetic.main.no_data.*
import java.lang.Exception

class MsgSystemFragment : MVPFragment<PMsgSystem>() , IRefresh, IStateController<XStateController> {
    private var mAdapter: MsgNoticeAdapter? = null

    override fun getStateView(): XStateController {
        return mView.findViewById(R.id.xStateController)
    }


    override fun showToolBarType(): ETitleType {
        return ETitleType.OVERLAP_TITLE
    }
    override fun getLayoutId(): Int {
        return R.layout.fragment_msg
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = MsgNoticeAdapter(activity, arrayListOf())
        val linearManager =
            LinearLayoutManager(activity)
        recyclerView.layoutManager = linearManager
        recyclerView.addItemDecoration(RecyclerViewDivider(mContext, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = mAdapter
        mAdapter!!.setOnItemClickListener{
                _,item->
            TipDialog(activity!!, item.title, item.content).show()
            p.setRead(item.id)
        }
        tvNoData.text = "暂无消息"
        p.getData(true)
    }
    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getData(true)
    }

    override fun onLoad() {
        p.getData(false)
    }

    fun update(fansList: List<MessageNoticeData.ListBean>){
        try {
            mAdapter?.let {
                it.update(fansList)
                if (fansList.isEmpty()) {
                    xStateController.showEmpty()
                } else {
                    xStateController.showContent()
                }
            }
        }catch (e: Exception){}
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }

}