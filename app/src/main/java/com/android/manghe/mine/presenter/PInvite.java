package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.fragment.InviteFragment;
import com.android.manghe.mine.model.FansRes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/19 16:58
 * desc   :
 */
public class PInvite extends XPresenter<InviteFragment> {

    private int mCurrentPage = 1;
    private final String PageSize = "10";
    private final List<FansRes.DataBean.ListBean> mAuctionList = new ArrayList<>();


    public void getData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mAuctionList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("size", PageSize);
//        data.put("level", mLevel);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERFANS, data, headMap)
                .get()
                .execute(new BaseBack<FansRes>() {

                    @Override
                    public void onSuccess(Call call, FansRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0 && res.data != null) {
                            mAuctionList.addAll(res.data.list);
                            getV().setCanLoadMore(res.data.list.size() == 10);
                            getV().update(mAuctionList);
                            if (mAuctionList.size() == 10) {
                                mCurrentPage++;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showErrorTip();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
