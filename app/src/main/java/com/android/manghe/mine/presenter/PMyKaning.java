package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.tools.EmptyUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.AuctionSocketReq;
import com.android.manghe.common.model.AuctionSocketRes;
import com.android.manghe.mine.fragment.MyKaningFragment;
import com.android.manghe.mine.model.UserRes;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PMyKaning extends XPresenter<MyKaningFragment> {
    public List<AuctionSocketRes.ListBean> mAuctionList;
    private Timer timer;

    public void loadData() {
        UserRes.UserInfo userInfo = UserHolder.getUserInfo(getV().getActivity().getApplicationContext());
        if (timer == null && userInfo != null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    AuctionSocketReq auction = new AuctionSocketReq();
                    auction.type = "get_auctions";
                    auction.mid = Integer.parseInt(userInfo.UID);
                    auction.page = 1;
                    auction.per_page = 20;
                    String sendJson = new Gson().toJson(auction);
                    getV().sendText(sendJson);
                }
            }, 0, 1000);
        }
    }

    public void stopSocketTimer(){
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }

    public void dealSocketData(String json) {
        if (!EmptyUtil.check(json)) {
            if (json.contains("get_auctions")) {
                AuctionSocketRes socketRes = new Gson().fromJson(json, AuctionSocketRes.class);
                if (!EmptyUtil.check(socketRes)) {
                    mAuctionList = socketRes.list != null ? socketRes.list : new ArrayList<>();
                    getV().update(mAuctionList);
                }
            }
        }
    }

}
