package com.android.manghe.mine.activity

import android.os.Bundle
import androidx.viewpager.widget.PagerAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.IntentController
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.mine.adapter.CustomerListAdapter
import com.android.manghe.mine.model.CustomerContentLevel
import com.android.manghe.mine.model.CustomerTitleLevel
import com.android.manghe.mine.model.CustomerRes
import com.android.manghe.mine.presenter.PCustomerService
import com.chad.library.adapter.base.entity.MultiItemEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import com.meiqia.meiqiasdk.util.MQIntentBuilder
import kotlinx.android.synthetic.main.activity_customer_service.*
import java.util.HashMap
import java.util.regex.Pattern

class CustomerServiceActivity : MVPActivity<PCustomerService>() {

    private var businessTel = ""

    private fun isStartWithNumber(str: String):Boolean {
        val pattern = Pattern.compile("[0-9]*")
        val isNum = pattern.matcher(str[0]+"")
        if (!isNum.matches()) {
            return false
        }
        return true
    }
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("客服中心").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)

        tvPhone.setOnClickListener {
            if(!businessTel.isNullOrEmpty() && isStartWithNumber(businessTel)){
                startActivity(IntentController.getTelIntent(businessTel))
            }
        }
        tvCustomer.setOnClickListener {
//            WebViewActivity.showActivity(
//                mContext, WebViewParameter.WebParameterBuilder()
//                    .setUrl(ConstantsUrl.host + ConstantsUrl.CHAT_WEB)
//                    .setShowProgress(true)
//                    .setCanHistoryGoBackOrForward(true)
//                    .setReloadable(true)
//                    .setReloadType(ERefreshWebType.ClickRefresh)
//                    .setCustomTitle("客服")
//                    .build())
            val userInfo = UserHolder.getUserInfo(this@CustomerServiceActivity)
            userInfo?.let {
                val clientInfo = HashMap<String, String>();
                clientInfo.put("mid", it.UID);
                clientInfo.put("nickName", it.nickname);
                clientInfo.put("avatar", it.avatar);
                startActivity(
                    MQIntentBuilder(this@CustomerServiceActivity)
                        .setClientInfo(clientInfo) // 设置顾客信息 PS: 这个接口只会生效一次,如果需要更新顾客信息,需要调用更新接口
                        .build()
                );
            }
        }

        recyclerView.layoutManager =
            LinearLayoutManager(this)
        p.getData()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_customer_service
    }
    fun update(customerList: List<CustomerRes.CatlistBean>, tel: String) {
        tvPhone.text = "电话：$tel"
        this.businessTel = tel
        tempViewPager.adapter = object : PagerAdapter() {
            override fun isViewFromObject(p0: View, p1: Any): Boolean {
                return p0 == p1
            }

            override fun getCount(): Int {
                return customerList.size
            }

            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val view = View(this@CustomerServiceActivity)
                container.addView(view)
                return view
            }

            override fun getPageTitle(position: Int): CharSequence? {
                return customerList[position].catname
            }

            override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
                container.removeView(`object` as View)
            }
        }
        tabLayout.setViewPager(tempViewPager)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                showList(customerList[position].list)
            }

            override fun onTabReselect(position: Int) {

            }

        })
        if (customerList.isNotEmpty())
            showList(customerList[0].list)
    }

    private fun showList(dataList: List<CustomerRes.CatlistBean.ListBean>) {
        val itemEntityList = java.util.ArrayList<MultiItemEntity>()

        for (data in dataList) {
            val titleLevel = CustomerTitleLevel(data.title)
            titleLevel.addSubItem(CustomerContentLevel(data.content))
            itemEntityList.add(titleLevel)
        }
        recyclerView.adapter = CustomerListAdapter(itemEntityList)
    }
}