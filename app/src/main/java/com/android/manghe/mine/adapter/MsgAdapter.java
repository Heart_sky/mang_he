package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.MessageIndexData;

import java.util.List;

public class MsgAdapter extends BaseRecyclerViewAdapter<MessageIndexData.ListBean, MsgAdapter.MsgHolder> {
    private Context mContext;

    public MsgAdapter(Context context, List<MessageIndexData.ListBean> list) {
        super(list);
        mContext = context;
    }

    public void update(List<MessageIndexData.ListBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(MsgHolder holder, MessageIndexData.ListBean item) {
        holder.setText(R.id.tvTitle, item.message_title);
        holder.setText(R.id.tvTime, item.create_time);
        holder.getView(R.id.tvPoint).setVisibility(item.status == 2 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public MsgHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new MsgHolder(inflateItemView(viewGroup, R.layout.item_msg));
    }

    public class MsgHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public MsgHolder(View itemView) {
            super(itemView);
        }
    }
}
