package com.android.manghe.mine.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.frame.view.XStateController
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.common.model.BaseBack
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.mine.adapter.CoinDetailAdapter
import com.android.manghe.mine.model.CoinDetailRes
import com.android.manghe.view.VerticalDividerItemDecoration
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_coin_detail_push.*
import okhttp3.Call

class CoinDetailFragment : MVPFragment<XPresenter<IView>>(), IRefresh , IStateController<XStateController> {
    override fun getStateView(): XStateController {
        return mView.findViewById(R.id.xStateController)
    }
    private var mCurrentPage = 1
    private var mDataList = arrayListOf<CoinDetailRes.CoinDetail>()
    private var mAdapter: CoinDetailAdapter? = null
    private var mType = 1


    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        loadData(true)
    }

    override fun onLoad() {
        loadData(false)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_coin_detail_push
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mType = arguments!!.getInt("type")
        mAdapter = CoinDetailAdapter(activity, arrayListOf<CoinDetailRes.CoinDetail>(), mType)
        recyclerView.layoutManager =
            LinearLayoutManager(activity)
        recyclerView.adapter = mAdapter
        recyclerView.addItemDecoration(VerticalDividerItemDecoration(activity, LinearLayoutManager.VERTICAL))
        loadData(true)
    }

    private fun loadData(isRefresh: Boolean) {
        if (isRefresh) {
            mCurrentPage = 1
            mDataList.clear()
        }
        val head = hashMapOf("UID" to UserHolder.getUID(activity), "TOKEN" to UserHolder.getUserInfo(activity).TOKEN)
        OKHttpUtil(activity).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.EXEMPT_AMOUNT_LOG
            +"?page="+mCurrentPage.toString()+"&type="+mType.toString(), head).get()
            .execute(object : BaseBack<CoinDetailRes>() {

                override fun onSuccess(call: Call, bean: CoinDetailRes?) {
                    if (bean != null && bean.code == 0 && bean.list != null) {
                        mDataList.addAll(bean.list)
                        mAdapter?.update(mDataList)

                        if (mDataList.isEmpty()) {
                            xStateController.showEmpty()
                        } else {
                            xStateController.showContent()
                        }

                        refreshLayout.isEnableLoadMore = mDataList.size != bean.list_total
                        mCurrentPage++
                    } else {
                        ToastUtil.showShort(activity, "加载失败")
                    }
                }

                override fun onFailure(e: Exception) {
                    super.onFailure(e)
                    ToastUtil.showShort(activity, "加载失败")
                }

                override fun onComplete() {

                }
            })
    }
}