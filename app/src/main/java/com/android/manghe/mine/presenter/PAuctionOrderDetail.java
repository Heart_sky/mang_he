package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.common.model.PaymentTypeRes;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshOrdersEvent;
import com.android.manghe.mine.activity.AuctionOrderDetailActivity;
import com.android.manghe.orderpay.model.AuctionOrderInfoRes;
import com.eightbitlab.rxbus.Bus;
import okhttp3.Call;

import java.util.HashMap;

public class PAuctionOrderDetail extends XPresenter<AuctionOrderDetailActivity> {

    public String orderId;
    public AuctionOrderInfoRes.DataBean orderDetail;
    public PaymentTypeRes.DataBean.PaymentBean selectedPayBean;

    public void setPayType(PaymentTypeRes.DataBean.PaymentBean selectedPayBean) {
        this.selectedPayBean = selectedPayBean;
    }

    public void getOrderDetail(boolean isShowLoading){
        if(isShowLoading)
            getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("order_id", orderId);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.AUCTIONORDERDETAIL, data, headMap)
                .get()
                .execute(new BaseBack<AuctionOrderInfoRes>() {

                    @Override
                    public void onSuccess(Call call, AuctionOrderInfoRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0 && res.data != null) {
                            orderDetail = res.data;
                            getV().update(res.data);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        getV().hideLoadingDialog();
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    /**
     * 确认收货
     */
    public void confirmGetGood(){
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("order_id", orderId);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.AUCTIONMAKESURE, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            Bus.INSTANCE.send(new RefreshOrdersEvent());
                            ToastUtil.showShort(getV(), "确认收货成功");
                        }else{
                            ToastUtil.showShort(getV(), "确认收货失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        ToastUtil.showShort(getV(), "确认收货失败");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
