package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.fragment.ToGetOrderFragment;
import com.android.manghe.mine.model.DirectBuyOrderList;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PToGetOrder extends XPresenter<ToGetOrderFragment> {
    private int PageSize = 10;
    private int mCurrentPage = 1;
    private int mType = 0;
    private List<DirectBuyOrderList.DataBean.ListBean> mAuctionList = new ArrayList<>();

    public void setType(int type) {
        mType = type;
    }

    public void loadData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mAuctionList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("status", mType + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERORDER, data, headMap)
                .get()
                .execute(new BaseBack<DirectBuyOrderList>() {

                    @Override
                    public void onSuccess(Call call, DirectBuyOrderList res) {
                        if (res != null && res.code == 0) {
                            mAuctionList.addAll(res.data.list);
                            getV().setCanLoadMore(mAuctionList.size() >= PageSize);
                            getV().update(mAuctionList);
                            mCurrentPage++;
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    public void confirmOrder(String orderId){
        HashMap<String, String> data = new HashMap<>();
        data.put("id", orderId);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERORDERQUERENSHOUHUO, data, headMap)
                .get()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            ToastUtil.showShort(getV().getActivity(), "确认收货成功");
                            getV().confirmOrderOk();
                        }else{
                            ToastUtil.showShort(getV().getActivity(), "确认收货失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        ToastUtil.showShort(getV().getActivity(), "确认收货失败");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
