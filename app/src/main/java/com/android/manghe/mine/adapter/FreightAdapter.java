package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.FreightInfoRes;

import java.util.List;

public class FreightAdapter extends BaseRecyclerViewAdapter<FreightInfoRes.LogisticBean, FreightAdapter.FreightHolder> {
    private Context mContext;

    public FreightAdapter(Context context, List<FreightInfoRes.LogisticBean> list) {
        super(list);
        mContext = context;
    }

    public void update(List<FreightInfoRes.LogisticBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(FreightHolder holder, FreightInfoRes.LogisticBean item) {
        holder.setText(R.id.item_express_time, item.getTime());
        holder.setText(R.id.item_express_desc, item.getContext());
    }

    @Override
    public FreightHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new FreightHolder(inflateItemView(viewGroup, R.layout.item_freight));
    }

    public class FreightHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public FreightHolder(View itemView) {
            super(itemView);
        }
    }
}
