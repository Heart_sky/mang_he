package com.android.manghe.mine.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;
import com.android.manghe.mine.presenter.PAccountSafe;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/28 13:33
 * desc   :
 */
public class AccountSafeActivity extends MVPActivity<PAccountSafe> {


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {

        getTitleBar().setToolbar("账号与安全").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine()
                .setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_account_safe;
    }

    public void onClickUnderAge(View view) {
        open(UnderAgeActivity.class);

    }

    public void onClickCancel(View view) {
        open(AccountCancelActivity.class);
    }

}
