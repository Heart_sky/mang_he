package com.android.manghe.mine.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/14 17:14
 * desc   :
 */
public class CommonPagerAdapter extends PagerAdapter {

    private List<View> mViews;

    public CommonPagerAdapter(List<View> views) {
        this.mViews = views;
    }



    public void updateViews(List<View> views) {
        this.mViews = views;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mViews.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView(mViews.get(position));
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(View container, int position) {
        View view = mViews.get(position);
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view);
        return mViews.get(position);
    }
}
