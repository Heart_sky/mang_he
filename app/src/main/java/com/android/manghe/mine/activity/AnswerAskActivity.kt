package com.android.manghe.mine.activity

import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.webkit.WebView
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.common.model.WebViewUserInfo
import com.android.manghe.common.webview.WebViewEventListener
import com.android.manghe.common.webview.WebViewFragment
import com.android.manghe.common.webview.WebViewParameter
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.user.activity.LoginActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_answer_ask.*

class AnswerAskActivity :MVPActivity<XPresenter<IView>>(), WebViewEventListener {
    private var webViewFragment: WebViewFragment? = null


    override fun getLayoutId(): Int {
        return R.layout.activity_answer_ask
    }
    override fun initData(savedInstanceState: Bundle?) {
        titleBar!!.setToolbar("问答").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(
            ContextCompat.getColor(this, R.color.white)
        )
        webViewFragment = WebViewFragment.newInstance(getParam(0),this)
        webViewFragment!!.setNeedKeyboardHandle(true)//需处理键盘弹出遮挡问题
        webViewFragment!!.setWebViewEventListener(this)
        supportFragmentManager
            .beginTransaction()
            .add(R.id.fragment_container, webViewFragment!!)
            .commitAllowingStateLoss()

        val dra1= getResources().getDrawable(R.mipmap.question_1);
        dra1.setBounds( 0, 0, dra1.getMinimumWidth(),dra1.getMinimumHeight());
        val dra2= getResources().getDrawable(R.mipmap.question_2);
        dra2.setBounds( 0, 0, dra2.getMinimumWidth(),dra2.getMinimumHeight());
        val dra3= getResources().getDrawable(R.mipmap.question_3);
        dra3.setBounds( 0, 0, dra3.getMinimumWidth(),dra3.getMinimumHeight());
        val dra4= getResources().getDrawable(R.mipmap.question_4);
        dra4.setBounds( 0, 0, dra4.getMinimumWidth(),dra4.getMinimumHeight());
        tvAll.setOnClickListener{
            tvAll.setCompoundDrawables(null,null,dra1,null)
            tvMine.setCompoundDrawables(null,null,dra4,null)
            webViewFragment?.loadUrl(ConstantsUrl.host +  "/home/test/?/=#/question-all")
        }
        tvMine.setOnClickListener{
            tvAll.setCompoundDrawables(null,null,dra2,null)
            tvMine.setCompoundDrawables(null,null,dra3,null)
            webViewFragment?.loadUrl(ConstantsUrl.host +  "/home/www/#/question-my")
        }
    }

    private fun getParam(type : Int):WebViewParameter{
        return WebViewParameter.WebParameterBuilder()
            .setUrl(ConstantsUrl.host + if(type == 0) "/home/test/?/=#/question-all" else "/home/www/#/question-my")
            .build()
    }

    override fun onPageFinished(view: WebView, url: String) {
        val userInfo = UserHolder.getUserInfo(this)
        if (userInfo == null){
            finish()
            open(LoginActivity::class.java)
        } else {
            val jsonUser = Gson().toJson(WebViewUserInfo(userInfo!!.TOKEN, userInfo.UID, userInfo.UNAME, userInfo.avatar))
            val js = "window.localStorage.setItem('user','$jsonUser');"
            val jsUrl = "javascript:localStorage.setItem('user','$jsonUser');"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                view.evaluateJavascript(js, null)
            } else {
                view.loadUrl(jsUrl)
            }
        }
    }

    override fun onResume() {
        webViewFragment?.resume()
        super.onResume()
    }

    override fun onPause() {
        webViewFragment?.pause()
        super.onPause()
    }

    override fun onDestroy() {
        webViewFragment?.destroy()
        super.onDestroy()
    }
}