package com.android.manghe.mine.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.activity.AddModifyAddrActivity;
import com.android.manghe.mine.model.AddressInfo;

import java.util.List;

public class AddressListAdapter extends BaseRecyclerViewAdapter<AddressInfo, AddressListAdapter.AddressListHolder> {
    private Context mContext;

    public AddressListAdapter(Context context, List<AddressInfo> list) {
        super(list);
        mContext = context;
    }

    public void update(List<AddressInfo> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(AddressListHolder holder, AddressInfo item) {
        holder.setText(R.id.tvName, item.name);
        holder.setText(R.id.tvTel, item.mobile);
        holder.setText(R.id.tvArea, item.area);
        holder.setText(R.id.tvAddressDetail, item.address);
        holder.setVisible(R.id.ivTick, TextUtils.equals("1",item.is_default));
        holder.setVisible(R.id.tvDefault, TextUtils.equals("1",item.is_default));
        holder.setOnClickListener(R.id.ivEdit, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AddModifyAddrActivity.class);
                intent.putExtra("AddressInfo",item);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public AddressListHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new AddressListHolder(inflateItemView(viewGroup, R.layout.item_address));
    }

    public class AddressListHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public AddressListHolder(View itemView) {
            super(itemView);
        }
    }
}
