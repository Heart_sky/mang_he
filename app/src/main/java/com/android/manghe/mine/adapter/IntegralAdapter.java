package com.android.manghe.mine.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.tools.ScreenUtil;
import com.android.manghe.R;
import com.android.manghe.mine.activity.IntegralDetailActivity;
import com.android.manghe.mine.model.IntegralModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/26 11:36
 * desc   :
 */
public class IntegralAdapter extends BaseQuickAdapter<IntegralModel.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;
    private View emptyView;
    private int itemSize;


    public void updateList(List<IntegralModel.DataBean.ListBean> list) {
        replaceData(list);
    }

    public IntegralAdapter(Context context, @Nullable List<IntegralModel.DataBean.ListBean> data) {
        super(R.layout.item_integral, data);
        this.mContext = context;
        itemSize = (ScreenUtil.getScreenWidth(mContext) - DisplayUtil.dip2px(mContext, 35)) / 3;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    @Override
    protected void convert(BaseViewHolder holder, IntegralModel.DataBean.ListBean item) {
        GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.getView(R.id.id_img_src));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(this.itemSize, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.leftMargin=DisplayUtil.dip2px(mContext,4);
        lp.rightMargin=DisplayUtil.dip2px(mContext,4);
        holder.itemView.findViewById(R.id.layoutContent).setLayoutParams(lp);
        holder.setText(R.id.id_tv_name, item.name);
        holder.setText(R.id.id_tv_price, item.score + "积分");
        holder.setText(R.id.id_tv_count, "已兑换" + item.sell + "份");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, IntegralDetailActivity.class);
                intent.putExtra("IntegralModel", item);
                mContext.startActivity(intent);
            }
        });


    }


}
