package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.MessageNoticeData;

import java.util.List;

public class MsgNoticeAdapter extends BaseRecyclerViewAdapter<MessageNoticeData.ListBean, MsgNoticeAdapter.MsgHolder> {
    private Context mContext;

    public MsgNoticeAdapter(Context context, List<MessageNoticeData.ListBean> list) {
        super(list);
        mContext = context;
    }

    public void update(List<MessageNoticeData.ListBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(MsgHolder holder, MessageNoticeData.ListBean item) {
        holder.setText(R.id.tvTitle, item.title);
        holder.setText(R.id.tvTime, item.createtime);
        holder.getView(R.id.tvPoint).setVisibility(item.is_read == 2 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public MsgHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new MsgHolder(inflateItemView(viewGroup, R.layout.item_msg));
    }

    public class MsgHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public MsgHolder(View itemView) {
            super(itemView);
        }
    }
}
