package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.UnderAgeFeedBackActivity;
import com.android.manghe.mine.model.UnderAgeModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/29 11:12
 * desc   :
 */
public class PUnderAgeFeedBack  extends XPresenter<UnderAgeFeedBackActivity> {

    private int currentPage = 1;
    private final String PageSize = "10";
    private final List<UnderAgeModel.DataBean.ListBean> underAgeList = new ArrayList<>();



    public void getUnderAgeList(boolean isRefresh) {
        //获取精选
        if (isRefresh) {
            currentPage = 1;
            underAgeList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_UNDER_AGE_GET, data, headMap)
                .get()
                .execute(new BaseBack<UnderAgeModel>() {
                    @Override
                    public void onSuccess(Call call, UnderAgeModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                underAgeList.addAll(res.data.list);
                                getV().setCanLoadMore(underAgeList.size() != res.data.list_total);
                                getV().updateList(underAgeList);
                                if (underAgeList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateList(underAgeList);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                });
    }

}
