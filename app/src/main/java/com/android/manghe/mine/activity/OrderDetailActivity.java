package com.android.manghe.mine.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.ToastUtil;
import com.android.manghe.R;
import com.android.manghe.box.model.MessageEvent;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.config.Config;
import com.android.manghe.mine.adapter.OrderDetailAdapter;
import com.android.manghe.mine.model.AllStateOrderModel;
import com.android.manghe.mine.model.OrderDetailModel;
import com.android.manghe.mine.presenter.POrderDetail;
import com.android.manghe.user.activity.LoginActivity;
import com.android.manghe.view.AutoHeightLayoutManager;
import com.android.manghe.view.dialog.NewTipDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/02 13:43
 * desc   : 订单详情页面
 */
public class OrderDetailActivity extends MVPActivity<POrderDetail> {

    private TextView mTvAddressName, mTvAddressTel, mTvAddressDetail, mTvOrderSN, mTvStatus,
            mTvPrice, mTvRemark;
    private Button mBtn;
    private RecyclerView mRecyclerView;
    private OrderDetailAdapter mAdapter;
    private AllStateOrderModel.DataBean.ListBean mCurrentAllSateOrderModel;
    private List<OrderDetailModel.DataBean> mList = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_order_detail;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        getTitleBar().setToolbar("订单详情").hideBottomLine()
                .setLeftIcon(R.mipmap.icon_back_black)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        if (UserHolder.getUserInfo(this) == null) {
            finish();
            open(LoginActivity.class);
            return;
        }
        mTvAddressName = findViewById(R.id.tvAddrName);
        mTvAddressTel = findViewById(R.id.tvAddrTel);
        mTvAddressDetail = findViewById(R.id.tvAddrDetail);
        mTvOrderSN = findViewById(R.id.tvOrderSN);
        mTvStatus = findViewById(R.id.tvStatus);
        mTvPrice = findViewById(R.id.tvPrice);
        mTvRemark = findViewById(R.id.tvRemark);
        mBtn = findViewById(R.id.btn);

        mRecyclerView = findViewById(R.id.id_recycler_view);
        mCurrentAllSateOrderModel = (AllStateOrderModel.DataBean.ListBean) getIntent().getSerializableExtra("AllStateOrderModel");
        initRecyclerView();
        if (mCurrentAllSateOrderModel != null) {
            getP().getOrderDetail(mCurrentAllSateOrderModel.order_sn);
        }
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new AutoHeightLayoutManager(this));
        mAdapter = new OrderDetailAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getP().orderDetail != null) {
                    if (getP().orderDetail.shipping_status.equals("1")) {
                        //点击确认收货
                        NewTipDialog newTipDialog = new NewTipDialog(mContext, "是否确认收货？");
                        newTipDialog.show();
                        newTipDialog.setListener(new NewTipDialog.ITipDialogListener() {
                            @Override
                            public void clickLeft() {

                            }

                            @Override
                            public void clickRight() {
                                getP().ConfirmReceive(getP().orderDetail.order_sn);


                            }
                        });
                    } else if (getP().orderDetail.shipping_status.equals("2")) {
                        Intent intent = new Intent(mContext, MarketShowOrderActivity.class);
                        intent.putExtra("orderSn", getP().orderDetail.order_sn);
                        intent.putExtra("orderId", getP().orderDetail.order_id);
                        intent.putExtra("goodId", getP().orderDetail.goods_id);
                        intent.putExtra("picUrl", getP().orderDetail.goods_thumb);
                        intent.putExtra("title", getP().orderDetail.goods_name);
                        startActivity(intent);
                    }
                }
            }
        });
    }


    public void update(OrderDetailModel.DataBean model) {
        mList.clear();
        mList.add(model);
        mAdapter.updateList(mList);
        mTvOrderSN.setText(model.order_sn);
        if (model.shipping_status.equals("0")) {
            mTvStatus.setText("待发货");
            mBtn.setVisibility(View.GONE);
        } else if (model.shipping_status.equals("1")) {
            mTvStatus.setText("已发货");
            mBtn.setVisibility(View.VISIBLE);
            mBtn.setText("确认收货");
        } else if (model.shipping_status.equals("2")) {
            mTvStatus.setText("已完成");
            mBtn.setVisibility(View.VISIBLE);
            if (model.is_rate.equals("0")) {
                mBtn.setText("待评价");
                mBtn.setEnabled(true);
            } else if (model.is_rate.equals("1")) {
                mBtn.setText("已评价");
                mBtn.setEnabled(false);
            }
        }
        mTvPrice.setText(model.sell_price);
        mTvRemark.setText(model.remark);
        mTvAddressTel.setText(model.mobile);
        mTvAddressName.setText(model.receiver);
        mTvAddressDetail.setText(model.address);
    }


    public void receiveOk() {
        getP().getOrderDetail(mCurrentAllSateOrderModel.order_sn);
        EventBus.getDefault().post(new MessageEvent(Config.EVENT_BUS_GET_ALL_ORDER));
    }


    public void showConfirmReceiveErrorTip() {
        ToastUtil.showLong(mContext, "确认收货失败，请重试");
    }


    public void showErrorTip() {
        NewTipDialog newTipDialog = new NewTipDialog(mContext, "获取失败,是否重新获取？");
        newTipDialog.show();
        newTipDialog.setListener(new NewTipDialog.ITipDialogListener() {
            @Override
            public void clickLeft() {

            }

            @Override
            public void clickRight() {
                getP().getOrderDetail(mCurrentAllSateOrderModel.order_sn);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getEventType() == Config.EVENT_BUS_GET_ALL_ORDER) {
            getP().getOrderDetail(mCurrentAllSateOrderModel.order_sn);
        }
    }

}
