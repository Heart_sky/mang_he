package com.android.manghe.mine.adapter;

import com.android.manghe.R;
import com.android.manghe.mine.model.CustomerRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

public class CommonProblemAdapter extends BaseQuickAdapter<CustomerRes.CatlistBean, BaseViewHolder> {


    public CommonProblemAdapter() {
        super(R.layout.item_common_problem_simple);
    }

    @Override
    protected void convert(BaseViewHolder helper, CustomerRes.CatlistBean item) {
        helper.setText(R.id.tvTitle, item.catname);
    }
}
