package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.google.gson.annotations.SerializedName;

public class MemberIndexRes extends NetBean<MemberIndexRes.DataBean> {

    public static class DataBean {
        /**
         * member : {"mid":"463619","username":"mk5f865af383ef5","ivt_id":"0","ivt_id_2":"0","ivt_id_3":"0","zone":"0","mobile":"","verify_mobile":"0","commission_total":"0.00","commission":"0.00","deduct_commission":"0.00","user_money":false,"frozen_money":"0.00","back_money":"0.00","status":"1","subscribe_time":"0","ivt_count":"0","ivt_level":"0","agent_rank":"0","partner_id":"0","partner_rank":"0","is_agent":"0","agent_time":"0","is_new":"0","coin":"0","free_coin":"0","auction_win":"0","voucher_amount":"199","is_robots":"0","from_ip":"","is_auction":"0","is_win":"0","invite_commission":"0.00","level":"0","hj_start_date":null,"exempt_amount":null,"red_packet_amount":"0.00","defaultPic":"/upload/woman.png","password":"4584578c26742a4fb1e423d1a909c77f","pay_password":"","salt":"3a1de9","photo":"https://thirdwx.qlogo.cn/mmopen/vi_32/h8DglDNtJd8vqfjUnffo3zDInkYvDe8yVJpSq2Wa6yvIZqBzKsOKzDJZxTg3zVfHrsNpGnkckJq2Ilj43nxxDg/132?v=1604242935","nickname":"Sammie.Zhang","realname":"","idcard":null,"rank_id":"0","email":"","verify_email":"0","address":"","birthday":"0000-00-00","sex":"1","ip":"120.229.255.240","lastip":"183.4.40.90","login_time":"6","login":"1604242393","lastlogin":"1604132491","c_time":"1602640627","intro":null,"qq":"","wx":"","mshop_img":null,"mshop_name":null,"mshop_notice":null,"mshop_share":null,"good_share":null,"free_coin_voucher_order_id":"0","thumb":null,"free_coin_present_num":"0"}
         * msgUnreadCount : 0
         * commission : 0.00
         * commission_total : 0.00
         * dfkCount : 0
         * dfhCount : 0
         * dshCount : 0
         * dpjCount : 0
         * tkCount : 0
         * scomss : 1
         */

        @SerializedName("member")
        public MemberBean member;
        @SerializedName("msgUnreadCount")
        public String msgUnreadCount;
        @SerializedName("commission")
        public String commission;
        @SerializedName("commission_total")
        public String commissionTotal;
        @SerializedName("dfkCount")
        public Integer dfkCount;
        @SerializedName("dfhCount")
        public Integer dfhCount;
        @SerializedName("dshCount")
        public Integer dshCount;
        @SerializedName("dpjCount")
        public Integer dpjCount;
        @SerializedName("tkCount")
        public Integer tkCount;
        @SerializedName("scomss")
        public String scomss;

        public static class MemberBean {
            /**
             * mid : 463619
             * username : mk5f865af383ef5
             * ivt_id : 0
             * ivt_id_2 : 0
             * ivt_id_3 : 0
             * zone : 0
             * mobile :
             * verify_mobile : 0
             * commission_total : 0.00
             * commission : 0.00
             * deduct_commission : 0.00
             * user_money : false
             * frozen_money : 0.00
             * back_money : 0.00
             * status : 1
             * subscribe_time : 0
             * ivt_count : 0
             * ivt_level : 0
             * agent_rank : 0
             * partner_id : 0
             * partner_rank : 0
             * is_agent : 0
             * agent_time : 0
             * is_new : 0
             * coin : 0
             * free_coin : 0
             * auction_win : 0
             * voucher_amount : 199
             * is_robots : 0
             * from_ip :
             * is_auction : 0
             * is_win : 0
             * invite_commission : 0.00
             * level : 0
             * hj_start_date : null
             * exempt_amount : null
             * red_packet_amount : 0.00
             * defaultPic : /upload/woman.png
             * password : 4584578c26742a4fb1e423d1a909c77f
             * pay_password :
             * salt : 3a1de9
             * photo : https://thirdwx.qlogo.cn/mmopen/vi_32/h8DglDNtJd8vqfjUnffo3zDInkYvDe8yVJpSq2Wa6yvIZqBzKsOKzDJZxTg3zVfHrsNpGnkckJq2Ilj43nxxDg/132?v=1604242935
             * nickname : Sammie.Zhang
             * realname :
             * idcard : null
             * rank_id : 0
             * email :
             * verify_email : 0
             * address :
             * birthday : 0000-00-00
             * sex : 1
             * ip : 120.229.255.240
             * lastip : 183.4.40.90
             * login_time : 6
             * login : 1604242393
             * lastlogin : 1604132491
             * c_time : 1602640627
             * intro : null
             * qq :
             * wx :
             * mshop_img : null
             * mshop_name : null
             * mshop_notice : null
             * mshop_share : null
             * good_share : null
             * free_coin_voucher_order_id : 0
             * thumb : null
             * free_coin_present_num : 0
             */

            @SerializedName("mid")
            public String mid;
            @SerializedName("username")
            public String username;
            @SerializedName("ivt_id")
            public String ivtId;
            @SerializedName("ivt_id_2")
            public String ivtId2;
            @SerializedName("ivt_id_3")
            public String ivtId3;
            @SerializedName("zone")
            public String zone;
            @SerializedName("mobile")
            public String mobile;
            @SerializedName("verify_mobile")
            public String verifyMobile;
            @SerializedName("commission_total")
            public String commissionTotal;
            @SerializedName("commission")
            public String commission;
            @SerializedName("deduct_commission")
            public String deductCommission;
            @SerializedName("user_money")
            public Boolean userMoney;
            @SerializedName("frozen_money")
            public String frozenMoney;
            @SerializedName("back_money")
            public String backMoney;
            @SerializedName("status")
            public String status;
            @SerializedName("subscribe_time")
            public String subscribeTime;
            @SerializedName("ivt_count")
            public String ivtCount;
            @SerializedName("ivt_level")
            public String ivtLevel;
            @SerializedName("agent_rank")
            public String agentRank;
            @SerializedName("partner_id")
            public String partnerId;
            @SerializedName("partner_rank")
            public String partnerRank;
            @SerializedName("is_agent")
            public String isAgent;
            @SerializedName("agent_time")
            public String agentTime;
            @SerializedName("is_new")
            public String isNew;
            @SerializedName("coin")
            public String coin;
            @SerializedName("free_coin")
            public String freeCoin;
            @SerializedName("auction_win")
            public String auctionWin;
            @SerializedName("voucher_amount")
            public String voucherAmount;
            @SerializedName("is_robots")
            public String isRobots;
            @SerializedName("from_ip")
            public String fromIp;
            @SerializedName("is_auction")
            public String isAuction;
            @SerializedName("is_win")
            public String isWin;
            @SerializedName("invite_commission")
            public String inviteCommission;
            @SerializedName("level")
            public int level;
            @SerializedName("hj_start_date")
            public long hjStartDate;
            @SerializedName("exempt_amount")
            public double exemptAmount;
            @SerializedName("red_packet_amount")
            public String redPacketAmount;
            @SerializedName("defaultPic")
            public String defaultPic;
            @SerializedName("password")
            public String password;
            @SerializedName("pay_password")
            public String payPassword;
            @SerializedName("salt")
            public String salt;
            @SerializedName("photo")
            public String photo;
            @SerializedName("nickname")
            public String nickname;
            @SerializedName("realname")
            public String realname;
            @SerializedName("idcard")
            public Object idcard;
            @SerializedName("rank_id")
            public String rankId;
            @SerializedName("email")
            public String email;
            @SerializedName("verify_email")
            public String verifyEmail;
            @SerializedName("address")
            public String address;
            @SerializedName("birthday")
            public String birthday;
            @SerializedName("sex")
            public String sex;
            @SerializedName("ip")
            public String ip;
            @SerializedName("lastip")
            public String lastip;
            @SerializedName("login_time")
            public String loginTime;
            @SerializedName("login")
            public String login;
            @SerializedName("lastlogin")
            public String lastlogin;
            @SerializedName("c_time")
            public String cTime;
            @SerializedName("intro")
            public Object intro;
            @SerializedName("qq")
            public String qq;
            @SerializedName("wx")
            public String wx;
            @SerializedName("mshop_img")
            public Object mshopImg;
            @SerializedName("mshop_name")
            public Object mshopName;
            @SerializedName("mshop_notice")
            public Object mshopNotice;
            @SerializedName("mshop_share")
            public Object mshopShare;
            @SerializedName("good_share")
            public Object goodShare;
            @SerializedName("free_coin_voucher_order_id")
            public String freeCoinVoucherOrderId;
            @SerializedName("thumb")
            public Object thumb;
            @SerializedName("free_coin_present_num")
            public String freeCoinPresentNum;
        }
    }
}
