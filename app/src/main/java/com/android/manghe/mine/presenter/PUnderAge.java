package com.android.manghe.mine.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.UnderAgeActivity;
import com.android.manghe.mine.model.UploadFileRes;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/28 13:42
 * desc   :
 */
public class PUnderAge extends XPresenter<UnderAgeActivity> {


    private ArrayList<String> mPicLocalPathListOne = new ArrayList<>();
    private ArrayList<String> mPicLocalPathListTwo = new ArrayList<>();
    private ArrayList<String> mPicLocalPathListThree = new ArrayList<>();
    private ArrayList<String> mPicUrlList = new ArrayList<>();


    public ArrayList<String> getPicLocalPathListOne() {
        return mPicLocalPathListOne;
    }

    public ArrayList<String> getPicLocalPathListTwo() {
        return mPicLocalPathListTwo;
    }

    public ArrayList<String> getPicLocalPathListThree() {
        return mPicLocalPathListThree;
    }

    public void submit(String realname, String id_number, String mobile, String content) {
        for (String path : mPicLocalPathListOne) {
            uploadFile(realname, id_number, mobile, content, path);
        }
        for (String path : mPicLocalPathListTwo) {
            uploadFile(realname, id_number, mobile, content, path);
        }
        for (String path : mPicLocalPathListThree) {
            uploadFile(realname, id_number, mobile, content, path);
        }
    }


    private void uploadFile(String realname, String id_number, String mobile, String content, String path) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBER_UPLOAD)
                .uploadFile("upFile", path)
                .addHead("UID", UserHolder.getUID(getV()))
                .addHead("TOKEN", UserHolder.getUserInfo(getV()).TOKEN)
                .build()
                .execute(new BaseBack<UploadFileRes>() {
                    @Override
                    public void onSuccess(Call call, UploadFileRes res) {
                        super.onSuccess(call, res);
                        if (res.code == 0 && !TextUtils.isEmpty(res.data.toString())) {
                            mPicUrlList.add(res.data.toString());
                            if (mPicUrlList.size() == (mPicLocalPathListOne.size() + mPicLocalPathListTwo.size() + mPicLocalPathListThree.size())) {
                                submitUnderAgeInfo(realname, id_number, mobile, content);
                            }
                        } else {
                            mPicUrlList.clear();
                            getV().hideLoadingDialog();
                            getV().submitFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        mPicUrlList.clear();
                        getV().hideLoadingDialog();
                        getV().submitFail(null);
                    }
                });

    }


    public void submitUnderAgeInfo(String realname, String id_number, String mobile, String content) {
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("realname", realname);
        data.put("id_number", id_number);
        data.put("mobile", mobile);
        data.put("content", content);
        data.put("prove_imgs", getImages());
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_UNDER_AGE_SENT, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            getV().submitOk(res.msg);
                        } else {
                            getV().submitFail(res.msg);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().submitFail(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }


    private String getImages() {
        String prove_imgs = "";
        for (int i = 0; i < mPicUrlList.size(); i++) {
            if (i == 0) {
                prove_imgs = mPicUrlList.get(i);
            } else {
                prove_imgs = prove_imgs + "," + mPicUrlList.get(i);
            }
        }
        return prove_imgs;
    }
}
