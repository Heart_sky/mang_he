package com.android.manghe.mine.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.widget.ImageView
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.Glide.GlideHelper
import com.android.base.tools.ToastUtil
import com.android.base.tools.codec.MD5
import com.android.manghe.R
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.config.events.RefreshUserInfoEvent
import com.android.manghe.mine.presenter.PBindTel
import com.eightbitlab.rxbus.Bus
import kotlinx.android.synthetic.main.activity_modify_tel.*
import kotlinx.android.synthetic.main.activity_modify_tel.etTel
import java.util.*

class ModifyTelActivity :MVPActivity<PBindTel>() {
    private val TotalCount = 60L * 1000

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("绑定手机号").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)
        newPicCode()
        initListener()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_modify_tel
    }

    private fun newPicCode(){
        val ivPicCode = ImageView(this)
        layoutPicCode.addView(ivPicCode)
        p.scodeToken = MD5.convertString(UUID.randomUUID().toString())
        GlideHelper.loadNoCache(this, ConstantsUrl.domain + ConstantsUrl.MEMBERWELCOMSCODE + "?scode_token=${p.scodeToken}", ivPicCode)
        ivPicCode.setOnClickListener {
            layoutPicCode.removeAllViews()
            newPicCode()
        }
    }
    private fun initListener(){
        tvSend.setOnClickListener {
            val tel = etTel.text.toString().trim()
            if(tel.isEmpty()){
                ToastUtil.showShort(this@ModifyTelActivity, "请输入手机号码")
                return@setOnClickListener
            }
            val picCode = etPicCode.text.toString().trim()
            if(TextUtils.isEmpty(picCode)){
                ToastUtil.showShort(this@ModifyTelActivity, "请输入图形验证码")
                return@setOnClickListener
            }
            showLoadingDialog()
            p.sendSmsCode(tel, picCode)
        }
        btnConfirm.setOnClickListener {
            val tel = etTel.text.toString().trim()
            val smsCode = etSmsCode.text.toString().trim()
            if(tel.isEmpty()){
                ToastUtil.showShort(this@ModifyTelActivity, "请输入手机号码")
                return@setOnClickListener
            }
            if(smsCode.isEmpty()){
                ToastUtil.showShort(this@ModifyTelActivity, "请输入验证码")
                return@setOnClickListener
            }
            showLoadingDialog()
            p.modify(tel, smsCode)
        }
    }
    fun closeLoadingDialog() {
        hideLoadingDialog()
    }

    fun sendCodeOk(){
        startCountDown()
    }
    fun sendCodeFail(text: String?){
        ToastUtil.showShort(this, if (TextUtils.isEmpty(text)) "发送验证码失败" else text)
    }

    private var mTimer : CountDownTimer? = null
    private fun startCountDown(){
        mTimer = object : CountDownTimer(TotalCount, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                tvSend.text = "${(millisUntilFinished / 1000)}s"
                tvSend.isClickable = false
            }

            override fun onFinish() {
                tvSend.text = "发送验证码"
                tvSend.isClickable = true
            }
        }
        mTimer!!.start()
    }
    private fun stopCountDown(){
        mTimer?.let{
            it.cancel()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopCountDown()
    }

    fun modifyOk() {
        ToastUtil.showShort(this, "修改成功")
        Bus.send(RefreshUserInfoEvent())
        val intent = Intent()
        intent.putExtra("tel", etTel.text.toString().trim())
        setResult(Activity.RESULT_OK,intent)
        finish()
    }

    fun modifyFail(text: String?) {
        ToastUtil.showShort(this, if (TextUtils.isEmpty(text)) "修改失败" else text)
    }
}