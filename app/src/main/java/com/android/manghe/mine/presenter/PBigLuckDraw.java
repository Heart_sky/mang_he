package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.BigLuckDrawActivity;
import com.android.manghe.mine.model.PlateConfigRes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.Call;

public class PBigLuckDraw extends XPresenter<BigLuckDrawActivity> {

    public int remainScore = 0, everyConsume = 0;//剩余积分,每次需要扣除的积分

    public void loadData() {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.BigLuckDrawDesc, headMap)
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        if (json != null && !json.isEmpty()) {
                            try {
                                String desc = new JSONObject(json).getString("data");
                                getV().updateDesc(desc);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        getV().hideLoadingDialog();
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    public void loadLuckDrawConfig(){
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.GetPlateConfig, headMap)
                .get()
                .execute(new BaseBack<PlateConfigRes>() {

                    @Override
                    public void onSuccess(Call call, PlateConfigRes res) {
                        if (res != null && res.data != null) {
                            remainScore = res.data.remain_score;
                            everyConsume = res.data.every_consume;
                            getV().updateScore(res.data.every_consume, res.data.remain_score);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void luckDrawConfirm(){
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.PlateTurn, headMap)
                .get()
                .execute(new BaseBack<PlateConfigRes>() {

                    @Override
                    public void onSuccess(Call call, PlateConfigRes res) {
                        loadLuckDrawConfig();
                    }
                });
    }
}
