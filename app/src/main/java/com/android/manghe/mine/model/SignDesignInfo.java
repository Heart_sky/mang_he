package com.android.manghe.mine.model;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/03 16:10
 * desc   : 自定义签到实体
 */
public class SignDesignInfo implements Serializable {


    //关于签到送钱的
    private int icon;
    private String name;
    private String desc;
    private String signMoney;
    private boolean isSign;

    //关于签到天数的
    private boolean open;
    private String value;
    private String day;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSignMoney() {
        return signMoney;
    }

    public void setSignMoney(String signMoney) {
        this.signMoney = signMoney;
    }

    public boolean isSign() {
        return isSign;
    }

    public void setSign(boolean sign) {
        isSign = sign;
    }

    public SignDesignInfo() {
    }

    public SignDesignInfo(int icon, String name, String desc, String signMoney, boolean isSign) {
        this.icon = icon;
        this.name = name;
        this.desc = desc;
        this.signMoney = signMoney;
        this.isSign = isSign;
    }

    public SignDesignInfo(boolean open, String value, String day) {
        this.open = open;
        this.value = value;
        this.day = day;
    }
}
