package com.android.manghe.mine.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.mine.adapter.CommonProblemAdapter
import com.android.manghe.mine.model.CustomerRes
import com.android.manghe.mine.presenter.PCommonProblem
import kotlinx.android.synthetic.main.activity_common_problem.*

class CommonProblemActivity  : MVPActivity<PCommonProblem>(){

    var adapter : CommonProblemAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_common_problem
    }
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("常见问题").setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black)

        adapter = CommonProblemAdapter()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        adapter!!.setOnItemClickListener { _, view, position ->
            open(CommonProblemDetailActivity::class.java, mapOf("problems" to adapter!!.data[position]))
        }
        p.getData()
    }

    fun updateView(dataList: List<CustomerRes.CatlistBean>){
        adapter?.let {
            it.replaceData(dataList)
        }
    }
}