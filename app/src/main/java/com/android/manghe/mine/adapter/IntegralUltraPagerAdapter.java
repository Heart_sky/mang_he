package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.market.model.MarketGoodDetailRes;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/30 15:07
 * desc   :
 */
public class IntegralUltraPagerAdapter extends PagerAdapter {

    private List<MarketGoodDetailRes.DataBean.ThumbsBean> mList = new ArrayList<>();
    private Context mContext;

    public IntegralUltraPagerAdapter(Context context, List<MarketGoodDetailRes.DataBean.ThumbsBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        if (mList == null) {
            return 0;
        }
        return mList.size();

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        MarketGoodDetailRes.DataBean.ThumbsBean model = mList.get(position);
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_pager_iv_item, null);
        ImageView imgView = view.findViewById(R.id.id_img_src);
        GlideHelper.loadWithHolderErr(mContext, model.src, imgView);
        container.addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
