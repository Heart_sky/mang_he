package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class AreaInfo extends NetBean<List<AreaInfo>> {


    /**
     * id : 2
     * name : 北京
     * parentid : 1
     * arrparentid : 0,1
     * child : 1
     */

    public String id;
    public String name;
    public String parentid;
    public String arrparentid;
    public String child;
}
