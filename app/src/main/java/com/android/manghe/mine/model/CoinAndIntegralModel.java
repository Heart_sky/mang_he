package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/14 17:24
 * desc   :
 */
public class CoinAndIntegralModel extends NetBean<CoinAndIntegralModel.DataBean> {


    /**
     * data : {"list":[{"id":"615","mid":"911730","amount":"39.20","type":"0","remark":"奖品12兑换开盒币:39.2","c_time":"2021-12-13 16:15:22","auction_id":null},{"id":"616","mid":"911730","amount":"39.20","type":"0","remark":"奖品14兑换开盒币:39.2","c_time":"2021-12-13 16:15:22","auction_id":null},{"id":"614","mid":"911730","amount":"39.20","type":"0","remark":"奖品11兑换开盒币:39.2","c_time":"2021-12-13 16:15:22","auction_id":null},{"id":"611","mid":"911730","amount":"110.40","type":"0","remark":"奖品77兑换开盒币:110.4","c_time":"2021-12-13 15:15:08","auction_id":null},{"id":"608","mid":"911730","amount":"55.20","type":"0","remark":"奖品619兑换开盒币:55.2","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"605","mid":"911730","amount":"110.40","type":"0","remark":"奖品621兑换开盒币:110.4","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"607","mid":"911730","amount":"55.20","type":"0","remark":"奖品618兑换开盒币:55.2","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"606","mid":"911730","amount":"25.60","type":"0","remark":"奖品622兑换开盒币:25.6","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"196","mid":"911730","amount":"55.20","type":"0","remark":"奖品106兑换开盒币:55.2","c_time":"2021-12-09 14:00:21","auction_id":null},{"id":"195","mid":"911730","amount":"55.20","type":"0","remark":"奖品105兑换开盒币:55.2","c_time":"2021-12-09 14:00:21","auction_id":null}],"list_total":65}
     * flag : true
     */

    public static class DataBean implements Serializable {
        /**
         * list : [{"id":"615","mid":"911730","amount":"39.20","type":"0","remark":"奖品12兑换开盒币:39.2","c_time":"2021-12-13 16:15:22","auction_id":null},{"id":"616","mid":"911730","amount":"39.20","type":"0","remark":"奖品14兑换开盒币:39.2","c_time":"2021-12-13 16:15:22","auction_id":null},{"id":"614","mid":"911730","amount":"39.20","type":"0","remark":"奖品11兑换开盒币:39.2","c_time":"2021-12-13 16:15:22","auction_id":null},{"id":"611","mid":"911730","amount":"110.40","type":"0","remark":"奖品77兑换开盒币:110.4","c_time":"2021-12-13 15:15:08","auction_id":null},{"id":"608","mid":"911730","amount":"55.20","type":"0","remark":"奖品619兑换开盒币:55.2","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"605","mid":"911730","amount":"110.40","type":"0","remark":"奖品621兑换开盒币:110.4","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"607","mid":"911730","amount":"55.20","type":"0","remark":"奖品618兑换开盒币:55.2","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"606","mid":"911730","amount":"25.60","type":"0","remark":"奖品622兑换开盒币:25.6","c_time":"2021-12-13 14:50:16","auction_id":null},{"id":"196","mid":"911730","amount":"55.20","type":"0","remark":"奖品106兑换开盒币:55.2","c_time":"2021-12-09 14:00:21","auction_id":null},{"id":"195","mid":"911730","amount":"55.20","type":"0","remark":"奖品105兑换开盒币:55.2","c_time":"2021-12-09 14:00:21","auction_id":null}]
         * list_total : 65
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean implements Serializable {
            /**
             * id : 615
             * mid : 911730
             * amount : 39.20
             * type : 0
             * remark : 奖品12兑换开盒币:39.2
             * c_time : 2021-12-13 16:15:22
             * auction_id : null
             */

            public String id;
            public String mid;
            public String amount;
            public String type;
            public String remark;
            public String c_time;
            public Object auction_id;
        }
    }
}
