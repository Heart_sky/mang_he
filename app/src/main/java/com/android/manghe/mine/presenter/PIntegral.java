package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.model.MainTopBannerBean;
import com.android.manghe.mine.activity.IntegralActivity;
import com.android.manghe.mine.model.IntegralModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/21 14:42
 * desc   :
 */
public class PIntegral extends XPresenter<IntegralActivity> {

    private int currentPage = 1;
    private final String PageSize = "10";
    private final List<IntegralModel.DataBean.ListBean> integralList = new ArrayList<>();


    /**
     * 获取积分列表的数据
     *
     * @param isRefresh
     * @param sort
     */
    public void getIntegralList(boolean isRefresh, int sort, String order) {
        //获取精选
        if (isRefresh) {
            currentPage = 1;
            integralList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        data.put("sort", sort + "");
        data.put("order", order);
        HashMap<String, String> headMap = new HashMap<>();
        if (UserHolder.getUserInfo(getV()) != null) {
            headMap.put("UID", UserHolder.getUID(getV()));
            headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
            headMap.put("PLATFORM", "android");
        }
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_Score_List, data, headMap)
                .get()
                .execute(new BaseBack<IntegralModel>() {
                    @Override
                    public void onSuccess(Call call, IntegralModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                integralList.addAll(res.data.list);
                                getV().setCanLoadMore(integralList.size() != res.data.list_total);
                                getV().updateIntegral(res.data.remain_score);
                                getV().updateList(integralList);
                                if (integralList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateList(integralList);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                });
    }

    public void getBanner() {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.BOx_INTEGRAL_COVER)
                .get()
                .execute(new BaseBack<MainTopBannerBean>() {
                    @Override
                    public void onSuccess(Call call, List<MainTopBannerBean> bannerList) {
                        if (bannerList != null && bannerList.size() != 0) {
                            getV().updateBanner(bannerList);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
