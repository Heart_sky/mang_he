package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.fragment.MsgFragment;
import com.android.manghe.mine.model.MessageIndexData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PMsg extends XPresenter<MsgFragment> {
    private int mCurrentPage = 1;
    private final List<MessageIndexData.ListBean> mMsgList = new ArrayList<>();

    public void getData(boolean isRefresh) {
        try{
            if (isRefresh) {
                mCurrentPage = 1;
                mMsgList.clear();
            }
            HashMap<String, String> data = new HashMap<>();
            data.put("page", mCurrentPage + "");
            HashMap<String, String> headMap = new HashMap<>();
            headMap.put("UID", UserHolder.getUID(getV().getActivity()));
            headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
            new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERMESSAGEINDEX, data, headMap)
                    .get()
                    .execute(new BaseBack<MessageIndexData>() {

                        @Override
                        public void onSuccess(Call call, MessageIndexData res) {
                            if (res != null && res.code == 0 && res.list != null) {
                                mMsgList.addAll(res.list);
                                getV().setCanLoadMore(res.list.size() == 10);
                                getV().update(mMsgList);
                                if (mMsgList.size() == 10) {
                                    mCurrentPage++;
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            super.onFailure(e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setRead(String id){
        try{
            HashMap<String, String> data = new HashMap<>();
            data.put("id", id);
            HashMap<String, String> headMap = new HashMap<>();
            headMap.put("UID", UserHolder.getUID(getV().getActivity()));
            headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
            new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERMESSAGEDETAIL, data, headMap)
                    .get()
                    .execute(new GsonBaseBack() {

                        @Override
                        public void onSuccess(Call call, String res) {
                            int i = 0;
                            for(MessageIndexData.ListBean bean : mMsgList){
                                i++;
                                if(bean.message_id.equals(id)){
                                    bean.status = 2;
                                    mMsgList.set(--i, bean);
                                    getV().update(mMsgList);
                                    break;
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            super.onFailure(e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
