package com.android.manghe.mine.model;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;

public class CustomerContentLevel extends AbstractExpandableItem implements MultiItemEntity {

    public String content;

    public CustomerContentLevel(String content) {
        this.content = content;
    }

    @Override
    public int getLevel() {
        return 1;
    }

    @Override
    public int getItemType() {
        return 1;
    }
}
