package com.android.manghe.mine.adapter;

import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.manghe.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class MineOrderMenuAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    List<Integer> iconResList;
    int width;
    public MineOrderMenuAdapter(@Nullable List<String> data, List<Integer> iconResList, int width) {
        super(R.layout.item_mine_order_item, data);
        this.iconResList = iconResList;
        this.width = width;
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tvTitle, item);
        helper.setImageResource(R.id.ivIcon, iconResList.get(helper.getAdapterPosition()));
        ((TextView)helper.getView(R.id.tvTitle)).setWidth(width);
    }
}
