package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.AccountCancelActivity;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/21 09:58
 * desc   :
 */
public class PAccountCacel extends XPresenter<AccountCancelActivity> {



    public void cancellationAccount(){
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MemberDestroy, headMap)
                .post(new HashMap<>()).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean netBean) {
                        if (netBean != null && netBean.code == 0) {
                            getV().doSignOut();
                        }else{
                            ToastUtil.showLong(getV(), "注销失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        getV().hideLoadingDialog();
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
