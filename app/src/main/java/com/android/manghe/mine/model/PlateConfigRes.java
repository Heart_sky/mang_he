package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class PlateConfigRes extends NetBean<PlateConfigRes.DataBean> {


    public static class DataBean {
        /**
         * id : 41105
         */

        public int every_consume;
        public int remain_score;
    }
}
