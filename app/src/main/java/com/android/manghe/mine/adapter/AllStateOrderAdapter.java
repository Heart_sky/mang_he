package com.android.manghe.mine.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.mine.activity.CheckWuLiuActivity;
import com.android.manghe.mine.activity.OrderDetailActivity;
import com.android.manghe.mine.model.AllStateOrderModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/01 15:08
 * desc   :
 */
public class AllStateOrderAdapter extends BaseQuickAdapter<AllStateOrderModel.DataBean.ListBean, BaseViewHolder> {

    private View emptyView;
    private Context mContext;
    private IOrderListener mListener;

    public AllStateOrderAdapter(Context context, @Nullable List<AllStateOrderModel.DataBean.ListBean> data) {
        super(R.layout.item_all_state_order, data);
        this.mContext = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void setListener(IOrderListener listener) {
        this.mListener = listener;
    }

    public void updateList(List<AllStateOrderModel.DataBean.ListBean> list) {
        replaceData(list);
    }

    @Override
    protected void convert(BaseViewHolder helper, AllStateOrderModel.DataBean.ListBean item) {
        helper.setText(R.id.tvOrderNo, "订单号：" + item.order_sn);
        TextView tvStatus = helper.itemView.findViewById(R.id.tvStatus);
        helper.setText(R.id.tvAmount, "共" + item.buy_num + "件");
        if (item.shipping_status.equals("0")) {
            tvStatus.setText("待发货");
        } else if (item.shipping_status.equals("1")) {
            tvStatus.setText("已发货");
        } else if (item.shipping_status.equals("2")) {
            tvStatus.setText("已完成");
        }
        GlideHelper.loadRoundTrans(mContext, item.goods_thumb, helper.getView(R.id.ivPic), 6);
        helper.setText(R.id.tvTitle, item.goods_name);
        helper.setText(R.id.id_tv_price, item.sell_price);
        helper.setText(R.id.id_tv_count, "x" + item.buy_num);

        if (item.shipping_status.equals("1")) {
            helper.setGone(R.id.layoutRemindOrder, true);
        } else {
            helper.setGone(R.id.layoutRemindOrder, false);
        }
        //订单已经完成
        if (item.shipping_status.equals("2")) {
            helper.setGone(R.id.tvShowOrder, true);
            if (item.is_rate.equals("0")) {
                helper.setEnabled(R.id.tvShowOrder, true);
                helper.setText(R.id.tvShowOrder, "待评价");
                helper.setBackgroundRes(R.id.tvShowOrder, R.drawable.btn_line_gray_shape);
            } else if (item.is_rate.equals("1")) {
                helper.setEnabled(R.id.tvShowOrder, false);
                helper.setText(R.id.tvShowOrder, "已评价");
                helper.setBackgroundRes(R.id.tvShowOrder, R.drawable.btn_full_white_shape);
            }
        } else {
            helper.setGone(R.id.tvShowOrder, false);
        }
        //点击到查看物流页面
        helper.itemView.findViewById(R.id.checkFreight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CheckWuLiuActivity.class);
                intent.putExtra("isAuction", false);
                intent.putExtra("AllStateOrderModel", item);
                mContext.startActivity(intent);
            }
        });

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtra("AllStateOrderModel", item);
                mContext.startActivity(intent);

            }
        });

        helper.itemView.findViewById(R.id.tvTakeOver).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onTakeOver(item);
                }
            }
        });
    }

    public interface IOrderListener {
        void onTakeOver(AllStateOrderModel.DataBean.ListBean item);
    }
}
