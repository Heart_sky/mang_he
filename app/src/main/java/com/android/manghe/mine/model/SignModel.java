package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/06 14:03
 * desc   :
 */
public class SignModel  extends NetBean<SignModel.DataBean> {


    /**
     * data : {"bonus":"0.2","sign_times_total":2}
     * flag : true
     */

    public static class DataBean implements Serializable {
        /**
         * bonus : 0.2
         * sign_times_total : 2
         */

        public String bonus;
        public int sign_times_total;
    }
}
