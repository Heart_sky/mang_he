package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.mine.model.VoucherRes;

import java.util.List;

public class VoucherAdapter extends BaseRecyclerViewAdapter<VoucherRes.DataBean, VoucherAdapter.VoucherHolder> {
    private Context mContext;
    private int mListType;

    public VoucherAdapter(Context context, List<VoucherRes.DataBean> list, int listType) {
        super(list);
        mContext = context;
        mListType = listType;
    }

    public void update(List<VoucherRes.DataBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(VoucherHolder holder, VoucherRes.DataBean item) {
        holder.setText(R.id.tvTitle, item.title);
        if(mListType == 1){
            holder.setText(R.id.tvCoin, "+"+item.coupon_value);
            holder.setVisible(R.id.tvOnlyBigMarket, true);
            holder.setText(R.id.tvTime, item.gain_time);
        }else if(mListType == 2){
            holder.setText(R.id.tvCoin, "-" + item.coupon_value);
            holder.setVisible(R.id.tvOnlyBigMarket, false);
            holder.setText(R.id.tvTime, item.used_time);
        }
    }

    @Override
    public VoucherHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new VoucherHolder(inflateItemView(viewGroup, R.layout.item_voucher));
    }

    public class VoucherHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public VoucherHolder(View itemView) {
            super(itemView);
        }
    }
}
