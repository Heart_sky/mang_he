package com.android.manghe.mine.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.AuctionSocketRes;
import com.android.manghe.detail.activity.GoodDetailActivity;
import com.android.manghe.user.activity.LoginActivity;

import java.util.List;

public class MyKaningAdapter extends RecyclerView.Adapter<MyKaningHolder> {
    private Context mContext;

    private AsyncListDiffer<AuctionSocketRes.ListBean> mDiffer;

    private DiffUtil.ItemCallback<AuctionSocketRes.ListBean> diffCallback = new DiffUtil.ItemCallback<AuctionSocketRes.ListBean>() {
        @Override
        public boolean areItemsTheSame(AuctionSocketRes.ListBean oldItem, AuctionSocketRes.ListBean newItem) {
            return TextUtils.equals(oldItem.id, newItem.id);
        }

        @Override
        public boolean areContentsTheSame(AuctionSocketRes.ListBean oldItem, AuctionSocketRes.ListBean newItem) {
            return oldItem.time == newItem.time;
        }
    };

    public AuctionSocketRes.ListBean getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public MyKaningAdapter(Context context) {
        mContext = context;
        mDiffer = new AsyncListDiffer<>(this, diffCallback);
    }

    public void update(List<AuctionSocketRes.ListBean> list) {
        mDiffer.submitList(list);
    }

    @NonNull
    @Override
    public MyKaningHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyKaningHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_kan_ing, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyKaningHolder holder, int position) {
        AuctionSocketRes.ListBean item = getItem(position);
        holder.tvTitle.setText(item.title);
        holder.tvMarketPrice.setText("￥" + item.getMarketPrice());
        holder.tvPrice.setText("￥" + item.getPrice());
        holder.tvName.setText(item.username);
        GlideHelper.loadRoundTrans(mContext, item.thumb, holder.ivPic, 6);

        holder.layoutHole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(UserHolder.getUserInfo(mContext) == null){
                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                }else {
                    Intent intent = new Intent(mContext, GoodDetailActivity.class);
                    intent.putExtra("auctionId", item.id);
                    mContext.startActivity(intent);
                }
            }
        });
    }
}

class MyKaningHolder extends RecyclerView.ViewHolder {
    public ImageView ivPic;
    public TextView tvMarketPrice,tvTitle, tvPrice,tvName;
    public View layoutHole;

    public MyKaningHolder(View itemView) {
        super(itemView);
        ivPic = itemView.findViewById(R.id.ivPic);
        tvMarketPrice = itemView.findViewById(R.id.tvMarketPrice);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvPrice = itemView.findViewById(R.id.tvPrice);
        tvName = itemView.findViewById(R.id.tvName);
        layoutHole = itemView.findViewById(R.id.layoutHole);
    }
}
