package com.android.manghe.mine.adapter;

import android.content.Context;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.mine.model.FeedbackInfoRes;
import com.android.manghe.view.preview.PreviewViewPagerActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class FeedbackListAdapter extends BaseQuickAdapter<FeedbackInfoRes.ListBean, BaseViewHolder> {

    private Context context;
    public String mid = "";

    public FeedbackListAdapter(Context context, String mid) {
        super(R.layout.item_feed_back);
        this.context = context;
        this.mid = mid;
    }

    @Override
    protected void convert(BaseViewHolder helper, FeedbackInfoRes.ListBean item) {
        helper.setGone(R.id.layoutGuanFang, !item.replycontent.isEmpty());
        helper.setGone(R.id.divider, !item.replycontent.isEmpty());

        helper.setText(R.id.tvReplyTime, item.replyTime != null ?  item.replyTime : "");
        helper.setText(R.id.tvReplyContent, item.replycontent);

        helper.setText(R.id.tvCreateTime, item.createtime);
        helper.setText(R.id.tvContent, item.content);
        helper.setGone(R.id.ivImg1, !item.imgone.isEmpty());
        helper.setGone(R.id.ivImg2, !item.imgtwo.isEmpty());
        helper.setGone(R.id.ivImg3, !item.imgthree.isEmpty());
        GlideHelper.loadRoundTrans(context, item.imgone, helper.getView(R.id.ivImg1), 6);
        GlideHelper.loadRoundTrans(context, item.imgtwo, helper.getView(R.id.ivImg2), 6);
        GlideHelper.loadRoundTrans(context, item.imgthree, helper.getView(R.id.ivImg3), 6);
        helper.getView(R.id.ivImg1).setOnClickListener(view -> {
            toPreview(0, item);
        });
        helper.getView(R.id.ivImg2).setOnClickListener(view -> {
            toPreview(1, item);
        });
        helper.getView(R.id.ivImg3).setOnClickListener(view -> {
            toPreview(2, item);
        });
    }

    private void toPreview(int imgPosition, FeedbackInfoRes.ListBean info){
        List<String> imgList = new ArrayList();
        if(!info.imgone.isEmpty()){
            imgList.add(info.imgone);
        }
        if(!info.imgtwo.isEmpty()){
            imgList.add(info.imgtwo);
        }
        if(!info.imgthree.isEmpty()){
            imgList.add(info.imgthree);
        }
        PreviewViewPagerActivity.showActivity(mContext, imgList, imgPosition);
    }
}
