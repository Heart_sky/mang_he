package com.android.manghe.mine.activity

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import android.view.View
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.view.MVPActivity
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.base.view.viewpager.CustomViewPagerAdapter
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.common.model.BaseBack
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.main.view.TabEntity
import com.android.manghe.mine.fragment.ScoreDetailFragment
import com.android.manghe.mine.model.ScoreDetailList
import com.android.manghe.user.activity.LoginActivity
import com.android.manghe.view.dialog.ScoreRuleDialog
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_budget_detail.*
import okhttp3.Call

class ScoreDetailActivity : MVPActivity<XPresenter<IView>>() {
    private var mAdapter: CustomViewPagerAdapter? = null

    private var scoreDetailPush: ScoreDetailFragment? = null
    private var scoreDetailPull: ScoreDetailFragment? = null

    private var ruleInfo = "";

    override fun getLayoutId(): Int {
        return R.layout.activity_score_detail
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar!!.setToolbar("积分明细", R.color.white)
            .setLeftIcon(R.mipmap.icon_back_white)
            .setRightIcon(R.mipmap.ic_help, View.OnClickListener {
                //弹出框
                val dialog = ScoreRuleDialog(
                    this@ScoreDetailActivity,
                    ruleInfo
                )
                dialog.show()
            })
            .hideBottomLine()
            .setBackgroundResource(R.drawable.bg_gradient_org_shape)

        if(UserHolder.getUserInfo(this) == null){
            finish()
            open(LoginActivity::class.java)
            return
        }


        tvTotal.text = intent.getStringExtra("score")

        val tabEntities = arrayListOf<CustomTabEntity>(TabEntity("收入明细", 0, 0), TabEntity("支出明细", 0, 0))
        tabLayout.setTabData(tabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                viewPager?.currentItem = position
            }

            override fun onTabReselect(position: Int) {

            }
        })
        val bundlePush = Bundle()
        bundlePush.putInt("type", 1)
        scoreDetailPush = ScoreDetailFragment()
        scoreDetailPush!!.arguments = bundlePush

        val bundlePull = Bundle()
        bundlePull.putInt("type", 2)
        scoreDetailPull = ScoreDetailFragment()
        scoreDetailPull!!.arguments = bundlePull

        mAdapter = CustomViewPagerAdapter(arrayListOf(scoreDetailPush, scoreDetailPull), supportFragmentManager)
        viewPager.adapter = mAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                tabLayout.currentTab = p0
            }

        })


        val head = hashMapOf("UID" to UserHolder.getUID(this), "TOKEN" to UserHolder.getUserInfo(this).TOKEN)
        OKHttpUtil(this).urlByHeadData(
            ConstantsUrl.domain + ConstantsUrl.SCORE_DETAIL
                    + "?page=0", head
        ).get()
            .execute(object : BaseBack<ScoreDetailList>() {

                override fun onSuccess(call: Call, bean: ScoreDetailList?) {
                    if (bean != null && bean.code == 0 && bean.explain != null) {
                        ruleInfo = bean.explain
                    }
                }

                override fun onFailure(e: Exception) {
                    super.onFailure(e)
                }

                override fun onComplete() {

                }
            })
    }

}
