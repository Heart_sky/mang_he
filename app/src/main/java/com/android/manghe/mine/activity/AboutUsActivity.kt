package com.android.manghe.mine.activity

import android.os.Build
import android.os.Bundle
import android.text.Html
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.view.MVPActivity
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.base.tools.Glide.GlideHelper
import com.android.base.tools.IntentController
import com.android.base.tools.PackageUtil
import com.android.manghe.R
import com.android.manghe.common.model.BaseBack
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.mine.model.AboutUsRes
import kotlinx.android.synthetic.main.activity_about_us.*
import okhttp3.Call

class AboutUsActivity: MVPActivity<XPresenter<IView>>() {

    private var tel = ""

    override fun getLayoutId(): Int {
        return R.layout.activity_about_us
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("关于我们").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)

        tvVersion.text = "V"+ PackageUtil.getVersionName(this)
        OKHttpUtil(this).url(ConstantsUrl.domain + ConstantsUrl.MAIN_ABOUTUS)
            .get()
            .execute(object : BaseBack<AboutUsRes>() {

                override fun onSuccess(call: Call?, res: AboutUsRes?) {
                    super.onSuccess(call, res)
                    if(res?.code == 0 && res?.data != null){
                        GlideHelper.load(this@AboutUsActivity, res.data.app_logo, ivLogo)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            tvContent.text = Html.fromHtml(res.data.aboutus.replace("<br />",""), Html.FROM_HTML_MODE_COMPACT)
                        } else {
                            tvContent.text = Html.fromHtml(res.data.aboutus.replace("<br />",""))
                        }

                        tvTel.text = "客服电话：${res.data.tel}"
                        tel = res.data.tel
                    }
                }

                override fun onFailure(e: Exception) {
                    super.onFailure(e)
                }

                override fun onComplete() {

                }
            })
        tvTel.setOnClickListener {
            startActivity(IntentController.getTelIntent(tel))
        }
    }
}