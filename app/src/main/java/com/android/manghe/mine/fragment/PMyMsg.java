package com.android.manghe.mine.fragment;

import android.text.TextUtils;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.MyMsgActivity;
import com.android.manghe.mine.model.UploadFileRes;
import okhttp3.Call;

import java.util.HashMap;

public class PMyMsg extends XPresenter<MyMsgActivity> {


    public void setAvatar(String path) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERDATA_EDITAVATAR)
                .uploadFile("file", path)
                .addHead("UID", UserHolder.getUID(getV()))
                .addHead("TOKEN", UserHolder.getUserInfo(getV()).TOKEN)
                .build()
                .execute(new BaseBack<UploadFileRes>() {
                    @Override
                    public void onSuccess(Call call, UploadFileRes res) {
                        super.onSuccess(call, res);
                        getV().hideLoadingDialog();
                        if (res.code == 0) {
                            getV().modifyAvatarOk(res.avatar);
                        } else {
                            getV().modifyFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().modifyFail(null);
                    }
                });
    }

    public void modifyNick(String nick) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("nickname", nick);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERDATA_EDITNICKNAME, headMap)
                .post(dataMap)
                .build()
                .execute(new BaseBack<NetBean>() {
                    @Override
                    public void onSuccess(Call call, NetBean netBean) {
                        getV().hideLoadingDialog();
                        if (netBean != null) {
                            if (netBean.code == 0) {
                                getV().modifyNickOk(nick);
                            } else {
                                getV().modifyFail(netBean.msg);
                            }
                        } else {
                            getV().modifyFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().modifyFail(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

    public void modifySex(String sex) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("sex", TextUtils.equals(sex, "0") ? "1" : "2");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERDATA_EDITSEXY, headMap)
                .post(dataMap)
                .build()
                .execute(new BaseBack<NetBean>() {
                    @Override
                    public void onSuccess(Call call, NetBean netBean) {
                        getV().hideLoadingDialog();
                        if (netBean != null) {
                            if (netBean.code == 0) {
                                getV().modifySexOk(TextUtils.equals(sex, "0") ? "1" : "2");
                            } else {
                                getV().modifyFail(netBean.msg);
                            }
                        } else {
                            getV().modifyFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().modifyFail(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

}
