package com.android.manghe.mine.presenter;

import android.text.TextUtils;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.FeedbackActivity;
import com.android.manghe.mine.model.FeedbackInfoRes;
import com.android.manghe.mine.model.UploadFileRes;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;

public class PFeedback extends XPresenter<FeedbackActivity> {

    private ArrayList<String> mPicLocalPathList = new ArrayList<>();
    private ArrayList<String> mPicUrlList = new ArrayList<>();

    public void setPicList(ArrayList<String> picPath) {
        mPicLocalPathList = picPath;
    }

    public ArrayList<String> getPicList() {
        return mPicLocalPathList;
    }

    public void submit(String content, String tel) {
        if (mPicLocalPathList.size() == 0) {
            //文字
            submitAllContent(content, tel);
        } else {
            //文字+图片
            for (String path : mPicLocalPathList) {
                uploadFile(content, tel, path);
            }
        }
    }

    private void uploadFile(String content, String tel, String path) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBER_UPLOAD)
                .uploadFile("upFile", path)
                .addHead("UID", UserHolder.getUID(getV()))
                .addHead("TOKEN", UserHolder.getUserInfo(getV()).TOKEN)
                .build()
                .execute(new BaseBack<UploadFileRes>() {
                    @Override
                    public void onSuccess(Call call, UploadFileRes res) {
                        super.onSuccess(call, res);
                        if (res.code == 0 && !TextUtils.isEmpty(res.data.toString())) {
                            mPicUrlList.add(res.data.toString());
                            if (mPicUrlList.size() == mPicLocalPathList.size()) {
                                submitAllContent(content, tel);
                            }
                        }else{
                            mPicUrlList.clear();
                            getV().hideLoadingDialog();
                            getV().submitFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        mPicUrlList.clear();
                        getV().hideLoadingDialog();
                        getV().submitFail(null);
                    }
                });

    }

    private void submitAllContent(String content, String tel) {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("content", content);
        data.put("mid", UserHolder.getUID(getV()));
        data.put("mobile", tel);

        if(mPicUrlList.size()!=0){
            data.put("imgone", mPicUrlList.get(0));
            if(mPicUrlList.size() > 1){
                data.put("imgtwo", mPicUrlList.get(1));
            }
            if(mPicUrlList.size() > 2){
                data.put("imgthree", mPicUrlList.get(2));
            }
        }
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");

        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERFEEDBACK, headMap)
                .post(data)
                .build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean bean) {
                        if (bean != null) {
                            if (bean.code == 0) {
                                mPicLocalPathList.clear();
                                mPicUrlList.clear();
                                getV().submitOk(bean.msg);
                            } else {
                                getV().submitFail(bean.msg);
                            }
                        } else {
                            getV().submitFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().submitFail(null);
                    }

                    @Override
                    public void onComplete() {
                        mPicUrlList.clear();
                        getV().hideLoadingDialog();
                    }
                });
    }

    public void getFeedBackMsgCount() {
        HashMap<String, String> data = new HashMap<>();
        data.put("page", "1");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.FEED_BACK_LIST, data, headMap)
                .get()
                .execute(new BaseBack<FeedbackInfoRes>() {

                    @Override
                    public void onSuccess(Call call, FeedbackInfoRes res) {
                        if (res != null && res.code == 0) {
                            getV().setFeedbackDot(res.unread_count > 0);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
