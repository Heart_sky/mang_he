package com.android.manghe.mine.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/31 19:50
 * desc   :
 */
public class testActivity extends MVPActivity {
    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {

    }

    @Override
    public int getLayoutId() {
        return R.layout.test;
    }
}
