package com.android.manghe.mine.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.Glide.GlideHelper
import com.android.base.tools.MediaUtil
import com.android.base.tools.ToastUtil
import com.android.base.tools.permission.OnPermissionResultListener
import com.android.base.tools.permission.PermissionUtil
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.config.events.RefreshUserInfoEvent
import com.android.manghe.mine.fragment.PMyMsg
import com.android.manghe.view.crop.ImageCropHelper
import com.android.manghe.view.dialog.ModifyNickDialog
import com.eightbitlab.rxbus.Bus
import com.isseiaoki.simplecropview.CropImageView
import com.longsh.optionframelibrary.OptionCenterDialog
import com.yanzhenjie.permission.runtime.Permission
import kotlinx.android.synthetic.main.activity_my_msg.*
import me.nereo.multi_image_selector.MultiImageSelector
import me.nereo.multi_image_selector.MultiImageSelectorActivity

class MyMsgActivity : MVPActivity<PMyMsg>(), OnPermissionResultListener {

    private val CODE_IMAGE_SELECT = 100;
    private val CODE_IMAGE_CROP = 101
    private val CODE_BIND_TEL = 102;

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("个人资料").setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black)
        val userInfo = UserHolder.getUserInfo(this)
        userInfo?.let {
            GlideHelper.loadCircle(this, it.avatar, ivAvatar)
            tvUid.text = it.UID
            tvNick.text = it.nickname
            tvSex.text = if (it.sex != null && TextUtils.equals("1", it.sex)) "男" else "女"
            tvTel.text = it.mobile
        }
        if(TextUtils.isEmpty(userInfo.mobile)){
            tvTel.text = "绑定手机号"
        }
        ivAvatar.setOnClickListener {
            PermissionUtil.requestPermissions(
                this, this,
                Permission.WRITE_EXTERNAL_STORAGE, Permission.CAMERA
            )
        }
        layoutNick.setOnClickListener {
            ModifyNickDialog(this@MyMsgActivity, object : ModifyNickDialog.IModifyNickNameListener {
                override fun confirm(nick: String) {
                    showLoadingDialog()
                    p.modifyNick(nick)
                }
            }).show()
        }
        layoutSex.setOnClickListener {
            val list = ArrayList<String>()
            list.add("男")
            list.add("女")
            val optionCenterDialog = OptionCenterDialog()
            optionCenterDialog.show(this, list)
            optionCenterDialog.setItemClickListener { _, _, position, _ ->
                optionCenterDialog.dismiss()
                showLoadingDialog()
                p.modifySex(position.toString())
            }
        }
        layoutTel.setOnClickListener {
            open(ModifyTelActivity::class.java, CODE_BIND_TEL)
        }
        tvModifyPsw.setOnClickListener {
            open(ModifyPswActivity::class.java)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_my_msg
    }

    override fun onGranted(permissions: MutableList<String>?) {
        MultiImageSelector.create()
            .showCamera(true) // show camera or not. true by default
            .single() // multi mode, default mode;
            .start(this, CODE_IMAGE_SELECT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CODE_IMAGE_SELECT) {
                val arrayList = data?.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT)
                if (arrayList != null && !arrayList.isEmpty()) {
                    ImageCropHelper.startCropFree(
                        this, arrayList[0],
                        ImageCropHelper.ImageCropParamsBuilder
                            .create()
                            .compressQuality(90)
                            .cropMode(CropImageView.CropMode.CIRCLE)
                            .outputMaxSize(500, 500)
                            .build(), CODE_IMAGE_CROP
                    )
                }

            } else if (requestCode == CODE_IMAGE_CROP) {
                val resultUri = ImageCropHelper.onActivityResult(resultCode, data)
                if (resultUri != null) {
                    showLoadingDialog()
                    val imagePath = MediaUtil.getPath(this, resultUri)
                    p.setAvatar(imagePath)
                }
            } else if(requestCode == CODE_BIND_TEL){
                modifyTelOk(data!!.getStringExtra("tel"))
            }
        }
    }

    fun modifyFail(text: String?) {
        ToastUtil.showShort(this, if (TextUtils.isEmpty(text)) "修改失败" else text)
    }

    fun modifyAvatarOk(avatarUrl: String?) {
        ToastUtil.showShort(this, "修改成功")
        avatarUrl?.let {
            GlideHelper.loadCircle(this, avatarUrl, ivAvatar)
            Bus.send(RefreshUserInfoEvent())
        }
    }

    fun modifyNickOk(nick: String) {
        tvNick.text = nick
        Bus.send(RefreshUserInfoEvent())
    }
    fun modifySexOk(sex: String) {
        tvSex.text = if(TextUtils.equals(sex, "1")) "男" else "女"
        Bus.send(RefreshUserInfoEvent())
    }

    fun modifyTelOk(tel: String) {
        tvTel.text = tel
        Bus.send(RefreshUserInfoEvent())
    }

}