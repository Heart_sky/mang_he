package com.android.manghe.mine.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.android.manghe.R;
import com.android.manghe.coupon.model.CouponRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.HashMap;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/21 15:58
 * desc   :
 */
public class PriceAdapter extends BaseQuickAdapter<CouponRes.DataBean.CouponInfo, BaseViewHolder> {

    private Context mContext;
    private IPriceListener mListener;


    private HashMap<String, Boolean> mSelectedMap = new HashMap<>();
    private CouponRes.DataBean.CouponInfo mSelectedItemObject;

    public void setListener(IPriceListener listener) {
        this.mListener = listener;
    }

    public PriceAdapter(Context context, @Nullable List<CouponRes.DataBean.CouponInfo> data) {
        super(R.layout.item_price, data);
        this.mContext = context;
    }

    public void update(List<CouponRes.DataBean.CouponInfo> list) {
        mSelectedMap.clear();
        CouponRes.DataBean.CouponInfo info;
        for (int i = 0; i < list.size(); i++) {
            info = list.get(i);
            mSelectedMap.put(info.id, i == 0);
            if (i == 0) {
                mSelectedItemObject = info;
            }
        }
        replaceData(list);
    }

    public void selectedItem(CouponRes.DataBean.CouponInfo info) {
        for (int i = 0; i < getData().size(); i++) {
            mSelectedMap.put(getData().get(i).id, false);
        }
        if (info != null) {
            mSelectedMap.put(info.id, true);
            mSelectedItemObject = info;
        } else {
            mSelectedItemObject = null;
        }
        notifyDataSetChanged();
    }

    public CouponRes.DataBean.CouponInfo getSelectedCoupon() {
        return mSelectedItemObject;
    }


    @Override
    protected void convert(BaseViewHolder helper, CouponRes.DataBean.CouponInfo item) {
        helper.setText(R.id.id_tv_give, "送" + item.free_coin + "币");
        helper.setText(R.id.id_tv_price, item.coin + "币");
        helper.setBackgroundRes(R.id.ly_body, mSelectedMap.get(item.id) ? R.drawable.btn_line_red_shape : R.drawable.shape_bg_slip_behind_2);
        helper.itemView.setOnClickListener(v -> {
            if (null != mListener)
                mListener.onClick(item, helper.getAdapterPosition());
        });

    }


    public interface IPriceListener {
        void onClick(CouponRes.DataBean.CouponInfo item, int position);
    }
}
