package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/29 14:40
 * desc   :
 */
public class UnderAgeModel extends NetBean<UnderAgeModel.DataBean> {


    /**
     * data : {"list":[{"id":"1","mid":"911745","realname":"xxx","id_number":"xxxxxx","mobile":"13333333333","prove_imgs":["https://www.mxmd88.com/upload/1/images/911785/20211227052235541857960_src.jpg","https://www.mxmd88.com/upload/1/images/211228/9f7eb9f225248a87.jpg","https://www.mxmd88.com/upload/1/images/211228/b0341c0e216eb41b.jpg"],"content":"要求退款啊","reply_content":null,"is_handle":"0","ip":"125.34.23.76","c_time":"2021-12-28 14:58:12"}],"list_total":1}
     * flag : true
     */


    public static class DataBean implements Serializable {
        /**
         * list : [{"id":"1","mid":"911745","realname":"xxx","id_number":"xxxxxx","mobile":"13333333333","prove_imgs":["https://www.mxmd88.com/upload/1/images/911785/20211227052235541857960_src.jpg","https://www.mxmd88.com/upload/1/images/211228/9f7eb9f225248a87.jpg","https://www.mxmd88.com/upload/1/images/211228/b0341c0e216eb41b.jpg"],"content":"要求退款啊","reply_content":null,"is_handle":"0","ip":"125.34.23.76","c_time":"2021-12-28 14:58:12"}]
         * list_total : 1
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean implements Serializable {
            /**
             * id : 1
             * mid : 911745
             * realname : xxx
             * id_number : xxxxxx
             * mobile : 13333333333
             * prove_imgs : ["https://www.mxmd88.com/upload/1/images/911785/20211227052235541857960_src.jpg","https://www.mxmd88.com/upload/1/images/211228/9f7eb9f225248a87.jpg","https://www.mxmd88.com/upload/1/images/211228/b0341c0e216eb41b.jpg"]
             * content : 要求退款啊
             * reply_content : null
             * is_handle : 0
             * ip : 125.34.23.76
             * c_time : 2021-12-28 14:58:12
             */

            public String id;
            public String mid;
            public String realname;
            public String id_number;
            public String mobile;
            public String content;
            public String reply_content;
            public String is_handle;
            public String ip;
            public String c_time;
            public List<String> prove_imgs;
            public String reply_time;
        }
    }
}
