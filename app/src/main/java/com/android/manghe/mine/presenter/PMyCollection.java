package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.model.CollectionRes;
import com.android.manghe.mine.activity.MyCollection;

import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PMyCollection extends XPresenter<MyCollection> {
    private int mCurrentPage = 1;
    private final String PageSize = "10";
    private final List<CollectionRes.DataBean.ListBean> mCollectionList = new ArrayList<>();
    public void getData(boolean isRefresh){
        if (isRefresh) {
            getV().setCanLoadMore(false);
            mCurrentPage = 1;
            mCollectionList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("size", PageSize);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MyCollection,data, headMap)
                .get()
                .execute(new BaseBack<CollectionRes>() {

                    @Override
                    public void onSuccess(Call call, CollectionRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0 && res.data != null) {
                            mCollectionList.addAll(res.data.list);
                            getV().setCanLoadMore(res.data.list.size() >= 10);
                            getV().update(mCollectionList);
                            if (mCollectionList.size() == 10) {
                                mCurrentPage++;
                            }
                        }else{
                            getV().update(mCollectionList);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void cancelCollection(String id){
        HashMap<String, String> data = new HashMap<>();
        data.put("auction_id", id);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.CancelCollection, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if(res.code == 0) {
                                ToastUtil.showShort(getV(), "取消成功");
                                getData(true);
                            }else{
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        }else{
                            ToastUtil.showShort(getV(), "取消失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        ToastUtil.showShort(getV(), "取消失败");
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
