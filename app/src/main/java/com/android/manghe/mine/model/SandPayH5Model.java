package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SandPayH5Model extends NetBean<SandPayH5Model.Data> {


    /**
     * data : {"paycode":{"head":{"respTime":"20220117140323","respMsg":"成功","version":"1.0","respCode":"000000"},"body":{"totalAmount":"000000000010","credential":"<html><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/><\/head><body><form id ='pay_form' name='pay_form' action='https://cashier.sandpay.com.cn/gateway/api/order/jumpQuickH5' method='post' accept-charset='UTF-8'><input type='hidden' name = 'sid' id='sid' value='SMP2201171403236312233598'/><input type='hidden' name = 'mid' id='mid' value='6888806041519'/><input type='hidden' name = 'orderCode' id='orderCode' value='20220117140323_489'/><input type='hidden' name = 'totalAmount' id='totalAmount' value='000000000010'/><\/form><\/body><script type='text/javascript'>document.pay_form.submit() <\/script><\/html>","orderCode":"20220117140323_489"}},"order":{"bid":"0","voucher_id":"94","mid":911767,"title":"芒铺开盒币","thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/d/29174_src.jpg","summary":"玩转芒铺赢好礼！","content":"芒铺开盒币","mobile":" ","c_time":1642399403,"start_time":"0","end_time":"0","pay_id":71,"pay_time":0,"count":1,"cost_price":"30.00","cost_amount":30,"price":"0.10","amount":0.1,"coin":"30","free_coin":"1","coin_total":30,"status":1,"favourable_value":0,"order_id":437,"log_id":489,"out_trade_no":"20220117140323_489","order_sn":"20220117140323_489","goods_name":"芒铺开盒币","order_amount":0.1,"items":[{"service_identify":"10000001","subject":"芒铺开盒币","product_type":"3","goods_count":1}]}}
     * flag : true
     */


    public static class Data implements Serializable {
        /**
         * paycode : {"head":{"respTime":"20220117140323","respMsg":"成功","version":"1.0","respCode":"000000"},"body":{"totalAmount":"000000000010","credential":"<html><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/><\/head><body><form id ='pay_form' name='pay_form' action='https://cashier.sandpay.com.cn/gateway/api/order/jumpQuickH5' method='post' accept-charset='UTF-8'><input type='hidden' name = 'sid' id='sid' value='SMP2201171403236312233598'/><input type='hidden' name = 'mid' id='mid' value='6888806041519'/><input type='hidden' name = 'orderCode' id='orderCode' value='20220117140323_489'/><input type='hidden' name = 'totalAmount' id='totalAmount' value='000000000010'/><\/form><\/body><script type='text/javascript'>document.pay_form.submit() <\/script><\/html>","orderCode":"20220117140323_489"}}
         * order : {"bid":"0","voucher_id":"94","mid":911767,"title":"芒铺开盒币","thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/d/29174_src.jpg","summary":"玩转芒铺赢好礼！","content":"芒铺开盒币","mobile":" ","c_time":1642399403,"start_time":"0","end_time":"0","pay_id":71,"pay_time":0,"count":1,"cost_price":"30.00","cost_amount":30,"price":"0.10","amount":0.1,"coin":"30","free_coin":"1","coin_total":30,"status":1,"favourable_value":0,"order_id":437,"log_id":489,"out_trade_no":"20220117140323_489","order_sn":"20220117140323_489","goods_name":"芒铺开盒币","order_amount":0.1,"items":[{"service_identify":"10000001","subject":"芒铺开盒币","product_type":"3","goods_count":1}]}
         */

        @SerializedName("paycode")
        public Paycode paycode;
        @SerializedName("order")
        public Order order;

        public static class Paycode implements Serializable {
            /**
             * head : {"respTime":"20220117140323","respMsg":"成功","version":"1.0","respCode":"000000"}
             * body : {"totalAmount":"000000000010","credential":"<html><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/><\/head><body><form id ='pay_form' name='pay_form' action='https://cashier.sandpay.com.cn/gateway/api/order/jumpQuickH5' method='post' accept-charset='UTF-8'><input type='hidden' name = 'sid' id='sid' value='SMP2201171403236312233598'/><input type='hidden' name = 'mid' id='mid' value='6888806041519'/><input type='hidden' name = 'orderCode' id='orderCode' value='20220117140323_489'/><input type='hidden' name = 'totalAmount' id='totalAmount' value='000000000010'/><\/form><\/body><script type='text/javascript'>document.pay_form.submit() <\/script><\/html>","orderCode":"20220117140323_489"}
             */

            @SerializedName("head")
            public Head head;
            @SerializedName("body")
            public Body body;

            public static class Head implements Serializable {
                /**
                 * respTime : 20220117140323
                 * respMsg : 成功
                 * version : 1.0
                 * respCode : 000000
                 */

                @SerializedName("respTime")
                public String respTime;
                @SerializedName("respMsg")
                public String respMsg;
                @SerializedName("version")
                public String version;
                @SerializedName("respCode")
                public String respCode;
            }

            public static class Body implements Serializable {
                /**
                 * totalAmount : 000000000010
                 * credential : <html><head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'/></head><body><form id ='pay_form' name='pay_form' action='https://cashier.sandpay.com.cn/gateway/api/order/jumpQuickH5' method='post' accept-charset='UTF-8'><input type='hidden' name = 'sid' id='sid' value='SMP2201171403236312233598'/><input type='hidden' name = 'mid' id='mid' value='6888806041519'/><input type='hidden' name = 'orderCode' id='orderCode' value='20220117140323_489'/><input type='hidden' name = 'totalAmount' id='totalAmount' value='000000000010'/></form></body><script type='text/javascript'>document.pay_form.submit() </script></html>
                 * orderCode : 20220117140323_489
                 */

                @SerializedName("totalAmount")
                public String totalAmount;
                @SerializedName("credential")
                public String credential;
                @SerializedName("orderCode")
                public String orderCode;
            }
        }

        public static class Order implements Serializable {
            /**
             * bid : 0
             * voucher_id : 94
             * mid : 911767
             * title : 芒铺开盒币
             * thumb : https://www.mxmd88.com/upload/1/images/gallery/i/d/29174_src.jpg
             * summary : 玩转芒铺赢好礼！
             * content : 芒铺开盒币
             * mobile :
             * c_time : 1642399403
             * start_time : 0
             * end_time : 0
             * pay_id : 71
             * pay_time : 0
             * count : 1
             * cost_price : 30.00
             * cost_amount : 30
             * price : 0.10
             * amount : 0.1
             * coin : 30
             * free_coin : 1
             * coin_total : 30
             * status : 1
             * favourable_value : 0
             * order_id : 437
             * log_id : 489
             * out_trade_no : 20220117140323_489
             * order_sn : 20220117140323_489
             * goods_name : 芒铺开盒币
             * order_amount : 0.1
             * items : [{"service_identify":"10000001","subject":"芒铺开盒币","product_type":"3","goods_count":1}]
             */

            @SerializedName("bid")
            public String bid;
            @SerializedName("voucher_id")
            public String voucherId;
            @SerializedName("mid")
            public Integer mid;
            @SerializedName("title")
            public String title;
            @SerializedName("thumb")
            public String thumb;
            @SerializedName("summary")
            public String summary;
            @SerializedName("content")
            public String content;
            @SerializedName("mobile")
            public String mobile;
            @SerializedName("c_time")
            public Integer cTime;
            @SerializedName("start_time")
            public String startTime;
            @SerializedName("end_time")
            public String endTime;
            @SerializedName("pay_id")
            public Integer payId;
            @SerializedName("pay_time")
            public Integer payTime;
            @SerializedName("count")
            public Integer count;
            @SerializedName("cost_price")
            public String costPrice;
            @SerializedName("cost_amount")
            public Integer costAmount;
            @SerializedName("price")
            public String price;
            @SerializedName("amount")
            public Double amount;
            @SerializedName("coin")
            public String coin;
            @SerializedName("free_coin")
            public String freeCoin;
            @SerializedName("coin_total")
            public Integer coinTotal;
            @SerializedName("status")
            public Integer status;
            @SerializedName("favourable_value")
            public Integer favourableValue;
            @SerializedName("order_id")
            public String orderId;
            @SerializedName("log_id")
            public Integer logId;
            @SerializedName("out_trade_no")
            public String outTradeNo;
            @SerializedName("order_sn")
            public String orderSn;
            @SerializedName("goods_name")
            public String goodsName;
            @SerializedName("order_amount")
            public Double orderAmount;
            @SerializedName("items")
            public List<Items> items;


            public static class Items implements Serializable {
                /**
                 * service_identify : 10000001
                 * subject : 芒铺开盒币
                 * product_type : 3
                 * goods_count : 1
                 */

                @SerializedName("service_identify")
                public String serviceIdentify;
                @SerializedName("subject")
                public String subject;
                @SerializedName("product_type")
                public String productType;
                @SerializedName("goods_count")
                public Integer goodsCount;
            }
        }
    }
}
