package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.fragment.OneLevelFansFragment;
import com.android.manghe.mine.model.FansRes;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class POneLevelFans extends XPresenter<OneLevelFansFragment> {
    private int mCurrentPage = 1;
    private final String PageSize = "10";
    private final List<FansRes.DataBean.ListBean> mAuctionList = new ArrayList<>();


    private String mLevel;

    public void setLevel(String level) {
        mLevel = level;
    }

    public void getData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mAuctionList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("size", PageSize);
//        data.put("level", mLevel);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERFANS, data, headMap)
                .get()
                .execute(new BaseBack<FansRes>() {

                    @Override
                    public void onSuccess(Call call, FansRes res) {
                        if (res != null && res.code == 0 && res.data != null) {
                            mAuctionList.addAll(res.data.list);
                            getV().setCanLoadMore(res.data.list.size() == 10);
                            getV().update(mAuctionList);
                            if (mAuctionList.size() == 10) {
                                mCurrentPage++;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
