package com.android.manghe.mine.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.title.ETitleType;
import com.android.base.frame.view.MVPFragment;
import com.android.base.tools.ToastUtil;
import com.android.manghe.R;
import com.android.manghe.mine.adapter.InviteAdapter;
import com.android.manghe.mine.model.FansRes;
import com.android.manghe.mine.presenter.PInvite;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import static com.android.base.frame.title.ETitleType.NO_TITLE;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/22 15:15
 * desc   :  我的邀请
 */
public class InviteFragment extends MVPFragment<PInvite> implements IRefresh {

    private RecyclerView mRecyclerView;
    private InviteAdapter mAdapter;


    @Override
    protected ETitleType showToolBarType() {
        return NO_TITLE;
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return mView.findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        getP().getData(true);
    }

    @Override
    public void onLoad() {
        getP().getData(false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new InviteAdapter(mContext, new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
        showLoadingDialog();
        getP().getData(true);


    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState, View parent) {


    }

    public void update(List<FansRes.DataBean.ListBean> listBeans) {
        mAdapter.updateList(listBeans);
    }

    public void showErrorTip() {
        ToastUtil.showLong(mContext, "获取失败，请重试");
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_invite;
    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }
}
