package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.fragment.MyKanAllFragment;
import com.android.manghe.mine.model.MyKanRes;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PMyKanAll extends XPresenter<MyKanAllFragment> {
    private int PageSize = 10;
    private int mCurrentPage = 1;
    private int mType = 0;
    private List<MyKanRes.MyKan.ListBean> mAuctionList = new ArrayList<>();

    public void setType(int type) {
        mType = type;
    }

    public void loadData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mAuctionList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("size", PageSize + "");
        data.put("type", mType + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERAUCTIONORDER, data, headMap)
                .get()
                .execute(new BaseBack<MyKanRes>() {

                    @Override
                    public void onSuccess(Call call, MyKanRes res) {
                        if (res != null && res.code == 0 && res.data != null && res.data.list != null) {
                            mAuctionList.addAll(res.data.list);
                            getV().setCanLoadMore(mAuctionList.size() >= PageSize);
                            getV().update(mAuctionList);
                            mCurrentPage++;
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
