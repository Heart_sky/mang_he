package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.manghe.R;
import com.android.manghe.mine.model.SignDesignInfo;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/03 16:09
 * desc   :
 */
public class SignAdapter extends BaseQuickAdapter<SignDesignInfo, BaseViewHolder> {

    private Context mContext;
    private ISignListener mListener;

    public void setListener(ISignListener listener) {
        this.mListener = listener;
    }

    public SignAdapter(Context context, @Nullable List<SignDesignInfo> data) {
        super(R.layout.item_sign, data);
        this.mContext = context;
    }

    public void updateList(List<SignDesignInfo> list) {
        replaceData(list);
    }

    @Override
    protected void convert(BaseViewHolder helper, SignDesignInfo item) {
        helper.setImageResource(R.id.ivImg1, item.getIcon());
        helper.setText(R.id.id_tv_name, item.getName());
        helper.setText(R.id.id_tv_desc, item.getDesc());
        helper.setText(R.id.id_tv_money, "+" + item.getSignMoney());
        helper.setBackgroundRes(R.id.id_tv_is_sign, item.isSign() ?
                R.drawable.btn_disable_round_selector : R.drawable.btn_gradient_shape);
        helper.setTextColor(R.id.id_tv_is_sign,
                ContextCompat.getColor(mContext, item.isSign() ? R.color.white : R.color.colorFont33));

        helper.itemView.findViewById(R.id.id_tv_is_sign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(helper.getAdapterPosition() + 1);
                }
            }
        });
    }

    public interface ISignListener {
        void onClick(int position);
    }
}
