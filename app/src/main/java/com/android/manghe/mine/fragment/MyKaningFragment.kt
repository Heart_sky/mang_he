package com.android.manghe.mine.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IStateController
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.XStateController
import com.android.manghe.R
import com.android.manghe.common.frame.fragment.MVPWebSocketFragment
import com.android.manghe.common.model.AuctionSocketRes
import com.android.manghe.mine.adapter.MyKaningAdapter
import com.android.manghe.mine.presenter.PMyKaning
import com.zhangke.websocket.ErrorResponse
import com.zhangke.websocket.Response
import kotlinx.android.synthetic.main.fragment_my_kan.*


class MyKaningFragment : MVPWebSocketFragment<PMyKaning>(), IStateController<XStateController> {
    override fun getStateView(): XStateController {
        return mView.findViewById(R.id.xStateController)
    }
    override fun onMessageResponse(message: Response<*>) {
        p.dealSocketData(message.responseText)
    }

    override fun onSendMessageError(error: ErrorResponse) {
        p.loadData()
    }
    private var mAdapter: MyKaningAdapter? = null

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_my_kaning
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = MyKaningAdapter(mContext)
        recyclerView.layoutManager =
            LinearLayoutManager(mContext)
        recyclerView.itemAnimator = null
        recyclerView.adapter = mAdapter
        p.loadData()
    }

    fun update(dataList: List<AuctionSocketRes.ListBean>) {
        try{
            mAdapter?.let {
                it.update(dataList)
                if (dataList.isEmpty()) {
                    xStateController.showEmpty()
                } else {
                    xStateController.showContent()
                }
            }
        }catch (e: Exception){

        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }


    override fun onDestroy() {
        super.onDestroy()
        p.stopSocketTimer()
    }
}
