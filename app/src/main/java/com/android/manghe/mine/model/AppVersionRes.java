package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class AppVersionRes extends NetBean<AppVersionRes.AppVersion> {

    public static class AppVersion{
        public String version;
        public String name;
        public String content;
        public String down_url;
    }

}
