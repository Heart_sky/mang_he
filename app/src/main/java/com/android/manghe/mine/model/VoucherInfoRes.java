package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class VoucherInfoRes extends NetBean {

    /**
     * content : 1.商城购买商品时使用。
     * name : 【全局】购物卡说明
     * flag : true
     * data : null
     */

    public String content;
    public String name;
}
