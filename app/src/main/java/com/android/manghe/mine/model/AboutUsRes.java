package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class AboutUsRes extends NetBean<AboutUsRes.AboutUs> {
    public static class AboutUs{
        public String aboutus;
        public String tel;
        public String app_logo;
        public String version;
    }
}
