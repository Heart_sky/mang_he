package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class FansRes extends NetBean<FansRes.DataBean> {


    public static class DataBean {
        public List<ListBean> list;

        public static class ListBean {
            /**
             * ivt_mid : 29003
             * score : 0
             * c_time : 1546950025
             * id : 9472
             * mid : 91162
             * level : 1
             * nickname :
             * username : 18373857***
             * free_coin : 10
             * photo : https:\/\/www.shanqu8.com\/common\/photo.gif
             */

            public String ivt_mid;
            public String score;
            public String c_time;
            public String id;
            public String mid;
            public String level;
            public String nickname;
            public String username;
            public String free_coin;
            public String photo;
        }
    }
}
