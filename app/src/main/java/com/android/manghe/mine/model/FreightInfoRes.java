package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class FreightInfoRes extends NetBean<FreightInfoRes.DataBean> {

    public static class DataBean {
        public String order_sn;
        public List<LogisticBean> wuliu;
        public String express;
        public String contact_mobile;
        public String express_num;

    }

    public static class LogisticBean {
        public String time;
        public String context;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getContext() {
            return context;
        }

        public void setContext(String context) {
            this.context = context;
        }
    }
}
