package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.mine.model.FansRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/23 10:39
 * desc   :
 */
public class InviteAdapter extends BaseQuickAdapter<FansRes.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;
    private View emptyView;

    public InviteAdapter(Context context, @Nullable List<FansRes.DataBean.ListBean> data) {
        super(R.layout.item_invite, data);
        this.mContext = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void updateList(List<FansRes.DataBean.ListBean> listBeans) {
        replaceData(listBeans);
    }

    @Override
    protected void convert(BaseViewHolder helper, FansRes.DataBean.ListBean item) {
        GlideHelper.loadAvatar(mContext, item.photo, helper.getView(R.id.id_img_src));
        helper.setText(R.id.id_tv_name, item.username);
        helper.setText(R.id.id_tv_time, "注册时间：" + item.c_time);
    }
}
