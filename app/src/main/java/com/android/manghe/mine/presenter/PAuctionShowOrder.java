package com.android.manghe.mine.presenter;

import android.text.TextUtils;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.AuctionShowOrderActivity;
import com.android.manghe.mine.model.UploadFileRes;
import com.google.gson.Gson;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;

public class PAuctionShowOrder extends XPresenter<AuctionShowOrderActivity> {
    private String orderId = "";
    private String goodId = "";

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    private ArrayList<String> mPicLocalPathList = new ArrayList<>();
    private ArrayList<String> mPicUrlList = new ArrayList<>();

    public void setPicList(ArrayList<String> picPath) {
        mPicLocalPathList = picPath;
    }

    public ArrayList<String> getPicList() {
        return mPicLocalPathList;
    }

    public void submit(String content, int rating) {
        if (mPicLocalPathList.size() == 0) {
            //文字
            submitAllContent(content, rating);
        } else {
            //文字+图片
            for (String path : mPicLocalPathList) {
                uploadFile(content, rating, path);
            }
        }
    }

    private void uploadFile(String content, int rating, String path) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBER_UPLOAD)
                .uploadFile("upFile", path)
                .addHead("UID", UserHolder.getUID(getV()))
                .addHead("TOKEN", UserHolder.getUserInfo(getV()).TOKEN)
                .build()
                .execute(new BaseBack<UploadFileRes>() {
                    @Override
                    public void onSuccess(Call call, UploadFileRes res) {
                        super.onSuccess(call, res);
                        if (res.code == 0 && !TextUtils.isEmpty(res.data.toString())) {
                            mPicUrlList.add(res.data.toString());
                            if (mPicUrlList.size() == mPicLocalPathList.size()) {
                                submitAllContent(content, rating);
                            }
                        } else {
                            mPicUrlList.clear();
                            getV().hideLoadingDialog();
                            getV().submitFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        mPicUrlList.clear();
                        getV().hideLoadingDialog();
                        getV().submitFail(null);
                    }
                });

    }

    private void submitAllContent(String content, int rating) {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("order_id", orderId);
        data.put("content", content);
        data.put("star", rating + "");
        if(mPicUrlList.size() != 0)
            data.put("thumbs", new Gson().toJson(mPicUrlList));

        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");

        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ACUTION_COMMENT, headMap)
                .post(data)
                .build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean bean) {
                        if (bean != null) {
                            if (bean.code == 0) {
                                mPicLocalPathList.clear();
                                mPicUrlList.clear();
                                getV().submitOk(bean.msg);
                            } else {
                                getV().submitFail(bean.msg);
                            }
                        } else {
                            getV().submitFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().submitFail(null);
                    }

                    @Override
                    public void onComplete() {
                        mPicUrlList.clear();
                        getV().hideLoadingDialog();
                    }
                });
    }
}
