package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

/**
 * Created by lnest-php on 2017/9/26.
 */

public class ScoreDetailList extends NetBean<ScoreDetailList.ListBean> {

    /**
     * list : [{"id":"113","mid":"4","type":"1","amount":"1","remark":"第1天连续签到积分","c_time":"2017-09-26 09:22:53"},{"id":"50","mid":"4","type":"1","amount":"3","remark":"第3天连续签到积分","c_time":"2017-09-22 09:50:55"},{"id":"23","mid":"4","type":"1","amount":"2","remark":"第2天连续签到积分","c_time":"2017-09-21 11:36:08"},{"id":"4","mid":"4","type":"1","amount":"10","remark":"第1天连续签到积分","c_time":"2017-09-20 16:14:29"}]
     * list_total : 4
     * list_pages : 1
     * flag : true
     * code : 0
     * msg : 操作成功
     * data : null
     * time : 1506391804
     */

    public int list_total;
    public int list_pages;
    public String explain;
    public List<ListBean> list;

    public static class ListBean {
        /**
         * id : 113
         * mid : 4
         * type : 1
         * amount : 1
         * remark : 第1天连续签到积分
         * c_time : 2017-09-26 09:22:53
         */

        public String id;
        public String mid;
        public String type;
        public String amount;
        public String remark;
        public String c_time;
    }
}