package com.android.manghe.mine.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.view.MVPActivity
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.mine.adapter.AddressListAdapter
import com.android.manghe.mine.model.AddressInfo
import com.android.manghe.mine.presenter.PAddressList
import com.android.manghe.view.dialog.NewTipDialog
import com.longsh.optionframelibrary.OptionMaterialDialog
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_address_list.*
import kotlinx.android.synthetic.main.no_data_address.*

class AddressListActivity :MVPActivity<PAddressList>(),IRefresh {

    var isToSelect = false

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    private var mAdapter: AddressListAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_address_list
    }

    override fun onBackPressed() {
        if(p.mAddressList.isNotEmpty()) {
            var resultItem: AddressInfo? = null
            p.mAddressList.forEach {
                if (TextUtils.equals("1", it.is_default)) {
                    resultItem = it
                }
            }
            if (resultItem == null) {
                resultItem = p.mAddressList[0]
            }
            val intent = Intent()
            intent.putExtra("addressInfo", resultItem)
            setResult(Activity.RESULT_OK, intent)
        }
        finish()
    }

    override fun initData(savedInstanceState: Bundle?) {
        isToSelect = intent.getBooleanExtra("isToSelect", false)

        titleBar.setToolbar("收货地址").setLeftIcon(R.mipmap.icon_back_black, object :View.OnClickListener{
            override fun onClick(v: View?) {
                onBackPressed()
            }

        }).setTitleAndStatusBgColor(R.color.white)
        mAdapter = AddressListAdapter(this, p.mAddressList)
        val linearManager =
            LinearLayoutManager(this)
        recyclerView.addItemDecoration(RecyclerViewDivider(this, LinearLayoutManager.VERTICAL))
        recyclerView.layoutManager = linearManager
        recyclerView.adapter = mAdapter
        mAdapter!!.setOnItemLongClickListener { view, item ->
            if(!isToSelect) {
                val mDialog = NewTipDialog(this, "确定删除该收货地址吗")
                mDialog.show()
                mDialog.setListener(object : NewTipDialog.ITipDialogListener {
                    override fun clickRight() {
                        showLoadingDialog()
                        p.deleteAddress(item.id)
                    }

                    override fun clickLeft() {

                    }
                })
            }
        }

        mAdapter!!.setOnItemClickListener{_,item->
            if(isToSelect){
                val intent = Intent()
                intent.putExtra("addressInfo", item)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }

        btnAddAddr.setOnClickListener {
            open(AddModifyAddrActivity::class.java)
        }

        btnAddAddrNoData.setOnClickListener {
            open(AddModifyAddrActivity::class.java)
        }
    }

    override fun onResume() {
        super.onResume()
        p.getAddressList()
    }
    override fun onLoad() {

    }
    override fun onRefresh() {
        layoutNoDataAddress.visibility = View.VISIBLE
        btnAddAddr.visibility = View.GONE
        p.getAddressList()
    }

    fun update(addrList: List<AddressInfo>) {
        mAdapter?.let {
            it.update(addrList)
        }
        layoutNoDataAddress.visibility = if(addrList.isEmpty()) View.VISIBLE else View.GONE
        btnAddAddr.visibility = if(addrList.isNotEmpty()) View.VISIBLE else View.GONE
    }
}