package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class MyKanRes extends NetBean<MyKanRes.MyKan> {

    public static class MyKan {
        public List<ListBean> list;

        public static class ListBean {

            public int auctioning_itemType;
            public int auctioning_time;
            public int auctioning_start;
            public String auctioning_id;
            public String auctioning_period;
            public String auctioning_title;
            public String auctioning_thumb;
            public String auctioning_price;
            public String auctioning_mid;
            public String auctioning_username;

            public String address_id;
            public String address;
            public String amount;
            public String area;
            public String auction_id;
            public String brokerage;
            public String c_time;
            public String change_price_reason;
            public String cid;
            public String close_reason;
            public String coin_count;
            public String coin_type;
            public String comment_time;
            public String content;
            public String cost_price;
            public String express_id;
            public String express_message;
            public String express_name;
            public String express_num;
            public String express_pinyin;
            public String express_price;
            public String express_time;
            public String first_countdown;
            public String for_new;
            public String id;
            public String is_virtual;
            public String join_count;
            public String market_price;
            public String mid;
            public String mid_coin_count;
            public String mid_count;
            public String mid_last_price;
            public String mid_order_id;
            public String mobile;
            public int order_code;
            public String order_sn;
            public String order_status;
            public String pay_amount;
            public String pay_fee;
            public String pay_id;
            public String pay_name;
            public String pay_time;
            public String per_countdown;
            public String per_price;
            public String period;
            public String photo;
            public String price;
            public String receive_expire;
            public String receive_message;
            public String receive_name;
            public String receive_pay_reason;
            public String receive_time;
            public String returns_rate;
            public String returns_score;
            public String send_time;
            public String start_price;
            public String status;
            public String safe_price;
            public String safe_rate;
            public String safe_end;
            public String thumb;
            public List<String> thumbs;
            public String title;
            public int type;
            public String username;
            public String virtual_code;
            public String zone;
        }
    }
}
