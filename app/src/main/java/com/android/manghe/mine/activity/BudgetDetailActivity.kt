package com.android.manghe.mine.activity

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.android.base.frame.view.MVPActivity
import com.android.base.view.viewpager.CustomViewPagerAdapter
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.main.view.TabEntity
import com.android.manghe.mine.fragment.CoinDetailFragment
import com.android.manghe.mine.presenter.PBudgetDetail
import com.android.manghe.user.activity.LoginActivity
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_budget_detail.*

class BudgetDetailActivity : MVPActivity<PBudgetDetail>() {
    private var mAdapter: CustomViewPagerAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_budget_detail
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar!!.setToolbar("抵用金明细", R.color.white)
            .setLeftIcon(R.mipmap.icon_back_white)
            .hideBottomLine()
            .setBackgroundResource(R.drawable.bg_gradient_org_shape)

        if (UserHolder.getUserInfo(this) == null){
            finish()
            open(LoginActivity::class.java)
            return
        }
        tvTotal.text = intent.getDoubleExtra("exemptAmount", 0.0).toString()

        val tabEntities = arrayListOf<CustomTabEntity>(TabEntity("支出", 0, 0), TabEntity("收入", 0, 0))
        tabLayout.setTabData(tabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                viewPager?.currentItem = position
            }

            override fun onTabReselect(position: Int) {

            }
        })
        val bundlePush = Bundle()
        bundlePush.putInt("type", 1)
        val coinDetailPush = CoinDetailFragment()
        coinDetailPush.arguments = bundlePush

        val bundlePull = Bundle()
        bundlePull.putInt("type", 2)
        val coinDetailPull = CoinDetailFragment()
        coinDetailPull.arguments = bundlePull

        mAdapter = CustomViewPagerAdapter(arrayListOf(coinDetailPush, coinDetailPull), supportFragmentManager)
        viewPager.adapter = mAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                tabLayout.currentTab = p0
            }

        })
    }

}
