package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class VoucherRes extends NetBean<VoucherRes.DataBean> {


    /**
     * list : []
     * list_total : 0
     * remain_coupon_total : 0
     * flag : true
     * data : null
     */

    public int list_total;
    public double remain_coupon_total;
    public String voucher_explain;
    public List<DataBean> list;

    public static class DataBean{
        public String title;
        public String gain_time;
        public String used_time;
        public String coupon_value;
    }
}
