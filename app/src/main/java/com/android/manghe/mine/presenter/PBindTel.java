package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.ModifyTelActivity;

import java.util.HashMap;

import okhttp3.Call;

public class PBindTel extends XPresenter<ModifyTelActivity> {
    public String scodeToken = "";

    public void sendSmsCode(String tel, String picCode) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERORSENDMESSAGE)
                .post("mobile", tel)
                .post("act", "sms_code")
                .post("scode", picCode)
                .post("scode_token", scodeToken)
                .build().execute(new BaseBack<NetBean>() {

            @Override
            public void onSuccess(Call call, NetBean bean) {
                if (bean.code == 0) {
                    getV().sendCodeOk();
                } else {
                    getV().sendCodeFail(bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().sendCodeFail(null);
            }

            @Override
            public void onComplete() {
                getV().closeLoadingDialog();
            }
        });
    }


    public void modify(String tel, String code) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBER_BIND_MOBILE, headMap)
                .post("mobile", tel)
                .post("verifycode", code)
                .post("scode_token", scodeToken).build()
                .execute(new BaseBack<NetBean>() {

            @Override
            public void onSuccess(Call call, NetBean bean) {
                if (bean.code == 0) {
                    getV().modifyOk();
                } else {
                    getV().modifyFail(bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().modifyFail(null);
            }

            @Override
            public void onComplete() {
                getV().closeLoadingDialog();
            }
        });
    }
}
