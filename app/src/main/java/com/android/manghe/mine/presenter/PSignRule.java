package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.activity.SignRuleActivity;
import com.android.manghe.mine.model.SignInRCData;

import java.util.HashMap;

import okhttp3.Call;

public class PSignRule extends XPresenter<SignRuleActivity> {

    public void getRule(){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<String, String>();
        headMap.put("UID",UserHolder.getUID(getV()));
        headMap.put("TOKEN",UserHolder.getUserInfo(getV()).TOKEN);
        new OKHttpUtil(getV()).urlByHeadData(
                ConstantsUrl.domain + ConstantsUrl.MEMBERFSIGN,
                new HashMap<String, String>(),
                headMap
        ).get().execute(new BaseBack<SignInRCData>() {

            @Override
            public void onSuccess(Call call, SignInRCData bean) {
                if (bean.code == 0 && bean.data != null) {
                    getV().showText(bean.data.explain);
                }else{
                    ToastUtil.showShort(getV(), "获取规则失败");
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                ToastUtil.showShort(getV(), "获取规则失败");
            }

            @Override
            public void onComplete() {
                getV().hideLoadingDialog();
            }
        });
    }
}
