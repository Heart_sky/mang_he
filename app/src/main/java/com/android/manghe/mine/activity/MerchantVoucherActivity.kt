package com.android.manghe.mine.activity

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import android.text.TextUtils
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.view.MVPActivity
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.base.view.viewpager.CustomViewPagerAdapter
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.common.model.BaseBack
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.main.view.TabEntity
import com.android.manghe.mine.fragment.MerchantVoucherFragment
import com.android.manghe.mine.model.VoucherInfoRes
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_budget_detail.tabLayout
import kotlinx.android.synthetic.main.activity_budget_detail.tvTotal
import kotlinx.android.synthetic.main.activity_budget_detail.viewPager
import kotlinx.android.synthetic.main.activity_voucher.*
import okhttp3.Call

/**
 * 我的购物卡
 * @author ZhangWeiJun
 * @date 2019/5/28
 */

class MerchantVoucherActivity : MVPActivity<XPresenter<IView>>() {
    private var mAdapter: CustomViewPagerAdapter? = null

    private var voucherDetailPush: MerchantVoucherFragment? = null
    private var voucherDetailPull: MerchantVoucherFragment? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_voucher
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar!!.setToolbar("我的购物卡", R.color.white)
            .setLeftIcon(R.mipmap.icon_back_white)
            .hideBottomLine()
            .setBackgroundResource(R.drawable.bg_gradient_org_shape)

        val tabEntities = arrayListOf<CustomTabEntity>(TabEntity("收入明细", 0, 0), TabEntity("支出明细", 0, 0))
        tabLayout.setTabData(tabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                viewPager?.currentItem = position
            }

            override fun onTabReselect(position: Int) {

            }
        })
        val bundlePush = Bundle()
        bundlePush.putInt("type", 1)
        voucherDetailPush = MerchantVoucherFragment()
        voucherDetailPush!!.arguments = bundlePush
        voucherDetailPush!!.setListener(object : MerchantVoucherFragment.IMerchantVoucherFragmentListener{
            override fun showTotalVoucher(total: String?) {
                if(total!= null)
                    tvTotal.text = total
            }
        })

        val bundlePull = Bundle()
        bundlePull.putInt("type", 2)
        voucherDetailPull = MerchantVoucherFragment()
        voucherDetailPull!!.arguments = bundlePull
        voucherDetailPull!!.setListener(object : MerchantVoucherFragment.IMerchantVoucherFragmentListener{
            override fun showTotalVoucher(total: String?) {
                if(total!= null)
                    tvTotal.text = total
            }
        })

        mAdapter = CustomViewPagerAdapter(arrayListOf(voucherDetailPush, voucherDetailPull), supportFragmentManager)
        viewPager.adapter = mAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                tabLayout.currentTab = p0
            }
        })

        val head = hashMapOf("UID" to UserHolder.getUID(this), "TOKEN" to UserHolder.getUserInfo(this).TOKEN)
        OKHttpUtil(this).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.VOUCHER_EXPLAIN, head).get()
            .execute(object : BaseBack<VoucherInfoRes>() {

                override fun onSuccess(call: Call, bean: VoucherInfoRes?) {
                    if(bean != null && !TextUtils.isEmpty(bean.content)) {
                        tvExplain.text = bean.content
                    }
                }

                override fun onFailure(e: Exception) {
                    super.onFailure(e)
                }

                override fun onComplete() {

                }
            })
    }

}
