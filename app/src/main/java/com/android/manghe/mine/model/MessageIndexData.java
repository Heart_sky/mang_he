package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class MessageIndexData extends NetBean<MessageIndexData.DataBean> {

    public List<ListBean> list;

    public static class DataBean {

    }

    public static class ListBean {
        public String mid;
        public String message_id;
        public int status;
        public String message_type;
        public String message_title;
        public String message_content;
        public String create_time;
    }
}
