package com.android.manghe.mine.model;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/21 15:46
 * desc   :
 */
public class IndexModel implements Serializable {


    private int icon;
    private Integer  giveIcon;
    private String name;
    private String desc;
    private String tag;


    public Integer getGiveIcon() {
        return giveIcon;
    }

    public void setGiveIcon(Integer giveIcon) {
        this.giveIcon = giveIcon;
    }

    public IndexModel() {
    }

    public IndexModel(String name) {
        this.name = name;
    }

    public IndexModel(int icon, Integer giveIcon) {
        this.icon = icon;
        this.giveIcon = giveIcon;
    }


    public IndexModel(int icon, String name, String desc, String tag) {
        this.icon = icon;
        this.name = name;
        this.desc = desc;
        this.tag = tag;
    }

    public IndexModel(int icon, String name, String tag) {
        this.icon = icon;
        this.name = name;
        this.tag = tag;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
