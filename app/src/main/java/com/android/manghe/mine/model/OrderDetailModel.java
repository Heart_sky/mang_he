package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/02 14:46
 * desc   :
 */
public class OrderDetailModel extends NetBean<OrderDetailModel.DataBean> {


    /**
     * data : {"id":"78794","mid":"911730","order_id":"72481","goods_id":"2140","goods_name":"红牡丹 骨瓷餐具套装陶瓷碗碟盘套装碗具6-8人用碗筷套装风华正茂 28件 方形配置","buy_num":"10","sell_price":"479.00","score_price":"56","c_time":"2021-11-30 17:28:22","express_id":null,"express_no":null,"shipping_time":"","shipping_status":"0","order_sn":"2021113017282253549910_78794","order_type":"2","goods_thumb":"https://www.mxmd88.com/upload/1/images/211128/e8d2f516bc868e6e.jpg","express":"","expend_score":"560","remark":""}
     * flag : true
     */

    public static class DataBean implements Serializable {
        /**
         * id : 78794
         * mid : 911730
         * order_id : 72481
         * goods_id : 2140
         * goods_name : 红牡丹 骨瓷餐具套装陶瓷碗碟盘套装碗具6-8人用碗筷套装风华正茂 28件 方形配置
         * buy_num : 10
         * sell_price : 479.00
         * score_price : 56
         * c_time : 2021-11-30 17:28:22
         * express_id : null
         * express_no : null
         * shipping_time :
         * shipping_status : 0
         * order_sn : 2021113017282253549910_78794
         * order_type : 2
         * goods_thumb : https://www.mxmd88.com/upload/1/images/211128/e8d2f516bc868e6e.jpg
         * express :
         * expend_score : 560
         * remark :
         */

        public String id;
        public String mid;
        public String order_id;
        public String goods_id;
        public String goods_name;
        public String buy_num;
        public String sell_price;
        public String score_price;
        public String c_time;
        public Object express_id;
        public Object express_no;
        public String shipping_time;
        public String shipping_status;
        public String order_sn;
        public String order_type;
        public String goods_thumb;
        public String express;
        public String expend_score;
        public String remark;
        public String receiver;
        public String mobile;
        public String address;
        public String is_rate;//0.未评价;1.已评价;
    }
}
