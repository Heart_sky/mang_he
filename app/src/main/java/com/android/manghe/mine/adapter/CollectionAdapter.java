package com.android.manghe.mine.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.tools.BigDecimalUtils;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.index.model.CollectionRes;

import java.util.List;

public class CollectionAdapter extends BaseRecyclerViewAdapter<CollectionRes.DataBean.ListBean, CollectionAdapter.CollectionHolder> {
    private Context mContext;
    private  ICollectionListener mListener;

    public CollectionAdapter(Context context, List<CollectionRes.DataBean.ListBean> list, ICollectionListener listener) {
        super(list);
        mContext = context;
        mListener = listener;
    }

    public interface ICollectionListener{
        void cancel(String id);
    }

    public void update(List<CollectionRes.DataBean.ListBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(CollectionHolder holder, CollectionRes.DataBean.ListBean item) {
        GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.getView(R.id.ivPic));
        holder.setText(R.id.tvMarketPrice, "市场价:￥" + item.market_price);
        holder.setText(R.id.tvTitle, item.title);
        holder.setText(R.id.tvPrice, "均价:￥" + BigDecimalUtils.round(item.avg, 2));
        holder.setText(R.id.tvGetAccount, item.success_m_num+"人砍到");
        holder.setOnClickListener(R.id.tvCancelCollection, v -> {
            //取消收藏
            mListener.cancel(item.id);
        });
    }

    @Override
    public CollectionHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new CollectionHolder(inflateItemView(viewGroup, R.layout.item_collection));
    }

    public class CollectionHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public CollectionHolder(View itemView) {
            super(itemView);
        }
    }
}
