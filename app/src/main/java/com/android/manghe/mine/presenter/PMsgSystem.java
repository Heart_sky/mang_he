package com.android.manghe.mine.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.fragment.MsgSystemFragment;
import com.android.manghe.mine.model.MessageNoticeData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PMsgSystem extends XPresenter<MsgSystemFragment> {
    private int mCurrentPage = 1;
    private final List<MessageNoticeData.ListBean> mMsgList = new ArrayList<>();

    public void getData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mMsgList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("catid","40");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBER_CUSTOMER, data, headMap)
                .get()
                .execute(new BaseBack<MessageNoticeData>() {

                    @Override
                    public void onSuccess(Call call, MessageNoticeData res) {
                        if (res != null && res.code == 0 && res.list != null) {
                            mMsgList.addAll(res.list);
                            getV().setCanLoadMore(res.list.size() == 10);
                            getV().update(mMsgList);
                            if (mMsgList.size() == 10) {
                                mCurrentPage++;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void setRead(String id){
        HashMap<String, String> data = new HashMap<>();
        data.put("id", id);
        data.put("catid", "40");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getActivity()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERMESSAGENOTICEDETAIL, data, headMap)
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String res) {
                        int i = 0;
                        for(MessageNoticeData.ListBean bean : mMsgList){
                            i++;
                            if(bean.id.equals(id)){
                                bean.is_read = 2;
                                mMsgList.set(--i, bean);
                                getV().update(mMsgList);
                                break;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
