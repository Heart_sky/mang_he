package com.android.manghe.mine.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/03 15:52
 * desc   :  签到信息的实体
 */
public class SignInfoModel extends NetBean<SignInfoModel.DataBean> {


    /**
     * data : {"total":0.3,"continuous_days":2,"list":{"bonus_1":{"is_open":true,"value":"0.1"},"bonus_2":{"is_open":true,"value":"0.2"},"bonus_3":{"is_open":false,"value":"0.3"},"bonus_4":{"is_open":false,"value":"0.4"},"bonus_5":{"is_open":false,"value":"0.5"},"bonus_6":{"is_open":false,"value":"0.6"},"bonus_7":{"is_open":false,"value":"0.7"},"today_buy1_award":{"amount":"0.4","is_complete":false},"today_buy5_award":{"amount":"0.5","is_complete":false},"score_exchange":{"amount":"0.7","is_complete":false},"full100_award":{"amount":"10","today_buy_total":"0","is_complete":false}}}
     * flag : true
     */
    public static class DataBean implements Serializable {
        /**
         * total : 0.3
         * continuous_days : 2
         * list : {"bonus_1":{"is_open":true,"value":"0.1"},"bonus_2":{"is_open":true,"value":"0.2"},"bonus_3":{"is_open":false,"value":"0.3"},"bonus_4":{"is_open":false,"value":"0.4"},"bonus_5":{"is_open":false,"value":"0.5"},"bonus_6":{"is_open":false,"value":"0.6"},"bonus_7":{"is_open":false,"value":"0.7"},"today_buy1_award":{"amount":"0.4","is_complete":false},"today_buy5_award":{"amount":"0.5","is_complete":false},"score_exchange":{"amount":"0.7","is_complete":false},"full100_award":{"amount":"10","today_buy_total":"0","is_complete":false}}
         */

        public double total;
        public int continuous_days;
        public ListBean list;

        public static class ListBean implements Serializable {
            /**
             * bonus_1 : {"is_open":true,"value":"0.1"}
             * bonus_2 : {"is_open":true,"value":"0.2"}
             * bonus_3 : {"is_open":false,"value":"0.3"}
             * bonus_4 : {"is_open":false,"value":"0.4"}
             * bonus_5 : {"is_open":false,"value":"0.5"}
             * bonus_6 : {"is_open":false,"value":"0.6"}
             * bonus_7 : {"is_open":false,"value":"0.7"}
             * today_buy1_award : {"amount":"0.4","is_complete":false}
             * today_buy5_award : {"amount":"0.5","is_complete":false}
             * score_exchange : {"amount":"0.7","is_complete":false}
             * full100_award : {"amount":"10","today_buy_total":"0","is_complete":false}
             */

            public Bonus1Bean bonus_1;
            public Bonus2Bean bonus_2;
            public Bonus3Bean bonus_3;
            public Bonus4Bean bonus_4;
            public Bonus5Bean bonus_5;
            public Bonus6Bean bonus_6;
            public Bonus7Bean bonus_7;
            public TodayBuy1AwardBean today_buy1_award;
            public TodayBuy5AwardBean today_buy5_award;
            public ScoreExchangeBean score_exchange;
            public Full100AwardBean full100_award;

            public static class Bonus1Bean implements Serializable {
                /**
                 * is_open : true
                 * value : 0.1
                 */

                public boolean is_open;
                public String value;
            }

            public static class Bonus2Bean implements Serializable {
                /**
                 * is_open : true
                 * value : 0.2
                 */

                public boolean is_open;
                public String value;
            }


            public static class Bonus3Bean implements Serializable {
                /**
                 * is_open : false
                 * value : 0.3
                 */

                public boolean is_open;
                public String value;
            }

            public static class Bonus4Bean implements Serializable {
                /**
                 * is_open : false
                 * value : 0.4
                 */

                public boolean is_open;
                public String value;
            }

            public static class Bonus5Bean implements Serializable {
                /**
                 * is_open : false
                 * value : 0.5
                 */

                public boolean is_open;
                public String value;
            }

            public static class Bonus6Bean implements Serializable {
                /**
                 * is_open : false
                 * value : 0.6
                 */

                public boolean is_open;
                public String value;
            }

            public static class Bonus7Bean implements Serializable {
                /**
                 * is_open : false
                 * value : 0.7
                 */

                public boolean is_open;
                public String value;
            }

            public static class TodayBuy1AwardBean implements Serializable {
                /**
                 * amount : 0.4
                 * is_complete : false
                 */

                public String amount;
                public boolean is_complete;
            }

            public static class TodayBuy5AwardBean implements Serializable {
                /**
                 * amount : 0.5
                 * is_complete : false
                 */

                public String amount;
                public boolean is_complete;
            }

            public static class ScoreExchangeBean implements Serializable {
                /**
                 * amount : 0.7
                 * is_complete : false
                 */

                public String amount;
                public boolean is_complete;
            }

            public static class Full100AwardBean implements Serializable {
                /**
                 * amount : 10
                 * today_buy_total : 0
                 * is_complete : false
                 */

                public String amount;
                public String today_buy_total;
                public boolean is_complete;
            }
        }
    }
}
