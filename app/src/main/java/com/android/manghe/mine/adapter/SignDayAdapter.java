package com.android.manghe.mine.adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.manghe.R;
import com.android.manghe.mine.model.SignDesignInfo;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/03 17:10
 * desc   :
 */
public class SignDayAdapter extends BaseQuickAdapter<SignDesignInfo, BaseViewHolder> {

    private Context mContext;

    public SignDayAdapter(Context context, @Nullable List<SignDesignInfo> data) {
        super(R.layout.item_sign_day, data);
        this.mContext = context;
    }

    public SignDesignInfo getSingDesignInfo(int times) {
        if (times <= 0) {
            return getData().get(0);
        } else if (times == 7) {
            return getData().get(times - 1);
        } else {
            return getData().get(times);
        }
    }

    @Override
    protected void convert(BaseViewHolder helper, SignDesignInfo item) {

        try {
            helper.setText(R.id.id_txt_money, item.getValue());
            if (item.isOpen()) {
                //已经签到
                helper.setImageResource(R.id.id_img_src, R.mipmap.sign_gray);
                helper.setText(R.id.id_txt_state, "已签到");
                helper.setTextColor(R.id.id_txt_money, ContextCompat.getColor(mContext, R.color.white));
            } else {
                //还没有签到
                helper.setImageResource(R.id.id_img_src, R.mipmap.sign_yellow);
                helper.setText(R.id.id_txt_state, item.getDay());
                helper.setTextColor(R.id.id_txt_money, ContextCompat.getColor(mContext, R.color.yellow500));
            }

            if (helper.getAdapterPosition() == 6) {
                helper.setImageResource(R.id.id_img_src, R.mipmap.sign_gift_box);
                helper.setGone(R.id.id_re_bg, true);
                helper.setGone(R.id.id_txt_money, false);
            } else {
                helper.setGone(R.id.id_re_bg, false);
                helper.setGone(R.id.id_txt_money, true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
