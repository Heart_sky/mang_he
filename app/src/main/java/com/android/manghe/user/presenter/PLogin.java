package com.android.manghe.user.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.PostBuilder;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshIndexNoticeEvent;
import com.android.manghe.mine.model.UserRes;
import com.android.manghe.user.activity.LoginActivity;
import com.android.manghe.utils.DeviceIdUtil;
import com.eightbitlab.rxbus.Bus;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;

public class PLogin extends XPresenter<LoginActivity> {
    public String giftAmount = "";

    public void login(String tel, String psw, boolean isFirstLogin) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERORLOGIN)
                .post("username", tel)
                .post("password", psw)
                .build()
                .execute(new BaseBack<UserRes>() {

                    @Override
                    public void onSuccess(Call call, UserRes userInfo) {
                        if (userInfo.code == 0) {
                            userInfo.data.UPSW = psw;
                            UserHolder.setUserInfo(getV(), userInfo.data);
                            getV().loginOk(isFirstLogin);
                            Bus.INSTANCE.send(userInfo.data);
                            Bus.INSTANCE.send(new RefreshIndexNoticeEvent());
                            MobclickAgent.onProfileSignIn(userInfo.data.UID);

                            Map<String, Object> data = new HashMap<String, Object>();
                            data.put("sign_in", 1);
                            MobclickAgent.onEventObject(getV(), "sign_in", data);

                            //AppTrack
                            if (userInfo.data != null && !TextUtils.isEmpty(userInfo.data.UID)) {
                                appTrackLogin(userInfo.data.UID);
                            }

                        } else {
                            getV().loginFail(userInfo.msg);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().loginFail(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().closeLoadingDialog();
                    }
                });
    }

    public void otherLoginToService(int type, String openid, String unionid, String nickname, String avatar) {
        HashMap<String, String> map = new HashMap<>();
        map.put("openid", openid);
        map.put("nickname", nickname);
        map.put("avatar", avatar);
        map.put("unionid", unionid);
        String typeName = "wx";
        switch (type) {
            case 1:
                typeName = "wb";
                break;
            case 2:
                typeName = "wx";
                break;
            case 3:
                typeName = "qq";
                break;
        }
        map.put("type", typeName);
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERORSENDTHREELOGIN)
                .post(map)
                .build()
                .execute(new BaseBack<UserRes>() {

                    @Override
                    public void onSuccess(Call call, UserRes userInfo) {
                        try {
                            getV().closeLoadingDialog();
                            if (userInfo != null && userInfo.code == 0 && userInfo.data != null) {
                                if (TextUtils.isEmpty(userInfo.data.UID)) {
                                    //没有绑定过手机
//                                    getV().toBindTel(avatar, openid
//                                            , map.get("type"), nickname);
                                    autoBind(avatar, openid, map.get("type"), nickname);
                                } else {
                                    UserHolder.setUserInfo(getV(), userInfo.data);
                                    getV().loginOk(false);
                                    Bus.INSTANCE.send(userInfo.data);
                                    Bus.INSTANCE.send(new RefreshIndexNoticeEvent());
                                    String tempTypeName = "";
                                    switch (type) {
                                        case 1:
                                            tempTypeName = "wb";
                                            break;
                                        case 2:
                                            tempTypeName = "wx";
                                            break;
                                        case 3:
                                            tempTypeName = "qq";
                                            break;
                                    }
                                    MobclickAgent.onProfileSignIn(tempTypeName, userInfo.data.UID);
                                    Map<String, Object> data = new HashMap<String, Object>();
                                    data.put("sign_in", 1);
                                    MobclickAgent.onEventObject(getV(), "sign_in", data);

                                    //AppTrack
                                    if (userInfo.data != null && !TextUtils.isEmpty(userInfo.data.UID)) {
                                        appTrackLogin(userInfo.data.UID);
                                    }
                                }
                            } else {
                                getV().loginFail(userInfo.msg);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().closeLoadingDialog();
                        getV().loginFail(null);
                    }

                    @Override
                    public void onComplete() {
                        try {
                            getV().closeLoadingDialog();
                        } catch (Exception e) {
                        }
                    }
                });
    }

    private void autoBind(String avatar, String openid, String type, String nick) {
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("PLATFORM", "android");
        headMap.put("DEVICEID", DeviceIdUtil.getUniqueId(getV()));
        PostBuilder builder = new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBER_OAUTH_REGISTER, headMap)
                .post("password", "qutu123456")
                .post("openid", openid)
                .post("type", type)
                .post("avatar", TextUtils.isEmpty(avatar) ? "" : avatar)
                .post("nickname", TextUtils.isEmpty(nick) ? "" : nick);
        builder.post("gift_amount",!TextUtils.isEmpty(giftAmount) ? giftAmount: "0");
        builder.build().execute(new BaseBack<UserRes>() {

            @Override
            public void onSuccess(Call call, UserRes userInfo) {
                if (userInfo.code == 0) {
                    UserHolder.setUserInfo(getV(), userInfo.data);
                    getV().loginOk(TextUtils.isEmpty(giftAmount));
                    Bus.INSTANCE.send(userInfo.data);
                    Bus.INSTANCE.send(new RefreshIndexNoticeEvent());
                    Map<String, Object> data = new HashMap<String, Object>();
                    data.put("register", 1);
                    MobclickAgent.onEventObject(getV(), "register", data);

                    //AppTrack
                    if (userInfo.data != null && !TextUtils.isEmpty(userInfo.data.UID)) {
                        Map regMap = new HashMap();
                        regMap.put("userid", userInfo.data.UID);
                        MobclickAgent.onEvent(mContext, "__register", regMap);
                    }
                } else {
                    getV().loginFail(userInfo.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().loginFail(null);
            }

            @Override
            public void onComplete() {
                getV().closeLoadingDialog();
            }
        });
    }

    private void appTrackLogin(String uid) {
        //AppTrack
        Map regMap = new HashMap();
        regMap.put("userid", uid);
        MobclickAgent.onEvent(getV(), "__login", regMap);
    }
}
