package com.android.manghe.user.presenter;

import android.text.TextUtils;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.PostBuilder;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.mine.model.UserRes;
import com.android.manghe.user.activity.RegActivity;
import com.android.manghe.user.model.OpenInstallInstallData;
import com.android.manghe.utils.DeviceIdUtil;
import com.fm.openinstall.OpenInstall;
import com.fm.openinstall.listener.AppInstallAdapter;
import com.fm.openinstall.model.AppData;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;

public class PReg extends XPresenter<RegActivity> {

    public String avatar= "";
    public String nickName = "";
    public String openid = "";
    public String type = "";

    public String inviteId = "";
    public String scodeToken = "";

    public String giftAmount = "";

    public void reg(String tel, String psw, String code){
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("PLATFORM", "android");
        headMap.put("DEVICEID", DeviceIdUtil.getUniqueId(getV()));
        PostBuilder builder = new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERORSIGN, headMap)
                .post("mobile",tel)
                .post("password",psw)
                .post("verifycode", code)
                .post("avatar", TextUtils.isEmpty(avatar) ? "":avatar)
                .post("nickname", TextUtils.isEmpty(nickName) ? "":nickName)
                .post("scode_token", scodeToken);
        builder.post("gift_amount", !TextUtils.isEmpty(giftAmount) ? giftAmount : "0");
        if(!TextUtils.isEmpty(type)){
            builder.post("type",type);
            builder.post("openid", openid);
        }
        if(!TextUtils.isEmpty(inviteId)){
            builder.post("inviter_id", inviteId);
        }
        builder.build().execute(new BaseBack<UserRes>() {

                    @Override
                    public void onSuccess(Call call, UserRes bean) {
                        if (bean.code == 0) {
                            getV().regOk();
                            Map<String, Object> data = new HashMap<String, Object>();
                            data.put("register",1);
                            MobclickAgent.onEventObject(getV(), "register", data);

                            //AppTrack
                            if(bean.data!=null && !TextUtils.isEmpty(bean.data.UID)) {
                                Map regMap = new HashMap();
                                regMap.put("userid", bean.data.UID);
                                MobclickAgent.onEvent(mContext, "__register", regMap);
                            }
                        }else{
                            getV().regFail(bean.msg);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().regFail(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().closeLoadingDialog();
                    }
                });
    }

    public void getInviteMsg(){
        //获取OpenInstall安装数据
        OpenInstall.getInstall(new AppInstallAdapter() {
            @Override
            public void onInstall(AppData appData) {
                //获取渠道数据
                try {
                    String channelCode = appData.getChannel();
                    //获取自定义数据
                    String bindData = appData.getData();
//                System.out.println("OpenInstall getInstall : installData = " + bindData + ", channelCode = " + channelCode);
                    if (!TextUtils.isEmpty(bindData)) {
                        OpenInstallInstallData data = new Gson().fromJson(bindData, OpenInstallInstallData.class);
                        if (data != null && data.mid != null && !data.mid.isEmpty()) {
                            inviteId = data.mid;
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void sendCode(String tel, String picCode){
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERORSENDMESSAGE)
                .post("mobile",tel)
                .post("act","sms_register")
                .post("scode", picCode)
                .post("scode_token", scodeToken)
                .build().execute(new BaseBack<NetBean>() {

            @Override
            public void onSuccess(Call call, NetBean bean) {
                if (bean.code == 0) {
                    getV().sendCodeOk();
                }else{
                    getV().sendCodeFail(bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().sendCodeFail(null);
            }

            @Override
            public void onComplete() {
                getV().closeLoadingDialog();
            }
        });
    }


}
