package com.android.manghe.user.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.user.activity.ResetPswActivity;
import okhttp3.Call;

public class PResetPsw extends XPresenter<ResetPswActivity> {

    public String scodeToken = "";
    public void resetPsw(String tel, String psw, String code){
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERORFORGETPW)
                .post("mobile",tel)
                .post("password",psw)
                .post("sms_code", code)
                .build().execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean bean) {
                        if (bean.code == 0) {
                            getV().resetOk();
                        }else{
                            getV().resetFail(bean.msg);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().resetFail(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().closeLoadingDialog();
                    }
                });
    }

    public void sendCode(String tel, String picCode){
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERORSENDMESSAGE)
                .post("mobile",tel)
                .post("scode", picCode)
                .post("scode_token", scodeToken)
                .post("act","sms_chpass")
                .build().execute(new BaseBack<NetBean>() {

            @Override
            public void onSuccess(Call call, NetBean bean) {
                if (bean.code == 0) {
                    getV().sendCodeOk();
                }else{
                    getV().sendCodeFail(bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().sendCodeFail(null);
            }

            @Override
            public void onComplete() {
                getV().closeLoadingDialog();
            }
        });
    }


}
