package com.android.manghe.user.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.ImageView
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.Glide.GlideHelper
import com.android.base.tools.ToastUtil
import com.android.base.tools.codec.MD5
import com.android.manghe.R
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.user.presenter.PResetPsw
import kotlinx.android.synthetic.main.activity_forget.*
import kotlinx.android.synthetic.main.activity_forget.etCode
import kotlinx.android.synthetic.main.activity_forget.etPicCode
import kotlinx.android.synthetic.main.activity_forget.etPsw
import kotlinx.android.synthetic.main.activity_forget.etTel
import kotlinx.android.synthetic.main.activity_forget.ivEye
import kotlinx.android.synthetic.main.activity_forget.ivX
import kotlinx.android.synthetic.main.activity_forget.layoutPicCode
import kotlinx.android.synthetic.main.activity_forget.tvSendCode
import java.util.*

class ResetPswActivity : MVPActivity<PResetPsw>() {

    private var mIsPswVisible = false
    private var mIsPswAgainVisible = false
    private val TotalCount = 60L * 1000;

    companion object {
        fun showActivity(context: Context) {
            val intent = Intent(context, ResetPswActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId() = R.layout.activity_forget

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("找回密码").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setTitleAndStatusBgColor(R.color.white)
        initListener()
        newPicCode()
    }
    private fun newPicCode(){
        val ivPicCode = ImageView(this)
        layoutPicCode.addView(ivPicCode)
        p.scodeToken = MD5.convertString(UUID.randomUUID().toString())
        GlideHelper.loadNoCache(this, ConstantsUrl.domain + ConstantsUrl.MEMBERWELCOMSCODE + "?scode_token=${p.scodeToken}", ivPicCode)
        ivPicCode.setOnClickListener {
            layoutPicCode.removeAllViews()
            newPicCode()
        }
    }
    private fun initListener() {
        layoutConfirm.setOnClickListener {
            finish()
        }
        ivEye.setOnClickListener {
            changePswVisible()
        }
        ivEyeAgain.setOnClickListener {
            changePswAgainVisible()
        }
        tvSendCode.setOnClickListener {
            val tel = etTel.text.toString().trim()
            if(TextUtils.isEmpty(tel)){
                ToastUtil.showShort(this, "请输入您的手机号")
                return@setOnClickListener
            }
            val picCode = etPicCode.text.toString().trim()
            if(TextUtils.isEmpty(picCode)){
                ToastUtil.showShort(this, "请输入图形验证码")
                return@setOnClickListener
            }
            showLoadingDialog()
            p.sendCode(tel,picCode)
        }
        layoutConfirm.setOnClickListener {
            val tel = etTel.text.toString().trim()
            val psw = etPsw.text.toString().trim()
            val pswAgrin = etPswAgain.text.toString().trim()
            val code = etCode.text.toString().trim()
            if (TextUtils.isEmpty(tel)) {
                ToastUtil.showShort(this@ResetPswActivity, "请输入手机号码")
                return@setOnClickListener
            }
            if (TextUtils.isEmpty(psw)) {
                ToastUtil.showShort(this@ResetPswActivity, "请输入密码")
                return@setOnClickListener
            }
            if(!TextUtils.equals(pswAgrin, psw)){
                ToastUtil.showShort(this@ResetPswActivity, "两次输入密码不一致")
                return@setOnClickListener
            }
            val picCode = etPicCode.text.toString().trim()
            if(TextUtils.isEmpty(picCode)){
                ToastUtil.showShort(this, "请输入图形验证码")
                return@setOnClickListener
            }
            showLoadingDialog()
            p.resetPsw(tel, psw, code)
        }
        etTel.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ivX.visibility = if(etTel.text.toString().isNotEmpty()) View.VISIBLE else View.GONE
            }

        })

        ivX.setOnClickListener {
            etTel.setText("")
        }
    }

    private fun changePswVisible() {
        etPsw.transformationMethod =
            if (mIsPswVisible) PasswordTransformationMethod.getInstance() else HideReturnsTransformationMethod.getInstance()
        mIsPswVisible = !mIsPswVisible
        ivEye.setImageResource(if (mIsPswVisible) R.mipmap.visible else R.mipmap.invisible)
    }

    private fun changePswAgainVisible() {
        etPswAgain.transformationMethod =
            if (mIsPswAgainVisible) PasswordTransformationMethod.getInstance() else HideReturnsTransformationMethod.getInstance()
        mIsPswAgainVisible = !mIsPswAgainVisible
        ivEyeAgain.setImageResource(if (mIsPswAgainVisible) R.mipmap.visible else R.mipmap.invisible)
    }

    fun resetOk() {
        ToastUtil.showShort(this, "重置密码成功")
        finish()
    }

    fun resetFail(text: String?) {
        ToastUtil.showShort(this, if (TextUtils.isEmpty(text)) "重置密码失败" else text)
    }

    fun closeLoadingDialog() {
        hideLoadingDialog()
    }

    fun sendCodeOk(){
        startCountDown()
    }
    fun sendCodeFail(text: String?){
        ToastUtil.showShort(this, if (TextUtils.isEmpty(text)) "发送验证码失败" else text)
    }

    private var mTimer : CountDownTimer? = null
    private fun startCountDown(){
        mTimer = object : CountDownTimer(TotalCount, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                tvSendCode.text = "${(millisUntilFinished / 1000)}s"
                tvSendCode.isClickable = false
            }

            override fun onFinish() {
                tvSendCode.text = "发送验证码"
                tvSendCode.isClickable = true
            }
        }
        mTimer!!.start()
    }
    private fun stopCountDown(){
        mTimer?.let{
            it.cancel()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopCountDown()
    }
}