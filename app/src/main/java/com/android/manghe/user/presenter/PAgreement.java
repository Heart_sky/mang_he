package com.android.manghe.user.presenter;

import android.text.TextUtils;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.user.activity.AgreementActivity;
import com.android.manghe.user.model.SecretRes;
import com.google.gson.Gson;
import okhttp3.Call;

public class PAgreement extends XPresenter<AgreementActivity> {
    public void loadAgreement(){
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.ACUTIONHOMEBLOCK+"?mark=agreement").get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        getV().hideLoadingDialog();
                        if(!TextUtils.isEmpty(json)) {
                            NetBean<String> res = new Gson().fromJson(json, NetBean.class);
                            if (res.code == 0 && res.data != null && res.data != null) {
                                getV().showAgreement(res.data.replace("<br/>",""));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void loadVipAgreement(){
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.ACUTIONHOMEBLOCK+"?mark=hyxy").get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        getV().hideLoadingDialog();
                        if(!TextUtils.isEmpty(json)) {
                            NetBean<String> res = new Gson().fromJson(json, NetBean.class);
                            if (res.code == 0 && res.data != null && res.data != null) {
                                getV().showAgreement(res.data.replace("<br/>",""));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    public void loadSecret(){
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERAGREEMENT).get()
                .execute(new BaseBack<SecretRes>() {

                    @Override
                    public void onSuccess(Call call, SecretRes res) {
                        getV().hideLoadingDialog();
                        if (res.code == 0 && res.data != null && res.data.yszc != null) {
                            getV().showAgreement(res.data.yszc);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void loadMemberService(){
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBERAGREEMENT).get()
                .execute(new BaseBack<SecretRes>() {

                    @Override
                    public void onSuccess(Call call, SecretRes res) {
                        getV().hideLoadingDialog();
                        if (res.code == 0 && res.data != null && res.data.yszc != null) {
                            getV().showAgreement(res.data.yszc);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

}
