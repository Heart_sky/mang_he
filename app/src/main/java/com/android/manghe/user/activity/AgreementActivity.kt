package com.android.manghe.user.activity

import android.os.Build
import android.os.Bundle
import android.text.Html
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.user.presenter.PAgreement
import kotlinx.android.synthetic.main.activity_agreement.*

class AgreementActivity : MVPActivity<PAgreement>() {
    private var type = 0 //0隐私政策 1服务协议 2会员服务协议

    override fun initData(savedInstanceState: Bundle?) {
        type = intent.getIntExtra("type", 0)

        var title = ""
        when (type) {
            0 -> {
                title = "隐私政策"
                p.loadSecret()
            }
            1 -> {
                title = "用户服务协议"
                p.loadAgreement()
            }
            2 -> {
                title = "会员协议"
                p.loadVipAgreement()
            }
        }

        titleBar.setToolbar(title).setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black)


    }

    override fun getLayoutId(): Int {
        return R.layout.activity_agreement
    }

    fun showAgreement(text: String){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.text = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        }else{
            tvContent.text = Html.fromHtml(text)
        }
    }
}