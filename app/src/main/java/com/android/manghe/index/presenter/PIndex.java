package com.android.manghe.index.presenter;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.box.model.BoxCategoriesModel;
import com.android.manghe.box.model.BoxListModel;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.box.model.BoxPrizeModel;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.coupon.model.CouponBannerRes;
import com.android.manghe.index.fragment.IndexFragment;
import com.android.manghe.index.model.FunctionBean;
import com.android.manghe.index.model.HotBuyTimeLimitRes;
import com.android.manghe.index.model.MainTopBannerBean;
import com.android.manghe.index.model.TodayNewInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PIndex extends XPresenter<IndexFragment> {

    //----底部商品列表START
    public String currentBottomCategoryId = "";
    private int currentPage = 1;
    private final String PageSize = "99999";
    private final List<BoxModel.DataBean> bottomGoodList = new ArrayList<>();
    //----底部商品列表End


    public void getBanner() {
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.FRAGMENT1_BANNER)
                .post("app_name", "meng_xiang")
                .build()
                .execute(new BaseBack<MainTopBannerBean>() {

                    @Override
                    public void onSuccess(Call call, List<MainTopBannerBean> bannerList) {
                        if (bannerList != null && bannerList.size() != 0) {
                            getV().updateBanner(bannerList);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getMiddleBanner() {
        HashMap<String, String> data = new HashMap<>();
        data.put("typeid", "6");
        new OKHttpUtil(getV().getActivity()).url(ConstantsUrl.domain + ConstantsUrl.GETBANNER, data)
                .get()
                .execute(new BaseBack<CouponBannerRes>() {

                    @Override
                    public void onSuccess(Call call, List<CouponBannerRes> res) {
                        if (res != null && res.size() != 0) {
                            getV().updateMiddleBanner(res);
                        } else {
                            getV().updateMiddleBanner(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        e.printStackTrace();
                        getV().updateMiddleBanner(null);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    public void getFunctions() {
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.FRAGMENT1_MENUS)
                .post(new HashMap<String, String>())
                .build()
                .execute(new BaseBack<FunctionBean>() {

                    @Override
                    public void onSuccess(Call call, List<FunctionBean> functionBeanList) {
                        if (functionBeanList != null && functionBeanList.size() != 0) {
                            getV().updateFunction(functionBeanList);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

//    public void getNotice() {
//        String url = ConstantsUrl.domain + ConstantsUrl.REGISTER_EXEMPT_LIST;
//        HashMap<String, String> headMap = new HashMap<>();
//        UserRes.UserInfo userInfo = UserHolder.getUserInfo(getV().getActivity());
//        if (userInfo != null) {
//            url = url + "?mid=" + userInfo.UID;
//            headMap.put("UID", userInfo.UID);
//            headMap.put("TOKEN", userInfo.TOKEN);
//        }
//        new OKHttpUtil(getV().getContext()).urlByHeadData(url, headMap)
//                .get()
//                .execute(new BaseBack<NoticeBean>() {
//
//                    @Override
//                    public void onSuccess(Call call, List<NoticeBean> noticeList) {
//                        if (noticeList != null && noticeList.size() != 0) {
//                            getV().updateNotice(noticeList);
//                        }
//                    }
//                });
//    }

    public void getBoxPrize() {
        //获取中奖的消息
        HashMap<String, String> data = new HashMap<>();
        data.put("page","1");
        data.put("size", "100");
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.Box_Prize_List,data)
                .get()
                .execute(new BaseBack<BoxPrizeModel>() {

                    @Override
                    public void onSuccess(Call call, BoxPrizeModel res) {
                        if (res != null && res.data != null) {
                            getV().updateNotice(res.data.list);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void getHotBuyTimeLimit() {
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.IndexData)
                .get()
                .execute(new BaseBack<HotBuyTimeLimitRes>() {

                    @Override
                    public void onSuccess(Call call, HotBuyTimeLimitRes res) {
                        if (res != null && res.data != null) {
                            getV().updateHotBuyTimeLimit(res.data);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getTodayNew() {
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.TodayNew)
                .get()
                .execute(new BaseBack<TodayNewInfo>() {

                    @Override
                    public void onSuccess(Call call, List<TodayNewInfo> infoList) {
                        if (infoList != null && !infoList.isEmpty()) {
                            getV().updateTodayNew(infoList);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getBottomGridGoodTab() {
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.Box_Categories)
                .get()
                .execute(new BaseBack<BoxCategoriesModel>() {

                    @Override
                    public void onSuccess(Call call, BoxCategoriesModel boxCategoriesModel) {
                        if (boxCategoriesModel.data != null && !boxCategoriesModel.data.list.isEmpty()) {
                            getV().updateBottomGridGoodTab(boxCategoriesModel.data.list);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getBottomGridGood(boolean isRefresh) {
        //获取精选
        if (isRefresh) {
            currentPage = 1;
            bottomGoodList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        data.put("cat_id", currentBottomCategoryId);
        new OKHttpUtil(getV().getActivity()).url(ConstantsUrl.domain + ConstantsUrl.Box_List, data)
                .get()
                .execute(new BaseBack<BoxListModel>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onSuccess(Call call, BoxListModel res) {
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                bottomGoodList.addAll(res.data.list);
                                getV().setCanLoadMore(bottomGoodList.size() != res.data.list_total);
                                getV().updateBottomGridGood(bottomGoodList);
                                if (bottomGoodList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateBottomGridGood(bottomGoodList);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                });
    }
}
