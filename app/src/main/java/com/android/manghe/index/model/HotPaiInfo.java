package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class HotPaiInfo extends NetBean<List<HotPaiInfo>> {


        /**
         * auction_id : 1
         * title : 100元话费 全国通用 移动/联通/电信
         * avg_price : 0.00
         * success_get_num : 0
         * thumb : http://www.ycdy8.com/upload/1/images/gallery/1/1/38_src.jpg
         */

        public String auction_id;
        public String title;
        public String avg_price;
        public String success_get_num;
        public String thumb;
}
