package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class NoticeBean extends NetBean<List<NoticeBean>> {

        /**
         * id : 1
         * mid : 463620
         * type : income
         * amount : 10.00
         * remark : 注册赠送免单额度10
         * c_time : 1604245812
         * member_nickname : Sammie.Zhang
         */

        public String id;
        public String mid;
        public String type;
        public String amount;
        public String remark;
        public String c_time;
        public String member_nickname;
}
