package com.android.manghe.index.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.fragment.TheBestFragment;
import com.android.manghe.index.model.TheBestRes;
import okhttp3.Call;

public class PTheBest extends XPresenter<TheBestFragment> {

    /**
     * @param id 档期ID
     */
    public void getData(int id) {
        String url = ConstantsUrl.domain + ConstantsUrl.HIGH_POPULARITY_3;
        if (id != -1) {
            url += "&dangqi_id=" + id;
        }
        new OKHttpUtil(getV().getContext()).url(url)
                .get()
                .execute(new BaseBack<TheBestRes>() {

                    @Override
                    public void onSuccess(Call call, TheBestRes res) {
                        if (res != null && res.code == 0 && res.data != null) {
                            getV().updateTopView(res.data);
                            if (id == -1) {
                                getV().updateBottomView(res.data);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }


}
