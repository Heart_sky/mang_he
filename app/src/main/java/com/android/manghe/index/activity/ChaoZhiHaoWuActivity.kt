package com.android.manghe.index.activity

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.DisplayUtil
import com.android.manghe.R
import com.android.manghe.common.model.ShowBoxEvent
import com.android.manghe.common.model.ShowIndexEvent
import com.android.manghe.index.presenter.PChaoZhiHaoWu
import com.android.manghe.market.adapter.MarketGoodsGridAdapter
import com.android.manghe.market.model.MarketAuctionRes
import com.android.manghe.view.GridAuctionSpacesItemDecoration
import com.eightbitlab.rxbus.Bus
import com.eightbitlab.rxbus.registerInBus
import com.google.android.material.tabs.TabLayout
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_chao_zhi_hao_wu.*
import kotlinx.android.synthetic.main.activity_chao_zhi_hao_wu.tabLayout
import kotlinx.android.synthetic.main.activity_main.*

/**
 * 小分类的商品列表界面
 */
class ChaoZhiHaoWuActivity: MVPActivity<PChaoZhiHaoWu>(),IRefresh {
    private var mAdapter : MarketGoodsGridAdapter? = null
    private val catIds = arrayOf("","137", "151", "146")

    override fun getLayoutId(): Int {
        return R.layout.activity_chao_zhi_hao_wu
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("超值好物").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        val catId = intent.getStringExtra("id")
        p.setCategoryId(catId)
        Bus.observe<ShowBoxEvent>().subscribe {
            finish()
        }.registerInBus(this)
        initListener()
        initListView()

        //tabLayout
        tabLayout.addTab(tabLayout.newTab().setText("人气卖品"))
        tabLayout.addTab(tabLayout.newTab().setText("限时特惠"))
        tabLayout.addTab(tabLayout.newTab().setText("智能家电"))
        tabLayout.addTab(tabLayout.newTab().setText("优选推荐"))

        when(catId){
            catIds[1]->{
                //限时特惠
                p.setCategoryId(catIds[1])
                tabLayout.getTabAt(1)?.select()
            }
            catIds[2]->{
                //智能家电
                p.setCategoryId(catIds[2])
                tabLayout.getTabAt(2)?.select()
            }
            catIds[3]->{
                //优选推荐
//                p.setCategoryId(catIds[3])
                tabLayout.getTabAt(3)?.select()
            }
        }
    }

    var isFirstComing = true
    private fun initListener(){
        tabLayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    if(!isFirstComing){
                        p.setCategoryId(catIds[tabLayout.selectedTabPosition])
                    }
                    isFirstComing = false
                    setFilter(0)
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })

        tvComprehensive.setOnClickListener{
            //综合
            setFilter(0)
        }
        tvSalesVolume.setOnClickListener{
            //销量
            setFilter(1)
        }
        tvDiscount.setOnClickListener{
            //折扣
            setFilter(2)
        }
        layoutPrice.setOnClickListener{
            //价钱
            setFilter(3)
        }

    }

    private fun initListView(){
        recyclerView.layoutManager =
            GridLayoutManager(this, 2)
        recyclerView.addItemDecoration(GridAuctionSpacesItemDecoration(DisplayUtil.dip2px(this, 8f)))
        mAdapter = MarketGoodsGridAdapter(
            this,false,
            ArrayList<MarketAuctionRes.AuctionInfo>()
        )
        recyclerView.adapter = mAdapter
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getAuctionData(true)
    }

    override fun onLoad() {
        p.getAuctionData(false)
    }
    private fun setPriceSortState(type: Int){
        p.mPriceSort = type
        when(type){
            0-> ivSort.setImageResource(R.mipmap.ic_sort)
            1->ivSort.setImageResource(R.mipmap.ic_sort_top)
            2->ivSort.setImageResource(R.mipmap.ic_sort_down)
        }
    }

    private fun setFilter(type: Int){
        if(type != 3){
            setPriceSortState(0)
        }else{
            when(p.mPriceSort){
                0-> setPriceSortState(1)
                1-> setPriceSortState(2)
                2-> setPriceSortState(1)
            }
        }
        tvComprehensive.setTextColor(ContextCompat.getColor(this, if(type == 0) R.color.red else R.color.black))
        tvSalesVolume.setTextColor(ContextCompat.getColor(this, if(type == 1) R.color.red else R.color.black))
        tvDiscount.setTextColor(ContextCompat.getColor(this, if(type == 2) R.color.red else R.color.black))
        tvPrice.setTextColor(ContextCompat.getColor(this, if(type == 3) R.color.red else R.color.black))
        p.mFilterType = type
        p.getAuctionData(true)
    }
    fun update(dataList : List<MarketAuctionRes.AuctionInfo>){
        mAdapter?.let {
            it.update(dataList)
            if (dataList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
        layoutFilter.visibility = if(dataList.isNotEmpty()) View.VISIBLE else View.GONE
    }


    fun setCanLoadMore(canLoadMore : Boolean){
        refreshLayout.isEnableLoadMore = canLoadMore
    }
    override fun onDestroy() {
        super.onDestroy()
        Bus.unregister(this)
    }
}