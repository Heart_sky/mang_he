package com.android.manghe.index.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.manghe.R
import com.android.manghe.index.adapter.HotPaiAdapter
import com.android.manghe.index.model.HotPaiInfo
import com.android.manghe.index.presenter.PHotPai
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_hot_pai.*

/**
 * 人气拍品
 */
class HotPaiFragment : MVPFragment<PHotPai>(), IRefresh {

    private var mAdapter: HotPaiAdapter? = null

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_hot_pai
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {
        mAdapter = HotPaiAdapter(activity, arrayListOf())
        val linearManager =
            LinearLayoutManager(activity)
        recyclerView.layoutManager = linearManager
        recyclerView.adapter = mAdapter
        p.getData()
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView!!.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getData()
    }

    override fun onLoad() {

    }


    fun update(hotPaiInfoList: List<HotPaiInfo>) {
        mAdapter?.let {
            it.update(hotPaiInfoList)
        }
    }

}