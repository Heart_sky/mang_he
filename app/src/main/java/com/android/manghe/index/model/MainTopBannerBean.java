package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class MainTopBannerBean extends NetBean<List<MainTopBannerBean>> {

    public String src;
    public String href;
    public String title;
    public String path;
    public String link;
    public String thumb;
}
