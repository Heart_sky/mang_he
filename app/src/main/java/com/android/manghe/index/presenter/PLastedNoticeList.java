package com.android.manghe.index.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.activity.LastedNoticeListActivity;
import com.android.manghe.index.model.OldNoticeBean;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.List;

public class PLastedNoticeList extends XPresenter<LastedNoticeListActivity> {
    private int PageSize = 10;
    private int mCurrentPage = 1;
    private List<OldNoticeBean.DataBean.ListBean> mLastList = new ArrayList<>();

    public void loadData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mLastList.clear();
        }
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.ACUTIONRECENTLYORDER)
                .post("page", mCurrentPage + "")
                .build()
                .execute(new BaseBack<OldNoticeBean>() {

                    @Override
                    public void onSuccess(Call call, OldNoticeBean res) {
                        if (res != null && res.code == 0 && res.data != null && res.data.list != null) {
                            mLastList.addAll(res.data.list);
//                            getV().setCanLoadMore(res.data.list.size() == PageSize);
                            getV().update(mLastList);
                            mCurrentPage++;
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
