package com.android.manghe.index.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeModel;
import com.android.manghe.utils.WindowUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/29 16:06
 * desc   :
 */
public class newBoxOpenAdapter extends BaseQuickAdapter<BoxPrizeModel.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;
    private int itemSize;

    public newBoxOpenAdapter(int layoutResId, Context context, @Nullable List<BoxPrizeModel.DataBean.ListBean> data) {
        super(layoutResId, data);
        this.mContext = context;
    }

    public newBoxOpenAdapter(Context context, @Nullable List<BoxPrizeModel.DataBean.ListBean> data, int size) {
        super(R.layout.item_open_box, data);
        this.mContext = context;
        this.itemSize = size;
    }

    @Nullable
    @Override
    public BoxPrizeModel.DataBean.ListBean getItem(int position) {
        int newPosition = position % getData().size();
        return getData().get(newPosition);
    }

    @Override
    public int getItemViewType(int position) {
        int count = getHeaderLayoutCount() + getData().size();
        if (count <= 0) {
            count = 1;
        }
        int newPosition = position % count;
        return super.getItemViewType(newPosition);
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }


    public void update(List<BoxPrizeModel.DataBean.ListBean> list) {
        replaceData(list);
    }

    @Override
    protected void convert(BaseViewHolder holder, BoxPrizeModel.DataBean.ListBean item) {
        try {
            holder.itemView.setLayoutParams(new LinearLayout.LayoutParams(this.itemSize, LinearLayout.LayoutParams.WRAP_CONTENT));
            GlideHelper.loadWithHolderErr(mContext, item.goods_thumb, holder.itemView.findViewById(R.id.id_img_src));
            holder.setText(R.id.id_tv_price, "￥" + item.goods_price);
            RoundLinearLayout linearLayout = holder.itemView.findViewById(R.id.id_ly_level);
            ImageView imgLevel = holder.itemView.findViewById(R.id.id_img_level);
            TextView tvLevel = holder.itemView.findViewById(R.id.id_tv_level);
            WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, item.goods_probability_level);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
