package com.android.manghe.index.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.activity.TodayNewActivity;
import com.android.manghe.index.model.TodayNewInfo;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;


public class PTodayNew extends XPresenter<TodayNewActivity> {

    public List<TodayNewInfo> goodList = new ArrayList<>();

    public void getTodayNewList(){
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.TodayNew)
                .get()
                .execute(new BaseBack<TodayNewInfo>() {

                    @Override
                    public void onSuccess(Call call, List<TodayNewInfo> infoList) {
                        getV().hideLoadingDialog();
                        if (infoList != null) {
                            goodList = infoList;
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {
                        getV().update();
                    }
                });
    }

}
