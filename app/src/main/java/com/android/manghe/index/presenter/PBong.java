package com.android.manghe.index.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.fragment.BongFragment;
import com.android.manghe.index.model.BongInfo;

import okhttp3.Call;

import java.util.ArrayList;
import java.util.List;

public class PBong extends XPresenter<BongFragment> {

    public void getData(){
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.HIGH_POPULARITY_2)
                .get()
                .execute(new BaseBack<BongInfo>() {

                    @Override
                    public void onSuccess(Call call, List<BongInfo> bongInfoList) {
                        if (bongInfoList != null && bongInfoList.size() != 0) {
                            getV().update(bongInfoList);
                        }else{
                            getV().update(new ArrayList<>());
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
