package com.android.manghe.index.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.SingleLayoutHelper;
import com.android.base.tools.DisplayUtil;
import com.android.manghe.R;
import com.android.manghe.main.view.TabEntity;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;

/**
 * 首页最底下标签切换头部
 */
public class BottomBuyingTitleAdapter extends DelegateAdapter.Adapter<BottomBuyingTitleAdapter.BottomBuyingViewHolder> {

    private Context mContext;
    private String[] mTitles = {"正在热砍", "新手专享", "我在砍"};
    private int[] mTempIcon = {
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher};

    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private OnTabSelectListener mOnTabSelectListener;

    public BottomBuyingTitleAdapter(Context context, OnTabSelectListener onTabSelectListener) {
        mContext = context;
        mOnTabSelectListener = onTabSelectListener;
        for (int i = 0; i < mTitles.length; i++) {
            mTabEntities.add(new TabEntity(mTitles[i], mTempIcon[i], mTempIcon[i]));
        }
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        SingleLayoutHelper singleLayoutHelper = new SingleLayoutHelper();
        singleLayoutHelper.setBgColor(ContextCompat.getColor(mContext, R.color.white));
        singleLayoutHelper.setMarginTop(DisplayUtil.dip2px(mContext, 10));
        return singleLayoutHelper;
    }

    @NonNull
    @Override
    public BottomBuyingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new BottomBuyingViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_bottom_buy_title, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BottomBuyingViewHolder selectedViewHolder, int i) {
        if (selectedViewHolder.tabLayout.getTabCount() == 0) {
            selectedViewHolder.tabLayout.setTabData(mTabEntities);
            selectedViewHolder.tabLayout.setOnTabSelectListener(mOnTabSelectListener);
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 6;
    }

    class BottomBuyingViewHolder extends RecyclerView.ViewHolder {
        CommonTabLayout tabLayout;

        public BottomBuyingViewHolder(View itemView) {
            super(itemView);
            tabLayout = itemView.findViewById(R.id.tabLayout);
        }
    }
}
