package com.android.manghe.index.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxCategoriesModel;
import com.android.manghe.config.Config;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * 底部商品列表头部
 */
public class BottomGridGoodTabAdapter extends DelegateAdapter.Adapter<BottomGridGoodTabAdapter.AdapterViewHolder> {

    private CallBack callBack;
    private Context context;
    private List<BoxCategoriesModel.ListBean> categoryList = new ArrayList<>();
    private String selectedTabId = "";

    public BottomGridGoodTabAdapter(Context context, CallBack callBack) {
        this.context = context;
        this.callBack = callBack;
    }

    public void updateData(List<BoxCategoriesModel.ListBean> categoryList) {
        this.categoryList.clear();
        BoxCategoriesModel.ListBean item = new BoxCategoriesModel.ListBean();
        item.setSubtitle("为您推荐");
        item.setId("");
        item.setTitle("精选");
        categoryList.add(0, item);
        this.categoryList.addAll(categoryList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new BottomGridGoodTabAdapter.AdapterViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_bottom_grid_good_tab_2, viewGroup, false));
    }

    @Override
    public int getItemViewType(int position) {
        return 5;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder holder, int position) {
        holder.layoutHole.setVisibility(categoryList.isEmpty() ? View.GONE : View.VISIBLE);

        if (Config.BottomIsLoad.equals("x1001")) {
            holder.mTabLayout.removeAllTabs();
            for (int i = 0; i < categoryList.size(); i++) {
                holder.mTabLayout.addTab(holder.mTabLayout.newTab().setCustomView(getTabView(i, categoryList)));
            }
            holder.mTabLayout.post(new Runnable() {
                @Override
                public void run() {
                    holder.mTabLayout.scrollTo(0, 0);
                }
            });

        }

//
//        if (!categoryList.isEmpty()) {
//            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
//            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//            holder.recyclerView.setLayoutManager(layoutManager);
//            BaseQuickAdapter adapter = new BaseQuickAdapter<BoxCategoriesModel.ListBean, BaseViewHolder>(R.layout.item_bottom_grid_good_tab) {
//
//                @Override
//                protected void convert(BaseViewHolder baseViewHolder, BoxCategoriesModel.ListBean item) {
//                    TextView tvRecommendMainTitle = (TextView) baseViewHolder.itemView.findViewById(R.id.tvRecommendMainTitle);
//                    TextView tvRecommendSubTitle = (TextView) baseViewHolder.itemView.findViewById(R.id.tvRecommendSubTitle);
//                    tvRecommendMainTitle.setText(item.getTitle());
//                    tvRecommendSubTitle.setText(item.getSubtitle());
//
//                    tvRecommendMainTitle.setTextColor(ContextCompat.getColor(context, TextUtils.equals(item.getId(), selectedTabId) ? R.color.red400 : R.color.colorFont33));
//                    tvRecommendSubTitle.setTextColor(ContextCompat.getColor(context, TextUtils.equals(item.getId(), selectedTabId) ? R.color.white : R.color.colorFont66));
//                    tvRecommendSubTitle.setBackgroundResource(TextUtils.equals(item.getId(), selectedTabId) ? R.drawable.red_gradient_shape : R.color.transparent);
//                }
//            };
//            holder.recyclerView.setAdapter(adapter);
//            adapter.setNewData(categoryList);
//            adapter.setOnItemClickListener((adapter1, view, position1) -> {
//                String id = ((List<BoxCategoriesModel.ListBean>) adapter1.getData()).get(position1).getId();
//                selectedTabId = id;
//                if (!TextUtils.isEmpty(selectedTabId)) {
//                    adapter1.notifyDataSetChanged();
//
//                    holder.tvRecommendMainTitle.setTextColor(ContextCompat.getColor(context, R.color.colorFont33));
//                    holder.tvRecommendSubTitle.setTextColor(ContextCompat.getColor(context, R.color.colorFont66));
//                    holder.tvRecommendSubTitle.setBackgroundResource(R.color.transparent);
//                    callBack.onSelectedTab(selectedTabId);
//                }
//            });
//
//            //精选
//            holder.layoutJingXuan.setOnClickListener(view -> {
//                holder.tvRecommendMainTitle.setTextColor(ContextCompat.getColor(context, R.color.red400));
//                holder.tvRecommendSubTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
//                holder.tvRecommendSubTitle.setBackgroundResource(R.drawable.red_gradient_shape);
//                selectedTabId = "";
//                adapter.notifyDataSetChanged();
//                callBack.onSelectedTab("");
//            });
//        }
    }

    private View getTabView(int position, List<BoxCategoriesModel.ListBean> categoryList) {
        View view = View.inflate(context, R.layout.item_bottom_grid_good_tab, null);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        TextView tvTitle = view.findViewById(R.id.tvRecommendMainTitle);
        TextView tvSubTitle = view.findViewById(R.id.tvRecommendSubTitle);
        View line = view.findViewById(R.id.id_line);
        tvTitle.setText(categoryList.get(position).getTitle());
        tvSubTitle.setText(categoryList.get(position).getSubtitle());
        tvTitle.setTextColor(ContextCompat.getColor(context, position == 0 ? R.color.red400 : R.color.colorFont33));
        tvSubTitle.setTextColor(ContextCompat.getColor(context, position == 0 ? R.color.white : R.color.colorFont66));
        tvSubTitle.setBackgroundResource(position == 0 ? R.drawable.red_gradient_shape : R.color.transparent);
        line.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
        return view;
    }

    private void setSelectedTabStyle(TabLayout tabLayout, int position) {
        try {
            TextView tvTitle = null;
            TextView tvSubTitle = null;
            View line = null;
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                assert tab != null;
                tvTitle = tab.getCustomView().findViewById(R.id.tvRecommendMainTitle);
                tvSubTitle = tab.getCustomView().findViewById(R.id.tvRecommendSubTitle);
                line = tab.getCustomView().findViewById(R.id.id_line);
                tvTitle.setTextColor(ContextCompat.getColor(context, R.color.colorFont33));
                tvSubTitle.setTextColor(ContextCompat.getColor(context, R.color.colorFont66));
                tvSubTitle.setBackgroundResource(R.color.transparent);
                line.setVisibility(i == 0 ? View.GONE : View.VISIBLE);
            }
            TabLayout.Tab selectedTab = tabLayout.getTabAt(position);
            tvTitle = selectedTab.getCustomView().findViewById(R.id.tvRecommendMainTitle);
            tvSubTitle = selectedTab.getCustomView().findViewById(R.id.tvRecommendSubTitle);
            tvTitle.setTextColor(ContextCompat.getColor(context, R.color.red400));
            tvSubTitle.setTextColor(ContextCompat.getColor(context, R.color.white));
            tvSubTitle.setBackgroundResource(R.drawable.red_gradient_shape);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        return new LinearLayoutHelper();
    }

    class AdapterViewHolder extends RecyclerView.ViewHolder {
        public View layoutHole, layoutJingXuan;
        private TabLayout mTabLayout;




        public AdapterViewHolder(View itemView) {
            super(itemView);
            layoutHole = itemView.findViewById(R.id.layoutHole);
            layoutJingXuan = itemView.findViewById(R.id.layoutJingXuan);
            mTabLayout = itemView.findViewById(R.id.tabLayout);

            mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    setSelectedTabStyle(mTabLayout, mTabLayout.getSelectedTabPosition());
                    selectedTabId = categoryList.get(mTabLayout.getSelectedTabPosition()).getId();
                    Config.BottomIsLoad = selectedTabId;
                    callBack.onSelectedTab(selectedTabId);
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        }
    }

    public interface CallBack {
        void onSelectedTab(String categoryId);
    }
}
