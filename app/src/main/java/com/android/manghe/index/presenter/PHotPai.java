package com.android.manghe.index.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.fragment.HotPaiFragment;
import com.android.manghe.index.model.HotPaiInfo;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.List;

public class PHotPai extends XPresenter<HotPaiFragment> {

    public void getData(){
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.HIGH_POPULARITY_0)
                .get()
                .execute(new BaseBack<HotPaiInfo>() {

                    @Override
                    public void onSuccess(Call call, List<HotPaiInfo> hotPaiInfoList) {
                        if (hotPaiInfoList != null && hotPaiInfoList.size() != 0) {
                            getV().update(hotPaiInfoList);
                        }else{
                            getV().update(new ArrayList<>());
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
