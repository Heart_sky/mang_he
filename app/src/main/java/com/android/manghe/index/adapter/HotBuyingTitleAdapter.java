package com.android.manghe.index.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.SingleLayoutHelper;
import com.android.base.tools.DisplayUtil;
import com.android.manghe.R;

/**
 * 超值热砍Title
 */
public class HotBuyingTitleAdapter extends DelegateAdapter.Adapter<HotBuyingTitleAdapter.SelectedViewHolder> {

    private Context mContext;

    public HotBuyingTitleAdapter(Context context) {
        mContext = context;
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        SingleLayoutHelper singleLayoutHelper = new SingleLayoutHelper();
        singleLayoutHelper.setBgColor(ContextCompat.getColor(mContext, R.color.white));
        singleLayoutHelper.setMarginTop(DisplayUtil.dip2px(mContext, 10));
        return singleLayoutHelper;
    }

    @NonNull
    @Override
    public SelectedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SelectedViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_hot_buying_title, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedViewHolder selectedViewHolder, int i) {
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 4;
    }

    class SelectedViewHolder extends RecyclerView.ViewHolder {

        public SelectedViewHolder(View itemView) {
            super(itemView);
        }
    }
}
