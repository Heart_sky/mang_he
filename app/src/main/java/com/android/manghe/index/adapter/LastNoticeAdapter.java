package com.android.manghe.index.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.tools.BigDecimalUtils;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.index.model.OldNoticeBean;

import java.util.List;

public class LastNoticeAdapter extends BaseRecyclerViewAdapter<OldNoticeBean.DataBean.ListBean, LastNoticeAdapter.LastNoticeHolder> {
    private Context mContext;

    public LastNoticeAdapter(Context context, List<OldNoticeBean.DataBean.ListBean> list) {
        super(list);
        mContext = context;
    }

    public void update(List<OldNoticeBean.DataBean.ListBean> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(LastNoticeHolder holder, OldNoticeBean.DataBean.ListBean item) {
        GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.getView(R.id.ivPic));
        holder.setText(R.id.tvTitle, item.title);
        holder.setText(R.id.tvMarketPrice, "市场价：￥" + item.market_price);
        holder.setText(R.id.tvPrice, "成交价：￥" + item.price);
        double percent = BigDecimalUtils.round((Double.parseDouble(item.market_price) - Double.parseDouble(item.price)) / Double.parseDouble(item.market_price) * 100.00, 2);
        holder.setText(R.id.tvSave, percent >= 100 ? "99.99" : percent + "");
    }

    @Override
    public LastNoticeHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new LastNoticeHolder(inflateItemView(viewGroup, R.layout.item_last_notice));
    }

    public class LastNoticeHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public LastNoticeHolder(View itemView) {
            super(itemView);
        }
    }
}
