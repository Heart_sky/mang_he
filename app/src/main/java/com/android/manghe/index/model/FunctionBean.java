package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class FunctionBean extends NetBean<List<FunctionBean>> {

    /**
     * title : 充值
     * href : voucher
     * src : https://www.shanqu8.com/upload/1/images/gallery/q/r/964_src.png
     */

    public String title;
    public String src;
    public String href;
    public String goodscatid;//分类商品id
    public String id;//导航栏id
}
