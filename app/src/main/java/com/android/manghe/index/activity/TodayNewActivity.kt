package com.android.manghe.index.activity

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.DisplayUtil
import com.android.manghe.R
import com.android.manghe.index.adapter.TodayNewGridAdapter
import com.android.manghe.index.model.TodayNewInfo
import com.android.manghe.index.presenter.PTodayNew
import com.android.manghe.view.GridAuctionSpacesItemDecoration
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_market_cat_good_list.*

/**
 * 今日有好货商品列表
 */
class TodayNewActivity : MVPActivity<PTodayNew>() , IRefresh {
    private var mAdapter : TodayNewGridAdapter? = null

    override fun getLayoutId(): Int = R.layout.activity_today_new


    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("今日有好物").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setBackgroundColor(
            ContextCompat.getColor(this,R.color.white))
        initListView()
        showLoadingDialog()
        p.getTodayNewList()
    }

    private fun initListView(){
        recyclerView.layoutManager =
            GridLayoutManager(this, 2)
        recyclerView.addItemDecoration(GridAuctionSpacesItemDecoration(DisplayUtil.dip2px(this, 8f)))
        mAdapter = TodayNewGridAdapter(
            this,false,
            ArrayList<TodayNewInfo>()
        )
        recyclerView.adapter = mAdapter
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getTodayNewList()
    }

    override fun onLoad() {

    }

    fun update(){
        mAdapter?.let {
            it.update(p.goodList)
        }
    }
}