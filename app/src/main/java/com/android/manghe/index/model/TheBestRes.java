package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class TheBestRes extends NetBean<TheBestRes.DataBean> {


    public static class DataBean {
        /**
         * start_date : 2019.03.25
         * end_date : 2019.04.01
         * win_num : 2
         * market_amount : 15176.00
         * save_percent : 98.74
         * member_avatar : http://www.ycdy8.com/upload/1/images/photo/15430_thumb.jpg
         * member_id : 15430
         * member_nickname : 囚犯叫学生
         * order_list : [{"action_id":"214","thumb":"http://www.ycdy8.com/upload/1/images/gallery/9/c/13297_src.jpg","title":"微软Surface Pro 6 二合一平板笔记本 12.3英寸（第八代 i5 8G 128G ）","c_time":"1553940317","market_price":"7588.00","price":"97.60","save_percent":98.71},{"action_id":"214","thumb":"http://www.ycdy8.com/upload/1/images/gallery/9/c/13297_src.jpg","title":"微软Surface Pro 6 二合一平板笔记本 12.3英寸（第八代 i5 8G 128G ）","c_time":"1554025350","market_price":"7588.00","price":"94.30","save_percent":98.76}]
         * comment_list : [{"c_time":"1554126267","content":"测试测试测试测试测试测试测试测试","thumbs":["http://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg","http://www.ycdy8.com/upload/1/images/gallery/9/d/13298_src.png","http://www.ycdy8.com/upload/1/images/gallery/9/c/13297_src.jpg"],"title":"微软Surface Pro 6 二合一平板笔记本 12.3英寸（第八代 i5 8G 128G ）"},{"c_time":"1554126247","content":"测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试","thumbs":["http://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg","http://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg"],"title":"微软Surface Pro 6 二合一平板笔记本 12.3英寸（第八代 i5 8G 128G ）"}]
         * dangqi : {"current":"2","list":["1"]}
         */

        public String start_date;
        public String end_date;
        public String win_num;
        public String market_amount;
        public double save_percent;
        public String member_avatar;
        public String member_id;
        public String member_nickname;
        public DangqiBean dangqi;
        public List<OrderListBean> order_list;
        public List<CommentListBean> comment_list;

        public static class DangqiBean {
            /**
             * current : 2
             * list : ["1"]
             */

            public String current;
            public List<String> list;
        }

        public static class OrderListBean {
            /**
             * action_id : 214
             * thumb : http://www.ycdy8.com/upload/1/images/gallery/9/c/13297_src.jpg
             * title : 微软Surface Pro 6 二合一平板笔记本 12.3英寸（第八代 i5 8G 128G ）
             * c_time : 1553940317
             * market_price : 7588.00
             * price : 97.60
             * save_percent : 98.71
             */

            public String action_id;
            public String thumb;
            public String title;
            public String c_time;
            public String market_price;
            public String price;
            public double save_percent;
        }

        public static class CommentListBean {
            /**
             * c_time : 1554126267
             * content : 测试测试测试测试测试测试测试测试
             * thumbs : ["http://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg","http://www.ycdy8.com/upload/1/images/gallery/9/d/13298_src.png","http://www.ycdy8.com/upload/1/images/gallery/9/c/13297_src.jpg"]
             * title : 微软Surface Pro 6 二合一平板笔记本 12.3英寸（第八代 i5 8G 128G ）
             */

            public long c_time;
            public String content;
            public String title;
            public List<String> thumbs;
        }
    }
}
