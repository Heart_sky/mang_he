package com.android.manghe.index.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.GridLayoutHelper;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.market.activity.BoxDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class BottomGridGoodAdapter extends DelegateAdapter.Adapter<BottomGridGoodAdapter.AdapterViewHolder> {

    private Context context;
    private List<BoxModel.DataBean> mGoodList = new ArrayList<>();

    public BottomGridGoodAdapter(Context context) {
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void updateData(List<BoxModel.DataBean> goodList) {
        this.mGoodList.clear();
        this.mGoodList.addAll(goodList);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public BottomGridGoodAdapter.AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new BottomGridGoodAdapter.AdapterViewHolder(LayoutInflater.from(context).inflate(R.layout.item_box_list, viewGroup, false));
    }

    @Override
    public int getItemViewType(int position) {
        return 6;
    }

    @Override
    public void onBindViewHolder(@NonNull BottomGridGoodAdapter.AdapterViewHolder holder, int position) {
        BoxModel.DataBean item = mGoodList.get(position);
        GlideHelper.loadWithHolderErr(context, item.cover_pic, holder.itemView.findViewById(R.id.ivPic));
        if (item.prices != null && item.prices._$1 != null) {
            ((TextView) holder.itemView.findViewById(R.id.tvCurrentPrice))
                    .setText("￥" + item.prices._$1.sell_price);
        }
        ((TextView) holder.itemView.findViewById(R.id.tvTitle)).setText(item.name);
        ((TextView) holder.itemView.findViewById(R.id.id_tv_goods_total))
                .setText("共" + item.goods_list.size() + "款商品");

        ((TextView) holder.itemView.findViewById(R.id.id_tv_price_mim_max))
                .setText("商品价值：" + item.price_interval.min + "-" + item.price_interval.max);
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, BoxDetailActivity.class);
            intent.putExtra("BoxListModel", item);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mGoodList.size();
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        GridLayoutHelper gridHelper = new GridLayoutHelper(2);
        gridHelper.setGap(DisplayUtil.dip2px(context, 8f));
        gridHelper.setAutoExpand(false);
        gridHelper.setPaddingLeft(10);
        gridHelper.setPaddingRight(10);
        return gridHelper;
    }

    class AdapterViewHolder extends RecyclerView.ViewHolder {
        public AdapterViewHolder(View itemView) {
            super(itemView);

        }
    }
}
