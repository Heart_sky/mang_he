package com.android.manghe.index.fragment

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.VirtualLayoutManager
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.manghe.R
import com.android.manghe.box.model.BoxCategoriesModel
import com.android.manghe.box.model.BoxModel
import com.android.manghe.box.model.BoxPrizeModel
import com.android.manghe.config.Config
import com.android.manghe.config.events.RefreshIndexNoticeEvent
import com.android.manghe.coupon.model.CouponBannerRes
import com.android.manghe.index.adapter.*
import com.android.manghe.index.model.*
import com.android.manghe.index.presenter.PIndex
import com.eightbitlab.rxbus.Bus
import com.eightbitlab.rxbus.registerInBus
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_index_web_socket.recyclerView
import kotlinx.android.synthetic.main.fragment_index_web_socket.refreshLayout
import java.util.*

class IndexFragment : MVPFragment<PIndex>(), IRefresh {

    private val TopBannerFunctions_Layout = true  //广告和功能入口
    private val NoticeBanner_Layout = true //公告和中间广告
    private val HotBuyTimeLimit_Layout = true;//人气热卖和限时特惠
    private val TodayNew_Layout = true;//今日有好货
    private val BottomGridGoodTab_Layout = true //底部Grid商品TAB
    private val BottomGridGoodList_Layout = true //底部Grid商品列表

    private var topBannerFunctionsAdapter: TopBannerFunctionsAdapter? = null
    private var noticeAdapter: NoticeAdapter? = null
    private var hotBuyTimeLimitAdapter: HotBuyTimeLimitAdapter? = null
    private var todayNewAdapter: TodayNewAdapter? = null
    private var bottomGridGoodTabAdapter: BottomGridGoodTabAdapter? = null
    private var bottomGridGoodAdapter: BottomGridGoodAdapter? = null

    override fun getLayoutId() = R.layout.fragment_index
    override fun showToolBarType() = ETitleType.OVERLAP_TITLE
    override fun initData(savedInstanceState: Bundle?, parent: View) {
        initViews()
        initListener()
        loadData()
    }

    private fun initListener() {
        Bus.observe<RefreshIndexNoticeEvent>().subscribe {
            p.getBoxPrize()
        }.registerInBus(this)
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        loadData()
    }

    override fun onLoad() {
        p.getBottomGridGood(false)
    }

    private fun initViews() {
        val layoutManager = VirtualLayoutManager(activity!!.applicationContext)
        recyclerView.layoutManager = layoutManager
        val delegateAdapter = DelegateAdapter(layoutManager, true)
        recyclerView.adapter = delegateAdapter!!
        delegateAdapter.setAdapters(getAdapters())
    }

    /**
     * 分模块设置Adapter
     *
     * @return
     */
    private fun getAdapters(): List<DelegateAdapter.Adapter<*>> {
        val adapters = LinkedList<DelegateAdapter.Adapter<*>>()

        if (TopBannerFunctions_Layout) {
            topBannerFunctionsAdapter =
                TopBannerFunctionsAdapter(activity)
            adapters.add(topBannerFunctionsAdapter!!)
        }
        if (NoticeBanner_Layout) {
            noticeAdapter = NoticeAdapter(activity)
            adapters.add(noticeAdapter!!)
        }
        if (HotBuyTimeLimit_Layout) {
            hotBuyTimeLimitAdapter = HotBuyTimeLimitAdapter(activity);
            adapters.add(hotBuyTimeLimitAdapter!!)
        }
        if (TodayNew_Layout) {
            todayNewAdapter = TodayNewAdapter(activity)
            adapters.add(todayNewAdapter!!)
        }
        if (BottomGridGoodTab_Layout) {
            bottomGridGoodTabAdapter = BottomGridGoodTabAdapter(activity) { categoryId ->
                p.currentBottomCategoryId = categoryId
                p.getBottomGridGood(true)
            }
            adapters.add(bottomGridGoodTabAdapter!!)
        }
        if (BottomGridGoodList_Layout) {
            bottomGridGoodAdapter = BottomGridGoodAdapter(activity)
            adapters.add(bottomGridGoodAdapter!!)
        }
        return adapters
    }

    private fun loadData() {
        Config.BottomIsLoad = "x1001"
        p.getBanner()
        p.getFunctions()

        //p.getNotice()
        p.getBoxPrize()

        p.getMiddleBanner()

        p.getHotBuyTimeLimit()

        p.getTodayNew()

        p.getBottomGridGoodTab()
        p.getBottomGridGood(true)
    }

    fun updateBanner(topBannerBeanList: List<MainTopBannerBean>) {
        topBannerFunctionsAdapter?.let {
            it.updateBannerData(topBannerBeanList)
        }
    }

    fun updateFunction(functionList: List<FunctionBean>) {
        topBannerFunctionsAdapter?.let {
            it.updateFunctionsData(functionList)
        }
    }

//    fun updateNotice(oldNoticeBeanList: List<NoticeBean>) {
//        noticeAdapter?.let {
//            it.updateData(oldNoticeBeanList)
//        }
//    }

    fun updateNotice(oldNoticeBeanList: List<BoxPrizeModel.DataBean.ListBean>) {
        noticeAdapter?.let {
            it.updateData(oldNoticeBeanList)
        }
    }

    fun updateMiddleBanner(data: List<CouponBannerRes>?) {
        noticeAdapter?.let {
            it.updateBanner(data)
        }
    }

    fun updateHotBuyTimeLimit(data: HotBuyTimeLimitRes.DataBean) {
        hotBuyTimeLimitAdapter?.let {
            it.updateData(data)
        }
    }

    fun updateTodayNew(data: List<TodayNewInfo>) {
        todayNewAdapter?.let {
            it.updateData(data)
        }
    }

    fun updateBottomGridGoodTab(data: List<BoxCategoriesModel.ListBean>) {
        bottomGridGoodTabAdapter?.let {
            it.updateData(data)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun updateBottomGridGood(dataList: List<BoxModel.DataBean>) {
        bottomGridGoodAdapter?.let {
            it.updateData(dataList)
        }
    }


    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }

    override fun onDestroy() {
        super.onDestroy()
        noticeAdapter?.stopSlide()
    }
}