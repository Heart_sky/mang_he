package com.android.manghe.index.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.index.model.BongInfo;

import java.util.List;

/**
 * 本周爆品列表
 */
public class BongAdapter extends BaseRecyclerViewAdapter<BongInfo, BongAdapter.HotPaiHolder> {
    private Context mContext;

    public BongAdapter(Context context, List<BongInfo> list) {
        super(list);
        mContext = context;
    }

    public void update(List<BongInfo> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(HotPaiHolder holder, BongInfo item) {
        GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.getView(R.id.ivPic));
        holder.setText(R.id.tvTitle, item.title);
        holder.setText(R.id.tvSold, "爆卖" + item.success_get_num + "件");
        holder.setText(R.id.tvPrice, "￥" + item.avg_price);
        holder.setText(R.id.tvGetAccount, item.success_get_num+"人砍到");
        GlideHelper.loadRoundTrans(mContext,item.thumb,holder.getView(R.id.ivPic),6);
    }

    @Override
    public HotPaiHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new HotPaiHolder(inflateItemView(viewGroup, R.layout.item_bong));
    }

    public class HotPaiHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public HotPaiHolder(View itemView) {
            super(itemView);
        }
    }
}
