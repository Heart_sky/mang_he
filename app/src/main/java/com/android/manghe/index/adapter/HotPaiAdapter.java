package com.android.manghe.index.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;
import com.android.manghe.detail.activity.GoodDetailActivity;
import com.android.manghe.index.model.HotPaiInfo;

import java.util.List;

public class HotPaiAdapter extends BaseRecyclerViewAdapter<HotPaiInfo, HotPaiAdapter.HotPaiHolder> {
    private Context mContext;

    public HotPaiAdapter(Context context, List<HotPaiInfo> list) {
        super(list);
        mContext = context;
    }

    public void update(List<HotPaiInfo> list) {
        super.update(list);
    }

    @Override
    protected void bindDataToItemView(HotPaiHolder holder, HotPaiInfo item) {
        GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.getView(R.id.ivPic));
        holder.setText(R.id.tvTitle, item.title);
        holder.setText(R.id.tvPrice, "￥" + item.avg_price);
        holder.setText(R.id.tvGetAccount, item.success_get_num+"人砍到");
        GlideHelper.loadRoundTrans(mContext,item.thumb,holder.getView(R.id.ivPic),6);
        holder.setOnClickListener(R.id.tvToGet, v -> {
            //我要去砍
            Intent intent = new Intent(mContext, GoodDetailActivity.class);
            intent.putExtra("auctionId", item.auction_id);
            mContext.startActivity(intent);
        });
    }

    @Override
    public HotPaiHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new HotPaiHolder(inflateItemView(viewGroup, R.layout.item_hot_pai));
    }

    public class HotPaiHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public HotPaiHolder(View itemView) {
            super(itemView);
        }
    }
}
