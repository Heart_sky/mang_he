package com.android.manghe.index.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.View
import android.widget.RelativeLayout
import com.android.base.frame.activity.FragmentActivity
import com.android.base.tools.DisplayUtil
import com.android.base.tools.StatusBarUtil
import com.android.manghe.R
import com.android.manghe.index.fragment.BongFragment
import com.android.manghe.index.fragment.HotPaiFragment
import com.android.manghe.index.fragment.NewReleaseFragment
import com.android.manghe.index.fragment.TheBestFragment
import com.android.manghe.main.view.TabEntity
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import com.githang.statusbar.StatusBarCompat
import kotlinx.android.synthetic.main.activity_select_for_you.*

/**
 * 为你精选
 */
class SelectForYouActivity : FragmentActivity() {
    private val mTabEntities = ArrayList<CustomTabEntity>()
    private val mTitles = arrayOf("人气拍品", "新品首发", "本周爆品", "本周大神")

    companion object {
        fun showActivity(context: Context, position: Int) {
            val intent = Intent(context, SelectForYouActivity::class.java)
            intent.putExtra("position", position)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_select_for_you
    }

    override fun initData(savedInstanceState: Bundle?) {
        val lp = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, DisplayUtil.dip2px(this, 45f))
        lp.topMargin = StatusBarUtil.getStatusBarHeight(this)
        layoutTitleBar.layoutParams = lp


        ivBack.setOnClickListener { finish() }
        addFragment(
            R.id.container,
            HotPaiFragment::class.java,
            NewReleaseFragment::class.java,
            BongFragment::class.java,
            TheBestFragment::class.java
        )
        //tab
        for (i in mTitles.indices) {
            mTabEntities.add(TabEntity(mTitles[i], 0, 0))
        }
        tabLayout.setTabData(mTabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabReselect(position: Int) {
            }

            override fun onTabSelect(position: Int) {
                showFragment(position)
                setTopStyle(position)
            }
        })
        val initPagePosition = intent.getIntExtra("position", 0)
        tabLayout.currentTab = initPagePosition
        showFragment(initPagePosition)
        if (initPagePosition == 3) {
            setTopStyle(initPagePosition)
            StatusBarCompat.setStatusBarColor(this, ContextCompat.getColor(this, R.color.purplea700))
        }
    }

    private fun setTopStyle(position: Int) {
        ivTheBest.visibility = if (position == 3) View.VISIBLE else View.GONE
        ivBack.setImageResource(if (position == 3) R.mipmap.icon_back_white else R.mipmap.icon_back_black)
        tvTitle.setTextColor(
            ContextCompat.getColor(
                this@SelectForYouActivity,
                if (position == 3) R.color.white else R.color.colorFont33
            )
        )
        layoutTitleBar.setBackgroundColor(
            ContextCompat.getColor(
                this@SelectForYouActivity,
                if (position == 3) R.color.transparent else R.color.white
            )
        )
        tabLayout.setBackgroundColor(
            ContextCompat.getColor(
                this@SelectForYouActivity,
                if (position == 3) R.color.transparent else R.color.white
            )
        )
        tabLayout.textUnselectColor = ContextCompat.getColor(
            this@SelectForYouActivity,
            if (position == 3) R.color.purple100 else R.color.colorFont99
        )
        tabLayout.textSelectColor =
            ContextCompat.getColor(this@SelectForYouActivity, if (position == 3) R.color.white else R.color.colorFont33)
        tabLayout.indicatorColor = ContextCompat.getColor(
            this@SelectForYouActivity,
            if (position == 3) R.color.yellow500 else R.color.colorPrimary
        )
    }
}