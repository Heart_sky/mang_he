package com.android.manghe.index.activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.detail.activity.GoodDetailActivity
import com.android.manghe.index.adapter.LastNoticeAdapter
import com.android.manghe.index.model.OldNoticeBean
import com.android.manghe.index.presenter.PLastedNoticeList
import com.android.manghe.user.activity.LoginActivity
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_last_notice.*

class LastedNoticeListActivity : MVPActivity<PLastedNoticeList>(), IRefresh {

    private var mAdapter: LastNoticeAdapter? = null

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_last_notice
    }
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("最新成交").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)

        mAdapter = LastNoticeAdapter(this, arrayListOf())
        recyclerView.layoutManager =
            LinearLayoutManager(this)
        recyclerView.adapter = mAdapter
        mAdapter!!.setOnItemClickListener{_, item->
            item?.let {
                if(UserHolder.getUserInfo(this@LastedNoticeListActivity) == null){
                    open(LoginActivity::class.java)
                }else{
                    val intent = Intent(this@LastedNoticeListActivity, GoodDetailActivity::class.java)
                    intent.putExtra("isDeal", true)
                    intent.putExtra("orderId", item.id)
                    startActivity(intent)
                }
            }
        }
//        refreshLayout.autoRefresh()
        danMuView.setListener {
            danMuView.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()
        p.loadData(true)
    }

    override fun onRefresh() {
        p.loadData(true)
    }

    override fun onLoad() {
        p.loadData(false)
    }

    fun update(dataList: List<OldNoticeBean.DataBean.ListBean>) {
        mAdapter?.let {
            it.update(dataList)
            danMuView.add(dataList)
            if(dataList.isNotEmpty())
                danMuView.visibility = View.VISIBLE
        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }

    override fun onPause() {
        super.onPause()
        danMuView.clear()
    }
}
