package com.android.manghe.index.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.android.base.view.ViewHolderTool;
import com.android.manghe.R;
import com.android.manghe.view.AutoHeightGridView;

import java.util.List;

public class TheBestBottomAdapter extends DelegateAdapter.Adapter<TheBestBottomAdapter.TheBestViewHolder> {

    private Context mContext;
    private List<String> mDangQiList;
    private ITheBestBottomListener mListener;

    public interface ITheBestBottomListener{
        void onItemClick(int dangqiId);
    }

    public TheBestBottomAdapter(Context context,ITheBestBottomListener listener) {
        mContext = context;
        mListener = listener;
    }

    public void updateData(List<String> theBest) {
        mDangQiList = theBest;
        notifyDataSetChanged();
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        return new LinearLayoutHelper();
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    @Override
    public TheBestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TheBestViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_the_best_bottom, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TheBestViewHolder holder, int i) {
        if (mDangQiList != null && mDangQiList.size() != 0) {
            AutoHeightGridView gridView = holder.itemView.findViewById(R.id.gridView);
            gridView.setAdapter(new OrderGridAdapter());
            gridView.setOnItemClickListener((parent, view, position, id) -> {
                if(mListener != null){
                    mListener.onItemClick(Integer.parseInt(mDangQiList.get(position)));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class TheBestViewHolder extends RecyclerView.ViewHolder {

        public TheBestViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class OrderGridAdapter extends BaseAdapter {

        public OrderGridAdapter() {

        }

        @Override
        public int getCount() {
            return mDangQiList != null ? mDangQiList.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mDangQiList != null ? mDangQiList.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_dangqi, parent, false);
            }
            ((TextView) ViewHolderTool.get(convertView, R.id.tvDangQi)).setText("第"+mDangQiList.get(position)+"期");
            return convertView;
        }
    }
}
