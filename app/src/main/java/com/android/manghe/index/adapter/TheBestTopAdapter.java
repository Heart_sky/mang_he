package com.android.manghe.index.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.android.base.tools.DateTimeUtil;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.ViewHolderTool;
import com.android.base.view.recycler.RecyclerViewDivider;
import com.android.manghe.R;
import com.android.manghe.index.model.TheBestRes;
import com.android.manghe.view.AutoHeightGridView;
import com.android.manghe.view.AutoHeightLayoutManager;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class TheBestTopAdapter extends DelegateAdapter.Adapter<TheBestTopAdapter.TheBestViewHolder> {

    private Context mContext;
    private TheBestRes.DataBean mTheBest;

    public TheBestTopAdapter(Context context) {
        mContext = context;
    }

    public void updateData(TheBestRes.DataBean theBest) {
        mTheBest = theBest;
        notifyDataSetChanged();
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        return new LinearLayoutHelper();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @NonNull
    @Override
    public TheBestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TheBestViewHolder(LayoutInflater.from(mContext).inflate(R.layout.layout_the_best_top, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TheBestViewHolder holder, int i) {
        if (mTheBest != null) {
            GlideHelper.loadCircle(mContext, mTheBest.member_avatar, holder.itemView.findViewById(R.id.ivAvatar));
            ((TextView) holder.itemView.findViewById(R.id.tvName)).setText(mTheBest.member_nickname);
            ((TextView) holder.itemView.findViewById(R.id.tvPaiOk)).setText(mTheBest.win_num);
            ((TextView) holder.itemView.findViewById(R.id.tvPrice)).setText(mTheBest.market_amount);
            ((TextView) holder.itemView.findViewById(R.id.tvPercent)).setText(mTheBest.save_percent + "");

            AutoHeightGridView gridView = holder.itemView.findViewById(R.id.gridView);
            gridView.setAdapter(new OrderGridAdapter(mTheBest.order_list));

            holder.itemView.findViewById(R.id.layoutShaiHistory).setVisibility(mTheBest.comment_list != null && mTheBest.comment_list.size() != 0 ? View.VISIBLE : View.GONE);

            RecyclerView shaiRecyclerView = holder.itemView.findViewById(R.id.recyclerViewShai);
            shaiRecyclerView.setLayoutManager(new AutoHeightLayoutManager(mContext, LinearLayout.VERTICAL, false));
            shaiRecyclerView.addItemDecoration(new RecyclerViewDivider(mContext, LinearLayoutManager.VERTICAL));
            shaiRecyclerView.setAdapter(new CommentAdapter(mTheBest.comment_list));
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class TheBestViewHolder extends RecyclerView.ViewHolder {

        public TheBestViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class OrderGridAdapter extends BaseAdapter {
        List<TheBestRes.DataBean.OrderListBean> mOrderList;

        public OrderGridAdapter(List<TheBestRes.DataBean.OrderListBean> orderList) {
            mOrderList = orderList;
        }

        @Override
        public int getCount() {
            return mOrderList != null ? mOrderList.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mOrderList != null ? mOrderList.get(position) : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_grid_order, parent, false);
            }
            TheBestRes.DataBean.OrderListBean orderInfo = mOrderList.get(position);
            GlideHelper.loadRoundTrans(mContext, orderInfo.thumb, ViewHolderTool.get(convertView, R.id.ivPic), DisplayUtil.dip2px(mContext, 8));
            ((TextView) ViewHolderTool.get(convertView, R.id.tvPercent)).setText(orderInfo.save_percent + "%");
            ((TextView) ViewHolderTool.get(convertView, R.id.tvPrice)).setText("￥" + orderInfo.price);
            TextView tvOldPrice = ViewHolderTool.get(convertView, R.id.tvOldPrice);
            tvOldPrice.setText("￥" + orderInfo.market_price);
            tvOldPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            return convertView;
        }
    }


    public class CommentAdapter extends BaseQuickAdapter<TheBestRes.DataBean.CommentListBean, BaseViewHolder> {
        public CommentAdapter(List<TheBestRes.DataBean.CommentListBean> list) {
            super(R.layout.item_comment_list, list);
        }

        @Override
        protected void convert(BaseViewHolder holder, TheBestRes.DataBean.CommentListBean item) {
            GlideHelper.loadCircle(mContext, mTheBest.member_avatar, holder.getView(R.id.ivAvatar));
            holder.setText(R.id.tvName, mTheBest.member_nickname);
            holder.setText(R.id.tvTime, DateTimeUtil.longToString(item.c_time, "yyyy-MM-dd HH:mm:ss"));
            holder.setText(R.id.tvTitle, item.title);
            holder.setGone(R.id.layoutPic, item.thumbs.size() != 0);
            GlideHelper.loadWithHolderErr(mContext, item.thumbs.size() >= 1 ? item.thumbs.get(0) : "", holder.getView(R.id.ivPic0));
            GlideHelper.loadWithHolderErr(mContext, item.thumbs.size() >= 2 ? item.thumbs.get(1) : "", holder.getView(R.id.ivPic1));
            GlideHelper.loadWithHolderErr(mContext, item.thumbs.size() >= 3 ? item.thumbs.get(2) : "", holder.getView(R.id.ivPic2));
            GlideHelper.loadWithHolderErr(mContext, item.thumbs.size() >= 4 ? item.thumbs.get(3) : "", holder.getView(R.id.ivPic3));

            holder.setVisible(R.id.ivPic0, item.thumbs.size() >= 1);
            holder.setVisible(R.id.ivPic1, item.thumbs.size() >= 2);
            holder.setVisible(R.id.ivPic2, item.thumbs.size() >= 3);
            holder.setVisible(R.id.ivPic3, item.thumbs.size() >= 4);
        }
    }
}
