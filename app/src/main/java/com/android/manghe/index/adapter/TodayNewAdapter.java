package com.android.manghe.index.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.index.activity.TodayNewActivity;
import com.android.manghe.index.model.TodayNewInfo;
import com.android.manghe.market.activity.BoxListActivity;
import com.android.manghe.user.activity.LoginActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * 今日有好货
 */
public class TodayNewAdapter extends DelegateAdapter.Adapter<TodayNewAdapter.AdapterHolder> {

    private Context context;

    private List<TodayNewInfo> todayNewList = new ArrayList<>();

    public TodayNewAdapter(Context context) {
        this.context = context;
    }

    public void updateData(List<TodayNewInfo> todayNewList) {
        this.todayNewList.clear();
        this.todayNewList.addAll(todayNewList);
        notifyDataSetChanged();
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        return new LinearLayoutHelper();
    }

    @NonNull
    @Override
    public AdapterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AdapterHolder(LayoutInflater.from(context).inflate(R.layout.layout_today_new, viewGroup, false));
    }

    @Override
    public int getItemViewType(int position) {
        return 4;
    }
    @Override
    public void onBindViewHolder(@NonNull AdapterHolder holder, int i) {
        holder.tvNewCount.setText("今日上新" + todayNewList.size() + "款");
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        holder.recyclerView.setLayoutManager(layoutManager);
        TodayNewGoodItemAdapter adapter = new TodayNewGoodItemAdapter(context);
        holder.recyclerView.setAdapter(adapter);
        adapter.setNewData(todayNewList);
        holder.tvNewCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, UserHolder.getUserInfo(context) == null ? LoginActivity.class : TodayNewActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class AdapterHolder extends RecyclerView.ViewHolder {
        public TextView tvNewCount;
        public RecyclerView recyclerView;
        public AdapterHolder(View itemView) {
            super(itemView);
            tvNewCount = itemView.findViewById(R.id.tvNewCount);
            recyclerView = itemView.findViewById(R.id.recyclerView);
        }
    }

    private class TodayNewGoodItemAdapter extends BaseQuickAdapter<TodayNewInfo, BaseViewHolder> {
        public Context context;
        public TodayNewGoodItemAdapter(Context context) {
            super(R.layout.item_today_new);
            this.context = context;
        }

        @Override
        protected void convert(BaseViewHolder baseViewHolder, TodayNewInfo bean) {
            GlideHelper.loadRoundTrans(context, bean.thumb, baseViewHolder.getView(R.id.ivPic), DisplayUtil.dip2px(context, 3));
            baseViewHolder.setText(R.id.tvPrice, "￥"+bean.price);
            baseViewHolder.setText(R.id.tvTitle, bean.name);
//            baseViewHolder.getView(R.id.layoutHole).setOnClickListener(v -> {
//                if (UserHolder.getUserInfo(context) == null) {
//                    LoginActivity.Companion.showActivity(context);
//                } else {
//                    Intent intent = new Intent(mContext, MarketGoodDetailActivity.class);
//                    intent.putExtra("goodId", bean.id);
//                    mContext.startActivity(intent);
//                }
//            });

            baseViewHolder.getView(R.id.layoutHole).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, BoxListActivity.class);
                    intent.putExtra("mangheCatId", bean.manghe_cat_id);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
