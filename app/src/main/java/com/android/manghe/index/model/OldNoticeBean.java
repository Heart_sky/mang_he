package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class OldNoticeBean extends NetBean<OldNoticeBean.DataBean> {

    /**
     * data : {"list":[{"id":"2657038","title":"飞利浦（PHILIPS）电动剃须刀 荷兰进口S7310/12","auction_id":"21","c_time":"1552669153","thumb":"https://www.shanqu8.com/upload/1/images/gallery/4/w/177_src.jpg","mid":"5476","pay_time":"0","market_price":"1199.00","price":"135.30","mid_count":"50","mid_coin_count":"0","username":"竭斯底里的哭泣","returns_score":"0","safe_price":"134.50","safe_rate":"0","safe_end":"0","photo":"https://www.shanqu8.com/upload/1/images/photo/5476_src.jpg"},{"id":"2657037","title":"维达(Vinda) 手帕纸","auction_id":"95","c_time":"1552669153","thumb":"https://www.shanqu8.com/upload/1/images/gallery/f/3/544_src.jpg","mid":"27475","pay_time":"0","market_price":"19.90","price":"0.90","mid_count":"2","mid_coin_count":"0","username":"v别老发光浪费电","returns_score":"0","safe_price":"0.30","safe_rate":"0","safe_end":"0","photo":"https://www.shanqu8.com/upload/1/images/photo/27475_src.jpg"},{"id":"2657036","title":"溜溜梅 4袋组合452g 清梅*2原西梅雪梅组合","auction_id":"121","c_time":"1552669153","thumb":"https://www.shanqu8.com/upload/1/images/gallery/i/8/657_src.jpg","mid":"1470","pay_time":"0","market_price":"62.00","price":"1.00","mid_count":"2","mid_coin_count":"0","username":"节操是啥能吃么","returns_score":"0","safe_price":"0.60","safe_rate":"0","safe_end":"0","photo":"https://www.shanqu8.com/upload/1/images/photo/1470_src.jpg"},{"id":"2657035","title":"良品铺子 袋袋如意高端礼 盒  2282g ","auction_id":"217","c_time":"1552669138","thumb":"https://www.shanqu8.com/upload/1/images/gallery/d/c/13441_src.jpg","mid":"11567","pay_time":"0","market_price":"168.00","price":"16.40","mid_count":"7","mid_coin_count":"0","username":"向钱冲","returns_score":"0","safe_price":"15.70","safe_rate":"0","safe_end":"0","photo":"https://www.shanqu8.com/upload/1/images/photo/11567_src.jpg"},{"id":"2657034","title":"绿联无线充电器  苹果三星通用型","auction_id":"44","c_time":"1552669138","thumb":"https://www.shanqu8.com/upload/1/images/gallery/8/j/308_src.jpg","mid":"9650","pay_time":"0","market_price":"138.00","price":"12.70","mid_count":"10","mid_coin_count":"0","username":"乔丹","returns_score":"0","safe_price":"12.30","safe_rate":"0","safe_end":"0","photo":"https://www.shanqu8.com/upload/1/images/photo/9650_src.jpg"}]}
     * flag : true
     */

    public static class DataBean {
        public List<ListBean> list;

        public static class ListBean {
            /**
             * id : 2657038
             * title : 飞利浦（PHILIPS）电动剃须刀 荷兰进口S7310/12
             * auction_id : 21
             * c_time : 1552669153
             * thumb : https://www.shanqu8.com/upload/1/images/gallery/4/w/177_src.jpg
             * mid : 5476
             * pay_time : 0
             * market_price : 1199.00
             * price : 135.30
             * mid_count : 50
             * mid_coin_count : 0
             * username : 竭斯底里的哭泣
             * returns_score : 0
             * safe_price : 134.50
             * safe_rate : 0
             * safe_end : 0
             * photo : https://www.shanqu8.com/upload/1/images/photo/5476_src.jpg
             */

            public String id;
            public String title;
            public String auction_id;
            public String c_time;
            public String thumb;
            public String mid;
            public String pay_time;
            public String market_price;
            public String price;
            public String mid_count;
            public String mid_coin_count;
            public String username;
            public String returns_score;
            public String safe_price;
            public String safe_rate;
            public String safe_end;
            public String photo;
        }
    }
}
