package com.android.manghe.index.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.index.activity.ChaoZhiHaoWuActivity;
import com.android.manghe.index.model.HotBuyTimeLimitRes;

/**
 * 人气热卖和限时特惠
 */
public class HotBuyTimeLimitAdapter extends DelegateAdapter.Adapter<HotBuyTimeLimitAdapter.AdapterViewHolder> {

    private Context context;
    private HotBuyTimeLimitRes.DataBean dataBean;

    public HotBuyTimeLimitAdapter(Context context) {
        this.context = context;
    }

    public void updateData(HotBuyTimeLimitRes.DataBean dataBean) {
        this.dataBean = dataBean;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new HotBuyTimeLimitAdapter.AdapterViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_hot_buy_time_limit, viewGroup, false));
    }

    @Override
    public int getItemViewType(int position) {
        return 3;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder holder, int position) {
        holder.layoutHole.setVisibility(dataBean == null ? View.GONE : View.VISIBLE);
        if (dataBean != null) {
            try {
                if (dataBean.hot_goods != null && !dataBean.hot_goods.isEmpty()) {
                    GlideHelper.load(context, dataBean.hot_goods.get(0).thumb, R.drawable.default_hot, holder.ivLeftTop1Pic);
                    holder.tvLeftTop1Price.setText("￥"+dataBean.hot_goods.get(0).price);
                    GlideHelper.load(context, dataBean.hot_goods.get(1).thumb, R.drawable.default_hot, holder.ivLeftTop2Pic);
                    holder.tvLeftTop2Price.setText("￥"+dataBean.hot_goods.get(1).price);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (dataBean.xxth_goods != null && !dataBean.xxth_goods.isEmpty()) {
                    GlideHelper.load(context, dataBean.xxth_goods.get(0).thumb, R.drawable.default_hot, holder.ivRightTop1Pic);
                    GlideHelper.load(context, dataBean.xxth_goods.get(1).thumb, R.drawable.default_hot, holder.ivRightTop2Pic);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                GlideHelper.load(context, dataBean.intel_goods != null && !dataBean.intel_goods.isEmpty() ? dataBean.intel_goods.get(0).thumb : null, R.drawable.default_hot, holder.ivLeftBottomPic);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                GlideHelper.load(context, dataBean.hot_goods.get(0).thumb, R.drawable.default_hot, holder.ivRightBottomPic);
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.itemView.findViewById(R.id.layoutTopLeft).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChaoZhiHaoWuActivity.class);
                    intent.putExtra("id", "");
                    context.startActivity(intent);
                }
            });
            holder.itemView.findViewById(R.id.layoutTopRight).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChaoZhiHaoWuActivity.class);
                    intent.putExtra("id", "137");
                    context.startActivity(intent);
                }
            });
            holder.itemView.findViewById(R.id.layoutBottomLeft).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChaoZhiHaoWuActivity.class);
                    intent.putExtra("id", "151");
                    context.startActivity(intent);
                }
            });
            holder.itemView.findViewById(R.id.layoutBottomRight).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChaoZhiHaoWuActivity.class);
                    intent.putExtra("id", "146");
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        return new LinearLayoutHelper();
    }

    class AdapterViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivLeftTop1Pic, ivLeftTop2Pic, ivRightTop1Pic, ivRightTop2Pic, ivLeftBottomPic, ivRightBottomPic;
        public TextView tvLeftTop1Price, tvLeftTop2Price;
        public View layoutHole;

        public AdapterViewHolder(View itemView) {
            super(itemView);
            layoutHole = itemView.findViewById(R.id.layoutHole);
            ivLeftTop1Pic = itemView.findViewById(R.id.ivLeftTop1Pic);
            ivLeftTop2Pic = itemView.findViewById(R.id.ivLeftTop2Pic);
            ivRightTop1Pic = itemView.findViewById(R.id.ivRightTop1Pic);
            ivRightTop2Pic = itemView.findViewById(R.id.ivRightTop2Pic);
            ivLeftBottomPic = itemView.findViewById(R.id.ivLeftBottomPic);
            ivRightBottomPic = itemView.findViewById(R.id.ivRightBottomPic);
            tvLeftTop1Price = itemView.findViewById(R.id.tvLeftTop1Price);
            tvLeftTop2Price = itemView.findViewById(R.id.tvLeftTop2Price);
        }
    }

}
