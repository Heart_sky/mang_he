package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class HotBuyingBean extends NetBean<List<HotBuyingBean>> {

    public List<String> photo_list;
    public String title;
    public String hour;
    public String minute;
    public String second;
    public String currentPrice;
    public String oldPrice;
}
