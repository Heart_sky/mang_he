package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class CollectionRes extends NetBean<CollectionRes.DataBean> {


    public static class DataBean {
        /**
         * list_total : 6
         * list : [{"id":"268","thumb":"https://www.ycdy8.com/upload/1/images/gallery/g/d/13550_thumb.jpg","title":"一加 OnePlus 7 Pro  8GB+256GB 流体屏 颜色随机","market_price":"4499.00","success_m_num":"40","avg":"152.927869"},{"id":"238","thumb":"https://www.ycdy8.com/upload/1/images/gallery/c/3/13396_thumb.jpg","title":" iPhone XS  64GB 全网通 颜色随机","market_price":"7549.00","success_m_num":"25","avg":"366.893750"},{"id":"231","thumb":"https://www.ycdy8.com/upload/1/images/gallery/b/5/13362_thumb.jpg","title":"Apple iPhone XS Max 256GB 全网通 双卡双待 颜色随机","market_price":"9199.00","success_m_num":"35","avg":"518.502439"},{"id":"229","thumb":"https://www.ycdy8.com/upload/1/images/gallery/a/e/13335_thumb.png","title":"Apple  iPhone X 256G 全网通 颜色随机","market_price":"7436.00","success_m_num":"38","avg":"414.708889"},{"id":"228","thumb":"https://www.ycdy8.com/upload/1/images/gallery/a/a/13331_thumb.jpg","title":"大疆（DJI） 御mavic2 pro/zoom专业变焦版无人机可折叠航拍","market_price":"13186.00","success_m_num":"18","avg":"706.890909"},{"id":"227","thumb":"https://www.ycdy8.com/upload/1/images/gallery/a/2/13323_thumb.jpg","title":"Apple iPhone X (A1865) 64GB 全网通  颜色随机","market_price":"5999.00","success_m_num":"34","avg":"296.084615"}]
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean {
            /**
             * id : 268
             * thumb : https://www.ycdy8.com/upload/1/images/gallery/g/d/13550_thumb.jpg
             * title : 一加 OnePlus 7 Pro  8GB+256GB 流体屏 颜色随机
             * market_price : 4499.00
             * success_m_num : 40
             * avg : 152.927869
             */

            public String id;
            public String thumb;
            public String title;
            public String market_price;
            public String success_m_num;
            public String avg;
        }
    }
}
