package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class HotBuyTimeLimitRes extends NetBean<HotBuyTimeLimitRes.DataBean> {



    public static class DataBean {
        public List<GoodInfo> hot_goods;
        public List<GoodInfo> xxth_goods;
        public List<GoodInfo> intel_goods;
        public List<GoodInfo> food_goods;

    }

    public static class GoodInfo{
        public String id;
        public String name;
        public String price;
        public String score;
        public String end_time;
        public String team_num;
        public String team_price;
        public int run_time;
        public String thumb;
        public String manghe_cat_id;//盲盒id
    }
}
