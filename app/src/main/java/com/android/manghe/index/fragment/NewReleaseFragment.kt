package com.android.manghe.index.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.title.ETitleType
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.common.frame.fragment.MVPWebSocketFragment
import com.android.manghe.common.model.AuctionSocketRes
import com.android.manghe.index.adapter.NewsReleaseAuctionAdapter
import com.android.manghe.index.presenter.PNewRelease
import com.zhangke.websocket.ErrorResponse
import com.zhangke.websocket.Response
import kotlinx.android.synthetic.main.fragment_news_release.*
import java.util.*

/**
 *新品首发
 */
class NewReleaseFragment : MVPWebSocketFragment<PNewRelease>() {

    private var mAdapter: NewsReleaseAuctionAdapter? = null

    override fun getLayoutId() = R.layout.fragment_news_release
    override fun showToolBarType() = ETitleType.NO_TITLE
    override fun initData(savedInstanceState: Bundle?, parent: View) {
        initListView()
        p.loadGridData()
    }

    private fun initListView() {
        recyclerView!!.layoutManager =
            LinearLayoutManager(activity)
        recyclerView.addItemDecoration(RecyclerViewDivider(activity, LinearLayoutManager.VERTICAL))
        recyclerView.itemAnimator = null
        mAdapter = NewsReleaseAuctionAdapter(
            activity,
            ArrayList<AuctionSocketRes.ListBean>()
        )
        recyclerView.adapter = mAdapter
    }

    override fun onMessageResponse(message: Response<*>) {
        p.dealSocketData(message.responseText)
    }

    override fun onSendMessageError(error: ErrorResponse) {
    }

    fun updateData(dataList: List<AuctionSocketRes.ListBean>) {
        mAdapter?.update(dataList)
    }

    override fun onDestroy() {
        super.onDestroy()
        p.stopSocketTimer()
    }
}
