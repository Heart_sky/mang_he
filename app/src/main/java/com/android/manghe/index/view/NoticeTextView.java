package com.android.manghe.index.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.android.manghe.index.model.OldNoticeBean;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class NoticeTextView extends AppCompatTextView {
    private int mDuration; //文字从出现到显示消失的时间
    private int mInterval; //文字停留在中间的时长切换的间隔
    private List<OldNoticeBean> mNoticeList; //显示文字的数据源
    private int mY = 0; //文字的Y坐标
    private int mIndex = 0; //当前的数据下标
    private boolean isMove = true; //文字是否移动
    private boolean hasInit = false;
    private Paint paint = new Paint();

    public interface IClickNoticeItem {
        void onClick(OldNoticeBean oldNoticeBean);
    }

    private IClickNoticeItem onClickListener;

    public void setOnClickListener(NoticeTextView.IClickNoticeItem onClickListener) {
        this.onClickListener = onClickListener;
    }

    public NoticeTextView(Context context) {
        this(context, null);
    }

    public NoticeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (onClickListener != null) {
                    onClickListener.onClick(mNoticeList.get(mIndex));
                }

                break;
        }
        return true;
    }

    //设置数据源
    public void setNoticeList(List<OldNoticeBean> noticeList) {
        this.mNoticeList = noticeList;
    }

    //初始化默认值
    private void init() {
        mDuration = 500;
        mInterval = 1000;
        mIndex = 0;

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
//        paint.setTextSize(mSize);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
//        Log.i(TAG, "onSizeChanged: " + h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mNoticeList != null) {
            String text = getText().toString();
            //绘制前缀
            Rect indexBound = new Rect();
            paint.getTextBounds(" ", 0, 1, indexBound);
            canvas.drawText(" ", 0, 1, (indexBound.right - indexBound.left) + 20, mY, paint);

            if (mY == 0 && hasInit == false) {
                mY = getMeasuredHeight() - indexBound.top;
                hasInit = true;
            }
            //移动到最上面
            if (mY == 0 - indexBound.bottom) {
                mY = getMeasuredHeight() - indexBound.top;
                mIndex++;
            }
            //移动到中间
            if (mY == getMeasuredHeight() / 2 - (indexBound.top + indexBound.bottom) / 2) {
                isMove = false;
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        postInvalidate();
                        isMove = true;
                    }
                }, mInterval);
            }
            mY -= 1;
            //循环使用数据
            if (mIndex == mNoticeList.size()) {
                mIndex = 0;
            }
            //如果是处于移动状态时的,则延迟绘制
            //计算公式为一个比例,一个时间间隔移动组件高度,则多少毫秒来移动1像素
            if (isMove) {
                postInvalidateDelayed(mDuration / getMeasuredHeight());
            }
        }

    }
}
