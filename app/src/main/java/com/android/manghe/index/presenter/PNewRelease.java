package com.android.manghe.index.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.tools.EmptyUtil;
import com.android.manghe.index.fragment.NewReleaseFragment;
import com.android.manghe.common.model.AuctionSocketReq;
import com.android.manghe.common.model.AuctionSocketRes;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PNewRelease extends XPresenter<NewReleaseFragment> {

    private Timer timer;
    public void loadGridData() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    AuctionSocketReq auction = new AuctionSocketReq();
                    auction.type = "get_new_auctions";

                    auction.page = 1;
                    auction.per_page = 30;
                    auction.cid = 140;//新品首发ID=140
                    String sendJson = new Gson().toJson(auction);
                    getV().sendText(sendJson);
                }
            }, 1000, 1000);
        }
    }

    public void stopSocketTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void dealSocketData(String json) {
        if (!EmptyUtil.check(json)) {
            if (json.contains("get_new_auctions")) {
                AuctionSocketRes socketRes = new Gson().fromJson(json, AuctionSocketRes.class);
                if (!EmptyUtil.check(socketRes)) {
                    getV().updateData(socketRes.list != null ? socketRes.list : new ArrayList<>());
                }
            }
        }
    }

}
