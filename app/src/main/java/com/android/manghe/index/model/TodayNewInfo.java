package com.android.manghe.index.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class TodayNewInfo extends NetBean<List<TodayNewInfo>> {
    public String id;
    public String name;
    public String price;
    public String score;
    public String end_time;
    public String team_num;
    public String team_price;
    public int run_time;
    public String thumb;
    public int favourable_score;
    public String manghe_cat_id;//盲盒id
}
