package com.android.manghe.index.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.common.model.AuctionSocketRes;
import com.android.manghe.detail.activity.GoodDetailActivity;

import java.util.List;

public class NewsReleaseAuctionAdapter  extends RecyclerView.Adapter<NewsReleaseAuctionHolder> {
    private Context mContext;

    private AsyncListDiffer<AuctionSocketRes.ListBean> mDiffer;

    private DiffUtil.ItemCallback<AuctionSocketRes.ListBean> diffCallback = new DiffUtil.ItemCallback<AuctionSocketRes.ListBean>() {
        @Override
        public boolean areItemsTheSame(AuctionSocketRes.ListBean oldItem, AuctionSocketRes.ListBean newItem) {
            return TextUtils.equals(oldItem.id, newItem.id);
        }

        @Override
        public boolean areContentsTheSame(AuctionSocketRes.ListBean oldItem, AuctionSocketRes.ListBean newItem) {
            return oldItem.time == newItem.time;
        }
    };

    public AuctionSocketRes.ListBean getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }


    public NewsReleaseAuctionAdapter(Context context, List<AuctionSocketRes.ListBean> list) {
        mContext = context;
        mDiffer = new AsyncListDiffer<>(this, diffCallback);
    }

    public void update(List<AuctionSocketRes.ListBean> list) {
        mDiffer.submitList(list);
    }

    @NonNull
    @Override
    public NewsReleaseAuctionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_release_auction, parent, false);
        return new NewsReleaseAuctionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsReleaseAuctionHolder holder, int position) {
        AuctionSocketRes.ListBean item = getItem(position);

        GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.ivPic);
        holder.tvCurrentPrice.setText("￥" + item.getPrice());
        holder.tvTitle.setText( item.title);
        holder.tvGotAccount.setText("");
        setCountDown(holder.tvTime, item.time);

        holder.layoutHole.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GoodDetailActivity.class);
                intent.putExtra("auctionId", item.id);
                mContext.startActivity(intent);
            }
        });
    }

    private void setCountDown(TextView tvTime, int time) {
        int day = 60 * 60 * 24;
        int hour = 60 * 60;
        int min = 60;
        double d1 = time * 1.0 / day;
        if (time < 0) {
            time = 0;
        }
        if (d1 > 1) {
            int dayNum = time / day;
            int hourNum = time / hour % 24;
            int minuteNum = time / min % 60;
            tvTime.setText(dayNum + "天 " + hourNum + "时 " + minuteNum + "分");
        } else {
            String hour1 = String.format("%02d", time / 3600);
            String minute1 = String.format("%02d", time % 3600 / 60);
            String second1 = String.format("%02d", time % 60);
            tvTime.setText(hour1 + "：" + minute1 + "：" + second1);
        }
    }
}

class NewsReleaseAuctionHolder extends RecyclerView.ViewHolder {
    public ImageView ivPic;
    public TextView tvCurrentPrice,tvTitle, tvTime,tvGotAccount;
    public View layoutHole;

    public NewsReleaseAuctionHolder(View itemView) {
        super(itemView);
        ivPic = itemView.findViewById(R.id.ivPic);
        tvCurrentPrice = itemView.findViewById(R.id.tvCurrentPrice);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvGotAccount = itemView.findViewById(R.id.tvGotAccount);
        tvTime = itemView.findViewById(R.id.tvTime);
        layoutHole = itemView.findViewById(R.id.layoutHole);
    }
}
