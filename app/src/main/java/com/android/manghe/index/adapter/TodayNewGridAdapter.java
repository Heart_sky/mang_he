package com.android.manghe.index.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.index.model.TodayNewInfo;
import com.android.manghe.market.activity.BoxListActivity;
import com.android.manghe.user.activity.LoginActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class TodayNewGridAdapter extends BaseQuickAdapter<TodayNewInfo, BaseViewHolder> {
    private Context mContext;
    private boolean isFromCart;
    private View emptyView;

    public TodayNewGridAdapter(Context context, boolean isFromCart, List<TodayNewInfo> list) {
        super(R.layout.item_market_grid_auction, list);
        mContext = context;
        this.isFromCart = isFromCart;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void update(List<TodayNewInfo> list) {
//        super.update(list);
        replaceData(list);
    }

    @Override
    protected void convert(BaseViewHolder holder, TodayNewInfo item) {
        GlideHelper.loadWithHolderErr(mContext, isFromCart ? item.thumb : item.thumb, holder.getView(R.id.ivPic));
        holder.setText(R.id.tvCurrentPrice, "￥" + item.price);
        holder.setText(R.id.tvTitle, item.name);
//        holder.setGone(R.id.tvDiscount, Float.parseFloat(item.price) != 0);
//        holder.setText(R.id.tvDiscount, "赠" + item.favourable_score + "积分");

        holder.setOnClickListener(R.id.layoutContent, v -> {
            if (UserHolder.getUserInfo(mContext) != null) {
                Intent intent = new Intent(mContext, BoxListActivity.class);
                intent.putExtra("mangheCatId", item.manghe_cat_id);
                mContext.startActivity(intent);
            } else {
                mContext.startActivity(new Intent(mContext, LoginActivity.class));
            }
        });
    }
}
