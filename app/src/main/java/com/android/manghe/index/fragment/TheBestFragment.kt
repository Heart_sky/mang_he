package com.android.manghe.index.fragment

import android.os.Bundle
import android.view.View
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.VirtualLayoutManager
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.manghe.R
import com.android.manghe.index.adapter.TheBestBottomAdapter
import com.android.manghe.index.adapter.TheBestTopAdapter
import com.android.manghe.index.model.TheBestRes
import com.android.manghe.index.presenter.PTheBest
import kotlinx.android.synthetic.main.fragment_the_best.*
import java.util.*

/**
 * 本期大神
 */
class TheBestFragment : MVPFragment<PTheBest>() {

    private var mTheBestTopAdapter: TheBestTopAdapter? = null
    private var mTheBestBottomAdapter: TheBestBottomAdapter? = null

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_the_best
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

        val layoutManager = VirtualLayoutManager(activity!!.applicationContext)
        recyclerView.layoutManager = layoutManager

        val delegateAdapter = DelegateAdapter(layoutManager, true)
        recyclerView.adapter = delegateAdapter
        delegateAdapter.setAdapters(getAdapters())
        showLoadingDialog()
        p.getData(-1)
    }

    private fun getAdapters(): List<DelegateAdapter.Adapter<*>> {
        val adapters = LinkedList<DelegateAdapter.Adapter<*>>()

        mTheBestTopAdapter = TheBestTopAdapter(activity)
        adapters.add(mTheBestTopAdapter!!)

        mTheBestBottomAdapter = TheBestBottomAdapter(activity, TheBestBottomAdapter.ITheBestBottomListener { dangqiId ->
            showLoadingDialog()
            p.getData(dangqiId)
        })
        adapters.add(mTheBestBottomAdapter!!)

        return adapters
    }

    fun updateTopView(data: TheBestRes.DataBean) {
        mTheBestTopAdapter?.updateData(data)
    }

    fun updateBottomView(data: TheBestRes.DataBean) {
        data.dangqi?.list?.let {
            mTheBestBottomAdapter?.updateData(if (it.size > 6) it.subList(0, 6) else it)
        }
    }
}