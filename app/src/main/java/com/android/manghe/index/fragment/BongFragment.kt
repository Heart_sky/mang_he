package com.android.manghe.index.fragment

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.detail.activity.GoodDetailActivity
import com.android.manghe.index.adapter.BongAdapter
import com.android.manghe.index.model.BongInfo
import com.android.manghe.index.presenter.PBong
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_bong.*

/**
 * 本周爆品
 */
class BongFragment : MVPFragment<PBong>(), IRefresh {

    private var mAdapter: BongAdapter? = null

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }
    override fun getLayoutId(): Int {
        return R.layout.fragment_bong
    }
    override fun initData(savedInstanceState: Bundle?, parent: View?) {
        mAdapter = BongAdapter(activity, arrayListOf())
        val linearManager =
            LinearLayoutManager(activity)
        recyclerView.layoutManager = linearManager
        recyclerView.addItemDecoration(RecyclerViewDivider(mContext, LinearLayoutManager.VERTICAL))
        recyclerView.adapter = mAdapter
        mAdapter!!.setOnItemClickListener{
            _,item->
            val intent = Intent(mContext, GoodDetailActivity::class.java)
            intent.putExtra("auctionId", item.auction_id)
            mContext.startActivity(intent)
        }
        p.getData()
    }



    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getData()
    }

    override fun onLoad() {

    }

    fun update(bongList: List<BongInfo>){
        mAdapter?.let {
            it.update(bongList)
        }
    }

}