package com.android.manghe.box.presenter;

import android.view.View;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.box.activity.BoxActivity;
import com.android.manghe.box.model.BoxPrizeListModel;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/16 15:33
 * desc   :
 */
public class PBox2 extends XPresenter<BoxActivity> {

    private int currentPage = 1;
    private final String PageSize = "10";
    private final List<BoxPrizeListModel.DataBean.ListBean> prizeList = new ArrayList<>();

    public void getPrizeList(boolean isRefresh) {
        //获取精选
        if (isRefresh) {
            currentPage = 1;
            prizeList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_GET_ALL_PRIZE, data, headMap)
                .get()
                .execute(new BaseBack<BoxPrizeListModel>() {
                    @Override
                    public void onSuccess(Call call, BoxPrizeListModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                prizeList.addAll(res.data.list);
                                getV().setCanLoadMore(prizeList.size() != res.data.list_total);
                                getV().updateList(prizeList);
                                if (prizeList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateList(prizeList);
                            }
                        } else {
                            getV().showErrorTip();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showErrorTip();
                    }

                });
    }


    public void getExchangePrize(List<String> list, int exchange_way, String address_id) {
        HashMap<String, String> data = new HashMap<>();
        String prizeIds = "";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                prizeIds = list.get(i);
            } else {
                prizeIds = prizeIds + "," + list.get(i);
            }
        }
        data.put("prize_ids", prizeIds);
        data.put("exchange_way", exchange_way + "");
        data.put("address_id", address_id + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_EXCHANGE_PRIZE, headMap)
                .post(data).build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            getV().updateExchangePrize();
                        } else {
                            getV().showExchangeErrorTip();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().showExchangeErrorTip();
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }



    public void getReplaceRule(String url, View view) {
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + url)
                .get()
                .execute(new GsonBaseBack() {
                    @Override
                    public void onSuccess(Call call, String json) {
                        getV().hideLoadingDialog();
                        if (json != null && !json.isEmpty()) {
                            try {
                                String desc = new JSONObject(json).getString("data");
                                getV().showReplaceRuleWindow(desc, view);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }
                });
    }


    public void getIntegralRule(String url, View view) {
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + url)
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        getV().hideLoadingDialog();
                        if (json != null && !json.isEmpty()) {
                            try {
                                String desc = new JSONObject(json).getString("data");
                                getV().showIntegralWindow(desc, view);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }
                });
    }

    public void getCoinRule(String url, View view) {
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + url)
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        getV().hideLoadingDialog();
                        if (json != null && !json.isEmpty()) {
                            try {
                                String desc = new JSONObject(json).getString("data");
                                getV().showCoinWindow(desc, view);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }
                });
    }
}
