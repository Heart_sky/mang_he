package com.android.manghe.box.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.box.presenter.PBoxGoodDetail;
import com.android.manghe.market.model.MarketGoodDetailRes;
import com.android.manghe.mine.adapter.IntegralUltraPagerAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.squareup.picasso.Picasso;
import com.tmall.ultraviewpager.UltraViewPager;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/10 09:56
 * desc   :  商品详情
 */
public class BoxGoodDetailActivity extends MVPActivity<PBoxGoodDetail> implements IRefresh {

    private UltraViewPager mUltraViewPager;
    private TextView  mTvTitle;
    private BoxModel.DataBean.GoodsListBean mCurrentListBean;
    private String mGoodId;
    private IntegralUltraPagerAdapter mAdapter;
    private LinearLayout mLayoutDetailPics;
    private TextView mTvNoDetail;


    @Override
    public int getLayoutId() {
        return R.layout.activity_box_good_detail;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("商品详情").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().
                setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mUltraViewPager = findViewById(R.id.id_ultraViewPager);
        mTvTitle = findViewById(R.id.tvTitle);
        mLayoutDetailPics = findViewById(R.id.layoutDetailPics);
        mTvNoDetail = findViewById(R.id.tvNoDetail);

        mCurrentListBean = (BoxModel.DataBean.GoodsListBean) getIntent().getSerializableExtra("BoxModel");
        if (null != mCurrentListBean) {
            mTvTitle.setText(mCurrentListBean.name);
            mGoodId = mCurrentListBean.id;
            getP().loadData(mGoodId);
        }
    }

    public void updateView(MarketGoodDetailRes.DataBean data) {
        mAdapter = new IntegralUltraPagerAdapter(mContext, data.thumbs);
        mUltraViewPager.setAdapter(mAdapter);
        if (data.thumbs.size() > 1) {
            mUltraViewPager.initIndicator()
                    .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                    .setFocusColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                    .setNormalColor(ContextCompat.getColor(mContext, R.color.backgroundGray))
                    .setRadius(DisplayUtil.dip2px(mContext, 6f))
                    .setMargin(0, 0, 0, DisplayUtil.dip2px(mContext, 10f))
                    .setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)
                    .build();
            mUltraViewPager.setInfiniteLoop(true);
            mUltraViewPager.setAutoScroll(5000);
        } else {
            mUltraViewPager.disableIndicator();
            mUltraViewPager.setInfiniteLoop(false);
        }

        showWeb(data.content);

    }

    private void showWeb(String htmlString) {
        if (TextUtils.isEmpty(htmlString)) {
            mTvNoDetail.setVisibility(View.VISIBLE);
            return;
        }
        List<String> picUrlList = getP().htmlConvertPicList(htmlString);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (picUrlList.size() > 0) {
            mLayoutDetailPics.removeAllViews();
            for (String str : picUrlList) {
                ImageView imageView = new ImageView(BoxGoodDetailActivity.this);
                imageView.setAdjustViewBounds(true);
                imageView.setLayoutParams(lp);
                mLayoutDetailPics.addView(imageView);

                Picasso.get().load(str)
                        .fit()
                        .into(imageView);
                GlideHelper.load(BoxGoodDetailActivity.this, str, imageView);
            }

        }
    }



    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        getP().loadData(mGoodId);
    }

    @Override
    public void onLoad() {
    }
}
