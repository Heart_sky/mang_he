package com.android.manghe.box.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.box.activity.RuleActivity;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.mine.model.UserRes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/05 15:17
 * desc   :
 */
public class PRule extends XPresenter<RuleActivity> {


    public void getRule(String url){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        UserRes.UserInfo userInfo = UserHolder.getUserInfo(getV());
        if(userInfo != null) {
            headMap.put("UID", UserHolder.getUID(getV()));
            headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        }
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(url, headMap)
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        getV().hideLoadingDialog();
                        if (json != null && !json.isEmpty()) {
                            try {
                                String desc = new JSONObject(json).getString("data");
                                getV().showText(desc);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }
                });
    }
}
