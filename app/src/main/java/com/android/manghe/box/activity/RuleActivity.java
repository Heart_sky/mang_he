package com.android.manghe.box.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;
import com.android.manghe.box.presenter.PRule;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/05 15:16
 * desc   : 活动规则
 */
public class RuleActivity extends MVPActivity<PRule> {

    private TextView tvContent;
    private String mTitle;
    private String url;


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        mTitle=getIntent().getStringExtra("title");
        url=getIntent().getStringExtra("url");
        getTitleBar().setToolbar(mTitle).setLeftIcon(R.mipmap.icon_back_black).setBackgroundColor(ContextCompat.getColor(this,R.color.white));
        tvContent = findViewById(R.id.tvContent);
        getP().getRule(url);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_article;
    }

    public void showText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvContent.setText(Html.fromHtml(text));
        }
    }
}
