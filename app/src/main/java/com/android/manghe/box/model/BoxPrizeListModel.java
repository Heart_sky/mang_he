package com.android.manghe.box.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/25 15:40
 * desc   :
 */
public class BoxPrizeListModel extends NetBean<BoxPrizeListModel.DataBean> {


    /**
     * data : {"list":[{"id":"30","manghe_cat_id":"3","goods_id":"0","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":null,"goods_price":null,"exchange_way":"暂未兑换"},{"id":"29","manghe_cat_id":"3","goods_id":"2128","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"大宇（DAEWOO）挂烫机家用手持蒸汽熨烫机熨衣机旅游出差便携式熨斗HI-029-ZI","goods_price":"479.00","exchange_way":"暂未兑换"},{"id":"28","manghe_cat_id":"3","goods_id":"2129","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"美的(Midea)606升变频一级能效对开双门家用冰箱智能家电风冷无霜BCD-606WKPZM(E)大容量精细分储","goods_price":"3299.00","exchange_way":"暂未兑换"},{"id":"27","manghe_cat_id":"3","goods_id":"2130","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"智能扫地机器人低噪音懒人家用智能全自动三合一扫地机拖地机吸尘器小家电礼品免洗超薄智能一体机吸尘器莱信 扫吸拖三合一250s+千帕吸力+带拖地（纯白）","goods_price":"119.00","exchange_way":"暂未兑换"},{"id":"26","manghe_cat_id":"3","goods_id":"2131","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"小米（MI） 小米青萍多功能蓝牙网关接入米家门锁智能家居设备远程控制网关家电 青萍多功能蓝牙网关","goods_price":"69.00","exchange_way":"暂未兑换"},{"id":"25","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637740629","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"43","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"},{"id":"24","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637740260","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"42","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"},{"id":"23","manghe_cat_id":"1","goods_id":"2116","mid":"911730","created_at":"1637739513","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"41","goods_name":"Apple AirTag (单件装) 追踪器 适用于 iPhone iPad","goods_price":"229.00","exchange_way":"暂未兑换"},{"id":"22","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637738259","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"40","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"},{"id":"21","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637737849","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"39","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"}],"list_total":20}
     * flag : true
     */
    public static class DataBean implements Serializable {
        /**
         * list : [{"id":"30","manghe_cat_id":"3","goods_id":"0","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":null,"goods_price":null,"exchange_way":"暂未兑换"},{"id":"29","manghe_cat_id":"3","goods_id":"2128","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"大宇（DAEWOO）挂烫机家用手持蒸汽熨烫机熨衣机旅游出差便携式熨斗HI-029-ZI","goods_price":"479.00","exchange_way":"暂未兑换"},{"id":"28","manghe_cat_id":"3","goods_id":"2129","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"美的(Midea)606升变频一级能效对开双门家用冰箱智能家电风冷无霜BCD-606WKPZM(E)大容量精细分储","goods_price":"3299.00","exchange_way":"暂未兑换"},{"id":"27","manghe_cat_id":"3","goods_id":"2130","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"智能扫地机器人低噪音懒人家用智能全自动三合一扫地机拖地机吸尘器小家电礼品免洗超薄智能一体机吸尘器莱信 扫吸拖三合一250s+千帕吸力+带拖地（纯白）","goods_price":"119.00","exchange_way":"暂未兑换"},{"id":"26","manghe_cat_id":"3","goods_id":"2131","mid":"911730","created_at":"1637740902","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"44","goods_name":"小米（MI） 小米青萍多功能蓝牙网关接入米家门锁智能家居设备远程控制网关家电 青萍多功能蓝牙网关","goods_price":"69.00","exchange_way":"暂未兑换"},{"id":"25","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637740629","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"43","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"},{"id":"24","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637740260","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"42","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"},{"id":"23","manghe_cat_id":"1","goods_id":"2116","mid":"911730","created_at":"1637739513","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"41","goods_name":"Apple AirTag (单件装) 追踪器 适用于 iPhone iPad","goods_price":"229.00","exchange_way":"暂未兑换"},{"id":"22","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637738259","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"40","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"},{"id":"21","manghe_cat_id":"4","goods_id":"2132","mid":"911730","created_at":"1637737849","receive_way":"0","exchange_id":null,"total":"1.00","manghe_order_id":"39","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_price":"49.00","exchange_way":"暂未兑换"}]
         * list_total : 20
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean implements Serializable {
            /**
             * id : 30
             * manghe_cat_id : 3
             * goods_id : 0
             * mid : 911730
             * created_at : 1637740902
             * receive_way : 0
             * exchange_id : null
             * total : 1.00
             * manghe_order_id : 44
             * goods_name : null
             * goods_price : null
             * exchange_way : 暂未兑换
             */
            public String id;
            public String manghe_cat_id;
            public String goods_id;
            public String mid;
            public String created_at;
            public String receive_way;
            public Object exchange_id;
            public String total;
            public String manghe_order_id;
            public String goods_name;
            public String goods_price;
            public String exchange_way;
            public double may_exchange_coin;//置换多少盲盒币
            public double may_exchange_score;//置换多少积分
            public String goods_pic;
            public String probability_level;
        }
    }
}
