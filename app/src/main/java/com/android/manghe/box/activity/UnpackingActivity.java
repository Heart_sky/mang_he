package com.android.manghe.box.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.frame.title.ETitleType;
import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.tools.ToastUtil;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.box.presenter.PUnpacking;
import com.android.manghe.market.model.BoxTestOpenModel;
import com.android.manghe.utils.WindowUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/01 10:40
 * desc   : 测试正在开箱
 */
public class UnpackingActivity extends MVPActivity<PUnpacking> {

    private ImageView mImgGift, mImgTryGift, mImgBg;
    private BoxModel.DataBean mCurrentListBean;
    private View mView;
    private TextView mTvName, mTvOk, mTvPrice;

    private MediaPlayer mediaPlayer;
    private RoundLinearLayout mLinearLayout;
    private ImageView mImgLevel;
    private TextView mTvLevel;

    private BoxModel.DataBean mCurrentBox;
    private AnimatorSet animatorSet;


    @Override
    protected ETitleType showToolBarType() {
        return ETitleType.NO_TITLE;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        WindowUtils.hideStatusBar(this);
        //VoicePlayer.getInstance().init(this);
        mImgGift = findViewById(R.id.id_img_gift);
        mCurrentBox = (BoxModel.DataBean) getIntent().getSerializableExtra("BoxListModel");
        mImgBg = findViewById(R.id.id_img_bg);
        mImgBg.setScaleX(2.5f);
        mImgBg.setScaleY(2.5f);
        animatorSet = new AnimatorSet();
        Glide.with(this)
                .load(mCurrentBox.cover_pic)
                .crossFade()
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        mImgGift.setImageDrawable(resource);
                        startAnimator();
                    }
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView = findViewById(R.id.id_open_box_try);
                initView();
                starFreshenAnimation(mImgBg);
                mCurrentListBean = (BoxModel.DataBean) getIntent().getSerializableExtra("BoxListModel");
                if (mCurrentListBean != null) {
                    getP().getBoxTestOpen(mCurrentListBean.id);
                }
            }
        }, 100);

    }

    public void startPlay(int rawRes, boolean isLoop) {
        try {
            stopPlay();
            mediaPlayer = MediaPlayer.create(this, rawRes);
            mediaPlayer.setLooping(isLoop);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopPlay() {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        WindowUtils.hideStatusBar(this);
    }

    private void starFreshenAnimation(ImageView imageView) {
        Animation circle_anim = AnimationUtils.loadAnimation(this, R.anim.anim_round_rotate);
        LinearInterpolator interpolator = new LinearInterpolator();  //设置匀速旋转，在xml文件中设置会出现卡顿
        circle_anim.setInterpolator(interpolator);
        imageView.startAnimation(circle_anim);  //开始动画

    }


    private void initView() {
        mImgTryGift = mView.findViewById(R.id.id_img_real_gift);
        mTvName = mView.findViewById(R.id.id_tv_name);
        mTvPrice = mView.findViewById(R.id.id_tv_price_value);
        mTvOk = mView.findViewById(R.id.id_tv_ok);
        mLinearLayout = mView.findViewById(R.id.id_ly_level);
        mImgLevel = mView.findViewById(R.id.id_img_level);
        mTvLevel = mView.findViewById(R.id.id_tv_level);
        mTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlay();
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        stopPlay();
        mImgBg.clearAnimation();
        mImgGift.clearAnimation();
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopPlay();
        mImgBg.clearAnimation();
        mImgGift.clearAnimation();
    }

    /**
     * 开启动画
     */
    private void startAnimator() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(mImgGift, "scaleX", 1f, 1.2f, 1f);
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(mImgGift, "scaleY", 1f, 1.2f, 1f);
        animatorX.setRepeatCount(ValueAnimator.INFINITE);
        animatorY.setRepeatCount(ValueAnimator.INFINITE);
        animatorSet.setDuration(700);
        animatorSet.play(animatorX).with(animatorY);
        animatorSet.start();
        startPlay(R.raw.open_box_1, true);
    }

    public void updateDate(BoxTestOpenModel.DataBean model) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (UnpackingActivity.this.isFinishing()) {
                    return;
                }
                animatorSet.cancel();
                startPlay(R.raw.open_box_2, false);
                mView.setVisibility(View.VISIBLE);
                mImgGift.setVisibility(View.GONE);
                GlideHelper.loadWithHolderErr(mContext, model.thumb, mImgTryGift);
                mTvName.setText(model.goods_name);
                mTvPrice.setText("￥" + model.goods_price);
                WindowUtils.setBoxLevel(mContext, mLinearLayout, mImgLevel, mTvLevel, model.probability_level);
            }
        }, 2000);

    }

    public void setFailureTip(String context) {
        ToastUtil.showLong(this, context);
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_unpacking;
    }
}
