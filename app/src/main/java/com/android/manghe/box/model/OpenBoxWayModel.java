package com.android.manghe.box.model;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/06 11:30
 * desc   :
 */
public class OpenBoxWayModel {

    private String origin_price;
    private String discount;
    private String sell_price;
    private int num;
    private String label;

    public String getOrigin_price() {
        return origin_price;
    }

    public void setOrigin_price(String origin_price) {
        this.origin_price = origin_price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSell_price() {
        return sell_price;
    }

    public void setSell_price(String sell_price) {
        this.sell_price = sell_price;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
