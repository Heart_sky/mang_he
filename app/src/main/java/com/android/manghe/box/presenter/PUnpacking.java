package com.android.manghe.box.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.box.activity.UnpackingActivity;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.model.BoxTestOpenModel;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/01 10:42
 * desc   :
 */
public class PUnpacking  extends XPresenter<UnpackingActivity> {

    public void getBoxTestOpen(String manghe_id) {
        HashMap<String, String> data = new HashMap<>();
        data.put("manghe_id", manghe_id);
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.Box_Test_Open)
                .post(data).build()
                .execute(new BaseBack<BoxTestOpenModel>() {

                    @Override
                    public void onSuccess(Call call, BoxTestOpenModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.data != null) {
                            getV().updateDate(res.data);
                        }


                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().setFailureTip("网络错误,请点击礼物重试");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
