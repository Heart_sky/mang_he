package com.android.manghe.box.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/23 17:35
 * desc   :
 */

public class BoxGiftModel extends NetBean<BoxGiftModel.DataBean> {


    /**
     * data : {"list":[{"goods_id":"2132","goods_name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","goods_pic":"https: //www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png","prize_id":"21"}]}
     * flag : true
     */


    public static class DataBean {
        public List<ListBean> list;


    }

    public static class ListBean implements Serializable {
        /**
         * goods_id : 2132
         * goods_name : 正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜
         * goods_pic : https: //www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png
         * prize_id : 21
         */

        public String goods_id;
        public String goods_name;
        public String goods_pic;
        public String prize_id;
        public String goods_price;
        public String probability_level;
    }
}
