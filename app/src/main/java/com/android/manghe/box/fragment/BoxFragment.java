package com.android.manghe.box.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPFragment;
import com.android.base.tools.ToastUtil;
import com.android.manghe.R;
import com.android.manghe.box.adapter.BoxPrizeAdapter;
import com.android.manghe.box.adapter.ReplaceAdapter;
import com.android.manghe.box.model.BoxPrizeListModel;
import com.android.manghe.box.model.MessageEvent;
import com.android.manghe.box.presenter.PBox;
import com.android.manghe.config.Config;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshUserInfoEvent;
import com.android.manghe.market.activity.DeliverGoodsActivity;
import com.android.manghe.view.dialog.NewTipDialog;
import com.android.manghe.view.popWindow.ReplacePopWindow;
import com.android.manghe.view.popWindow.ReplaceRulePopWindow;
import com.eightbitlab.rxbus.Bus;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/20 17:22
 * desc   :
 */
public class BoxFragment extends MVPFragment<PBox> implements IRefresh {

    private BoxPrizeAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private TextView tvOriginalReplace, tvOriginalBuy;
    private ImageView mImgCheckAll;
    private boolean isGoodsCheck = true;
    private TextView mTvTotalPrice, mTvTotal;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_box;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState, View parent) {
        EventBus.getDefault().register(this);
        getTitleBar().setToolbar("盒柜").setTitleAndStatusBgColor(R.color.white)
                .hideBottomLine();
        mRecyclerView = parent.findViewById(R.id.recyclerView);
        tvOriginalReplace = parent.findViewById(R.id.tvOriginalReplace);
        tvOriginalBuy = parent.findViewById(R.id.tvOriginalBuy);
        mImgCheckAll = parent.findViewById(R.id.ivCheckAll);
        mTvTotalPrice = parent.findViewById(R.id.tvTotalPrice);
        mTvTotal = parent.findViewById(R.id.tvTotal);
        initRecycler();
        setListener();
        showLoadingDialog();
        getP().getPrizeList(true);
    }

    private void initRecycler() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new BoxPrizeAdapter(getActivity(), getContext(), new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);

    }

    private void setListener() {
        //发货
        tvOriginalBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdapter.getSelectCount() > 0) {
                    Intent intent = new Intent(mContext, DeliverGoodsActivity.class);
                    intent.putExtra("SelectGoods", (Serializable) mAdapter.getSelectList());
                    intent.putExtra("SelectCount", mAdapter.getSelectCount() + "");
                    intent.putExtra("SelectMoney", mAdapter.getAllSelectMoney() + "");
                    intent.putExtra("SelectIds", (Serializable) mAdapter.getAllIds());
                    startActivity(intent);
                } else {
                    ToastUtil.showLong(mContext, "请选择要发货的产品");
                }

            }
        });

        mImgCheckAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isGoodsCheck) {
                    mImgCheckAll.setImageResource(R.mipmap.tick_click);
                    mAdapter.setAllGoodsSelected();
                    mTvTotalPrice.setText(mAdapter.getAllSelectMoney() + "");
                    isGoodsCheck = false;
                } else {
                    mAdapter.clearChooseDeleteItemData();
                    mTvTotalPrice.setText("0.00");
                    mImgCheckAll.setImageResource(R.mipmap.icon_tick_default);
                    isGoodsCheck = true;
                }
                mTvTotal.setText(mAdapter.getSelectCount() + "");
            }
        });
        mAdapter.setListener(new BoxPrizeAdapter.IBoxPrizeListener() {
            @Override
            public void onSelect(BoxPrizeListModel.DataBean.ListBean listBean) {
                mAdapter.setGoodSelected(listBean);
                mTvTotalPrice.setText(mAdapter.getAllSelectMoney() + "");
                mImgCheckAll.setImageResource(mAdapter.isAllSelect() ? R.mipmap.tick_click : R.mipmap.icon_tick_default);
                mTvTotal.setText(mAdapter.getSelectCount() + "");

            }
        });

        //置换
        tvOriginalReplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAdapter.getSelectCount() > 0) {
                    getP().getReplaceRule(ConstantsUrl.BOx_REPLACE_RULE, v);
                } else {
                    ToastUtil.showLong(getActivity(), "请选择要置换的产品");
                }
            }
        });


    }

    public void showReplaceRuleWindow(String desc, View v) {
        ReplaceRulePopWindow replaceRulePopWindow = new ReplaceRulePopWindow(getActivity(), desc);
        replaceRulePopWindow.showPopWindow(v);
        replaceRulePopWindow.setListener(new ReplaceRulePopWindow.IReplaceRuleListener() {
            @Override
            public void onIntegral() {
                //置换积分
                getP().getIntegralRule(ConstantsUrl.BOx_REPLACE_INTEGRAL, v);

            }

            @Override
            public void onSent() {
                //发货
                if (mAdapter.getSelectCount() > 0) {
                    Intent intent = new Intent(mContext, DeliverGoodsActivity.class);
                    intent.putExtra("SelectGoods", (Serializable) mAdapter.getSelectList());
                    intent.putExtra("SelectCount", mAdapter.getSelectCount() + "");
                    intent.putExtra("SelectMoney", mAdapter.getAllSelectMoney() + "");
                    intent.putExtra("SelectIds", (Serializable) mAdapter.getAllIds());
                    startActivity(intent);
                } else {
                    ToastUtil.showLong(mContext, "请选择要发货的产品");
                }

            }

            @Override
            public void onReplace() {
                //置换盲盒币
                getP().getCoinRule(ConstantsUrl.BOx_REPLACE_COIN, v);
            }
        });
    }

    public void showErrorRuleTip() {
        ToastUtil.showLong(mContext, "置换失败，请重试");
    }

    public void showIntegralWindow(String desc, View v) {
        ReplacePopWindow window = new ReplacePopWindow(getActivity(), mAdapter.getSelectList(),
                mAdapter.getAllSelectCoin(), mAdapter.getAllSelectScore(), desc, ReplaceAdapter.REPLACE_CODE_SCORE);
        window.showPopWindow(v);
        window.setListener(new ReplacePopWindow.IReplaceListener() {
            @Override
            public void ok() {
                showIsOkTip(2);

            }
        });
    }

    public void showCoinWindow(String desc, View v) {
        ReplacePopWindow window = new ReplacePopWindow(getActivity(), mAdapter.getSelectList(),
                mAdapter.getAllSelectCoin(), mAdapter.getAllSelectScore(), desc, ReplaceAdapter.REPLACE_CODE_COIN);
        window.showPopWindow(v);
        window.setListener(new ReplacePopWindow.IReplaceListener() {
            @Override
            public void ok() {
                showIsOkTip(3);
            }
        });
    }


    private void showIsOkTip(int code) {
        NewTipDialog dialog = new NewTipDialog(getActivity(), "您确定要置换吗？");
        dialog.show();
        dialog.setListener(new NewTipDialog.ITipDialogListener() {
            @Override
            public void clickLeft() {

            }

            @Override
            public void clickRight() {
                showLoadingDialog();
                getP().getExchangePrize(mAdapter.getAllIds(), code, "");
            }
        });


    }


    public void updateList(List<BoxPrizeListModel.DataBean.ListBean> list) {
        mAdapter.initMap(list);
        mAdapter.updatePrizeList(list);
    }

    public void updateExchangePrize() {
        Bus.INSTANCE.send(new RefreshUserInfoEvent());
        ToastUtil.showLong(getActivity(), "恭喜你兑换成功");
        showLoadingDialog();
        if (mAdapter != null) {
            mAdapter.clearMap();
            mTvTotal.setText("0");
            mTvTotalPrice.setText("0.00");
            isGoodsCheck = true;
            mImgCheckAll.setImageResource(R.mipmap.icon_tick_default);
        }
        getP().getPrizeList(true);
    }

    public void showErrorTip() {
        ToastUtil.showLong(getActivity(), "获取盒柜商品失败，请重试");
    }

    public void showExchangeErrorTip() {
        ToastUtil.showLong(getActivity(), "置换失败，请重试");
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return mView.findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        if (mAdapter != null) {
            mAdapter.clearMap();
            mTvTotal.setText("0");
            mTvTotalPrice.setText("0.00");
            isGoodsCheck = true;
            mImgCheckAll.setImageResource(R.mipmap.icon_tick_default);
        }
        getP().getPrizeList(true);
    }

    @Override
    public void onLoad() {
        getP().getPrizeList(false);
    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getEventType() == Config.EVENT_BUS_GET_ORDER) {
            showLoadingDialog();
            if (mAdapter != null) {
                mAdapter.clearMap();
                mTvTotal.setText("0");
                mTvTotalPrice.setText("0.00");
                isGoodsCheck = true;
                mImgCheckAll.setImageResource(R.mipmap.icon_tick_default);
            }
            getP().getPrizeList(true);
        } else if (event.getEventType() == Config.EVENT_BUS_OPEN_SUCCESS) {
            //开盒成功后去获取奖品
            getP().getPrizeList(true);
        }
    }
}
