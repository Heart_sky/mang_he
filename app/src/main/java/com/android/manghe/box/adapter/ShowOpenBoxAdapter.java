package com.android.manghe.box.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.manghe.R;
import com.android.manghe.box.model.OpenBoxWayModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/06 11:21
 * desc   :
 */
public class ShowOpenBoxAdapter extends BaseQuickAdapter<OpenBoxWayModel, BaseViewHolder> {

    private Context mContext;

    public ShowOpenBoxAdapter(Context context, @Nullable List<OpenBoxWayModel> data) {
        super(R.layout.item_box_count, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, OpenBoxWayModel item) {
        LinearLayout lyYouHui = helper.itemView.findViewById(R.id.id_ly_youhui);
        TextView tvYouHui = helper.itemView.findViewById(R.id.id_tv_youhui);
        TextView tvWay = helper.itemView.findViewById(R.id.id_tv_way);
        TextView tvSalePrice = helper.itemView.findViewById(R.id.id_tv_sell_price);
        TextView tvOriginal = helper.itemView.findViewById(R.id.id_tv_original_price);
        RelativeLayout reBg = helper.itemView.findViewById(R.id.id_re);

        tvOriginal.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
        switch (item.getNum()) {
            case 1:
                reBg.setBackgroundResource(R.mipmap.box_bg_one);
                setDate(lyYouHui,tvYouHui, tvWay, tvSalePrice, tvOriginal, item);
                break;
            case 2:
                reBg.setBackgroundResource(R.mipmap.box_bg_two);
                setDate(lyYouHui,tvYouHui, tvWay, tvSalePrice, tvOriginal, item);
                break;
            case 3:
                reBg.setBackgroundResource(R.mipmap.box_bg_three);
                setDate(lyYouHui,tvYouHui, tvWay, tvSalePrice, tvOriginal, item);
                break;
            case 5:
                reBg.setBackgroundResource(R.mipmap.box_bg_five);
                setDate(lyYouHui,tvYouHui, tvWay, tvSalePrice, tvOriginal, item);
                break;
            case 10:
                reBg.setBackgroundResource(R.mipmap.box_bg_ten);
                setDate(lyYouHui,tvYouHui, tvWay, tvSalePrice, tvOriginal, item);
                break;
            default:
                break;
        }
    }

    private void setDate(LinearLayout lyYouHui,TextView tvYouHui, TextView tvWay, TextView tvSalePrice, TextView tvOriginal,
                         OpenBoxWayModel item) {
        if (!TextUtils.isEmpty(item.getDiscount())) {
            lyYouHui.setVisibility(View.VISIBLE);
            tvYouHui.setText("优惠￥" + item.getDiscount());
            tvOriginal.setText(item.getOrigin_price());
            tvOriginal.setVisibility(View.VISIBLE);
        } else {
            lyYouHui.setVisibility(View.GONE);
            tvOriginal.setVisibility(View.GONE);
        }
        tvWay.setText(item.getLabel());
        tvSalePrice.setText("￥" + item.getSell_price());


    }

}
