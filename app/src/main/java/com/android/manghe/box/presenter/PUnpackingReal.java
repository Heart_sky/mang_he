package com.android.manghe.box.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.box.activity.UnpackingRealActivity;
import com.android.manghe.box.model.BoxGiftModel;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/23 16:03
 * desc   :
 */
public class PUnpackingReal extends XPresenter<UnpackingRealActivity> {


    public void getBoxOpen(int order_id) {
        HashMap<String, String> data = new HashMap<>();
        data.put("order_id", order_id + "");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + String.format(ConstantsUrl.Box_Open, order_id), headMap)
                .get()
                .execute(new BaseBack<BoxGiftModel>() {

                    @Override
                    public void onSuccess(Call call, BoxGiftModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.data.list != null && res.data.list.size() > 0) {
                            getV().updateDate(res.data.list);
                        } else {
                            getV().setFailureTip("网络错误,请点击礼物重试");
                        }


                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().setFailureTip("网络错误,请点击礼物重试");
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();

                    }
                });
    }
}
