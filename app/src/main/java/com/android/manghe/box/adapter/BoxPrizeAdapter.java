package com.android.manghe.box.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.tools.BigDecimalUtils;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeListModel;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.ShowIndexEvent;
import com.android.manghe.mine.model.UserRes;
import com.android.manghe.user.activity.LoginActivity;
import com.android.manghe.utils.WindowUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.eightbitlab.rxbus.Bus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/21 14:09
 * desc   :
 */
public class BoxPrizeAdapter extends BaseQuickAdapter<BoxPrizeListModel.DataBean.ListBean, BaseViewHolder> {

    private Context context;
    private View emptyView;
    private IBoxPrizeListener mListener;
    private HashMap<String, Boolean> mCheckedMap = new HashMap<>();
    private HashMap<String, BoxPrizeListModel.DataBean.ListBean> mMoneyHashMap = new HashMap<>();

    public void setListener(IBoxPrizeListener listener) {
        this.mListener = listener;
    }

    public BoxPrizeAdapter(Activity activity, Context context, @Nullable List<BoxPrizeListModel.DataBean.ListBean> data) {
        super(R.layout.item_box_good, data);
        this.context = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data_box, null);
        emptyView.findViewById(R.id.ly_no_date).setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        emptyView.findViewById(R.id.id_tv_go_shopping).setOnClickListener(v -> {
            UserRes.UserInfo userInfo = UserHolder.getUserInfo(mContext);
            if (userInfo == null) {
                mContext.startActivity(new Intent(mContext, LoginActivity.class));
            } else {
                Bus.INSTANCE.send(new ShowIndexEvent());
            }
        });
        setEmptyView(emptyView);
    }

    public void initMap(List<BoxPrizeListModel.DataBean.ListBean> data) {
        if (data.size() > 0) {
            for (BoxPrizeListModel.DataBean.ListBean listBean : data) {
                if (mCheckedMap.get(listBean.id) == null) {
                    mCheckedMap.put(listBean.id, false);
                }
            }
        }
    }


    /**
     * 获取所选中的List
     *
     * @return
     */
    public List<BoxPrizeListModel.DataBean.ListBean> getSelectList() {
        return new ArrayList<>(mMoneyHashMap.values());
    }

    /**
     * 单选
     *
     * @param listBean
     */
    public void setGoodSelected(BoxPrizeListModel.DataBean.ListBean listBean) {
        try {
            mCheckedMap.put(listBean.id, !mCheckedMap.get(listBean.id));
            if (mCheckedMap.get(listBean.id)) {
                mMoneyHashMap.put(listBean.id, listBean);
            } else {
                mMoneyHashMap.remove(listBean.id);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 全选
     */
    public void setAllGoodsSelected() {
        for (Map.Entry<String, Boolean> entry : mCheckedMap.entrySet()) {
            mCheckedMap.put(entry.getKey(), true);
        }
        //获取所有的钱
        for (BoxPrizeListModel.DataBean.ListBean item : getData()) {
            mMoneyHashMap.put(item.id, item);
        }

        notifyDataSetChanged();
    }

    //判断是否全选
    public boolean isAllSelect() {
        boolean isSelect = true;
        for (Map.Entry<String, Boolean> entry : mCheckedMap.entrySet()) {
            if (!entry.getValue()) {
                isSelect = false;
                break;
            }
        }
        return isSelect;
    }


    //清除数据
    public void clearChooseDeleteItemData() {
        for (Map.Entry<String, Boolean> entry : mCheckedMap.entrySet()) {
            mCheckedMap.put(entry.getKey(), false);
        }

        mMoneyHashMap.clear();
        notifyDataSetChanged();
    }

    public void clearMap() {
        mCheckedMap.clear();
        mMoneyHashMap.clear();
    }

    /**
     * 获取所有的钱
     *
     * @return
     */
    public String getAllSelectMoney() {
        String money = "0.00";
        for (BoxPrizeListModel.DataBean.ListBean value : mMoneyHashMap.values()) {
            money = BigDecimalUtils.add(money, value.goods_price, 2);
        }
        return money;
    }

    /**
     * 获取所有能兑换的币
     *
     * @return
     */
    public Double getAllSelectCoin() {
        double coin = 0.00;
        for (BoxPrizeListModel.DataBean.ListBean value : mMoneyHashMap.values()) {
            coin = coin + value.may_exchange_coin;
        }
        return coin;
    }

    public Double getAllSelectScore() {
        double score = 0.00;
        for (BoxPrizeListModel.DataBean.ListBean value : mMoneyHashMap.values()) {
            score = score + value.may_exchange_score;
        }
        return score;
    }


    public List<String> getAllIds() {
        List<String> list = new ArrayList<>();
        for (BoxPrizeListModel.DataBean.ListBean value : mMoneyHashMap.values()) {
            list.add(value.id);
        }
        return list;
    }


    /**
     * 更新数据
     *
     * @param list
     */
    public void updatePrizeList(List<BoxPrizeListModel.DataBean.ListBean> list) {
        replaceData(list);
    }


    public int getSelectCount() {
        int i = 0;
        for (Map.Entry<String, Boolean> entry : mCheckedMap.entrySet()) {
            if (entry.getValue()) {
                i++;
            }
        }
        return i;
    }


    @Override
    protected void convert(BaseViewHolder helper, BoxPrizeListModel.DataBean.ListBean model) {
        GlideHelper.loadWithHolderErr(mContext, model.goods_pic, helper.getView(R.id.ivPic));
        helper.setText(R.id.tvGoodName, model.goods_name + "");
        helper.setText(R.id.tvSpec, "价值: ￥" + model.goods_price);
        helper.setImageResource(R.id.ivCheck, mCheckedMap.get(model.id) ? R.mipmap.tick_click : R.mipmap.icon_tick_default);

        RoundLinearLayout linearLayout = helper.itemView.findViewById(R.id.id_ly_level);
        ImageView imgLevel = helper.itemView.findViewById(R.id.id_img_level);
        TextView tvLevel = helper.itemView.findViewById(R.id.id_tv_level);
        WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, model.probability_level);
        helper.itemView.findViewById(R.id.ivCheck).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onSelect(model);
                }
            }
        });
    }

    public interface IBoxPrizeListener {
        void onSelect(BoxPrizeListModel.DataBean.ListBean listBean);
    }
}
