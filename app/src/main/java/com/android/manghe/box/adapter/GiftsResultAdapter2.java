package com.android.manghe.box.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxGiftModel;
import com.android.manghe.utils.WindowUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/06 15:19
 * desc   : 开盒结果的适配器 开三个或者两个
 */
public class GiftsResultAdapter2 extends BaseQuickAdapter<BoxGiftModel.ListBean, BaseViewHolder> {

    private Context mContext;

    public GiftsResultAdapter2(Context context, @Nullable List<BoxGiftModel.ListBean> data) {
        super(R.layout.item_show_box_two_three, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, BoxGiftModel.ListBean item) {
        TextView tv=helper.itemView.findViewById(R.id.id_tv_name_1);
        TextView tvPrice=helper.itemView.findViewById(R.id.id_tv_price_value_1);
        ImageView img=helper.itemView.findViewById(R.id.id_img_gift_1);

        tv.setText(item.goods_name);
        tvPrice.setText("￥"+item.goods_price);
        GlideHelper.loadWithHolderErr(mContext, item.goods_pic, img);

        RoundLinearLayout linearLayout = helper.itemView.findViewById(R.id.id_ly_level);
        ImageView imgLevel = helper.itemView.findViewById(R.id.id_img_level);
        TextView tvLevel = helper.itemView.findViewById(R.id.id_tv_level);
        WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, item.probability_level);
    }
}
