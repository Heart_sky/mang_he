package com.android.manghe.box.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxGiftModel;
import com.android.manghe.utils.WindowUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/24 11:42
 * desc   :
 */
public class GiftUltraPagerAdapter extends PagerAdapter {

    private List<BoxGiftModel.ListBean> mList = new ArrayList<>();
    private Context mContext;

    public GiftUltraPagerAdapter(Context context, List<BoxGiftModel.ListBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        if (mList == null) {
            return 0;
        }
        return mList.size();

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        BoxGiftModel.ListBean model = mList.get(position);
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_box_gift, null);
        ImageView imgView = view.findViewById(R.id.id_img_real_gift);
        TextView tvName = view.findViewById(R.id.id_tv_name);
        TextView tvPrice = view.findViewById(R.id.id_tv_price_value);
        RoundLinearLayout linearLayout = view.findViewById(R.id.id_ly_level);
        ImageView imgLevel = view.findViewById(R.id.id_img_level);
        TextView tvLevel = view.findViewById(R.id.id_tv_level);
        GlideHelper.loadWithHolderErr(mContext, model.goods_pic, imgView);
        tvName.setText(model.goods_name);
        tvPrice.setText("￥" + model.goods_price);
        WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, model.probability_level);
        container.addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
