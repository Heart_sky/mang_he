package com.android.manghe.box.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/05 15:35
 * desc   :
 */
public class BoxPrizeModel extends NetBean<BoxPrizeModel.DataBean> {


    /**
     * data : {"list":[{"user_id":"28953","goods_id":"2115","goods_name":"Apple AirTag 皮革扣环 - 红色(不包含AirTag)","goods_price":"169.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/32c6f572cd2293cb.jpg","nickname":"爱着李易峰"},{"user_id":"28946","goods_id":"2117","goods_name":"Apple AirTag 皮革扣环 - 鞍褐色(不包含AirTag)","goods_price":"169.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/58b8fe2a6b27b787.jpg","nickname":"我是个学生"},{"user_id":"28955","goods_id":"2127","goods_name":"MC V-196 电脑音响音箱家用桌面台式机超重低音炮USB影响长条双喇叭笔记本迷你手机小钢炮大音量 黑色经典版（有线）","goods_price":"59.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/f804b7341b8b9ec4.jpg","nickname":"毛毛"},{"user_id":"28961","goods_id":"2122","goods_name":"索尼（SONY）DSC-W830/W810/W800 便携数码相机/照相机/卡片机 高清摄像家用拍照 W830-黑色 套餐一","goods_price":"1579.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/h/z/29160_src.jpg","nickname":"巴黎夜"},{"user_id":"28965","goods_id":"2131","goods_name":"小米（MI） 小米青萍多功能蓝牙网关接入米家门锁智能家居设备远程控制网关家电 青萍多功能蓝牙网关","goods_price":"69.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/8/29169_src.jpg","nickname":"这个还是我"},{"user_id":"28970","goods_id":"2134","goods_name":"新茶金骏眉红茶瓷罐礼盒装250克手工制作茶叶 武夷山厂家桐木关古洪中秋节送礼礼品","goods_price":"829.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/0233d3294f5f3eb3.jpg","nickname":"你们抢不过我的"},{"user_id":"28982","goods_id":"2131","goods_name":"小米（MI） 小米青萍多功能蓝牙网关接入米家门锁智能家居设备远程控制网关家电 青萍多功能蓝牙网关","goods_price":"69.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/8/29169_src.jpg","nickname":"人海啊"},{"user_id":"28991","goods_id":"2129","goods_name":"美的(Midea)606升变频一级能效对开双门家用冰箱智能家电风冷无霜BCD-606WKPZM(E)大容量精细分储","goods_price":"3299.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/4/29165_src.jpg","nickname":"这真的是太爽了"},{"user_id":"28997","goods_id":"2128","goods_name":"大宇（DAEWOO）挂烫机家用手持蒸汽熨烫机熨衣机旅游出差便携式熨斗HI-029-ZI","goods_price":"479.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/3/29164_src.jpg","nickname":"信阳刘德华"},{"user_id":"29000","goods_id":"2120","goods_name":"佳能相机 ixus285 数码相机 卡片机  照相机 学生入门便携式家用照像机 IXUS285 HS 黑色 官方 标配","goods_price":"1729.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/h/x/29158_src.jpg","nickname":"不要来跟我抢"}],"list_total":10}
     * flag : true
     */
    public static class DataBean implements Serializable {
        /**
         * list : [{"user_id":"28953","goods_id":"2115","goods_name":"Apple AirTag 皮革扣环 - 红色(不包含AirTag)","goods_price":"169.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/32c6f572cd2293cb.jpg","nickname":"爱着李易峰"},{"user_id":"28946","goods_id":"2117","goods_name":"Apple AirTag 皮革扣环 - 鞍褐色(不包含AirTag)","goods_price":"169.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/58b8fe2a6b27b787.jpg","nickname":"我是个学生"},{"user_id":"28955","goods_id":"2127","goods_name":"MC V-196 电脑音响音箱家用桌面台式机超重低音炮USB影响长条双喇叭笔记本迷你手机小钢炮大音量 黑色经典版（有线）","goods_price":"59.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/f804b7341b8b9ec4.jpg","nickname":"毛毛"},{"user_id":"28961","goods_id":"2122","goods_name":"索尼（SONY）DSC-W830/W810/W800 便携数码相机/照相机/卡片机 高清摄像家用拍照 W830-黑色 套餐一","goods_price":"1579.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/h/z/29160_src.jpg","nickname":"巴黎夜"},{"user_id":"28965","goods_id":"2131","goods_name":"小米（MI） 小米青萍多功能蓝牙网关接入米家门锁智能家居设备远程控制网关家电 青萍多功能蓝牙网关","goods_price":"69.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/8/29169_src.jpg","nickname":"这个还是我"},{"user_id":"28970","goods_id":"2134","goods_name":"新茶金骏眉红茶瓷罐礼盒装250克手工制作茶叶 武夷山厂家桐木关古洪中秋节送礼礼品","goods_price":"829.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/211103/0233d3294f5f3eb3.jpg","nickname":"你们抢不过我的"},{"user_id":"28982","goods_id":"2131","goods_name":"小米（MI） 小米青萍多功能蓝牙网关接入米家门锁智能家居设备远程控制网关家电 青萍多功能蓝牙网关","goods_price":"69.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/8/29169_src.jpg","nickname":"人海啊"},{"user_id":"28991","goods_id":"2129","goods_name":"美的(Midea)606升变频一级能效对开双门家用冰箱智能家电风冷无霜BCD-606WKPZM(E)大容量精细分储","goods_price":"3299.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/4/29165_src.jpg","nickname":"这真的是太爽了"},{"user_id":"28997","goods_id":"2128","goods_name":"大宇（DAEWOO）挂烫机家用手持蒸汽熨烫机熨衣机旅游出差便携式熨斗HI-029-ZI","goods_price":"479.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/i/3/29164_src.jpg","nickname":"信阳刘德华"},{"user_id":"29000","goods_id":"2120","goods_name":"佳能相机 ixus285 数码相机 卡片机  照相机 学生入门便携式家用照像机 IXUS285 HS 黑色 官方 标配","goods_price":"1729.00","goods_thumb":"https: //www.mxmd88.com//upload/1/images/gallery/h/x/29158_src.jpg","nickname":"不要来跟我抢"}]
         * list_total : 10
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean implements Serializable {
            /**
             * mid : 28953
             * goods_id : 2115
             * goods_name : Apple AirTag 皮革扣环 - 红色(不包含AirTag)
             * goods_price : 169.00
             * goods_thumb : https: //www.mxmd88.com//upload/1/images/211103/32c6f572cd2293cb.jpg
             * nickname : 爱着李易峰
             * created_at "2021-11-05 11:40:38"
             * goods_probability_level 普通款
             */

            public String mid;
            public String goods_id;
            public String goods_name;
            public String goods_price;
            public String goods_thumb;
            public String nickname;
            public String created_at;
            public String goods_probability_level;
            public String buy_way;//购买方式
            public String member_avatar;//购买人的头像
        }
    }
}
