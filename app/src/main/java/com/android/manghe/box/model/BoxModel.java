package com.android.manghe.box.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/15 13:50
 * desc   : 单个box的数据
 */
public class BoxModel extends NetBean<BoxModel.DataBean> {


    /**
     * data : {"id":"4","name":"美好食光","cover_pic":"https://www.mxmd88.com//upload/1/images/gallery/h/d/29138_src.jpg","price":"30","sort_id":"4","goods_list":[{"id":"2135","name":"洋河蓝色经典 梦之蓝M6 52度 礼盒装 500ml*2瓶高度白酒 口感绵柔浓香型","price":"1606.00","thumb":"https://www.mxmd88.com//upload/1/images/211103/5ab4cb3bN0943eaf5.jpg","probability_level":"至尊款"},{"id":"2134","name":"新茶金骏眉红茶瓷罐礼盒装250克手工制作茶叶 武夷山厂家桐木关古洪中秋节送礼礼品","price":"829.00","thumb":"https://www.mxmd88.com//upload/1/images/211103/0233d3294f5f3eb3.jpg","probability_level":"隐藏款"},{"id":"2133","name":"三只松鼠巨型零食大礼包/内含30包 送礼送女友礼物芒果干网红薯片饼干锅巴辣条肉干肉脯/6斤装","price":"228.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/i/a/29171_src.jpg","probability_level":"稀有款"},{"id":"2132","name":"正宗德州五香扒鸡500g 山东特产卤味鸡肉熟食烧鸡 德州五香脱骨扒鸡德州真空袋装下酒菜凉菜","price":"49.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/i/9/29170_src.png","probability_level":"普通款"}],"price_interval":{"min":"49.00","max":"1606.00"}}
     * flag : true
     */

    public static class DataBean implements Serializable {
        /**
         * id : 22
         * name : 盒气满满
         * cover_pic : https://www.mxmd88.com//upload/1/images/gallery/e/b/30324_src.jpg
         * prices : {"1":{"origin_price":"69","discount":"","sell_price":"69","num":1,"label":"一发入魂"},"2":{"origin_price":"138","discount":"5","sell_price":"133.00","num":2,"label":"二发不重"},"3":{"origin_price":"207","discount":"10","sell_price":"197.00","num":3,"label":"三发不重"},"5":{"origin_price":"345","discount":"20","sell_price":"325.00","num":5,"label":"五发不重"},"10":{"origin_price":"690","discount":"50","sell_price":"640.00","num":10,"label":"十发不重"}}
         * sort_id : 5
         * front_level_ratio_config : {"1":"0.1%","2":"1.1%","3":"22.4%","4":"76.4%"}
         * is_sale : 1
         * goods_list : [{"id":"3497","name":"三星 SAMSUNG 心系天下W22 骁龙888 5G 折叠屏  16＋512GB雅瓷黑","price":"16999.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/e/6/30319_src.jpg","probability_level":"至尊款"},{"id":"3496","name":"HTC VIVE PRO2 PCVR 5K分辨率120度视场角单头盔智能VR眼镜2QAL100  Pro2 2.0手柄基站+无线升级套装","price":"14999.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/e/5/30318_src.jpg","probability_level":"至尊款"},{"id":"3495","name":"浪琴(Longines)手表 康卡斯潜水系列 机械钢带男表 L37814966","price":"13000.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/e/4/30317_src.jpg","probability_level":"至尊款"},{"id":"3494","name":"索尼（SONY）XR-75X91J 75英寸 全面屏 游戏电视 4K超高清HDR XR认知芯片 HDMI2.1 ","price":"10699.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/e/3/30316_src.jpg","probability_level":"至尊款"},{"id":"3493","name":"HT 复仇者联盟 钢铁侠  55cm 反浩克装甲气锤豪华版","price":"8000.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/137e72f632a3b960.jpg","probability_level":"至尊款"},{"id":"3491","name":"暴雪官方游戏周边守望先锋D.Va雕像超大型手办人偶模型玩具摆件 宋哈娜D.Va 大号","price":"4999.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/483ddfe312d858ab.jpg","probability_level":"至尊款"},{"id":"3489","name":"佳能（Canon）EOS 4000D 单反相机 APS画幅 入门级高清数码照相机 3000D同款新款 单机+18-55mm III镜头","price":"2929.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/e/2/30315_src.jpg","probability_level":"至尊款"},{"id":"3482","name":"乐高(LEGO)积木 机械系列 42083 布加迪Chiron 16岁+ 儿童玩具","price":"2699.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/e/0/30313_src.png","probability_level":"至尊款"},{"id":"3492","name":"Spalding斯伯丁篮球NBA球星签名款lq 76-638Y(科比24K紫色珠光)黑曼巴礼盒 7号标准球","price":"2388.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/c68081e99f3361c4.jpg","probability_level":"至尊款"},{"id":"3486","name":"重力星球（GravaStar）G1Pro灵笼联名款白鲨-14号无线蓝牙音箱","price":"2199.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/e/1/30314_src.png","probability_level":"至尊款"},{"id":"3484","name":"乐高(LEGO)积木 艺术系列ART 31203 世界地图 18岁+ 儿童玩具 马赛克像素画 ","price":"1899.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/a6a133225b65be5f.jpg","probability_level":"至尊款"},{"id":"3480","name":"Libratone小鸟耳机 AIR+第2代主动降噪真无线蓝牙耳机","price":"1598.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/z/30312_src.jpg","probability_level":"至尊款"},{"id":"3478","name":"米家 小米扫地机器人1S 激光+视觉融合双导航  2000Pa大吸力","price":"1399.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/x/30310_src.jpg","probability_level":"至尊款"},{"id":"3479","name":"任天堂（Nintendo） NS主机日版Switch Lite mini NSL掌上便携游戏机","price":"1359.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/y/30311_src.jpg","probability_level":"至尊款"},{"id":"3475","name":"傲风AutoFull 电竞椅人体工学椅子  荣耀之盾·天羽脚托款","price":"1299.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/b4a5a5a9c86f3a9b.jpg","probability_level":"至尊款"},{"id":"3473","name":"卡西欧（CASIO）EDIFICE男表 休闲防水 EFB-680BL-1AVUPR-100米防水","price":"1090.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/w/30309_src.png","probability_level":"至尊款"},{"id":"3471","name":"声阔 Soundcore降噪舱 Liberty Air 2 Pro主动降噪真无线TWS 矿石白","price":"999.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/v/30308_src.png","probability_level":"至尊款"},{"id":"3468","name":"贝多奇儿童电动卡丁车电动玩具车 【雪花白】双驱+大电瓶+蓝牙+高低速可调","price":"898.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/t/30306_src.png","probability_level":"隐藏款"},{"id":"3466","name":"之宝（ZIPPO）打火机 一帆风顺 经典热销  ZBT-3-182a 煤油防风火机","price":"869.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/cb18ac001885f1da.jpg","probability_level":"隐藏款"},{"id":"3465","name":"Divoom 点音TIVOO-MAX像素蓝牙音箱 家用无线智能闹钟音响 白色","price":"799.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/s/30305_src.jpg","probability_level":"隐藏款"},{"id":"3470","name":"罗技（G）G502  LIGHTSPEED 无线鼠标RGB 25600DPI 黑色 GPW Powerplay 无线充电","price":"709.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/u/30307_src.jpg","probability_level":"隐藏款"},{"id":"3462","name":"迪奥Dior旷野男士淡香氛60ml男士香氛 木质香调香氛 ","price":"695.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/p/30302_src.png","probability_level":"隐藏款"},{"id":"3463","name":"TIMBUK2拼色邮差包经典单肩包信使包男女帆布斜挎包 黄色/红色环保款 M","price":"658.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/q/30303_src.jpg","probability_level":"隐藏款"},{"id":"3464","name":"Steiff史戴芙Teddy Bear泰迪熊德国进口 奶油白色 22cm","price":"650.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/r/30304_src.jpg","probability_level":"隐藏款"},{"id":"3460","name":"点音（DIVOOM）LED背包女双肩包男学生电脑休闲大容量像素书包","price":"629.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/8056dc86153434ab.jpg","probability_level":"隐藏款"},{"id":"3458","name":"艾优 APIYOO 护颈仪 按摩仪颈部按摩器智能肩颈仪颈椎SUP肩椎护理仪 SN SUP 红色","price":"598.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/9c0ebbfb8e8bf4a8.jpg","probability_level":"隐藏款"},{"id":"3456","name":"小霸王跳舞毯体感游戏机家用双人瑜伽毯","price":"598.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/2c6d90f0eb02d4fa.jpg","probability_level":"隐藏款"},{"id":"3454","name":"乐高(LEGO)积木 超级英雄系列 76182 蝙蝠侠面具 18岁+ 儿童玩具","price":"549.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/68379870de399b24.jpg","probability_level":"隐藏款"},{"id":"3461","name":"LOFREE洛斐 奶茶无线蓝牙机械键盘 DOT圆点键盘","price":"499.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/0aaf67a92b0cddd5.png","probability_level":"隐藏款"},{"id":"3449","name":"猫王收音机x王者荣耀 小王子便携蓝牙音箱迷你小音响","price":"499.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/j/30296_src.jpg","probability_level":"隐藏款"},{"id":"3452","name":"腾讯态客王者荣耀神器正版机器人游戏辅助AI智能音箱助手SNK娜可露露英雄皮肤手办云台套装家用无线蓝牙音响","price":"479.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/m/30299_src.jpg","probability_level":"隐藏款"},{"id":"3451","name":"不莱玫拉杆箱女24英寸行李箱ins网红新款涂鸦DIY大容量密码箱","price":"476.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/l/30298_src.jpg","probability_level":"隐藏款"},{"id":"3450","name":"美职棒（MLB）潮流镂空表盘手表男 彩白色 MLB-NY605-4","price":"469.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/k/30297_src.png","probability_level":"隐藏款"},{"id":"3448","name":"沸点JUSTICE儿童专业双翘滑板 青少年入门男女生初学者滑板 探索-激光","price":"400.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/dc5e8754e42c6224.jpg","probability_level":"隐藏款"},{"id":"3447","name":"漫步者(EDIFIER) HECATE G2000蓝牙游戏音箱  黑色","price":"399.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/i/30295_src.jpg","probability_level":"隐藏款"},{"id":"3446","name":"咪鼠科技（MiMouse）S7B蓝牙双模无线鼠标 熔岩灰","price":"399.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/h/30294_src.png","probability_level":"隐藏款"},{"id":"3444","name":"DONHABobDylan鲍勃迪伦联名不可吹奏口琴项链","price":"399.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/g/30293_src.jpg","probability_level":"隐藏款"},{"id":"3438","name":"素士电动牙刷  成人美白声波震动情侣牙刷X3U梵高绿","price":"399.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/e5619777819e8705.jpg","probability_level":"隐藏款"},{"id":"3439","name":"美的（Midea）发热围巾通勤 短款","price":"353.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/e/30291_src.jpg","probability_level":"隐藏款"},{"id":"3441","name":"磁悬浮月球灯 深木纹底座 黑科技办公室摆件纪念品月亮灯小夜灯台灯","price":"348.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/f/30292_src.jpg","probability_level":"隐藏款"},{"id":"3436","name":"斐乐（FILA）手表女情侣潮流撞色防水运动手表 6035","price":"329.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/c/30289_src.jpg","probability_level":"稀有款"},{"id":"3433","name":"松下（Panasonic）电吹风机 纳诺怡护发 大功率速干  EH-JNA3C ","price":"329.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/a/30287_src.jpg","probability_level":"稀有款"},{"id":"3437","name":"艾优ApiYoo电动冲牙器便携式牙缝水牙线口腔牙齿清洁喷牙洁牙洗牙器 支持HUAWEI HiLink","price":"318.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/d/30290_src.jpg","probability_level":"稀有款"},{"id":"3434","name":"Divoom 点音Evo蓝牙音箱创意礼物像素闹钟复古可爱无线迷你带灯光电脑家用桌面小音响低音炮 黑色","price":"299.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/b/30288_src.jpg","probability_level":"稀有款"},{"id":"3429","name":"罗技 （G）G502 HERO主宰者游戏鼠标电竞鼠标有线","price":"299.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/9/30286_src.jpg","probability_level":"稀有款"},{"id":"3424","name":"黄油猫太空舱充电宝10000毫安时移动电源动漫周边二次元 柴犬款","price":"298.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/8/30285_src.jpg","probability_level":"稀有款"},{"id":"3419","name":"万代BANDAI  高达模型 RG敢达拼装玩具 1/144 系列RG 14 强袭自由 ","price":"279.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/7/30284_src.jpg","probability_level":"稀有款"},{"id":"3417","name":"牧马人K200掌托机械键盘 K200黑色混光黑轴【手托版】","price":"259.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/6/30283_src.jpg","probability_level":"稀有款"},{"id":"3402","name":"若客（ROKR）立体拼图老式放映机 上美影联名款","price":"249.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/3/30280_src.jpg","probability_level":"稀有款"},{"id":"3383","name":"Nike耐克双肩背包男女款2021秋冬新款休闲运动书包  ","price":"239.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/0/30277_src.png","probability_level":"稀有款"},{"id":"3410","name":"男士腰客潮牌斜挎包男女情侣腰包字母休闲运动单肩包胸包 王一博同款米白色","price":"234.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/4/30281_src.jpg","probability_level":"稀有款"},{"id":"3389","name":"迪士尼麦克风无线话筒音响一体适用唱吧全民K歌儿童家用蓝牙直播","price":"229.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/2/30279_src.jpg","probability_level":"稀有款"},{"id":"3372","name":"  Taito 初音 2nd Spring 春服 公主 兔耳  22.5CM","price":"227.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/u/30271_src.jpg","probability_level":"稀有款"},{"id":"3386","name":"B.Duck小黄鸭真无线蓝牙耳机TWS无线入耳式 K5耳机","price":"219.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/d/1/30278_src.png","probability_level":"稀有款"},{"id":"3355","name":"沃品（WOPOW）HM03P七彩杯加湿器 ","price":"209.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/i/30259_src.jpg","probability_level":"稀有款"},{"id":"3375","name":"格沵（GERM）潮牌可口可乐联名智能数显保温杯370ml 红色","price":"179.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/x/30274_src.png","probability_level":"稀有款"},{"id":"3399","name":"达尔优 电竞游戏 有线USB鼠标  5代【黑色】【CM655】","price":"169.00","thumb":"https://www.mxmd88.com//upload/1/images/211230/989344b9101cb104.jpg","probability_level":"稀有款"},{"id":"3374","name":"apiyoo艾优迪士尼米奇驱蚊手环","price":"169.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/w/30273_src.png","probability_level":"稀有款"},{"id":"3373","name":"艾优 APIYOO 皮卡丘加湿器","price":"168.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/v/30272_src.jpg","probability_level":"稀有款"},{"id":"3371","name":"复古怀旧汽车模型无线蓝牙音箱插卡U盘创意个性生日礼物车载音响","price":"168.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/t/30270_src.jpg","probability_level":"稀有款"},{"id":"3369","name":"腾讯QQfamily QWS320耳机头戴式 无线蓝牙音乐耳机粉色","price":"168.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/b2f61248e25e4d50.png","probability_level":"稀有款"},{"id":"3370","name":"dura MOBI蜂鸟音响骨传导无线蓝牙音箱 银色【单个装】","price":"159.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/s/30269_src.jpg","probability_level":"稀有款"},{"id":"3368","name":"Tokidoki淘奇多奇独角兽真无线蓝牙耳机","price":"158.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/r/30268_src.png","probability_level":"稀有款"},{"id":"3358","name":"独到（DUDAO）U5Pro无线蓝牙耳机挂耳式入超长待机续航手机单耳双耳篮牙运动跑步苹果安卓通用 灰色","price":"158.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/k/30261_src.jpg","probability_level":"稀有款"},{"id":"3367","name":"惠普（HP）DHE-6003 音响电脑迷你长条小音箱 电竞炫彩灯光","price":"149.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/q/30267_src.jpg","probability_level":"稀有款"},{"id":"3365","name":"GERM迪士尼联名陶瓷马克杯带盖勺女式杯子咖啡杯380ml 黑色","price":"129.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/o/30265_src.jpg","probability_level":"稀有款"},{"id":"3364","name":"沃品（WOPOW）MD01 USB插座  流氓兔款排插 浅蓝色","price":"129.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/9cb72d180994741d.jpg","probability_level":"稀有款"},{"id":"3363","name":"KILLWINEER回到未来手链项链男女ins潮流情侣闺蜜手链（两条装）","price":"129.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/7847c46d01ca7cf1.jpg","probability_level":"稀有款"},{"id":"3366","name":"xxoff情侣项链一对拼接款太阳月亮潮牌钛钢毛衣链 玫瑰花礼盒 ","price":"128.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/p/30266_src.jpg","probability_level":"稀有款"},{"id":"3362","name":"GUUKA赌神骰子项链叠戴组合假两件双层吊坠","price":"128.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/n/30264_src.jpg","probability_level":"稀有款"},{"id":"3361","name":"SANAG X6S无线蓝牙音箱 墨绿色 标配","price":"128.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/m/30263_src.jpg","probability_level":"稀有款"},{"id":"3357","name":"无火香薰卧室少女精油扩香石天然水晶石摆件","price":"126.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/a03f43375741b451.jpg","probability_level":"稀有款"},{"id":"3376","name":"保罗帽子男士运动纯棉棒球帽  黑色卡其标（硬顶款） ","price":"125.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/acb819f5b910c0db.jpg","probability_level":"稀有款"},{"id":"3360","name":"小米有品 焕醒绅士风手动剃须刀套装","price":"109.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/l/30262_src.jpg","probability_level":"稀有款"},{"id":"3359","name":"engue恩谷无线充电器磁吸15W快充 W7无线充 白色","price":"99.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/fb3a18fad1ff23f5.png","probability_level":"稀有款"},{"id":"3356","name":"欧丽源美白袪痘面膜女深层清洁收缩毛孔去黑头 祛痘泥膜","price":"89.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/j/30260_src.jpg","probability_level":"稀有款"},{"id":"3354","name":"REMAX 重低音TWS无线游戏头戴式耳机电脑电竞耳麦 RB-750HB 黑色","price":"88.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/b4bc4abfd92ef109.jpg","probability_level":"普通款"},{"id":"3353","name":"驰誉 儿童玩具汽车合金玩具车男孩汽车模型 1/32奔驰AMG GT车模","price":"79.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/h/30258_src.jpg","probability_level":"普通款"},{"id":"3352","name":"麦芙迪男士乳液面霜秋冬季美白精华乳 美白精华50g","price":"79.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/g/30257_src.jpg","probability_level":"普通款"},{"id":"3351","name":"机乐堂 JR-ZS198  光影系列车载手机架汽车用支架","price":"79.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/f/30256_src.jpg","probability_level":"普通款"},{"id":"3350","name":"欧丽源薰衣草舒缓睡眠喷雾75ML","price":"79.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/e/30255_src.jpg","probability_level":"普通款"},{"id":"3348","name":"铂典（POLVCDG） 蓝牙耳机 无线运动耳机 亮黑色","price":"79.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/c43a96bf29cbb224.png","probability_level":"普通款"},{"id":"3349","name":"力美GTX300朋克键鼠套装 复古圆键帽有线鼠标键盘","price":"76.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/a/30251_src.jpg","probability_level":"普通款"},{"id":"3347","name":"钛度TAIDU TSG202C有线电竞游戏鼠标","price":"72.00","thumb":"https://www.mxmd88.com//upload/1/images/211229/b92ed17bb2c6f4a5.jpg","probability_level":"普通款"},{"id":"3346","name":"博皓（prooral）T27声波电动牙刷","price":"69.00","thumb":"https://www.mxmd88.com//upload/1/images/gallery/c/6/30247_src.png","probability_level":"普通款"}]
         * price_interval : {"min":"69.00","max":"16999.00"}
         */

        public String id;
        public String name;
        public String cover_pic;
        public PricesBean prices;
        public String sort_id;
        public FrontLevelRatioConfigBean front_level_ratio_config;
        public String is_sale;
        public PriceIntervalBean price_interval;
        public List<GoodsListBean> goods_list;

        public static class PricesBean implements Serializable {
            /**
             * 1 : {"origin_price":"69","discount":"","sell_price":"69","num":1,"label":"一发入魂"}
             * 2 : {"origin_price":"138","discount":"5","sell_price":"133.00","num":2,"label":"二发不重"}
             * 3 : {"origin_price":"207","discount":"10","sell_price":"197.00","num":3,"label":"三发不重"}
             * 5 : {"origin_price":"345","discount":"20","sell_price":"325.00","num":5,"label":"五发不重"}
             * 10 : {"origin_price":"690","discount":"50","sell_price":"640.00","num":10,"label":"十发不重"}
             */

            @SerializedName("1")
            public PricesBean._$1Bean _$1;
            @SerializedName("2")
            public PricesBean._$2Bean _$2;
            @SerializedName("3")
            public PricesBean._$3Bean _$3;
            @SerializedName("5")
            public PricesBean._$5Bean _$5;
            @SerializedName("10")
            public PricesBean._$10Bean _$10;

            public static class _$1Bean implements Serializable {
                /**
                 * origin_price : 69
                 * discount :
                 * sell_price : 69
                 * num : 1
                 * label : 一发入魂
                 */

                public String origin_price;
                public String discount;
                public String sell_price;
                public int num;
                public String label;
            }


            public static class _$2Bean implements Serializable {
                /**
                 * origin_price : 138
                 * discount : 5
                 * sell_price : 133.00
                 * num : 2
                 * label : 二发不重
                 */

                public String origin_price;
                public String discount;
                public String sell_price;
                public int num;
                public String label;
            }

            public static class _$3Bean implements Serializable {
                /**
                 * origin_price : 207
                 * discount : 10
                 * sell_price : 197.00
                 * num : 3
                 * label : 三发不重
                 */

                public String origin_price;
                public String discount;
                public String sell_price;
                public int num;
                public String label;
            }


            public static class _$5Bean implements Serializable {
                /**
                 * origin_price : 345
                 * discount : 20
                 * sell_price : 325.00
                 * num : 5
                 * label : 五发不重
                 */

                public String origin_price;
                public String discount;
                public String sell_price;
                public int num;
                public String label;
            }


            public static class _$10Bean implements Serializable {
                /**
                 * origin_price : 690
                 * discount : 50
                 * sell_price : 640.00
                 * num : 10
                 * label : 十发不重
                 */

                public String origin_price;
                public String discount;
                public String sell_price;
                public int num;
                public String label;
            }
        }

        public static class FrontLevelRatioConfigBean implements Serializable {
            /**
             * 1 : 0.1%
             * 2 : 1.1%
             * 3 : 22.4%
             * 4 : 76.4%
             */

            @SerializedName("1")
            public String _$1;
            @SerializedName("2")
            public String _$2;
            @SerializedName("3")
            public String _$3;
            @SerializedName("4")
            public String _$4;
        }


        public static class PriceIntervalBean implements Serializable {
            /**
             * min : 69.00
             * max : 16999.00
             */

            public String min;
            public String max;
        }

        public static class GoodsListBean implements Serializable {
            /**
             * id : 3497
             * name : 三星 SAMSUNG 心系天下W22 骁龙888 5G 折叠屏  16＋512GB雅瓷黑
             * price : 16999.00
             * thumb : https://www.mxmd88.com//upload/1/images/gallery/e/6/30319_src.jpg
             * probability_level : 至尊款
             */

            public String id;
            public String name;
            public String price;
            public String thumb;
            public String probability_level;
        }
}
}
