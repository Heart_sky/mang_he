package com.android.manghe.box.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;

import java.util.ArrayList;
import java.util.List;

public class InfiniteLoopViewPagerAdapter extends PagerAdapter {

    private int mPageCount;
    private int mRealIndex;

    List<BoxModel.DataBean> mList=new ArrayList<>();
    private Context mContext;

    public InfiniteLoopViewPagerAdapter(Context context,List<BoxModel.DataBean> list) {
        mContext=context;
        this.mList = list;
        mPageCount = mList.size();
    }

    @Override
    public int getCount() {
        //将page总数返回整型最大，使之ViewPager可以无限向右滑动
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //将ViewPager的index转化为实际中Page的index
        mRealIndex = position % mPageCount;
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_box_gift_2, null);
        ImageView imgView = view.findViewById(R.id.id_img_gift);
        GlideHelper.loadWithHolderErr(mContext, mList.get(mRealIndex).cover_pic, imgView);
        container.addView(view);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        mRealIndex = position % mPageCount;
//        mImageView = mList.get(mRealIndex);
//        container.removeView(mImageView);
    }
}
