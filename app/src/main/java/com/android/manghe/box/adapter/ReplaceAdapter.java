package com.android.manghe.box.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.android.base.tools.BigDecimalUtils;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeListModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/22 16:14
 * desc   :
 */
public class ReplaceAdapter extends BaseQuickAdapter<BoxPrizeListModel.DataBean.ListBean, BaseViewHolder> {


    public final static int REPLACE_CODE_COIN = 101;
    public final static  int REPLACE_CODE_SCORE = 102;
    private Context mContext;
    private int mReplaceCode;

    public ReplaceAdapter(Context context, @Nullable List<BoxPrizeListModel.DataBean.ListBean> data,
                          int code) {
        super(R.layout.item_replace, data);
        this.mReplaceCode = code;
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, BoxPrizeListModel.DataBean.ListBean model) {
        GlideHelper.loadWithHolderErr(mContext, model.goods_pic, helper.getView(R.id.ivPic));
        helper.setText(R.id.tvGoodName, model.goods_name + "");
        if (mReplaceCode == REPLACE_CODE_COIN) {
            String temCoin = BigDecimalUtils.round(model.may_exchange_coin);
            if (temCoin.contains(".00")) {
                temCoin = temCoin.replace(".00", "");
            }

            helper.setText(R.id.tvPrice, temCoin + "币");
        } else if (mReplaceCode == REPLACE_CODE_SCORE) {
            String temSore = BigDecimalUtils.round(model.may_exchange_score);
            if (temSore.contains(".00")) {
                temSore = temSore.replace(".00", "");
            }
            helper.setText(R.id.tvPrice, temSore + "积分");
        }
    }
}
