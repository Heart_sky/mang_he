package com.android.manghe.box.presenter;

import android.content.Context;
import android.content.Intent;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.box.activity.BoxGoodDetailActivity;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.common.webview.ERefreshWebType;
import com.android.manghe.common.webview.WebViewActivity;
import com.android.manghe.common.webview.WebViewParameter;
import com.android.manghe.config.Config;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.model.MarketGoodDetailRes;
import com.android.manghe.mine.model.UserRes;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/10 10:00
 * desc   :
 */
public class PBoxGoodDetail extends XPresenter<BoxGoodDetailActivity> {

    public MarketGoodDetailRes.DataBean detail;

    public void loadData(String goodId) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MAIN_DETAIL + "?id=" + goodId)
                .get()
                .execute(new BaseBack<MarketGoodDetailRes>() {

                    @Override
                    public void onSuccess(Call call, MarketGoodDetailRes res) {
                        if (res != null) {
                            MarketGoodDetailRes.DataBean data = res.data;
                            if (data != null) {
                                getV().updateView(data);
                                detail = data;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public List<String> htmlConvertPicList(String html) {
        List<String> picUrlList = new ArrayList<>();
        try {
            int startIndex = 0;
            int endIndex = 0;
            final String startTagHttp = "http://www.manghe98.com/upload";
            final String startTagHttps = "https://www.manghe98.com/upload";
            final String endTagJpg = ".jpg";
            final String endTagPng = ".png";
            String currentHtml = html;
            while (currentHtml.contains(startTagHttp) || currentHtml.contains(startTagHttps)) {
                if(currentHtml.contains(startTagHttp)) {
                    startIndex = currentHtml.indexOf(startTagHttp);
                }else if(currentHtml.contains(startTagHttps)){
                    startIndex = currentHtml.indexOf(startTagHttps);
                }

                int pngIndex = currentHtml.indexOf(endTagPng);
                int jpgIndex = currentHtml.indexOf(endTagJpg);
                if (pngIndex > 0 && jpgIndex > 0) {
                    endIndex = Math.min(pngIndex, jpgIndex);
                } else if (pngIndex == -1) {
                    //图片是jpg
                    endIndex = jpgIndex;
                } else if (jpgIndex == -1) {
                    //图片是png
                    endIndex = pngIndex;
                }
                endIndex = endIndex + 4;
                String picUrl = currentHtml.substring(startIndex, endIndex);
                picUrlList.add(picUrl);
                currentHtml = currentHtml.substring(endIndex, currentHtml.length() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return picUrlList;
    }

    public void set53(Context context, UserRes.UserInfo userInfo) {
        String url = Config.Box_53_URL + Config.Box_53_DEVEICE
                + String.format(Config.BOX_53_ID, userInfo.UID)
                + String.format(Config.BOX_53_NAME, userInfo.UID);
        WebViewParameter parameters = new WebViewParameter.WebParameterBuilder()
                .setUrl(url)
                .setShowProgress(true)
                .setCanHistoryGoBackOrForward(true)
                .setReloadable(true)
                .setShowWebPageTitle(true)
                .setReloadType(ERefreshWebType.ClickRefresh)
                .build();
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra("parameters", parameters);
        context.startActivity(intent);
    }
}
