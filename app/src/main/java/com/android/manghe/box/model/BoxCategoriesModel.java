package com.android.manghe.box.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/04 15:15
 * desc   :
 */


public class BoxCategoriesModel extends NetBean<BoxCategoriesModel.DataBean> {


    /**
     * data : {"list":[{"id":"5","title":"数码","subtitle":"3C数码","listorder":"0"},{"id":"4","title":"美食","subtitle":"美好时光","listorder":"0"},{"id":"3","title":"家电","subtitle":"厨房小电","listorder":"0"},{"id":"2","title":"电脑","subtitle":"电脑办公","listorder":"0"},{"id":"1","title":"手机","subtitle":"苹果/iPhone","listorder":"0"}]}
     * flag : true
     */

    public static class DataBean {
        public List<ListBean> list;

    }


        public static class ListBean implements Serializable {
            /**
             * id : 5
             * title : 数码
             * subtitle : 3C数码
             * listorder : 0
             */

            private String id;
            private String title;
            private String subtitle;
            private String listorder;


            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public String getListorder() {
                return listorder;
            }

            public void setListorder(String listorder) {
                this.listorder = listorder;
            }
        }
}
