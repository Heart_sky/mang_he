package com.android.manghe.box.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PayWayModel extends NetBean<PayWayModel.Data> {


    /**
     * data : {"is_paid":"1","pay_type":"6","pay_way":"1"}
     * flag : true
     */

    public static class Data implements Serializable {
        /**
         * is_paid : 1
         * pay_type : 6
         * pay_way : 1
         */

        @SerializedName("is_paid")
        public String isPaid;
        @SerializedName("pay_type")
        public String payType;
        @SerializedName("pay_way")
        public String payWay;
    }
}
