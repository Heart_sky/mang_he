package com.android.manghe.box.model;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/15 14:19
 * desc   :
 */
public class MessageEvent {

    public int mType;
    public Object mObj;
    public Object mObjectB;
    public Object mObjectC;

    public MessageEvent(int type,Object obj){
        mType = type;
        mObj = obj;
    }

    public MessageEvent(int type) {
        mType = type;
    }

    public MessageEvent(int type, Object obj, Object objectB) {
        mType = type;
        mObj = obj;
        mObjectB = objectB;
    }

    public MessageEvent(int type, Object obj, Object objectB, Object objectC) {
        mType = type;
        mObj = obj;
        mObjectB = objectB;
        mObjectC = objectC;
    }

    public Object getObjectC() {
        return mObjectC;
    }

    public void setObjectC(Object objectC) {
        mObjectC = objectC;
    }

    public Object getObjectB() {
        return mObjectB;
    }

    public int getEventType(){
        return mType;
    }
    public Object getObject(){
        return mObj;
    }
}



