package com.android.manghe.toutiao;

import android.content.Context;
import android.util.Log;

import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.NetWorkUtil;
import com.android.base.tools.SPUtil;
import com.android.base.tools.codec.MD5;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.SPTag;
import com.android.manghe.utils.DeviceIdUtil;
import com.android.manghe.utils.DeviceInfoUtil;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/12/22 16:48
 * desc   :
 */
public class TouTiaoUtils {


    /**
     * 头条广告主
     * @param context
     * @param eventType
     */
    public static void touTiaoCallBack(Context context, int eventType) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("DEVICEID", DeviceIdUtil.getUniqueId(context));
        headMap.put("PLATFORM", "android");
        HashMap<String, String> data = new HashMap<>();
        data.put("mac", MD5.md5(NetWorkUtil.getLocalMacAddress(context).replace(":", "")));
        data.put("event_type", eventType + "");
        new OKHttpUtil(context).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.TouTiaoCallBack, headMap)
                .post(data)
                .build()
                .execute(new BaseBack<NetBean>() {
                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        SPUtil.put(context, SPTag.TAG_TouTiao, true);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }
                });
    }


    /**
     * 腾讯广告主
     * @param context
     */
    public static void TentCentCallBack(Context context) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("DEVICEID", DeviceIdUtil.getUniqueId(context));
        headMap.put("PLATFORM", "android");
        HashMap<String, String> data = new HashMap<>();
        data.put("imei", DeviceInfoUtil.getIMEIDeviceId(context));
        data.put("android_id", DeviceInfoUtil.getAndroidID(context));
        data.put("mac",NetWorkUtil.getLocalMacAddress(context).replace(":", ""));
        data.put("user_agent",DeviceInfoUtil.getUser_agent());
        new OKHttpUtil(context).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Box_Advertisement_Relation,headMap)
                .post(data)
                .build()
                .execute(new BaseBack<NetBean>() {
                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        Log.e("TentCentCallBack",res.msg);
                        SPUtil.put(context, SPTag.TAG_TentCent, true);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }
                });
    }

    /**
     * 推啊
     * @param context
     */
    public static void TuiACallBack(Context context) {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("DEVICEID", DeviceIdUtil.getUniqueId(context));
        headMap.put("PLATFORM", "android");
        HashMap<String, String> data = new HashMap<>();
        data.put("imei", DeviceInfoUtil.getIMEIDeviceId(context));
        data.put("user_agent",DeviceInfoUtil.getUser_agent());
        new OKHttpUtil(context).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.TuiA,headMap)
                .post(data)
                .build()
                .execute(new BaseBack<NetBean>() {
                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        Log.e("TuiACallBack",res.msg);
                        SPUtil.put(context, SPTag.TAG_TuiA, true);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }
                });
    }
}
