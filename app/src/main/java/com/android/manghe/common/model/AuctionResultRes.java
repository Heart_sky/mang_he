package com.android.manghe.common.model;

public class AuctionResultRes {
    public String type;
    public String  id;
    public int period;
    public float price;
    public int start;
    public int time;
    public int mid;
    public String username;
    public float last_price;
    public String title;
}
