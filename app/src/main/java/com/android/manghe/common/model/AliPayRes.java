package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class AliPayRes extends NetBean<AliPayRes.DataBean> {

    public static class DataBean {
        /**
         * paycode : alipay_sdk=alipay-sdk-php-20161101&app_id=2017051007189701&biz_content=%7B%22body%22%3A%22%5Cu5728%5Cu7ebf%5Cu652f%5Cu4ed8%22%2C%22subject%22%3A%222017091809403454994810%22%2C%22out_trade_no%22%3A%2220170918094034549948101025%22%2C%22total_amount%22%3A30%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=UTF-8&format=json&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Fjm.lnest.cc%2Fwelcome%2Fapprespond%2Falipayapp&sign_type=RSA2&timestamp=2017-09-18+09%3A47%3A46&version=1.0&sign=Bc3YHIazVCrp9wwSXJdOvPSesB2vClevGNa%2Fa4pVKRYqSykf6iMfBF7YPJfDV8i2Tq%2F%2FkZ0y5eUk7RbZHbmFlpgqBcTOu3nb6zIT%2BHu%2Bppnc4hF5LSDWIbpwN5bb3YkRnAiCYbq4UUD8kd3UUzYYaswABDgdTRQj79HX67DzaZhj4kESE%2BTcwRBvaGpMklaLNZDJFCkdYOyVTa785dAt3tPiEfYBaRNOMLRRcteCQ8ytAHctImxDNikO64ihDhyV%2F2N%2Fx%2B4lv8x2n7pPZ3Pn%2BxtYlcmX6PgdJOUPh%2BEhFfk5XXeWTUHo1aWZLBaeJT6gklQuQ1u71LL2KtpWMbrkdA%3D%3D
         */

        public String paycode;
        public String log_id;//支付商品的时候才有这个参数
        public OrderInfo order;//充值的时候才有这个参数
    }

    public static class OrderInfo {
        /**
         * id : 542
         * mid : 106171
         * order_id : 542
         * good_id : 114
         * goods_name : 测试
         * goods_spec :
         * spec :
         * buy_num : 1
         * cost_price : 10.00
         * sell_price : 2.00
         * score_price : 10
         * c_time : 1560408223
         * type : 1
         * obj_id : 0
         * goods_info : null
         * extension_id : 0
         * share_id : 0
         * verify_code_id : 0
         * team_num : 0
         * express_id : null
         * goods_id : 114
         * goods_market_price : 10.00
         * goods_discount_type : 0
         * goods_discount_amount : 0.00
         * goods_sid : 0
         * thumb : [{"path":"\/upload\/1\/images\/gallery\/9\/e\/13299_src.jpg","title":""}]
         * thumbs : [{"path":"\/upload\/1\/images\/gallery\/9\/e\/13299_src.jpg","title":""}]
         * img_src : https://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg
         * img_cover : https://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg
         */

        public String log_id;
    }
}
