package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class PaymentTypeRes extends NetBean<PaymentTypeRes.DataBean> {
    public static class DataBean {
        public List<PaymentBean> payment;

        public List<PaymentBean> getPayment() {
            return payment;
        }

        public void setPayment(List<PaymentBean> payment) {
            this.payment = payment;
        }

        public static class PaymentBean {
            public String pay_id;
            public String pay_code;
            public String pay_name;
            public String pay_fee;
            public String pay_desc;
            public String pay_order;
            public String enabled;
            public String is_cod;
            public String is_online;
            public String thumb;
            public boolean isRecommended;
        }
    }
}
