package com.android.manghe.common.frame.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.tools.TUtil;

import java.util.Map;

/**
 * 功能：界面跳转，值传递，初始化Presenter
 * Created by Sammie on 2017/9/19.
 */

public abstract class MVPNoTitleActivity<T extends XPresenter> extends ExtraNoTitleActivity {
    private T presenter;

    protected void open(Class clazz) {
        open(clazz, null);
    }

    protected void open(Class clazz, Map<String, Object> object) {
        startActivity(getParamIntent(clazz,object));
    }
    protected void open(Class clazz, Map<String, Object> object,int reqCode) {
        startActivityForResult(getParamIntent(clazz,object),reqCode);
    }

    private Intent getParamIntent(Class clazz, Map<String, Object> object){
        Intent intent = new Intent(this, clazz);
        if (object != null) {
            for (Map.Entry<String, Object> entry : object.entrySet()) {
                if (entry.getValue() instanceof String){
                    intent.putExtra(entry.getKey(),(String)entry.getValue());
                }else if(entry.getValue() instanceof Integer){
                    intent.putExtra(entry.getKey(),(Integer)entry.getValue());
                }else if(entry.getValue() instanceof Float){
                    intent.putExtra(entry.getKey(),(Float)entry.getValue());
                }else if(entry.getValue() instanceof Long){
                    intent.putExtra(entry.getKey(),(Long)entry.getValue());
                }else if(entry.getValue() instanceof Boolean){
                    intent.putExtra(entry.getKey(),(Boolean)entry.getValue());
                }
            }
        }
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        presenter = TUtil.getT(MVPNoTitleActivity.this, 0);
        if (presenter != null) {
            presenter.attachView(this);
        }
        super.onCreate(savedInstanceState);

        initData(savedInstanceState);
    }

    public T getP() {
        return presenter;
    }
}
