package com.android.manghe.common.model;

import java.io.Serializable;
import java.util.List;

public class JoinAuctionSocketRes {

    public String type;
    public int id;
    public int mid;
    public float price;
    public String username;
    public int time;
    public String code;
    public String msg;
    public String error;
    public List<SortBean> sort;

    public static class SortBean implements Serializable {
        public int mid;
        public String photo;
        public String username;
        public String count;
        public int itemType;
    }
}
