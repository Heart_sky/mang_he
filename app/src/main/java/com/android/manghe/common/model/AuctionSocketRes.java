package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.EmptyUtil;

import java.text.DecimalFormat;
import java.util.List;

public class AuctionSocketRes extends NetBean {

    public String type;
    public int list_total;
    public List<ListBean> list;

    public static class ListBean{
        public String id;
        public String period;
        public String title;
        public String thumb;
        private double price;
        private double market_price;
        public double safe_price;
        public int time;
        public int start;
        public String mid;
        public String  username;
        public long  startTime;
        public long  endTime;

        public ListBean(String id){
            this.id = id;
        }

        public String getPrice() {
            if(EmptyUtil.check(price)){
                return "0.0";
            }
            DecimalFormat decimalFormat = new DecimalFormat("0.0");
            return decimalFormat.format(price);
        }

        public String getMarketPrice() {
            if(EmptyUtil.check(market_price)){
                return "0.0";
            }
            DecimalFormat decimalFormat = new DecimalFormat("0.0");
            return decimalFormat.format(market_price);
        }
    }

}
