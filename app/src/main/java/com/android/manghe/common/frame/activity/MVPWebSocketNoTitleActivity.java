package com.android.manghe.common.frame.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.android.base.frame.presenter.XPresenter;
import com.zhangke.websocket.IWebSocketPage;
import com.zhangke.websocket.WebSocketServiceConnectManager;

public abstract class MVPWebSocketNoTitleActivity<T extends XPresenter> extends MVPNoTitleActivity<T> implements IWebSocketPage {
    private WebSocketServiceConnectManager mConnectManager;
    public boolean isCanSend = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConnectManager = new WebSocketServiceConnectManager(this, this);
        mConnectManager.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isCanSend = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isCanSend = false;
    }

    @Override
    public void sendText(String text) {
        if(isCanSend && mConnectManager != null) {
            mConnectManager.sendText(text);
        }
    }

    @Override
    public void reconnect() {
        mConnectManager.reconnect();
    }

    /**
     * 服务绑定成功时的回调，可以在此初始化数据
     */
    @Override
    public void onServiceBindSuccess() {

    }

    /**
     * WebSocket 连接成功事件
     */
    @Override
    public void onConnected() {

    }

    /**
     * WebSocket 连接出错事件
     *
     * @param cause 出错原因
     */
    @Override
    public void onConnectError(Throwable cause) {

    }

    /**
     * WebSocket 连接断开事件
     */
    @Override
    public void onDisconnected() {

    }

    @Override
    protected void onDestroy() {
        mConnectManager.onDestroy();
        super.onDestroy();
    }
}
