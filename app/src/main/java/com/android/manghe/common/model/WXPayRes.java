package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class WXPayRes extends NetBean<WXPayRes.DataBean> {

    public static class DataBean {
        /**
         * paycode : {"appid":null,"partnerid":null,"prepayid":null,"wxpackage":"Sign=WXPay","noncestr":null,"timestamp":1505698837,"sign":"E0C1A124618F5E3725DD3E7B4840D0A8"}
         */

        public PaycodeBean paycode;

        public static class PaycodeBean {
            /**
             * appid : null
             * partnerid : null
             * prepayid : null
             * wxpackage : Sign=WXPay
             * noncestr : null
             * timestamp : 1505698837
             * sign : E0C1A124618F5E3725DD3E7B4840D0A8
             */

            public String appid;
            public String partnerid;
            public String prepayid;
            public String wxpackage;
            public String noncestr;
            public int timestamp;
            public String sign;

        }
    }
}
