package com.android.manghe.common.webview;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by lindan on 18-1-15.
 * WebView参数实体
 */
public class WebViewParameter implements Serializable {
    private final String url;//要加载的URL
    private final String htmUrl;
    private final boolean reloadable;//是否允许刷新网页
    private final ERefreshWebType reloadType;// Web加载失败后，刷新页面的方式,默认下拉刷新
    private final boolean showProgress;//是否显示加载进度条
    private final boolean showWebPageTitle;//是否显示网页标题
    private final String customTitle;//要显示的自定义标题
    private final boolean canHistoryGoBackOrForward;//是否允许历史记录后退或前进
    private final HashMap<String, String> cookieMap;//请求携带的Cookie
    private final String customUserAgent;//用户自定义UserAgent
    private final boolean appendCustomUserAgent;//是否附加用户自定义UserAgent

    public WebViewParameter(WebParameterBuilder builder) {
        this.url = builder.url;
        this.htmUrl=builder.htmUt;
        this.reloadable = builder.reloadable;
        this.reloadType = builder.reloadType;
        this.showProgress = builder.showProgress;
        this.showWebPageTitle = builder.showWebPageTitle;
        this.customTitle = builder.customTitle;
        this.canHistoryGoBackOrForward = builder.canHistoryGoBackOrForward;
        this.cookieMap = builder.cookieMap;
        this.customUserAgent = builder.customUserAgent;
        this.appendCustomUserAgent = builder.appendCustomUserAgent;
    }


    public String getUrl() {
        return url;
    }

    public String getHtmUrl() {
        return htmUrl;
    }

    public boolean isReloadable() {
        return reloadable;
    }

    public ERefreshWebType getReloadType() {
        return reloadType;
    }

    public boolean isShowProgress() {
        return showProgress;
    }

    public boolean isShowWebPageTitle() {
        return showWebPageTitle;
    }

    public String getCustomTitle() {
        return customTitle;
    }

    public boolean isCanHistoryGoBackOrForward() {
        return canHistoryGoBackOrForward;
    }

    public HashMap<String, String> getCookieMap() {
        return cookieMap;
    }

    public String getCustomUserAgent() {
        return customUserAgent;
    }

    public boolean isAppendCustomUserAgent() {
        return appendCustomUserAgent;
    }

    public static class WebParameterBuilder {
        private String url;//要加载的URL
        private String htmUt;//要加载的htmUrl
        private boolean reloadable;//是否允许刷新网页
        private ERefreshWebType reloadType = ERefreshWebType.DragRefresh;// Web加载失败后，刷新页面的方式,默认下拉刷新
        private boolean showProgress;//是否显示加载进度条
        private boolean showWebPageTitle = true;//是否显示网页标题
        private String customTitle;//要显示的自定义标题
        private boolean canHistoryGoBackOrForward;//是否允许历史记录后退或前进
        private boolean allowAutoStartPlayMedia = true;//是否允许自动播放背景音乐
        private HashMap<String, String> cookieMap;//请求携带的Cookie
        private String customUserAgent;//用户自定义UserAgent
        private boolean appendCustomUserAgent = true;//是否附加用户自定义UserAgent
        private boolean canChooseLocalFile;//是否可以选取本地文件

        public WebParameterBuilder setUrl(String url) {
            this.url = url;
            cookieMap = new HashMap<>();
            return this;
        }


        public WebParameterBuilder setHtmlUrl(String url) {
            this.htmUt = url;
            cookieMap = new HashMap<>();
            return this;
        }


        public WebParameterBuilder setReloadable(boolean reloadable) {
            this.reloadable = reloadable;
            return this;
        }

        public WebParameterBuilder setReloadType(ERefreshWebType reloadType) {
            this.reloadType = reloadType;
            return this;
        }

        public WebParameterBuilder setShowProgress(boolean showProgress) {
            this.showProgress = showProgress;
            return this;
        }

        public WebParameterBuilder setShowWebPageTitle(boolean showWebPageTitle) {
            this.showWebPageTitle = showWebPageTitle;
            return this;
        }

        public WebParameterBuilder setCustomTitle(String customTitle) {
            this.customTitle = customTitle;
            return this;
        }

        public WebParameterBuilder setCanHistoryGoBackOrForward(boolean canHistoryGoBackOrForward) {
            this.canHistoryGoBackOrForward = canHistoryGoBackOrForward;
            return this;
        }

        public WebParameterBuilder setAllowAutoStartPlayMedia(boolean allowAutoStartPlayMedia) {
            this.allowAutoStartPlayMedia = allowAutoStartPlayMedia;
            return this;
        }

        public WebParameterBuilder addCookie(String name, String cookie) {
            cookieMap.put(name, cookie);
            return this;
        }

        public WebParameterBuilder setCustomUserAgent(String customUserAgent, boolean append) {
            this.customUserAgent = customUserAgent;
            this.appendCustomUserAgent = append;
            return this;
        }

        public WebParameterBuilder setCanChooseLocalFile(boolean canChooseLocalFile) {
            this.canChooseLocalFile = canChooseLocalFile;
            return this;
        }

        public WebViewParameter build() {
            return new WebViewParameter(this);
        }
    }
}
