package com.android.manghe.common.webview;

import java.io.Serializable;

/**
* Web加载失败后，刷新页面的方式
* @author ZhangWeijun
* @time 2018/6/8 17:28
*/
public enum ERefreshWebType implements Serializable {
    DragRefresh,//下拉刷新
    ClickRefresh//点击刷新按钮
}
