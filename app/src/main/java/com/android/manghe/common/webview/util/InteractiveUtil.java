package com.android.manghe.common.webview.util;

import android.text.TextUtils;
import com.android.manghe.common.webview.EInteractiveActionType;
import org.json.JSONObject;


/**
* java js交互工具类
* @author ZhangWeijun
* @time 2018/6/20 15:09
*/
public class InteractiveUtil {

    public synchronized static EInteractiveActionType convertActionType(String json){
        if(TextUtils.isEmpty(json)){
            return null;
        }
        try{
            JSONObject jsonObject = new JSONObject(json);
            if(jsonObject.has("actionType")){
                String actionType = jsonObject.getString("actionType");
                if(TextUtils.equals(actionType,"ToPay")){
                    return EInteractiveActionType.ToPay;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
