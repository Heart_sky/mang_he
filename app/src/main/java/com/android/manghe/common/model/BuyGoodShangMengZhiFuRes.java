package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

public class BuyGoodShangMengZhiFuRes extends NetBean<BuyGoodShangMengZhiFuRes.DataBean> {


    public static class DataBean  implements Serializable {
        /**
         * paycode : {"bank_list":"[{\"bank_code\":\"ABC\",\"bank_name\":\"农业银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"50000\",\"month_amount_limit\":\"500000\",\"single_amount_limit\":\"20000\"},{\"bank_code\":\"ICBC\",\"bank_name\":\"工商银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"200000\",\"month_amount_limit\":\"500000\",\"single_amount_limit\":\"10000\"},{\"bank_code\":\"ICBC\",\"bank_name\":\"工商银行\",\"business_code\":\"03\",\"card_type\":\"1\",\"day_amount_limit\":\"444444444\",\"month_amount_limit\":\"333333333\",\"single_amount_limit\":\"444444444\"},{\"bank_code\":\"CCB\",\"bank_name\":\"建设银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"222222\",\"month_amount_limit\":\"5555555\",\"single_amount_limit\":\"222222\"}]","binded_card_list":"[{\"bank_code\":\"ICBC\",\"bank_name\":\"工商银行\",\"bind_card_id\":\"118908214170\",\"business_code\":\"03\",\"card_no\":\"3017\",\"card_type\":\"0\",\"day_amount_limit\":\"200000\",\"month_amount_limit\":\"500000\",\"single_amount_limit\":\"10000\"}]","resp_code":"000000","sign":"","sign_type":""}
         * log_id : 161265
         * order : {"mid":"464267","order_sn":"2021010710542910152535","order_amount":35.79,"log_id":161265,"out_trade_no":"20210107105436_161265","items":[{"service_identify":"10000001","subject":"巧茗堂 防噪音睡眠隔音耳塞滤波降噪睡觉专用学生宿舍游泳防打呼噜耳罩飞机降噪静音减压 冰川蓝","product_type":"3","goods_count":"1"}]}
         */

        public PaycodeBean paycode;
        public Integer log_id;
        public OrderBean order;

        public static class PaycodeBean  implements Serializable{
            /**
             * bank_list : [{"bank_code":"ABC","bank_name":"农业银行","business_code":"03","card_type":"0","day_amount_limit":"50000","month_amount_limit":"500000","single_amount_limit":"20000"},{"bank_code":"ICBC","bank_name":"工商银行","business_code":"03","card_type":"0","day_amount_limit":"200000","month_amount_limit":"500000","single_amount_limit":"10000"},{"bank_code":"ICBC","bank_name":"工商银行","business_code":"03","card_type":"1","day_amount_limit":"444444444","month_amount_limit":"333333333","single_amount_limit":"444444444"},{"bank_code":"CCB","bank_name":"建设银行","business_code":"03","card_type":"0","day_amount_limit":"222222","month_amount_limit":"5555555","single_amount_limit":"222222"}]
             * binded_card_list : [{"bank_code":"ICBC","bank_name":"工商银行","bind_card_id":"118908214170","business_code":"03","card_no":"3017","card_type":"0","day_amount_limit":"200000","month_amount_limit":"500000","single_amount_limit":"10000"}]
             * resp_code : 000000
             * sign :
             * sign_type :
             */

            public String bank_list;
            public String binded_card_list;
            public String resp_code;
            public String sign;
            public String sign_type;
        }

        public static class OrderBean  implements Serializable{
            /**
             * mid : 464267
             * order_sn : 2021010710542910152535
             * order_amount : 35.79
             * log_id : 161265
             * out_trade_no : 20210107105436_161265
             * items : [{"service_identify":"10000001","subject":"巧茗堂 防噪音睡眠隔音耳塞滤波降噪睡觉专用学生宿舍游泳防打呼噜耳罩飞机降噪静音减压 冰川蓝","product_type":"3","goods_count":"1"}]
             */

            public String mid;
            public String order_sn;
            public Double order_amount;
            public Integer log_id;
            public String out_trade_no;
            public Integer c_time;
            public List<ItemsBean> items;

            public static class ItemsBean  implements Serializable{
                /**
                 * service_identify : 10000001
                 * subject : 巧茗堂 防噪音睡眠隔音耳塞滤波降噪睡觉专用学生宿舍游泳防打呼噜耳罩飞机降噪静音减压 冰川蓝
                 * product_type : 3
                 * goods_count : 1
                 */

                public String service_identify;
                public String subject;
                public String product_type;
                public String goods_count;
            }
        }
    }
}
