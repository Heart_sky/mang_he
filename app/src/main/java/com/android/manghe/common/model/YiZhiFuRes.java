package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class YiZhiFuRes extends NetBean<YiZhiFuRes.DataBean> {

    public static class DataBean {
        /**
         * paycode : alipay_sdk=alipay-sdk-php-20161101&app_id=2017051007189701&biz_content=%7B%22body%22%3A%22%5Cu5728%5Cu7ebf%5Cu652f%5Cu4ed8%22%2C%22subject%22%3A%222017091809403454994810%22%2C%22out_trade_no%22%3A%2220170918094034549948101025%22%2C%22total_amount%22%3A30%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&charset=UTF-8&format=json&method=alipay.trade.app.pay&notify_url=http%3A%2F%2Fjm.lnest.cc%2Fwelcome%2Fapprespond%2Falipayapp&sign_type=RSA2&timestamp=2017-09-18+09%3A47%3A46&version=1.0&sign=Bc3YHIazVCrp9wwSXJdOvPSesB2vClevGNa%2Fa4pVKRYqSykf6iMfBF7YPJfDV8i2Tq%2F%2FkZ0y5eUk7RbZHbmFlpgqBcTOu3nb6zIT%2BHu%2Bppnc4hF5LSDWIbpwN5bb3YkRnAiCYbq4UUD8kd3UUzYYaswABDgdTRQj79HX67DzaZhj4kESE%2BTcwRBvaGpMklaLNZDJFCkdYOyVTa785dAt3tPiEfYBaRNOMLRRcteCQ8ytAHctImxDNikO64ihDhyV%2F2N%2Fx%2B4lv8x2n7pPZ3Pn%2BxtYlcmX6PgdJOUPh%2BEhFfk5XXeWTUHo1aWZLBaeJT6gklQuQ1u71LL2KtpWMbrkdA%3D%3D
         */

        public PayCode paycode;
    }

    public static class PayCode {

        /**
         * institutionCode : 3178035283177702
         * institutionType : MERCHANT
         * merchantNo : 3178035283177702
         * outTradeNo : 20201228114658_158361
         * platform : ios_4.0
         * signType : CA
         * tradeAmt : 29900
         * tradeDesc : 萌象黑金会员卡
         * tradeNo : 20201228100000210002109495949724
         * tradeType : acquiring
         * sign : w/NRdgl6eCQp3Bvm7wuEXy1YnGN3NSl79X94g5x9v/DLC+yYfHQNxsmgXOda3qfHwIUb6J91w09L4xOSIMjXapDJJO3hoXKhyQKwi3Vv5H8vvZr2Y53E6r2fIDndBlWfIemOlFFEEYBaGHGPvp8KExN8yYdmoMSPzpj/0PUCuT0=
         */

        public String institutionCode;
        public String institutionType;
        public String merchantNo;
        public String outTradeNo;
        public String platform;
        public String signType;
        public String tradeAmt;
        public String tradeDesc;
        public String tradeNo;
        public String tradeType;
        public String sign;
    }
}
