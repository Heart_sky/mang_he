package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class RecommendSecondLevelInfo extends NetBean<List<RecommendSecondLevelInfo>> {
    public String id;
    public String catname;
    public String subname;
    public String parentid;
    public String arrparentid;
    public String arrchildid;
    public String thumb;
    public String child;
}