package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

public class BuyMemberShangMengZhiFuRes extends NetBean<BuyMemberShangMengZhiFuRes.DataBean> {


    /**
     * 1/images/gallery/5/0/2data : {"paycode":{"bank_list":"[{\"bank_code\":\"ABC\",\"bank_name\":\"农业银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"50000\",\"month_amount_limit\":\"500000\",\"single_amount_limit\":\"20000\"},{\"bank_code\":\"ICBC\",\"bank_name\":\"工商银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"200000\",\"month_amount_limit\":\"500000\",\"single_amount_limit\":\"10000\"},{\"bank_code\":\"ICBC\",\"bank_name\":\"工商银行\",\"business_code\":\"03\",\"card_type\":\"1\",\"day_amount_limit\":\"444444444\",\"month_amount_limit\":\"333333333\",\"single_amount_limit\":\"444444444\"},{\"bank_code\":\"CCB\",\"bank_name\":\"建设银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"222222\",\"month_amount_limit\":\"5555555\",\"single_amount_limit\":\"222222\"}]","resp_code":"000000","sign":"","sign_type":""},"order":{"bid":"0","voucher_id":"93","mid":464267,"title":"萌象黑金会员卡","thumb":"https://www.youtu888.com/upload/8693_src.png","summary":"玩转萌象赢好礼！","content":"黑金会员卡","mobile":" ","c_time":1609771618,"start_time":"0","end_time":"0","pay_id":69,"pay_time":0,"count":1,"cost_price":"0.00","cost_amount":0,"price":"299.00","amount":286.2,"coin":"0","free_coin":"0","coin_total":0,"status":1,"favourable_value":12.8,"order_id":152061,"log_id":160488,"out_trade_no":"20210104224658_160488","order_sn":"20210104224658_160488","goods_name":"萌象黑金会员卡","order_amount":286.2,"items":[{"service_identify":"10000001","subject":"萌象黑金会员卡","product_type":"3","goods_count":1}]}}
     * flag : true
     * code : 0
     * msg : 操作成功
     * time : 1609771618
     */

    public static class DataBean implements Serializable {
        /**
         * paycode : {"bank_list":"[{\"bank_code\":\"ABC\",\"bank_name\":\"农业银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"50000\",\"month_amount_limit\":\"500000\",\"single_amount_limit\":\"20000\"},{\"bank_code\":\"ICBC\",\"bank_name\":\"工商银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"200000\",\"month_amount_limit\":\"500000\",\"single_amount_limit\":\"10000\"},{\"bank_code\":\"ICBC\",\"bank_name\":\"工商银行\",\"business_code\":\"03\",\"card_type\":\"1\",\"day_amount_limit\":\"444444444\",\"month_amount_limit\":\"333333333\",\"single_amount_limit\":\"444444444\"},{\"bank_code\":\"CCB\",\"bank_name\":\"建设银行\",\"business_code\":\"03\",\"card_type\":\"0\",\"day_amount_limit\":\"222222\",\"month_amount_limit\":\"5555555\",\"single_amount_limit\":\"222222\"}]","resp_code":"000000","sign":"","sign_type":""}
         * order : {"bid":"0","voucher_id":"93","mid":464267,"title":"萌象黑金会员卡","thumb":"https://www.youtu888.com/upload/1/images/gallery/5/0/28693_src.png","summary":"玩转萌象赢好礼！","content":"黑金会员卡","mobile":" ","c_time":1609771618,"start_time":"0","end_time":"0","pay_id":69,"pay_time":0,"count":1,"cost_price":"0.00","cost_amount":0,"price":"299.00","amount":286.2,"coin":"0","free_coin":"0","coin_total":0,"status":1,"favourable_value":12.8,"order_id":152061,"log_id":160488,"out_trade_no":"20210104224658_160488","order_sn":"20210104224658_160488","goods_name":"萌象黑金会员卡","order_amount":286.2,"items":[{"service_identify":"10000001","subject":"萌象黑金会员卡","product_type":"3","goods_count":1}]}
         */

        public PaycodeBean paycode;
        public OrderBean order;

        public static class PaycodeBean implements Serializable {
            /**
             * bank_list : [{"bank_code":"ABC","bank_name":"农业银行","business_code":"03","card_type":"0","day_amount_limit":"50000","month_amount_limit":"500000","single_amount_limit":"20000"},{"bank_code":"ICBC","bank_name":"工商银行","business_code":"03","card_type":"0","day_amount_limit":"200000","month_amount_limit":"500000","single_amount_limit":"10000"},{"bank_code":"ICBC","bank_name":"工商银行","business_code":"03","card_type":"1","day_amount_limit":"444444444","month_amount_limit":"333333333","single_amount_limit":"444444444"},{"bank_code":"CCB","bank_name":"建设银行","business_code":"03","card_type":"0","day_amount_limit":"222222","month_amount_limit":"5555555","single_amount_limit":"222222"}]
             * resp_code : 000000
             * sign :
             * sign_type :
             */

            public String bank_list;
            public String resp_code;
            public String sign;
            public String sign_type;

            public String binded_card_list;
        }

        public static class OrderBean implements Serializable {
            /**
             * bid : 0
             * voucher_id : 93
             * mid : 464267
             * title : 萌象黑金会员卡
             * thumb : https://www.youtu888.com/upload/1/images/gallery/5/0/28693_src.png
             * summary : 玩转萌象赢好礼！
             * content : 黑金会员卡
             * mobile :
             * c_time : 1609771618
             * start_time : 0
             * end_time : 0
             * pay_id : 69
             * pay_time : 0
             * count : 1
             * cost_price : 0.00
             * cost_amount : 0
             * price : 299.00
             * amount : 286.2
             * coin : 0
             * free_coin : 0
             * coin_total : 0
             * status : 1
             * favourable_value : 12.8
             * order_id : 152061
             * log_id : 160488
             * out_trade_no : 20210104224658_160488
             * order_sn : 20210104224658_160488
             * goods_name : 萌象黑金会员卡
             * order_amount : 286.2
             * items : [{"service_identify":"10000001","subject":"萌象黑金会员卡","product_type":"3","goods_count":1}]
             */

            public String bid;
            public String voucher_id;
            public Integer mid;
            public String title;
            public String thumb;
            public String summary;
            public String content;
            public String mobile;
            public Integer c_time;
            public String start_time;
            public String end_time;
            public Integer pay_id;
            public Integer pay_time;
            public Integer count;
            public String cost_price;
            public Integer cost_amount;
            public String price;
            public Double amount;
            public String coin;
            public String free_coin;
            public Integer coin_total;
            public Integer status;
            public Double favourable_value;
            public Integer order_id;
            public Integer log_id;
            public String out_trade_no;
            public String order_sn;
            public String goods_name;
            public Double order_amount;
            public List<ItemsBean> items;

            public static class ItemsBean implements Serializable {
                /**
                 * service_identify : 10000001
                 * subject : 萌象黑金会员卡
                 * product_type : 3
                 * goods_count : 1
                 */

                public String service_identify;
                public String subject;
                public String product_type;
                public Integer goods_count;
            }
        }
    }
}
