package com.android.manghe.common.frame.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.frame.view.MVPActivity;
import com.zhangke.websocket.IWebSocketPage;
import com.zhangke.websocket.WebSocketServiceConnectManager;

public abstract class MVPWebSocketActivity<T extends XPresenter> extends MVPActivity<T> implements IWebSocketPage {
    private WebSocketServiceConnectManager mConnectManager;
    public boolean isCanSend = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            mConnectManager = new WebSocketServiceConnectManager(this, this);
            mConnectManager.onCreate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isCanSend = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isCanSend = false;
    }

    @Override
    public void sendText(String text) {
        try{
            if(isCanSend) {
                mConnectManager.sendText(text);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void reconnect() {
        try{
            mConnectManager.reconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 服务绑定成功时的回调，可以在此初始化数据
     */
    @Override
    public void onServiceBindSuccess() {

    }

    /**
     * WebSocket 连接成功事件
     */
    @Override
    public void onConnected() {

    }

    /**
     * WebSocket 连接出错事件
     *
     * @param cause 出错原因
     */
    @Override
    public void onConnectError(Throwable cause) {

    }

    /**
     * WebSocket 连接断开事件
     */
    @Override
    public void onDisconnected() {

    }

    @Override
    protected void onDestroy() {
        try{
            mConnectManager.onDestroy();
        }catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
