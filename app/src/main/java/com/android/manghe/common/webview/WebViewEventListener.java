package com.android.manghe.common.webview;

import android.graphics.Bitmap;
import android.webkit.WebView;

/**
 * Created by lindan on 18-1-15.
 * WebView事件监听器
 */
public interface WebViewEventListener {
    default void onProgressChange(int progress) {
    }

    default void onReceivedTitle(String title) {
    }

    default void onPageFinished(WebView view, String url) {
    }

    default void onPageStarted(String url, Bitmap favicon) {
    }

    default void onLoadError() {
    }

    default boolean shouldOverrideUrlLoading(String url) {
        return false;
    }

    default boolean onDownload(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
        return false;
    }

    default void jsCallJava(String jsonParam) {
    }

    default void goToLogin(){}
}
