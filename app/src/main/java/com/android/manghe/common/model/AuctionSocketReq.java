package com.android.manghe.common.model;

public class AuctionSocketReq {
    public String type;
    public String id;
    public int  page;
    public int  per_page;
    public int cid;
    public int mid;
    public int for_new;
    public String token;
}
