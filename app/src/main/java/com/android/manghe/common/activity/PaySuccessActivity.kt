package com.android.manghe.common.activity

import android.os.Bundle
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.cache.StatusHolder
import com.android.manghe.cache.UserHolder
import com.android.manghe.config.Config
import com.android.manghe.config.events.PaySuccessEvent
import com.android.manghe.config.events.RefreshOrdersEvent
import com.android.manghe.main.MainActivity
import com.android.manghe.mine.activity.MyKanActivity
import com.android.manghe.mine.activity.MyOrderActivity
import com.android.manghe.utils.MoneyUtil
import com.eightbitlab.rxbus.Bus
import com.fm.openinstall.OpenInstall
import com.umeng.analytics.MobclickAgent
import kotlinx.android.synthetic.main.activity_pay_success.*
import java.util.HashMap

class PaySuccessActivity : MVPActivity<XPresenter<IView>>() {
    override fun getLayoutId(): Int {
        return R.layout.activity_pay_success
    }

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun initData(savedInstanceState: Bundle?) {
        try {
            Bus.send(RefreshOrdersEvent())
            Bus.send(PaySuccessEvent())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        tvBackToIndex.setOnClickListener {
            finish()
            open(MainActivity::class.java, hashMapOf<String, Any>("isToChangeTab" to true, "position" to 4))
        }
        tvCheckMoney.setOnClickListener {
            finish()
            when (StatusHolder.mCurrentPayType) {
                0 -> {
                    //充值
                    open(MainActivity::class.java, hashMapOf<String, Any>("isToChangeTab" to true, "position" to 4))
                }
                1 -> {
                    //竞拍
                    open(MyKanActivity::class.java)
                }
                2 -> {
                    //商城
                    open(MyOrderActivity::class.java)
                }
            }
        }
        when (StatusHolder.mCurrentPayType) {
            0 -> {
                //充值
                tvCheckMoney.text = "查看余额"
                OpenInstall.reportEffectPoint(
                    Config.OpenInstall_Voucher,
                    MoneyUtil.changeY2F(StatusHolder.mCurrentVoucherPrice).toLong()
                )

                val mapValue = HashMap<String, String>()
                mapValue["charge"] = "1"
                MobclickAgent.onEventValue(
                    this,
                    "charge",
                    mapValue,
                    MoneyUtil.changeY2F(StatusHolder.mCurrentVoucherPrice)
                )

                try {
                    val successPayMap = HashMap<String, String>()
                    successPayMap.put("userid", UserHolder.getUID(this))
                    successPayMap.put("orderid", System.currentTimeMillis().toString())
                    successPayMap.put("item", "购券")
                    successPayMap.put("amount", MoneyUtil.changeY2F(StatusHolder.mCurrentVoucherPrice).toString())
                    MobclickAgent.onEvent(this, "__finish_payment", successPayMap)
                } catch (e: Exception) {
                }

            }
            1 -> {
                //竞拍
                tvCheckMoney.text = "查看订单"
            }
            2 -> {
                //商城
                tvCheckMoney.text = "查看订单"
            }
        }
    }

}
