package com.android.manghe.common.activity

import android.os.Bundle
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.config.events.PaySuccessEvent
import com.android.manghe.config.events.RefreshOrdersEvent
import com.android.manghe.main.MainActivity
import com.eightbitlab.rxbus.Bus
import kotlinx.android.synthetic.main.activity_pay_fail.*

class PayFailActivity : MVPActivity<XPresenter<IView>>() {
    override fun getLayoutId(): Int {
        return R.layout.activity_pay_fail
    }

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun initData(savedInstanceState: Bundle?) {
        try {
            Bus.send(RefreshOrdersEvent())
            Bus.send(PaySuccessEvent())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        tvBackToMe.setOnClickListener {
            finish()
            open(MainActivity::class.java, hashMapOf<String, Any>("isToChangeTab" to true, "position" to 4))
        }
        tvBackToIndex.setOnClickListener {
            finish()
            open(MainActivity::class.java, hashMapOf<String, Any>("isToChangeTab" to true, "position" to 0))
        }
    }

}
