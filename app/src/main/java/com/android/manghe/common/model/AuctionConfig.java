package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

public class AuctionConfig extends NetBean<AuctionConfig.DataBean> {
    public static class DataBean implements Serializable {
        /**
         * site_name : 港湾有巢趣买
         * seo_title : 港湾有巢趣买
         * seo_keywords : 港湾有巢趣买
         * seo_description : 港湾有巢趣买
         * cloudurl2 :
         * mail_type : 1
         * mail_server : smtp.qq.com
         * cloudurl :
         * bucketname :
         * page_listrows : 15
         * rank_points_login : 5
         * pay_points_order : 1
         * freight_free :
         * logCount : 390691
         * weibo_login : 1
         * page_size : 15
         * qq_login : 1
         * sms_mod : 0
         * mail_open : 0
         * sms_open : 1
         * sms_type : 1
         * order_share : 0
         * order_back : 0
         * rank_points_info : 20
         * isPhoto : 2
         * isVoice : 1
         * isIdcard : 1
         * isMail : 1
         * isDaren : 50
         * isJpDaren : 50
         * voice_open : 1
         * isagree : 0
         * comss : 1
         * keywords : 商品、品牌、种类
         * time_home : 0
         * withdraw_fee : 0.5%|一个工作日
         * 0.3%|三个工作日
         * 0|一周内
         * business_tel : 400-966-0902
         * goods_isSold : 0
         * goods_isDisplayPrice : 1
         * goods_isComment : 1
         * goods_isBuyNeedId : 0
         * refund_days : 7
         * is_rate : 0
         * agent_province : 10
         * agent_city : 5
         * is_upgrade_status : 1
         * is_area : 0
         * confirm_days : 7
         * cloudsave : 0
         * contact_email :
         * contact_hotline :
         * reg_bind_sms : 0
         * prov_sl : 4
         * comm_time : 1
         * goods_comm : 0
         * unit : 件
         * lottery_rule : <p>活动时间【开始时间】-【结束时间】;</p>
         * <p>1)【结束时间】后，从拼团成功用户中随机抽取【中奖人数】个中奖用户;</p>
         * <p>2)运气爆棚奖【中奖人数】名：【标题】，【优惠券】;</p>
         * <p>3)中奖用户会在名单公布后，尽快为您完成发货;</p>
         * <p>4)未中奖用户抽奖结束后为您发起全额退款，请耐心等待到账</p>
         * <p>5)中奖名单公布可以首页抽奖里查看呦~</p>
         * free_rule : <p>活动时间【开始时间】-【结束时间】;</p>
         * <p>1)申请结束后从拼团成功的订单中随机抽取【中奖人数】名试用者赠送商品;</p>
         * <p>2)拼团成功后即可获得【优惠券】;</p>
         * <p>3)中奖用户会在名单公布后，尽快为您完成发货;</p>
         * aa_rule : <p>1)由团长指定收货地址发起团购，发起后团长需支付一人价格；</p>
         * <p>2)团长分享给好友支付所需金额，在有效时间内凑足人数购买，则卖家发货至团长指定收货地址；</p>
         * <p>3)团长负责收货，并将货品均分给团成员；</p>
         * <p>4)若有效时间内未凑齐人数购买，则团购失败，已付款项会原路退还到用户的支付帐号；</p>
         * skin_tpl : mobile
         * per_fee : 500
         * per_deposit : 5000
         * com_fee : 1000
         * com_deposit : 100000
         * app_logo : [{"path":"\/upload\/1\/source\/images\/share_logo.png","title":""}]
         * app_text : [{"path":"\/demo\/1\/images\/gallery\/p\/i\/919_src.png","title":"xz-txt1"}]
         * ios_url : https://itunes.apple.com/cn/app/%E7%88%B1%E5%B0%9A%E8%B6%A3%E4%B9%B0/id1293989627?l=zh&ls=1&mt=8
         * android_url : http://pintuancc.b0.upaiyun.com/upload/apk/inest-debug.apk
         * order_num : 8
         * copyright : Copyright©2014-2015 港湾有巢科技有限公司
         * icp_code : 闽B2-20050063号、20100037号
         * goods_list : 0
         * auction_rule : (1) 拍币来源：到购券区选适合的商家优惠券，优惠券可以到指定商家使用，购买并赠送拍币。
         * (2) 商品趣买初始价以起始价为准，每出一次出价会消耗一定数量的拍币（网络服务费），同时商品价格以加价幅度值递增。
         * (3) 在初始倒计时内即可出价，初始倒计时后进入趣买倒计时，当您出价后，该件商品的计时器将被自动重置到趣买倒计时，以便其他用户进行出价趣买。如果没有其他用户对该件商品出价，计时器归零时，您便成功获得得了该商品购买权。
         * (4) 若趣买成功，请在3天内以成交价购买商品，超过3天未下单，视为放弃，不返积分。
         * (5) 若趣买失败，可返还所消耗拍币倍率的积分（赠币除外），积分可以在商城换购商品。
         * (6) 平台严禁违规操作，最终解释权归****所有。
         * ivtad : 趣买一分钱，当两份钱花。真实惠，衣食住行全都能，省钱赚券。
         * worktime : 工作日9:00-18:00
         * app_checking : 0.0.0
         * contact_qq : 200530020
         * score_expired : 365
         * certification_expire : 0
         * certification_amount : 1000
         * contact_phone : 13500000000
         * contact_fax : 000-1110000
         * contact_address : 福建厦门湖里区软件园二期望海路41号506
         * ask_limitime : 1
         * ask_score : 10
         * view_score : 5
         * contact_lat : 118.182869,24.490305
         * auction_continuity : 0
         * sms_admin : 0
         * index_voucher_size : 5
         * subscribe_wx : 1
         * lang : {"msg_bad_data":"数据发送自未被信任的域名，如有疑问，请联系管理员","unit_buy":"立即购买","unit_cart":"加入购物车","unit_score":"积分","unit_coin":"趣豆","unit_free_coin":"赠豆","unit_voucher":"购物卡","unit_jingmai":"趣买","unit_button_go":"立即趣买","unit_gobuy":"商城"}
         */

        public String site_name;
        public String seo_title;
        public String seo_keywords;
        public String seo_description;
        public String cloudurl2;
        public String mail_type;
        public String mail_server;
        public String cloudurl;
        public String bucketname;
        public String page_listrows;
        public String rank_points_login;
        public String pay_points_order;
        public String freight_free;
        public String logCount;
        public String weibo_login;
        public String page_size;
        public String qq_login;
        public String sms_mod;
        public String mail_open;
        public String sms_open;
        public String sms_type;
        public String order_share;
        public String order_back;
        public String rank_points_info;
        public String isPhoto;
        public String isVoice;
        public String isIdcard;
        public String isMail;
        public String isDaren;
        public String isJpDaren;
        public String voice_open;
        public String isagree;
        public String comss;
        public String keywords;
        public String time_home;
        public String withdraw_fee;
        public String business_tel;
        public String goods_isSold;
        public String goods_isDisplayPrice;
        public String goods_isComment;
        public String goods_isBuyNeedId;
        public String refund_days;
        public String is_rate;
        public String agent_province;
        public String agent_city;
        public String is_upgrade_status;
        public String is_area;
        public String confirm_days;
        public String cloudsave;
        public String contact_email;
        public String contact_hotline;
        public String reg_bind_sms;
        public String prov_sl;
        public String comm_time;
        public String goods_comm;
        public String unit;
        public String lottery_rule;
        public String free_rule;
        public String aa_rule;
        public String skin_tpl;
        public String per_fee;
        public String per_deposit;
        public String com_fee;
        public String com_deposit;
        public String app_logo;
        public String app_text;
        public String ios_url;
        public String android_url;
        public String order_num;
        public String copyright;
        public String icp_code;
        public String goods_list;
        public String auction_rule;
        public String ivtad;
        public String worktime;
        public String app_checking;
        public String contact_qq;
        public String score_expired;
        public String certification_expire;
        public String certification_amount;
        public String contact_phone;
        public String contact_fax;
        public String contact_address;
        public String ask_limitime;
        public String ask_score;
        public String view_score;
        public String contact_lat;
        public String auction_continuity;
        public String sms_admin;
        public String index_voucher_size;
        public String subscribe_wx;
        public String switch_index_voucher;
        public String switch_index_team;
        public String switch_index_headline;
        public String switch_boot_animation;
        public int comment_switch;
        public int exempt_rule_switch;
        public int order_exempt_rule_switch;
        public LangBean lang;

        public static class LangBean implements Serializable {
            /**
             * msg_bad_data : 数据发送自未被信任的域名，如有疑问，请联系管理员
             * unit_buy : 立即购买
             * unit_cart : 加入购物车
             * unit_score : 积分
             * unit_coin : 趣豆
             * unit_free_coin : 赠币
             * unit_voucher : 购物卡
             * unit_jingmai : 趣买
             * unit_button_go : 立即趣买
             * unit_gobuy : 商城
             */

            public String msg_bad_data;
            public String unit_buy;
            public String unit_cart;
            public String unit_score;
            public String unit_coin;
            public String unit_free_coin;
            public String unit_voucher;
            public String unit_jingmai;
            public String unit_button_go;
            public String unit_gobuy;
            public String return_times_rate_key;
            public String return_times_rate_value;
        }
    }
}
