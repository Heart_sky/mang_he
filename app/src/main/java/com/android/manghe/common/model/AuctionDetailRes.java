package com.android.manghe.common.model;

import java.io.Serializable;
import java.util.List;

public class AuctionDetailRes {
    public String type;
    public AuctionBean auction;

    public static class AuctionBean implements Serializable {

        public int time;
        public int start;
        public int mid_count;
        public String active_time;
        public String brokerage;
        public String c_time;
        public String cid;
        public String coin_type;
        public String content;
        public String cost_price;
        public String description;
        public String end_hour;
        public String expire;
        public String express_price;
        public String first_countdown;
        public String for_new;
        public String id;
        public String is_virtual;
        public String keywords;
        public String listorder;
        public String market_price;
        public String mid;
        public String order_sn;
        public String per_countdown;
        public String per_price;
        public String period;
        public String period_interval;
        public String period_left;
        public String price;
        public String receive_deadline;
        public String returns_rate;
        public String send_time;
        public String start_hour;
        public String start_price;
        public String status;
        public String start_time;
        public String thumb;
        public List<String> thumbs;
        public String title;
        public String u_time;
        public String safe_price;
        public String safe_rate;
        public String top_price;
    }
}
