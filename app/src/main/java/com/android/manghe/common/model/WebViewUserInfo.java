package com.android.manghe.common.model;

public class WebViewUserInfo {


    /**
     * token : 0997f004224402a44ea7ab4bf823e09b
     * mid : 94137
     * uname : 15989027318
     * avatar: http://xxxxxx
     */

    public String token;
    public String mid;
    public String uname;
    public String avatar;

    public WebViewUserInfo(String token, String mid, String uname, String avatar) {
        this.token = token;
        this.mid = mid;
        this.uname = uname;
        this.avatar = avatar;
    }
}
