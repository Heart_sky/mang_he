package com.android.manghe.common.frame.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.frame.view.MVPFragment;
import com.zhangke.websocket.IWebSocketPage;
import com.zhangke.websocket.WebSocketServiceConnectManager;

public abstract class MVPWebSocketFragment<T extends XPresenter> extends MVPFragment<T> implements IWebSocketPage {

    protected WebSocketServiceConnectManager mConnectManager;

    public boolean isCanSend = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try{
            mConnectManager = new WebSocketServiceConnectManager(getActivity().getApplicationContext(), this);
            mConnectManager.onCreate();
        }catch (Exception e){
            e.printStackTrace();
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        isCanSend = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isCanSend = false;
    }

    @Override
    public void sendText(String text) {
        try {
            if (isCanSend) {
                mConnectManager.sendText(text);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void reconnect() {
        try{
            mConnectManager.reconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 服务绑定成功时的回调，可以在此初始化数据
     */
    @Override
    public void onServiceBindSuccess() {

    }

    /**
     * WebSocket 连接成功事件
     */
    @Override
    public void onConnected() {

    }

    /**
     * WebSocket 连接出错事件
     *
     * @param cause 出错原因
     */
    @Override
    public void onConnectError(Throwable cause) {

    }

    /**
     * WebSocket 连接断开事件
     */
    @Override
    public void onDisconnected() {

    }

    @Override
    public void onDestroy() {
        try{
            if(mConnectManager!= null)
                mConnectManager.onDestroy();
        }catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
