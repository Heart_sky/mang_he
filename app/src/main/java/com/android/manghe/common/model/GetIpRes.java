package com.android.manghe.common.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class GetIpRes extends NetBean<GetIpRes.DataBean> {
    public static class DataBean{
        public String ip = "";
    }

}
