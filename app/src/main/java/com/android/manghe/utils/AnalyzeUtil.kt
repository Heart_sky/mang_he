package com.android.manghe.utils

import android.content.Context
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.base.okhttp.okUtil.callback.GsonBaseBack
import com.android.manghe.cache.UserHolder
import com.android.manghe.config.ConstantsUrl

class AnalyzeUtil {
    companion object{

        fun clickRule(context: Context, pos :Int){
            OKHttpUtil(context).url(ConstantsUrl.domain + ConstantsUrl.ClickRule)
                .post("mid",UserHolder.getUID(context))
                .post("pos", "$pos")//1.产品详情页;2.确认订单页
                .build()
                .execute(object : GsonBaseBack<Any?>(){

                })
        }


    }
}