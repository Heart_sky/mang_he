package com.android.manghe.utils;

import static com.alipay.sdk.tid.TidHelper.getIMEI;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.util.UUID;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/02/11 13:53
 * desc   :
 */
public class DeviceInfoUtil {


    /**
     * 得到设备AndroidID，需要设备添加 Google账户
     *
     * @param context
     * @return
     */

    public static String getAndroidID(Context context) {
        @SuppressLint("HardwareIds") String androidID = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return androidID;

    }
    public static String getUser_agent() {
        return System.getProperty("http.agent");
    }


    /**
     * 得到设备IMEI值
     *
     * @param context
     * @return
     */

    @SuppressLint("HardwareIds")
    public static String getIMEIDeviceId(Context context) {
        String deviceId;
        TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
        }
        assert mTelephony != null;
        if (mTelephony.getDeviceId() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                deviceId = mTelephony.getImei();
            } else {
                deviceId = mTelephony.getDeviceId();
            }
        } else {
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceId;

    }

    /**
     * 得到设备序列号
     *
     * @param context
     * @return
     */

    @SuppressLint("HardwareIds")
    public static String getSimSerialNumber(Context context) {

        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
        }
        return tm.getSimSerialNumber();

    }

    /**
     * 得到设备唯一识别码
     *
     * @param context
     * @return
     */

    public static String getUniqueNumber(Context context) {

        String androidID = getAndroidID(context);

        String imei = getIMEI(context);

        String simSerialNumber = getSimSerialNumber(context);

        UUID uuid = new UUID(androidID.hashCode(),

                ((long) imei.hashCode() << 32) | simSerialNumber.hashCode());

        return uuid.toString();

    }

}
