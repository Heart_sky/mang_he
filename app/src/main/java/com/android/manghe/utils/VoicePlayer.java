package com.android.manghe.utils;

import android.content.Context;
import android.media.MediaPlayer;

import com.android.manghe.R;

public class VoicePlayer {

    static VoicePlayer instance;
    private Context context;
    private MediaPlayer mediaPlayer;

    private boolean isPause;

    public static VoicePlayer getInstance() {
        if (instance == null) {
            synchronized (VoicePlayer.class) {
                VoicePlayer temp = instance;
                if (temp == null) {
                    synchronized (VoicePlayer.class) {
                        temp = new VoicePlayer();
                    }
                    instance = temp;
                }
            }
        }
        return instance;
    }

    public void init(Context context) {
        this.context = context;
    }

    public void startPlay(int rawRes) {
        try {
            stopPlay();
            mediaPlayer = MediaPlayer.create(context, rawRes);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startPlay() {
        try {
            if (mediaPlayer != null) {
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
            } else {
                mediaPlayer = MediaPlayer.create(context, R.raw.box_bg);
                mediaPlayer.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pausePlay() {
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void stopPlay() {
        try {
            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLoop() {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.setLooping(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
