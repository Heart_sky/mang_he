package com.android.manghe.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.index.model.FunctionBean;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/22 15:01
 * desc   :
 */
public class WindowUtils {


    public static void backgroundAlpha(Activity activity, float bgAlpha) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        activity.getWindow().setAttributes(lp);
    }

    public static void setBoxLevel(Context context, RoundLinearLayout roundLinearLayout, ImageView imgLevel,
                                   TextView tvLevel, String content) {
        if (content != null)
            if (content.contains("普通款")) {
                roundLinearLayout.getDelegate().setBackgroundColor(ContextCompat.getColor(context, R.color.box_normal)).update();
                imgLevel.setImageResource(R.mipmap.box_nomal);
                tvLevel.setTextColor(ContextCompat.getColor(context, R.color.box_normal_text));
                tvLevel.setText(content);

            } else if (content.contains("至尊款")) {
                roundLinearLayout.getDelegate().setBackgroundColor(ContextCompat.getColor(context, R.color.box_zhi_zun)).update();
                imgLevel.setImageResource(R.mipmap.box_zhi_zun);
                tvLevel.setTextColor(ContextCompat.getColor(context, R.color.box_zhi_zun_text));
                tvLevel.setText(content);

            } else if (content.contains("稀有款")) {
                roundLinearLayout.getDelegate().setBackgroundColor(ContextCompat.getColor(context, R.color.box_xi_you)).update();
                imgLevel.setImageResource(R.mipmap.box_xi_you);
                tvLevel.setTextColor(ContextCompat.getColor(context, R.color.box_xi_you_text));
                tvLevel.setText(content);

            } else if (content.contains("隐藏款")) {
                roundLinearLayout.getDelegate().setBackgroundColor(ContextCompat.getColor(context, R.color.box_hide)).update();
                imgLevel.setImageResource(R.mipmap.box_hide);
                tvLevel.setTextColor(ContextCompat.getColor(context, R.color.box_hide_text));
                tvLevel.setText(content);

            }

    }

    private static int MIN_CLICK_DELAY_TIME = 1000;
    private static long lastClickTime;

    public static boolean isFastClick() {
        boolean flag = false;
        long curClickTime = System.currentTimeMillis();
        if ((curClickTime - lastClickTime) >= MIN_CLICK_DELAY_TIME) {
            flag = true;
        }
        lastClickTime = curClickTime;
        return flag;
    }

    private static boolean isStatusbarVisible(Activity activity) {
        int uiOptions = activity.getWindow().getDecorView().getSystemUiVisibility();
        boolean isStatusbarHide = ((uiOptions | View.SYSTEM_UI_FLAG_FULLSCREEN) == uiOptions);
        return !isStatusbarHide;
    }


    public static void hideStatusBar(Activity activity) {
        if (isStatusbarVisible(activity)) {
            int uiOptions = activity.getWindow().getDecorView().getSystemUiVisibility();
            uiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            activity.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        }
    }

    public static int getRawScreenH(Context context) {
        int width = 0, height = 0;
        final DisplayMetrics metrics = new DisplayMetrics();
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        // For JellyBean 4.2 (API 17) and onward
        display.getRealMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;
        return height;
    }

    public static int getRawScreenW(Context context) {
        int width = 0, height = 0;
        final DisplayMetrics metrics = new DisplayMetrics();
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        // For JellyBean 4.2 (API 17) and onward
        display.getRealMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;
        return width;
    }
}
