package com.android.manghe.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class AppUtils {

    public static  String getPackageName(Context context) {
        try {

            PackageManager packageManager = context.getPackageManager();

            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);

            return packageInfo.packageName;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String getOrderNumber() {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
        String date = simpleDateFormat.format(new Date());
        Random random = new Random();
        int ranNum = (int) (random.nextDouble() * (99999 - 10000 + 1)) + 10000;
        return date + ranNum;
    }

    public static String getOrderTime() {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
        String date = simpleDateFormat.format(new Date());
        return date;
    }

    public static String getOrderExpireTime() {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
        Calendar cal = Calendar.getInstance();
        try {
            Date date = simpleDateFormat.parse(getOrderTime());
            if (date != null) {
                cal.setTime(date);
                cal.add(Calendar.MINUTE, 30);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = simpleDateFormat.format(cal.getTime());
        return date;
    }
}
