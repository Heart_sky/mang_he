package com.android.manghe.coupon.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class CouponInputRes extends NetBean<CouponInputRes.DataBean> {


    /**
     * data : {"list":[{"id":"8","bid":"0","thumb":"https://www.ycdy8.com/upload/1/images/gallery/q/s/13925_src.jpg","title":"自选金额","cid":"0","zone":"","area":"","summary":"玩转萌象赢好礼！","content":"<span><span>赠送1砍币，全新砍价模式，砍币用于砍价使用，（根据中华人民共和国法律法规规定砍币及积分不能提现，不能转让）<\/span><\/span><span><\/span>","mobile":" ","coin":"1","cost_price":"1.00","price":"1.00","stock":"499333","sold":"667","start_time":"0","end_time":"0","c_time":"1538733287","u_time":"1566197500","listorder":"1","verify":"1","verify_reason":"","verify_time":"1566197500","status":"1","free_coin":"0","favourable_interval":"0.1-0.1","business_id":"","business_name":""}]}
     * flag : true
     */


    public static class DataBean {
        public List<ListBean> list;

        public static class ListBean {
            /**
             * id : 8
             * bid : 0
             * thumb : https://www.ycdy8.com/upload/1/images/gallery/q/s/13925_src.jpg
             * title : 自选金额
             * cid : 0
             * zone :
             * area :
             * summary : 玩转萌象赢好礼！
             * content : <span><span>赠送1砍币，全新砍价模式，砍币用于砍价使用，（根据中华人民共和国法律法规规定砍币及积分不能提现，不能转让）</span></span><span></span>
             * mobile :
             * coin : 1
             * cost_price : 1.00
             * price : 1.00
             * stock : 499333
             * sold : 667
             * start_time : 0
             * end_time : 0
             * c_time : 1538733287
             * u_time : 1566197500
             * listorder : 1
             * verify : 1
             * verify_reason :
             * verify_time : 1566197500
             * status : 1
             * free_coin : 0
             * favourable_interval : 0.1-0.1
             * business_id :
             * business_name :
             */

            public String id;
            public String bid;
            public String thumb;
            public String title;
            public String cid;
            public String zone;
            public String area;
            public String summary;
            public String content;
            public String mobile;
            public String coin;
            public String cost_price;
            public String price;
            public String stock;
            public String sold;
            public String start_time;
            public String end_time;
            public String c_time;
            public String u_time;
            public String listorder;
            public String verify;
            public String verify_reason;
            public String verify_time;
            public String status;
            public String free_coin;
            public String favourable_interval;
            public String business_id;
            public String business_name;
        }
    }
}
