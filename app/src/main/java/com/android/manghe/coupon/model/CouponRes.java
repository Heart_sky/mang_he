package com.android.manghe.coupon.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class CouponRes extends NetBean<CouponRes.DataBean> {
    public static class DataBean {
        public List<CouponRes.DataBean.CouponInfo> list;
        public String charge_declare;//充值必读

        public static class CouponInfo {
            public int itemType;
            public String id;
            public String thumb;
            public String title;
            public String summary;
            public String content;
            public String mobile;
            public String coin;
            public String free_coin;
            public String price;
            public String stock;
            public String sold;
            public String start_time;
            public String end_time;
            public String c_time;
            public String u_time;
            public String listorder;
            public String status;
        }
    }

}
