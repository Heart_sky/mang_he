package com.android.manghe.coupon.adapter;

import android.content.Context;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.common.model.PaymentTypeRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.HashMap;
import java.util.List;

public class PaymentAdapter extends BaseQuickAdapter<PaymentTypeRes.DataBean.PaymentBean, BaseViewHolder> {
    private Context mContext;
    private HashMap<String, Boolean> mSelectedMap = new HashMap<>();
    private PaymentTypeRes.DataBean.PaymentBean mSelectedItemObject;
    private boolean isShowDesc = true;

    public PaymentAdapter(Context context, List<PaymentTypeRes.DataBean.PaymentBean> list) {
        super(R.layout.item_payment, list);
        mContext = context;
    }

    public void hideDesc(){
        isShowDesc = false;
    }

    public void update(List<PaymentTypeRes.DataBean.PaymentBean> list) {
        mSelectedMap.clear();
        PaymentTypeRes.DataBean.PaymentBean info;
        for (int i = 0; i < list.size(); i++) {
            info = list.get(i);
            mSelectedMap.put(info.pay_id, i == 0);
            if (i == 0) {
                info.isRecommended = true;
                list.set(0, info);
                mSelectedItemObject = info;
            }
        }
        replaceData(list);
    }

    public void selectedItem(PaymentTypeRes.DataBean.PaymentBean info) {
        for (int i = 0; i < getData().size(); i++) {
            mSelectedMap.put(getData().get(i).pay_id, false);
        }
        mSelectedMap.put(info.pay_id, true);
        mSelectedItemObject = info;
        notifyDataSetChanged();
    }

    public PaymentTypeRes.DataBean.PaymentBean getSelectedPayment() {
        return mSelectedItemObject;
    }

    @Override
    protected void convert(BaseViewHolder holder, PaymentTypeRes.DataBean.PaymentBean item) {
        holder.setText(R.id.tvName, item.pay_name);
        holder.setText(R.id.tvDesc, item.pay_desc);
        holder.setGone(R.id.tvDesc, isShowDesc);
        GlideHelper.load(mContext, item.thumb, holder.getView(R.id.ivIcon));
        holder.setImageResource(R.id.ivCheck, mSelectedMap.get(item.pay_id) ? R.mipmap.tick_click : R.mipmap.tick_normal);
        holder.setGone(R.id.tvTip, item.isRecommended);
    }
}
