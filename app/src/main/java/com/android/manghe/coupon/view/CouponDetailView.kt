package com.android.manghe.coupon.view

import android.content.Context
import android.text.Html
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import com.android.base.tools.BigDecimalUtils
import com.android.manghe.R
import com.android.manghe.coupon.model.CouponRes


class CouponDetailView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    private var layoutContent: View? = null
    private var layoutHole: View? = null


    private var tvPrice : TextView? = null
    private var tvGiftBi : TextView? = null
    private var tvDesc : TextView? = null
    private var tvRule : TextView? = null
    private var btnConfirm : Button? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.activity_coupon_detail_dialog, null)
        addView(view)
        tvPrice = view.findViewById(R.id.tvPrice)
        tvGiftBi = view.findViewById(R.id.tvGiftBi)
        tvDesc = view.findViewById(R.id.tvDesc)
        tvRule = view.findViewById(R.id.tvRule)
        layoutContent = view.findViewById(R.id.layoutContent)
        layoutHole = view.findViewById(R.id.layoutHole)
        btnConfirm = view.findViewById(R.id.btnConfirm)


        initListener()
    }

    fun setData(couponInfo: CouponRes.DataBean.CouponInfo) {
        couponInfo.price?.let {
            tvPrice?.text = BigDecimalUtils.round(it, 0)
        }
        couponInfo.coin?.let {
            tvGiftBi?.text = it
        }
        couponInfo.summary?.let {
            tvDesc?.text = it
        }
        couponInfo.content?.let {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                tvRule?.text = Html.fromHtml(it, Html.FROM_HTML_MODE_LEGACY);
            } else {
                tvRule?.text = Html.fromHtml(it);
            }
        }
    }


    private fun initListener() {
        btnConfirm?.setOnClickListener {
            hide()
        }
        layoutHole?.setOnClickListener {
            hide()
        }
        layoutContent?.setOnClickListener {

        }
    }

    fun hide() {
        val amin = AnimationUtils.loadAnimation(context, R.anim.open_out);
        amin.interpolator = DecelerateInterpolator()
        amin.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                visibility = View.GONE
            }

            override fun onAnimationStart(animation: Animation?) {

            }

        })
        layoutContent?.startAnimation(amin)
    }
    fun show(){
        visibility = View.VISIBLE
        val amin = AnimationUtils.loadAnimation(context, R.anim.open_in);
        amin.interpolator = DecelerateInterpolator()
        amin.fillAfter = true
        layoutContent!!.startAnimation(amin)
    }
}