package com.android.manghe.splash.adapter;

import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.android.manghe.R;

public class GuidePagerAdapter extends PagerAdapter {
    private boolean isMultiScr;

    public GuidePagerAdapter() {
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView iv = (ImageView) LayoutInflater.from(container.getContext()).inflate(R.layout.layout_pager_iv_item, null);
        switch (position) {
            case 0:
                iv.setImageResource(R.mipmap.guide_1);
                break;
            case 1:
                iv.setImageResource(R.mipmap.guide_2);
                break;
            case 2:
                iv.setImageResource(R.mipmap.guide_3);
                break;
        }
        container.addView(iv);
        return iv;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ImageView view = (ImageView) object;
        container.removeView(view);
    }
}
