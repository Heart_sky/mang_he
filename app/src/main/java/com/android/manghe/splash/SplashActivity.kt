package com.android.manghe.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.View
import android.view.View.OnClickListener
import androidx.viewpager.widget.ViewPager
import com.android.base.frame.presenter.IView
import com.android.base.frame.presenter.XPresenter
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.SPUtil
import com.android.manghe.R
import com.android.manghe.config.SPTag
import com.android.manghe.main.MainActivity
import com.android.manghe.splash.adapter.GuidePagerAdapter
import com.android.manghe.view.dialog.AgreementDialog
import com.android.manghe.view.dialog.AskContinueToReadAgreementDialog
import com.android.manghe.view.dialog.NewTipDialog
import com.fm.openinstall.OpenInstall
import com.fm.openinstall.listener.AppWakeUpAdapter
import com.fm.openinstall.model.AppData
import com.tmall.ultraviewpager.UltraViewPager
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : MVPActivity<XPresenter<IView>>() {

    private var wakeUpAdapter: AppWakeUpAdapter? = null

    override fun getLayoutId() = R.layout.activity_splash
    override fun showToolBarType() = ETitleType.NO_TITLE

    override fun initData(savedInstanceState: Bundle?) {
        if (SPUtil.contains(this@SplashActivity, SPTag.TAG_AppDirty)) {
            //打开过APP，显示闪屏
            showWelcomeView()
        } else {
            //第一次打开APP，显示导航, 弹出框
            if (SPUtil.contains(this@SplashActivity, SPTag.TAG_AGREEMENT)) {
                //打开过隐私窗
                showGuideView()
            } else {
                showAgreementDialog()
                ultraViewPager.visibility = View.GONE
                ReSecond.visibility = View.GONE;
            }
        }
        try {
            wakeUpAdapter = object : AppWakeUpAdapter() {
                override fun onWakeUp(p0: AppData?) {
                    //获取渠道数据
                    //            String channelCode = appData.getChannel();
                    //            //获取绑定数据
                    //            String bindData = appData.getData();
                    //            Log.d("OpenInstall", "getWakeUp : wakeupData = " + appData.toString());
                }

            }
            //获取唤醒参数
            OpenInstall.getWakeUp(getIntent(), wakeUpAdapter)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showAgreementDialog() {
        AgreementDialog(this, object : AgreementDialog.IAgreementListener {
            override fun ok() {
                SPUtil.put(mContext, SPTag.TAG_AGREEMENT, true)
                showGuideView()
            }

            override fun noOk() {
                AskContinueToReadAgreementDialog(this@SplashActivity,
                    object : AskContinueToReadAgreementDialog.IAskContinueToReadAgreement {
                        override fun ok() {
                            SPUtil.put(mContext, SPTag.TAG_AGREEMENT, true)
                            showGuideView()
                        }
                    })
                    .show()
            }
        }).show()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        OpenInstall.getWakeUp(intent, wakeUpAdapter)
    }

    override fun onDestroy() {
        super.onDestroy()
        wakeUpAdapter = null

    }

    private fun showWelcomeView() {
        ultraViewPager.visibility = View.GONE
        ReSecond.visibility = View.GONE;
        Handler().postDelayed({
            open(MainActivity::class.java)
            finish()
        }, 400)
    }

    private fun timeStart() {
        val timer: CountDownTimer = object : CountDownTimer(6000, 1000) {
            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                tvSecond.text = "" + millisUntilFinished / 1000 + "秒"
                if (tvSecond.text.equals("0秒")) {
                    tvSecond.text = "跳过"
                    tvSecond.setOnClickListener(object : OnClickListener {
                        override fun onClick(v: View?) {
                            SPUtil.put(mContext, SPTag.TAG_AppDirty, true)
                            open(MainActivity::class.java)
                            finish()
                        }
                    })
                }
            }

            override fun onFinish() {

            }
        }
        timer.start()

    }

    private fun showGuideView() {
        ultraViewPager.visibility = View.VISIBLE
        ReSecond.visibility = View.VISIBLE;
        ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL)
        //UltraPagerAdapter 绑定子view到UltraViewPager
        val adapter = GuidePagerAdapter()
        ultraViewPager.adapter = adapter
        ultraViewPager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
                btnEnterApp.visibility = if (p0 == 2) View.VISIBLE else View.GONE
            }

        })
        btnEnterApp.setOnClickListener {
            SPUtil.put(this, SPTag.TAG_AppDirty, true)
            open(MainActivity::class.java)
            finish()
        }

        timeStart()


    }
}
