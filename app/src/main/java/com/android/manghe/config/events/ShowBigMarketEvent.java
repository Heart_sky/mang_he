package com.android.manghe.config.events;

import com.android.manghe.index.model.FunctionBean;

public class ShowBigMarketEvent {

    public int index;
    public FunctionBean functionBean;
    public String goodscatid;

    public ShowBigMarketEvent(int index) {
        this.index = index;

    }

    public ShowBigMarketEvent(FunctionBean functionBean) {
        this.functionBean = functionBean;
    }

    public ShowBigMarketEvent(String goodscatid) {
        this.goodscatid = goodscatid;
    }

}
