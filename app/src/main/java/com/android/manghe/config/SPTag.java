package com.android.manghe.config;

public class SPTag {
    public static final String TAG_AppDirty = "TAG_AppDirty";
    public static final String TAG_MainPageDirty = "TAG_MainPageDirty";
    public static final String TAG_MainShowGuideDirty = "TAG_MainShowGuideDirty";
    public static final String TAG_UserInfo = "TAG_UserInfo";
    public static final String TAG_AuctionConfig = "TAG_AuctionConfig";
    public static final String TAG_IsFirstOpenMainPage = "TAG_IsFirstOpenMainPage";
    public static final String TAG_DeviceId = "TAG_DeviceId";
    public static final String TAG_AGREEMENT = "TAG_AGREEMENT";

    public static final String TAG_TouTiao = "TAG_TouTiao";
    public static final String TAG_TentCent = "TAG_TentCent";
    public static final String TAG_TuiA = "TAG_TuiA";
    public static final String TAG_IsSecondTokenInvalidate = "TAG_IsSecondTokenInvalidate";
}
