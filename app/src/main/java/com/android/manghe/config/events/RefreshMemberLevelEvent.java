package com.android.manghe.config.events;

public class RefreshMemberLevelEvent {
    public int memberLevel;

    public RefreshMemberLevelEvent(int memberLevel) {
        this.memberLevel = memberLevel;
    }
}
