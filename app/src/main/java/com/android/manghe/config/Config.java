package com.android.manghe.config;

import com.android.base.tools.SdCardUtil;

import java.io.File;

public class Config {

    public static final String AppRootDir = SdCardUtil.getRootPath() + File.separator + "MiaoKan" + File.separator;

    public static final String AppUpdatePath = AppRootDir + "update";


    //序列化文件
    public static final String SerializePath = AppRootDir + "ser" + File.separator;
    public static final String SerializeSearchHistory = SerializePath + "SerializeSearchHistory.ini";
    public static final String SerializeTuiA = SerializePath + "SerializeTuiA.ini";
    public final static String UserAgreementUri = "http://saptest1.dyajb.com:9985/protocols/license.html"; // 安居小宝用户协议
    public final static String SecretUri = "http://saptest1.dyajb.com:9985/protocols/privacy_policy.html";// --安居小宝隐私政策


    public static final String OpenInstall_Voucher = "effect_voucher";//充值

    public static final String APP_TAG = "meng_xiang";

    public static final String BOX_PAY_URI = "https://www.manghe98.com/protocol/zfxy.html";
    public static final String BOX_RULE_URI = "https://www.manghe98.com/protocol/mpgz.html";
    public static final String BOX_USER_SERVICE_URI = "https://www.manghe98.com/protocol/user_service.html";
    public static final String BOX_PLAY_RULE = "https://www.manghe98.com/gonglue/khgl.html";

    public static final String Box_53_URL = "https://www13.53kf.com/webCompany.php?kf_sign=TYzMjMTYzOY0NTEwNDUxOTE5MTYxMDE2NzI1Njg1NTE%253D&arg=10568551&tfrom=61&style=1";
    public static final String Box_53_DEVEICE = "?device=android";
    public static final String BOX_53_ID = "&u_cust_id=%s";
    public static final String BOX_53_NAME = "&u_cust_name=%s";
    public static String BottomIsLoad = "x1001";

    //eventBusCode
    public static final int EVENT_BUS_SHOW_ORDER = 102;//显示盲盒订单
    public static final int EVENT_BUS_GET_ORDER = 103;//重新在获取盲盒订单
    public static final int EVENT_BUS_GET_ALL_ORDER = 104;//重新去获取订单,改变状态
    public static final int EVENT_BUS_OPEN_MUSIC = 107;//打开音乐
    public static final int EVENT_BUS_CLOSE_MUSIC = 108;//关闭音乐
    public static final int EVENT_BUS_OPEN_SUCCESS = 109;//开盒成功
    public static final int EVENT_BUS_REPLACE_INTEGRAL_SUCCESS = 110;//置换积分成功
    public static final int EVENT_BUS_SAD_PAY_RESULT_UPAY = 111;//杉德支付中的银联支付
    public static final int EVENT_BUS_SAD_PAY_RESULT_ALPAY = 112;//杉德支付中的阿里支付
    public static final int EVENT_BUS_CLOSE_ACTIVITY = 113;//用来关闭其他页面的

}
