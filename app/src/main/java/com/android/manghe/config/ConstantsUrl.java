package com.android.manghe.config;

/**
 * Created by Administrator on 2016/8/12.
 * <p/>
 * 应用请求接口
 */
public class ConstantsUrl {

    /**
     * 域名
     */
    public static final String host = "https://www.manghe98.com/";
    public static final String domain = "https://www.manghe98.com/api/";
    public static final String sockecturl = "wss://www.ycdy8.com:7273";//正式

    /**
     * 第三方数据
     */
    public static final String Default_WX_APPID = "wx8234671226537855";
    public static final String Default_WX_SERCET = "f6082277f8d0df386d51a4dccb520246";

    public static final String QQ_APPID = "1112061531";    //申请的qq
    public static final String QQ_SECRET = "1BzQUuohOwJDr1aR";

    public static final String Bugly_APPID = "34ae98ecf4";//友盟的buglg

    public static String PAY_WX_APPID = "wx85a6978fbeadb8e7";    //申请的wx appid
    public static String PAY_WX_SERCET = "ab530854fbc14cb9f19b6e02001f52b2";    //申请的wxsercet


    public static final String YiZhiFuLicense = "aeba023db77ad9e709ac1f4ed7fae4b27c4813fd003d2f612188e78be809d995aa3c9d96be25eec05a4d4b77d2ec5dff94eb1c41589ffef2df33f2cb67889a661676f99cbb07ebdb166d6b17951ff485fec31105c5e4352482bd395cb376f39dc05cc0b9bb843d0b0d2af369409054b2add615a3dbb351a7f297399975a37318";//翼支付

    public static final String WX_CONFIG = "paymentinfo/wxapp";//获取Config内容
    public static final String WX_CONFIG2 = "paymentinfo/wxapp2";

    public static final String CouponConfig = "voucher/zixuan";

    //53快服需要的配置信息
    public static String visitorId = "";//53快服需要的访问id;
    public static final String ARG = "10568551";
    public static final String STYLE = "1";
    public static final String KFAPPID = "28108SJJ";

//===========================================================================================================
    /**
     * 主页
     */
    public static final String AddCollect = "member/auction_favorite";//收藏商品
    public static final String GetCollect = "member/is_favorite";//获取收藏状态
    public static final String FLOWPAYMENT = "flow/payment";       //获取支付方式
    public static final String VOUCHERLIST = "voucher/index";       //商务券列表
    public static final String MEMBERVOUCHER = "member/voucher";       //购劵列表
    public static final String AUTO_OFFER = "auction/auto_bid_set";          //自动出价
    public static final String ACUTIONREHISTORY = "auction/history";       //历史成交记录
    public static final String ACUTIONREJOINLOG = "auction/joinlog";       //出价记录
    public static final String ACUTIONREJOINLOG_ING = "auction/in_progress_joinlog";       //正在出价记录
    public static final String ACUTIONRECENTLYORDER = "auction/order";       //最近成交
    public static final String REGISTER_EXEMPT_LIST = "home/get_register_exempt_list";       //最近成交
    public static final String ACUTIONCOMMENT = "auction/comment";       //详情晒单
    public static final String ACUTIONORDER_DETAIL = "auction/order_detail";       //最新成交详情
    public static final String ACUTIONHOMEBLOCK = "home/block";       //趣买文章碎片
    public static final String ACUTION_COMMENT = "member/auction_comment";       //趣买晒单发布
    public static final String VOUCHERDETAIL = "member/voucher_detail";       //趣买购物卡详情
    public static final String ACUTIONCONFIG = "home/get_config";       //趣买站点配置
    public static final String GETBANNER = "home/get_banner";       //趣买站点配置
    public static final String ACUTIONCONTENT = "auction/content";       //活动商品详情
    public static final String FRAGMENT1_BANNER = "home/banner";       //首页轮播图
    public static final String FRAGMENT1_MENUS = "home/get_menus";       //首页菜单项
    public static final String MAIN_ABOUTUS = "login/about";        //关于我们
    public static final String MAIN_LISTDATA = "goods/lists";          //商品列表页
    public static final String MAIN_UPDATE_VERSION = "goods/version";          //更新
    public static final String MAIN_DETAIL = "goods/show";             //商品详情
    public static final String MAIN_LISTDATA_SQUARE = "goods/square";  //广场列表
    public static final String MAIN_SQUARE_UPDATE = "member/issue";    //发布广场
    public static final String MAINLIST_DAPAI = "goods/brand";         //超值大牌
    public static final String NATIONLISTDATA = "goods/nation";        //海淘列表
    public static final String MAINLIST_XIANSHI = "goods/kill";        //秒杀商品列表
    public static final String MAINLIST_COMMENT = "goods/rate";        //详情页评价列表
    public static final String MAINLIST_DETAILSHARE = "goods/team";    //团购详情页
    public static final String MAINLIST_GOODLUCK = "goods/luck";       //中奖活动详情
    public static final String SORTLISTDATA = "goods/getcat";          //商品分类
    public static final String MEMBERPAY = "order/check";              //下单界面
    public static final String MEMBERPAYDONETOShare = "order/getcommonid";           //支付成功后分享
    public static final String MEMBERPAYDONE = "order/done";           //提交订单
    public static final String SEARCHLOGISTICS = "member/orderShip";           //查看物流
    public static final String ACTION_ORDERSHIP = "member/auctionOrderShip"; //查看物流
    public static final String MEMBER_DETAILORDER_PAY = "order/pay";           //订单详情里面
    public static final String MAIN_GOODSCATEGORYDATA = "goods/category";          //直购商品目录
    public static final String DIRECTBUY_GOODSLISTSDATA = "goods/lists";          //直购商品列表
    public static final String DIRECTBUY_RECOMMED_GOODSLISTSDATA = "goods/rec_lists";          //热门推荐商品列表
    public static final String DIRECTBUY_FIRMORDER = "flow/buy";          //直购确认收货
    public static final String DIRECTBUY_SUBMITORDER = "flow/done";          //直购提交订单
    public static final String DIRECTBUY_PAYMETHOD = "flow/checkout";          //直购订单选择支付方式
    public static final String DIRECTBUY_TOPAY = "flow/pay";          //直购订单提交支付
    public static final String AUCTIONMAKESURE = "member/auction_receive";          //趣买订单确认收货
    public static final String AUCTIONCHECKOUT = "member/auction_checkout";          //趣买订单提交支付
    public static final String VOUCHERCHECKOUT = "member/voucher_checkout";          //购劵订单提交支付
    public static final String SCORE_DETAIL = "score/index";          //积分明细
    public static final String VOUCHER_INCOME = "member/voucher_income_log";//购物卡收入明细
    public static final String VOUCHER_EXPAND = "member/voucher_expend_log";//购物卡支出明细
    public static final String VOUCHER_EXPLAIN = "member/voucher_explain";//购物卡说明
    public static final String AUCTION_SEARCH = "auction/search";//搜索分类商品
    public static final String GET_IP = "home/get_ip";//获取ip
    public static final String ADD_TO_CARD = "flow/add_product";//加入购物车
    public static final String DELETE_CARD = "flow/del_cart";//删除购物车
    public static final String GET_CARD_GOODS = "flow/checkout";//获取购物车商品列表
    public static final String GET_CARD_RECOMMEND = "flow/getrecommendgoods";//购物车页面获取推荐商品
    public static final String EDIT_CARD = "flow/edit_cart";//编辑购物车商品
    public static final String PinDanRecord = "member/help_log";//免单的拼单进度
    public static final String PinDanGongLue = "home/block?mark=zlgl";//免单的拼单进度
    public static final String PinDanRule = "home/block?mark=zlgz";//助力规则
    public static final String SIGNSTATIST = "sign/sign_statist";//签到信息
    public static final String ZHULI_RULE = "home/block?mark=mxspzlgz";//商品助力规则
    public static final String SIGN = "sign/sign_redpacket";//签到
    public static final String RED_PACKAGE_LIST = "member/no_open_exempt_help_list";//红包列表
    public static final String GET_RED_PACKAGE = "member/open_help_by_id";//红包列表
    public static final String BigLuckDrawDesc = "home/block?mark=jpsm";//大转盘说明
    public static final String BigLuckDrawRule = "home/block?mark=plate_rule";//大转盘规则
    public static final String OpenBoxRule = "home/block?mark=gzsm";//大转盘规则
    public static final String PlateTurn = "member/plate_turn";//扣除抽奖积分
    public static final String GetPlateConfig = "member/get_plate_config";//获取抽奖配置
    public static final String MemberDestroy = "member/destroy";//注销账号
    public static final String GET_MIAN_DAN_GOOD = "";//确认领取面单商品
    public static final String BigLuckDrawHelp = "home/block?mark=dzpsm";//大转盘问号
    /**
     * 店铺
     */
    public static final String MEMBERCOLLECTSTOREACTION = "store/guanzhu";        //关注店铺
    public static final String MEMBERCOLLECTSTOREMESSAGE = "store/index";         //店铺首页信息与优惠券等
    public static final String MEMBERCOLLECTSTOREGOODS = "store/goods";           //店铺商品列表
    public static final String MEMBERCOLLECTSTOREGOODSNEW = "store/goods_news";   //关注店铺上新列表
    public static final String MEMBERCOLLECTSTOREINFO = "store/info";             //店铺详情
    public static final String MEMBERCOLLECTSTOREGETYOUHUIQUAN = "store/coupon_get"; //领取优惠券
    /**
     * 会员
     */
    public static final String READ_ALL_FEED_BACK = "content/read_all_mes";//已读全部反馈记录
    public static final String FEED_BACK_LIST = "content/feedback_list";//反馈列表
    public static final String SignWithdraw = "member/redpack_withdraw_apply";//签到提现
    public static final String WITHDRAW = "member/withdraw_apply"; // 佣金提现
    public static final String MEMBERINDEX = "member/index";        //会员配置和信息
    public static final String MEMBER_INVITE_STATISTICS = "member/invite_commission_sum";
    public static final String MEMBERWELCOMSCODE = "welcome/scode";              //图形验证码
    public static final String MEMBERAGREEMENT = "home/get_yszc";                //用户服务协议
    public static final String MEMBERMESSAGEINDEX = "message/index";              //消息主页
    public static final String MEMBERMESSAGENUM = "member/unread_message_num";      //消息未读数量
    public static final String FEEDBACK_MESSAGENUM = "content/read_mes";      //投诉建议未读数量

    public static final String MEMBERMESSAGEDETAIL = "message/detail";              //消息详细
    public static final String MEMBERMESSAGENOTICEDETAIL = "content/show";              //消息详细
    public static final String MEMBERORDINGDANGNUM = "member/info";              //会员信息
    public static final String MEMBERORLOGIN = "login/act_login";                 //手机密码登录
    public static final String MEMBERORSIGN = "login/register";
    public static final String MEMBERORFORGETPW = "login/forget"; //忘记密码
    public static final String MEMBERORSENDMESSAGE = "login/send_verify_code";    //发送验证码
    public static final String MEMBERORSENDTHREELOGIN = "login/oauth_login";
    public static final String MEMBER_OAUTH_REGISTER = "login/oauth_register";
    public static final String MEMBERDATA_EDITAVATAR = "member/photo";            //头像修改
    public static final String MEMBERDATA_EDITSEXY = "member/chsex";     //修改性别
    public static final String MEMBERDATA_EDITNICKNAME = "member/chnickname";     //修改昵称
    public static final String MEMBERDATA_EDITMOBLE = "member/chmobile";          //修改手机号
    public static final String MEMBER_BIND_MOBILE = "member/bind_mobile";
    public static final String MEMBERDATA_EDITPD = "member/chpwd";                //修改密码
    public static final String MEMBERADDRESSSHOW = "member/addressShow";         //查看收货地址详情
    public static final String MEMBERFEEDBACK = "content/index/gbook";          //意见反馈
    public static final String MEMBERFSIGNACTION = "sign/action";          //签到提交
    public static final String MEMBERFSIGN = "sign/index";          //签到
    public static final String MEMBERGETAREA = "member/getArea";//会员收货地址列表
    public static final String MEMBERADDRESSLIST = "member/addressList";          //会员收货地址列表
    public static final String MEMBERADDRESSADD = "member/addressUpdate";            //收货地址添加
    public static final String MEMBERADDRESSDETELE = "member/addressDelete";      //收货地址删除
    public static final String MEMBERADDRESSDEFAULT = "member/addressDefault";    //默认地址设置
    public static final String MEMBERADDRESSUPDATE = "member/addressUpdate";      //收货地址修改
    public static final String MEMBERYOUHUIQUAN = "member/coupon";                //会员优惠券列表
    public static final String MEMBERCHOUJIANG = "member/order_lottery";          //会员抽奖列表
    public static final String MEMBERMONEY = "member/comms_list";                 //会员佣金明细列表
    public static final String MEMBERMONEY_CASH = "member/withdraw_commission";   //可用佣金取现
    public static final String MEMBERMONEY_CASH_LOG = "member/withdraw_commission_log";//取现记录列表
    public static final String MEMBERMONEY_LEVEL_FANS = "member/commission";      //一级，二级粉丝列表
    public static final String MEMBERCOLLECT = "member/fav";                      //会员收藏收藏列表
    public static final String MEMBERCOLLECTSTORE = "member/store_fav";           //会员收藏店铺列表
    public static final String MEMBERCOLLECTDETELE = "member/favDelete";          //收藏取消
    public static final String MEMBERCOLLECTADDORDETELE = "member/favAddOrDelete";//收藏取消与添加
    public static final String MEMBERTUAN = "member/team";                        //会员团列表
    public static final String MEMBEMESSAGE = "member/message";                   //会员消息列表
    public static final String MEMBEMESSAGEDETELE = "member/messageRemove";       //消息删除
    public static final String MEMBEMESSAGEREAD = "member/messageRead";           //消息已读
    public static final String MEMBERTUAN_RANK = "member/team_sort";              //拼团王
    public static final String MEMBERFANS = "member/myfans_list";                  //会员粉丝列表
    public static final String MEMBERAUCTIONORDER = "member/auction_order";                      //竞买订单列表
    public static final String MEMBERMERVOUCHER = "member/voucher";                      //我的购物卡订单列表
    public static final String MEMBERMERVOUCHERDETAIL = "member/voucher_detail";                      //我的购物卡订单列表详细
    public static final String AUCTIONORDERDETAIL = "member/auction_order_detail";                      //趣买订单详细
    public static final String MEMBERORDER = "member/order";                      //会员订单列表
    public static final String MEMBERORDERTUIKUANG = "member/refund_order_lists"; //会员退款订单列表
    public static final String MEMBERORDERCANCEL = "member/orderCancel";          //订单取消
    public static final String MEMBERORDERDETELE = "member/orderDelete";          //订单删除
    public static final String MEMBERORDERQUERENSHOUHUO = "member/finishOrder";  //确认收货
    public static final String MEMBERORDERDETAIL = "member/orderDetail";          //订单详情
    public static final String MEMBERORDERPINJIA = "member/orderRate";            //订单评价
    public static final String MEMBERORGET = "member/submit_apply_refund";        //提交退款申请
    public static final String MEMBERORREFOUNDDETAIL = "member/refund_detail";    //退款详情
    public static final String MEMBERORREFOUNDADDRESS = "member/business_address";//商家地址
    public static final String MEMBERORREFOUNDTUIDINGDANG = "member/shipping";    //提交退货订单
    public static final String MEMBERORREFOUNDMESSAGE = "member/refund_shipping"; //退货信息
    public static final String MEMBER_CUSTOMER = "content/index";          //常见问题
    public static final String MEMBER_INVITATION = "member/myivt";          //邀请
    public static final String MEMBER_INVITATION_LIST = "member/myivt_list";          //邀请列表
    public static final String MEMBER_VERIFY = "member/verifyidcard";          //实名认证申请
    public static final String verifyidcard = "member/verifyidcard";          //实名认证查询
    public static final String MEMBER_UPLOAD = "member/upload";          //图片上传统一入口
    public static final String MEMBER_CHECKIDCARD = "member/checkIdCard";          //检查是否需要实名认证
    public static final String EXEMPT_AMOUNT_LOG = "member/exempt_amount_log";          //抵用金明细
    public static final String SetInvite = "member/set_invite";
    public static final String MyCollection = "member/my_favorites";
    public static final String CancelCollection = "member/cancel_auction_favorite";
    public static final String Question = "question/ask";
    public static final String FreeCoin = "home/get_register_free_coin";
    public static final String REG_EXEMPT_AMOUNT = "index/get_reg_exempt_amount";
    public static final String REG_COUPON_AMOUNT = "index/get_reg_coupon_amount";
    public static final String IsOrderPayed = "welcome/respond/%s?ajax=1";
    public static final String IndexData = "goods/index_data";//首页数据
    public static final String TodayNew = "goods/today_goods";//今日有好物
    public static final String RecommendSecondLevel = "goods/rec_category";//热门推荐二级分类
    public static final String GetInviteCommissionLog = "member/get_invite_commission_log";//佣金记录
    public static final String PlaceGift = "member/plate_present";
    public static final String CheckQuickPayResult = "paymentinfo/shangmeng_chaxun";

    // 快捷支付
    public static final String ShangMengCardInfo = "paymentinfo/shangmeng_cardinfo";
    public static final String ShangMengNewOrder = "paymentinfo/shangmeng_new_order";//调用之后会发送验证码
    public static final String ShangMengPay = "shangmeng_pay";//支付
    public static final String ShangMengDelCard = "paymentinfo/remove_card";//删除绑定的卡



    public static final String ShaiOrder_WEB = "/home/www/#/share";//幸运晒单
    public static final String RunTop_WEB = "/home/www/#/questions";//冲顶答题
    public static final String ROUND_WEB = "home/test/?/=#/plate";//大转盘
    public static final String GUIDE_WEB = "qp/zindex.html";//新手指南
    public static final String CHAT_WEB = "chat";//客服

    public static final String TouTiaoCallBack = "home/toutiao_callback";
    public static final String ClickRule = "goods/exempt_rule_click";

    public static final String HIGH_POPULARITY_0 = "index/high_popularity?id=0";//人气拍品
    public static final String HIGH_POPULARITY_1 = "index/high_popularity?id=1";//新品首发
    public static final String HIGH_POPULARITY_2 = "index/high_popularity?id=2";//本周爆品
    public static final String HIGH_POPULARITY_3 = "index/high_popularity?id=3";//本期大神


    //==================新版新接口=============================

    public static final String Box_Score_List = "goods/score_goods_lists";//积分列表
    public static final String Box_List = "manghe/lists";//盲盒列表
    public static final String Box_Categories = "manghe/categories";//盲盒分类
    public static final String Box_Prize_List = "manghe/prize_lists";//开盒中奖列表
    public static final String Box_Detail = "manghe/manghe_detail";//单个商品详情
    public static final String Box_Test_Open = "manghe/test_open";//试玩接口
    public static final String Box_Comment_List = "manghe/comment_list";//晒单分享
    public static final String Box_Check_Order = "member/manghe_checkout";//订单检测
    public static final String Box_Check_YU_ER = "member/manghe_done";//检测盲盒余额
    public static final String Box_Submit_Order = "member/manghe_order_pay";//提交盲盒订单
    public static final String Box_Open = "member/manghe_prize?order_id=%s";//开箱
    public static final String Box_GET_ALL_PRIZE = "member/prize_lists";//获取所有的开奖记录
    public static final String Box_EXCHANGE_PRIZE = "member/prize_exchange";//置换盲盒币
    public static final String Box_SCORE_EXCHANGE = "member/score_exchange_goods";//积分置换商品

    public static final String Box_GET_ALL_ORDERS = "member/my_orders";//获取所有的订单
    public static final String BOx_CONFIRM_RECEIVE = "member/confirm_receive";//确认收货
    public static final String BOx_ORDER_DETAIL = "member/order_item_detail";//获取订单详情
    public static final String BOx_ORDER_SHIP = "member/order_item_ship";//获取订单详情
    public static final String BOx_ORDER_RATE = "member/order_item_rate";//订单评价
    public static final String BOx_SIGN = "sign/sign_in";//签到
    public static final String BOx_SIGN_TAKE_BONUS = "sign/take_bonus";//领取任务奖励
    public static final String BOx_SIGN_INFO = "sign/sign_in_pageinfo";//签到界面信息
    public static final String BOx_REPLACE_RULE = "home/block?mark=zhgz";//置换规则
    public static final String BOx_REPLACE_COIN = "home/block?mark=khbtx";//置换开盒币提醒
    public static final String BOx_REPLACE_INTEGRAL = "home/block?mark=jftx";//置换积分提醒
    public static final String BOx_INTEGRAL_COVER = "home/get_banner?typeid=3";//积分商城封面图
    public static final String BOx_INTEGRAL_TOP_UP = "home/get_banner?typeid=5";//充值页面封面图
    public static final String BOx_INTEGRAL_LOG = "/member/score_log";//积分明细
    public static final String BOx_COIN_LOG = "member/coin_log";//赠币和盲盒币明细
    public static final String Box_UNDER_AGE_SENT = "member/juvenile_protect";//未成年人保护提交信息
    public static final String Box_UNDER_AGE_GET = "member/juvenile_protect_log";//获取未成年反馈纪录
    public static final String Box_GET_PAY_WAY="order/pay_status";//获取订单的支付方式
    public static final String Box_Advertisement_Relation="home/advertisement_relation";//腾讯广告主
    public static final String TuiA = "home/tuia_callback";//推啊广告主
}

