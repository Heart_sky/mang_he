package com.android.manghe.cache;

import android.content.Context;
import com.android.base.tools.SharedPreferencesUtil;
import com.android.manghe.common.model.AuctionConfig;
import com.android.manghe.config.SPTag;

public class ConfigHolder {


    public static void setConfigInfo(Context context, AuctionConfig.DataBean config){
        SharedPreferencesUtil.setObj(context, SPTag.TAG_AuctionConfig, config);
    }

    public static AuctionConfig.DataBean getConfigInfo(Context context){
        Object tempObj = SharedPreferencesUtil.getObj(context, SPTag.TAG_AuctionConfig);
        if(tempObj != null){
            return (AuctionConfig.DataBean)tempObj;
        }
        return new AuctionConfig.DataBean();
    }
}
