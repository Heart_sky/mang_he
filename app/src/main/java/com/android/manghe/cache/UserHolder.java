package com.android.manghe.cache;

import android.content.Context;
import com.android.base.tools.SharedPreferencesUtil;
import com.android.manghe.config.SPTag;
import com.android.manghe.mine.model.UserRes;

public class UserHolder {


    public static void setUserInfo(Context context, UserRes.UserInfo userInfo){
        if(userInfo == null)
            return;
        SharedPreferencesUtil.setObj(context, SPTag.TAG_UserInfo, userInfo);
    }

    public static UserRes.UserInfo getUserInfo(Context context){
        Object tempObj = SharedPreferencesUtil.getObj(context, SPTag.TAG_UserInfo);
        if(tempObj != null){
            return (UserRes.UserInfo)tempObj;
        }
        return null;
    }

    public static String getUID(Context context){
        UserRes.UserInfo userInfo = getUserInfo(context);
        if(userInfo != null){
            return userInfo.UID;
        }else{
            return "";
        }
    }

    public static void clearInfo(Context context){
        SharedPreferencesUtil.setObj(context, SPTag.TAG_UserInfo, null);
    }

}
