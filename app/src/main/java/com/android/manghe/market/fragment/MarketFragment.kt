package com.android.manghe.market.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.base.frame.view.MVPFragment
import com.android.base.tools.DisplayUtil
import com.android.manghe.R
import com.android.manghe.cache.UserHolder
import com.android.manghe.market.activity.MarketCatGoodListActivity
import com.android.manghe.market.adapter.MarketCategoryAdapter
import com.android.manghe.market.adapter.MarketCategoryContentAdapter
import com.android.manghe.market.presenter.PMarket
import com.android.manghe.search.activity.SearchBigMarketActivity
import com.android.manghe.user.activity.LoginActivity
import kotlinx.android.synthetic.main.fragment_market.*
import kotlinx.android.synthetic.main.view_search.*
import java.lang.Exception


/**
 * 分类页面
 */
class MarketFragment : MVPFragment<PMarket>() {

    private var mCatAdapter: MarketCategoryAdapter? = null
    private var mCatContentAdapter: MarketCategoryContentAdapter? = null

    private var mGoodscatid = ""

    override fun getLayoutId(): Int {
        return R.layout.fragment_market
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {
        //搜索栏
        val searchView = LayoutInflater.from(activity).inflate(R.layout.view_search, null, false)
        val searchLayoutParam =
            RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                DisplayUtil.dip2px(activity, 36f)
            )
        searchLayoutParam.addRule(RelativeLayout.CENTER_IN_PARENT)
        searchView.layoutParams = searchLayoutParam
        titleBar.addCenterView(searchView)
            .setBackgroundColor(ContextCompat.getColor(activity!!, R.color.white))

        initView()
        initListener()
        p.getCategories()
    }

    private fun initView() {
        recyclerViewCategory.layoutManager =
            LinearLayoutManager(activity)
        mCatAdapter = MarketCategoryAdapter(activity)
        recyclerViewCategory.adapter = mCatAdapter

        recyclerViewContent.layoutManager =
            GridLayoutManager(activity, 3)
        mCatContentAdapter = MarketCategoryContentAdapter(activity)
        recyclerViewContent.adapter = mCatContentAdapter
    }

    private fun initListener() {
//        tvSearch.setOnClickListener {
//            //进入搜索页面
//            if (UserHolder.getUserInfo(activity) != null) {
//                //进入搜索页面
//                SearchBigMarketActivity.showActivity(activity!!)
//            } else {
//                open(LoginActivity::class.java)
//            }
//
//        }
        mCatAdapter?.setOnItemClickListener { _, _, position: Int ->
            chooseOnLevel(mCatAdapter!!.data[position].id)
        }
        mCatContentAdapter?.setOnItemClickListener { _, _, position: Int ->
            try {
                val catInfo = mCatContentAdapter!!.data[position]
                open(
                    MarketCatGoodListActivity::class.java,
                    mapOf("id" to catInfo.id, "title" to catInfo.catname)
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    fun updateCatView() {
        mCatAdapter?.let {
            it.setNewData(p.mCategoryList)
            chooseOnLevel(mGoodscatid)
        }
        if (p.mCategoryList.isNotEmpty()) {
            p.getCategoryContent(mGoodscatid)
        }
    }

    fun updateContentView() {
        mCatContentAdapter?.let {
            it.setNewData(p.mContentList)
        }
    }


    fun setWillShowTitle(goodscatid: String) {
        mGoodscatid = goodscatid
        mCatAdapter?.let {
            if (!it.data.isNullOrEmpty()) {
                chooseOnLevel(goodscatid)
            }
        }
    }


    private fun chooseOnLevel(goodscatid: String) {
        mCatAdapter!!.updateSelected(goodscatid)
        p.getCategoryContent(goodscatid)
    }


}