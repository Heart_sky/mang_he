package com.android.manghe.market.adapter;

import android.content.Context;

import androidx.core.content.ContextCompat;

import com.android.manghe.R;
import com.android.manghe.market.model.MarketCatInfo;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;

public class MarketCategoryAdapter extends BaseQuickAdapter<MarketCatInfo, BaseViewHolder> {
    private Context mContext;


    public MarketCategoryAdapter(Context context) {
        super(R.layout.item_market_category, new ArrayList<>());
        mContext = context;
    }

    public void updateSelected(int position) {
        try {
            for (int i = 0; i < getData().size(); i++) {
                getData().get(i).isSelected = false;
            }
            getData().get(position).isSelected = true;
            replaceData(getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateSelected(String goodscatid) {
        try {
            for (int i = 0; i < getData().size(); i++) {
                getData().get(i).isSelected = false;
            }
            for (int i = 0; i < getData().size(); i++) {
                if (getData().get(i).id.equals(goodscatid)) {
                    getData().get(i).isSelected = true;
                    break;
                }
            }
            replaceData(getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void convert(BaseViewHolder helper, MarketCatInfo item) {
        helper.setText(R.id.tvCatName, item.catname);
        helper.setGone(R.id.tagView, item.isSelected);
        helper.setTextColor(R.id.tvCatName, ContextCompat.getColor(mContext, item.isSelected ? R.color.colorFont33 : R.color.colorFont99));
        helper.setBackgroundColor(R.id.layoutItem, ContextCompat.getColor(mContext, item.isSelected ? R.color.white : R.color.backgroundGray));
    }
}
