package com.android.manghe.market.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeListModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/02 15:23
 * desc   :
 */
public class GoodsAdapter extends BaseQuickAdapter<BoxPrizeListModel.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;

    public GoodsAdapter(Context context, @Nullable List<BoxPrizeListModel.DataBean.ListBean> data) {
        super(R.layout.item_goods, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, BoxPrizeListModel.DataBean.ListBean model) {
        GlideHelper.loadWithHolderErr(mContext, model.goods_pic, helper.getView(R.id.ivPic));
        helper.setText(R.id.tvGoodName, model.goods_name + "");
        helper.setText(R.id.tvCount, "数量：1");
    }
}
