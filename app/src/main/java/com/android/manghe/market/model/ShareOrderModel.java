package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/10 16:33
 * desc   :
 */

public class ShareOrderModel extends NetBean<ShareOrderModel.DataBean> {


    /**
     * data : {"list":[{"id":"837","mid":"1715","order_id":"0","good_id":"2103","goods_spec":"","star":"5","content":"大VVSVS大V大SAV都是","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/4/29165_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/0/29161_src.jpg"],"state":"0","c_time":"2021-11-23 14:56:43","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"旧年枕上","goods_name":"Apple iPhone 13 (A2634) 256GB 蓝色 支持移动联通电信5G 双卡双待","goods_price":"6799.00","goods_probability_level":"至尊款"},{"id":"836","mid":"5670","order_id":"0","good_id":"2106","goods_spec":"","star":"5","content":"是的VS的VSDVDSV","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/h/z/29160_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/h/y/29159_src.jpg"],"state":"0","c_time":"2021-11-04 14:56:33","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"夏沫伤痕","goods_name":"Apple iPhone 11 (A2223) 128GB 紫色 移动联通电信4G手机 双卡双待","goods_price":"4399.00","goods_probability_level":"至尊款"},{"id":"835","mid":"15802","order_id":"0","good_id":"2104","goods_spec":"","star":"5","content":"是的GV都是VB地方吧","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/3/29164_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/1/29162_src.jpg"],"state":"0","c_time":"2021-11-02 14:56:20","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"騎著鴨子找火雞","goods_name":"Apple iPhone 13 mini (A2629) 256GB 蓝色 手机 支持移动联通电信5G","goods_price":"5999.00","goods_probability_level":"至尊款"},{"id":"834","mid":"1646","order_id":"0","good_id":"2105","goods_spec":"","star":"5","content":"是的VS大V是的VS大V","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/7/29168_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/6/29167_src.jpg"],"state":"0","c_time":"2021-11-08 14:56:07","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"白緍纱","goods_name":"Apple iPhone 12 (A2404) 128GB 白色 支持移动联通电信5G 双卡双待手机","goods_price":"5099.00","goods_probability_level":"至尊款"},{"id":"833","mid":"27096","order_id":"0","good_id":"2106","goods_spec":"","star":"5","content":"测试测试测试测试车","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/a/29171_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png"],"state":"0","c_time":"2021-11-09 14:55:49","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"苯苯","goods_name":"Apple iPhone 11 (A2223) 128GB 紫色 移动联通电信4G手机 双卡双待","goods_price":"4399.00","goods_probability_level":"至尊款"}],"list_total":5}
     * flag : true
     */



    public static class DataBean implements Serializable {
        /**
         * list : [{"id":"837","mid":"1715","order_id":"0","good_id":"2103","goods_spec":"","star":"5","content":"大VVSVS大V大SAV都是","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/4/29165_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/0/29161_src.jpg"],"state":"0","c_time":"2021-11-23 14:56:43","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"旧年枕上","goods_name":"Apple iPhone 13 (A2634) 256GB 蓝色 支持移动联通电信5G 双卡双待","goods_price":"6799.00","goods_probability_level":"至尊款"},{"id":"836","mid":"5670","order_id":"0","good_id":"2106","goods_spec":"","star":"5","content":"是的VS的VSDVDSV","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/h/z/29160_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/h/y/29159_src.jpg"],"state":"0","c_time":"2021-11-04 14:56:33","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"夏沫伤痕","goods_name":"Apple iPhone 11 (A2223) 128GB 紫色 移动联通电信4G手机 双卡双待","goods_price":"4399.00","goods_probability_level":"至尊款"},{"id":"835","mid":"15802","order_id":"0","good_id":"2104","goods_spec":"","star":"5","content":"是的GV都是VB地方吧","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/3/29164_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/1/29162_src.jpg"],"state":"0","c_time":"2021-11-02 14:56:20","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"騎著鴨子找火雞","goods_name":"Apple iPhone 13 mini (A2629) 256GB 蓝色 手机 支持移动联通电信5G","goods_price":"5999.00","goods_probability_level":"至尊款"},{"id":"834","mid":"1646","order_id":"0","good_id":"2105","goods_spec":"","star":"5","content":"是的VS大V是的VS大V","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/7/29168_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/6/29167_src.jpg"],"state":"0","c_time":"2021-11-08 14:56:07","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"白緍纱","goods_name":"Apple iPhone 12 (A2404) 128GB 白色 支持移动联通电信5G 双卡双待手机","goods_price":"5099.00","goods_probability_level":"至尊款"},{"id":"833","mid":"27096","order_id":"0","good_id":"2106","goods_spec":"","star":"5","content":"测试测试测试测试车","thumbs":["https://www.mxmd88.com/upload/1/images/gallery/i/a/29171_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/9/29170_src.png"],"state":"0","c_time":"2021-11-09 14:55:49","u_time":"0","listorder":"0","buy_num":"0","sid":"0","username":"苯苯","goods_name":"Apple iPhone 11 (A2223) 128GB 紫色 移动联通电信4G手机 双卡双待","goods_price":"4399.00","goods_probability_level":"至尊款"}]
         * list_total : 5
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean implements Serializable {
            /**
             * id : 837
             * mid : 1715
             * order_id : 0
             * good_id : 2103
             * goods_spec :
             * star : 5
             * content : 大VVSVS大V大SAV都是
             * thumbs : ["https://www.mxmd88.com/upload/1/images/gallery/i/4/29165_src.jpg","https://www.mxmd88.com/upload/1/images/gallery/i/0/29161_src.jpg"]
             * state : 0
             * c_time : 2021-11-23 14:56:43
             * u_time : 0
             * listorder : 0
             * buy_num : 0
             * sid : 0
             * username : 旧年枕上
             * goods_name : Apple iPhone 13 (A2634) 256GB 蓝色 支持移动联通电信5G 双卡双待
             * goods_price : 6799.00
             * goods_probability_level : 至尊款
             * member_avatar "https://www.mxmd88.com/upload/1/images/photo/1715_thumb.jpg",
             */

            public String id;
            public String mid;
            public String order_id;
            public String good_id;
            public String goods_spec;
            public String star;
            public String content;
            public String state;
            public String c_time;
            public String u_time;
            public String listorder;
            public String buy_num;
            public String sid;
            public String username;
            public String goods_name;
            public String goods_price;
            public String goods_probability_level;
            public List<String> thumbs;
            public String member_avatar;
        }
    }
}
