package com.android.manghe.market.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.android.base.tools.BigDecimalUtils;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.market.activity.BoxDetailActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/25 14:30
 * desc   :
 */
public class BoxListAdapter extends BaseQuickAdapter<BoxModel.DataBean, BaseViewHolder> {
    private Context mContext;
    private View emptyView;

    public BoxListAdapter(Context context, @Nullable List<BoxModel.DataBean> data) {
        super(R.layout.item_box_list, data);
        this.mContext = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void updateDate(List<BoxModel.DataBean> data) {
        replaceData(data);

    }

    @Override
    protected void convert(BaseViewHolder holder, BoxModel.DataBean item) {
        try {
            GlideHelper.loadWithHolderErr(mContext, item.cover_pic, holder.itemView.findViewById(R.id.ivPic));
            if (item.prices != null && item.prices._$1 != null) {
                holder.setText(R.id.tvCurrentPrice, "￥" + BigDecimalUtils.round(item.prices._$1.sell_price));
            }
            holder.setText(R.id.tvTitle, item.name);
            holder.setText(R.id.id_tv_price_mim_max, "商品价值：" + item.price_interval.min + "-" + item.price_interval.max);
            holder.setText(R.id.id_tv_goods_total, "共" + item.goods_list.size() + "款商品");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, BoxDetailActivity.class);
                    intent.putExtra("BoxListModel", item);
                    mContext.startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
