package com.android.manghe.market.activity

import android.graphics.Color
import android.os.Bundle
import com.android.base.frame.activity.FragmentActivity
import com.android.base.frame.model.FragmentEntity
import com.android.base.frame.title.ETitleType
import com.android.manghe.R
import com.android.manghe.cache.ConfigHolder
import com.android.manghe.main.view.TabEntity
import com.android.manghe.market.fragment.MarketCommentFragment
import com.android.manghe.market.fragment.MarketDetailFragment
import com.android.manghe.view.MyScrollView
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_market_good_detail.*


class MarketGoodDetailActivity : FragmentActivity() {
    private val mTabEntities = ArrayList<CustomTabEntity>()
    private var mDetailFragment : MarketDetailFragment? = null
    private val mTitles = arrayListOf("详情", "评论")
    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }
    override fun initData(savedInstanceState: Bundle?) {
        val data = Bundle()
        data.putString("goodId", intent.getStringExtra("goodId"))

        val config = ConfigHolder.getConfigInfo(this)
        if (config != null) {
            if(config.comment_switch == 1) {
                addFragment(
                    R.id.container,
                    FragmentEntity(MarketDetailFragment::class.java, data),
                    FragmentEntity(MarketCommentFragment::class.java, data)
                )
            }else{
                addFragment(
                    R.id.container,
                    FragmentEntity(MarketDetailFragment::class.java, data)
                )
                mTitles.removeAt(1)
            }
        }
        mDetailFragment = getFragment(0) as MarketDetailFragment
        mDetailFragment?.setScrollListener(MyScrollView.OnScrollListener { _, y, _, _ ->
            if (y <= 0) {
                //设置渐变的头部的背景颜色
                layoutTabContainer.setBackgroundColor(Color.argb(0, 255, 255, 255))
                tabLayout.alpha = 0f
            } else if (y > 0 && y <= mDetailFragment!!.mPicHeight) {
                //滑动距离小于banner图的高度时，设置背景和字体颜色颜色透明度渐变
                val scale: Float = y.toFloat() / mDetailFragment!!.mPicHeight
                val alpha = (scale * 255).toInt()
                layoutTabContainer.setBackgroundColor(Color.argb(alpha, 255, 255, 255))
                tabLayout.alpha = alpha.toFloat()
            } else {
                //滑动到banner下面设置普通颜色
                layoutTabContainer.setBackgroundColor(Color.WHITE)
                tabLayout.alpha = 1f
            }
        })


        for (i in mTitles.indices) {
            mTabEntities.add(TabEntity(mTitles[i], 0, 0))
        }
        tabLayout.setTabData(mTabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabReselect(position: Int) {
            }

            override fun onTabSelect(position: Int) {
                showFragment(position)
            }
        })

        ivBack.setOnClickListener {
            finish()
        }
    }

    fun showComment(){
        tabLayout.currentTab = 1
        showFragment(1)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_market_good_detail
    }
}
