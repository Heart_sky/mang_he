package com.android.manghe.market.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.GridLayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.activity.BoxGoodDetailActivity;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.utils.WindowUtils;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/02/17 09:29
 * desc   :
 */
public class ShowBoxGoodsDelegateAdapter extends DelegateAdapter.Adapter<ShowBoxGoodsDelegateAdapter.MyRecylcerView> {

    private Context mContext;
    private List<BoxModel.DataBean.GoodsListBean> data;

    public ShowBoxGoodsDelegateAdapter(Context mContext, List<BoxModel.DataBean.GoodsListBean> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        GridLayoutHelper gridHelper = new GridLayoutHelper(2);
        gridHelper.setGap(DisplayUtil.dip2px(mContext, 8f));
        gridHelper.setAutoExpand(false);
        gridHelper.setPaddingTop(20);
        gridHelper.setPaddingBottom(20);
        gridHelper.setPaddingLeft(15);
        gridHelper.setPaddingRight(15);
        return gridHelper;
    }

    @NonNull
    @Override
    public MyRecylcerView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRecylcerView(LayoutInflater.from(mContext).inflate(R.layout.item_show_goods, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull MyRecylcerView holder, int position) {
        BoxModel.DataBean.GoodsListBean item = data.get(position);
        try {
            GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.imgPic);
            holder.tvPrice.setText("市场价：" + "￥" + item.price);
            holder.tvTitle.setText(item.name);
            WindowUtils.setBoxLevel(mContext, holder.linearLayout, holder.imgLevel, holder.tvLevel,
                    item.probability_level);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, BoxGoodDetailActivity.class);
                    intent.putExtra("BoxModel", item);
                    mContext.startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

   static class MyRecylcerView extends RecyclerView.ViewHolder {

        private TextView tvPrice, tvTitle, tvLevel;
        private ImageView imgPic, imgLevel;
        private RoundLinearLayout linearLayout;


        public MyRecylcerView(@NonNull View itemView) {
            super(itemView);
            tvLevel = itemView.findViewById(R.id.id_tv_level);
            tvPrice = itemView.findViewById(R.id.id_tv_price_value);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            imgPic = itemView.findViewById(R.id.ivPic);
            imgLevel = itemView.findViewById(R.id.id_img_level);
            linearLayout = itemView.findViewById(R.id.id_ly_level);
        }
    }
}
