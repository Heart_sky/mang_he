package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.activity.BoxListActivity;

import java.util.HashMap;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/25 14:24
 * desc   :
 */
public class PBoxList extends XPresenter<BoxListActivity> {


    public void getBoxList(String manghe_id) {
        HashMap<String, String> data = new HashMap<>();
        data.put("manghe_id", manghe_id);
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.Box_Detail, data)
                .get()
                .execute(new BaseBack<BoxModel>() {

                    @Override
                    public void onSuccess(Call call, BoxModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.data != null) {
                            getV().updateList(res.data);
                        }

                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
