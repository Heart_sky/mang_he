package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.fragment.MarketTabFragment;
import com.android.manghe.market.model.MarketAuctionRes;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PMarketTab extends XPresenter<MarketTabFragment> {

    private int mCurrentPage = 1;
    private final String PageSize = "10";
    private final List<MarketAuctionRes.AuctionInfo> mAuctionList = new ArrayList<>();

    private String mId;

    public void setCategoryId(String id) {
        mId = id;
    }

    public void getAuctionData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mAuctionList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("size", PageSize);
        data.put("id", mId);
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.DIRECTBUY_GOODSLISTSDATA, data)
                .get()
                .execute(new BaseBack<MarketAuctionRes>() {

                    @Override
                    public void onSuccess(Call call, MarketAuctionRes res) {
                        if (res != null && res.code == 0 && res.list != null) {
                            mAuctionList.addAll(res.list);
                            getV().setCanLoadMore(mAuctionList.size() != res.list_total);
                            getV().update(mAuctionList);
                            if (mAuctionList.size() != res.list_total) {
                                mCurrentPage++;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
