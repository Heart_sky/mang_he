package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshCartEvent;
import com.android.manghe.market.fragment.MarketDetailFragment;
import com.android.manghe.market.model.CartGoodListRes;
import com.android.manghe.market.model.MarketCommentRes;
import com.android.manghe.market.model.MarketGoodDetailRes;
import com.android.manghe.mine.model.MemberIndexRes;
import com.eightbitlab.rxbus.Bus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PMarketGoodDetail extends XPresenter<MarketDetailFragment> {
    public String goodId = "";
    public MarketGoodDetailRes.DataBean detail;
    public double canUseEdu;//抵用金
    public boolean isMember;

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public void loadData() {
        new OKHttpUtil(getV().getActivity()).url(ConstantsUrl.domain + ConstantsUrl.MAIN_DETAIL + "?id=" + goodId)
                .get()
                .execute(new BaseBack<MarketGoodDetailRes>() {

                    @Override
                    public void onSuccess(Call call, MarketGoodDetailRes res) {
                        if (res != null) {
                            MarketGoodDetailRes.DataBean data = res.data;
                            if (data != null) {
                                getV().updateView(data);
                                detail = data;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void loadComment() {
        HashMap<String, String> data = new HashMap<>();
        data.put("qty", 1 + "");
        data.put("id", goodId);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MAINLIST_COMMENT, data, headMap)
                .get()
                .execute(new BaseBack<MarketCommentRes>() {

                    @Override
                    public void onSuccess(Call call, MarketCommentRes res) {
                        if (res != null && res.code == 0 && res.data != null && res.data.list_total != 0) {
                            getV().showComment(res.data.list.get(0), res.data.list_total);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void addToCard(String spec, int qty) {
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("spec", spec);
        data.put("qty", qty + "");
        data.put("id", goodId);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ADD_TO_CARD, headMap)
                .post(data).build()
                .execute(new BaseBack<CartGoodListRes>() {

                    @Override
                    public void onSuccess(Call call, CartGoodListRes res) {
                        getV().hideLoadingDialog();
                        if (res != null) {
                            if (res.code == 0) {
                                ToastUtil.showLong(getV().getActivity(), "加入购物车成功");
                                Bus.INSTANCE.send(new RefreshCartEvent());
                            } else {
                                ToastUtil.showLong(getV().getActivity(), res.msg);
                            }
                        } else {
                            ToastUtil.showLong(getV().getActivity(), "提交数据失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        try{
                            ToastUtil.showLong(getV().getActivity(), "提交数据失败");
                        }catch (Exception e1){
                            e1.printStackTrace();
                        }
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

    public List<String> htmlConvertPicList(String html) {
        List<String> picUrlList = new ArrayList<>();
        try {
            int startIndex = 0;
            int endIndex = 0;
            final String startTagHttp = "http://www.youtu888.com/upload";
            final String startTagHttps = "https://www.youtu888.com/upload";
            final String endTagJpg = ".jpg";
            final String endTagPng = ".png";
            String currentHtml = html;
            while (currentHtml.contains(startTagHttp) || currentHtml.contains(startTagHttps)) {
                if(currentHtml.contains(startTagHttp)) {
                    startIndex = currentHtml.indexOf(startTagHttp);
                }else if(currentHtml.contains(startTagHttps)){
                    startIndex = currentHtml.indexOf(startTagHttps);
                }

                int pngIndex = currentHtml.indexOf(endTagPng);
                int jpgIndex = currentHtml.indexOf(endTagJpg);
                if (pngIndex > 0 && jpgIndex > 0) {
                    endIndex = Math.min(pngIndex, jpgIndex);
                } else if (pngIndex == -1) {
                    //图片是jpg
                    endIndex = jpgIndex;
                } else if (jpgIndex == -1) {
                    //图片是png
                    endIndex = pngIndex;
                }
                endIndex = endIndex + 4;
                String picUrl = currentHtml.substring(startIndex, endIndex);
                picUrlList.add(picUrl);
                currentHtml = currentHtml.substring(endIndex, currentHtml.length() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return picUrlList;
    }
    public void getMemberInfo() {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERINDEX, headMap)
                .get()
                .execute(new BaseBack<MemberIndexRes>() {

                    @Override
                    public void onSuccess(Call call, MemberIndexRes res) {
                        if (res.code == 0 && res.data != null) {
                            canUseEdu = res.data.member.exemptAmount;
                            isMember = res.data.member.level > 0;
                            getV().updateExemptAmount();
                        }
                    }
                });
    }
}
