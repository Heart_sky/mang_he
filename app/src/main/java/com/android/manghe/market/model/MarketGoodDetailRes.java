package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class MarketGoodDetailRes extends NetBean<MarketGoodDetailRes.DataBean> {

    public static class DataBean {
        /**
         * id : 227
         * name : 美克斯（MKS）360度旋转蒸脸 NV8398A
         * content : <div style="text-align:center;">
         <img src="https://www.ycdy8.com/upload/1/images/edit/20190608032438464940718_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032439714057782_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032439573493319_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032440804612134_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032440307765912_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032440648409409_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032441543434852_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032441559238666_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032441026277819_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032442720060108_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032442766899735_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032442820190685_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032442016678763_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032442206695754_src.jpg" alt="" /><img src="https://www.ycdy8.com/upload/1/images/edit/20190608032443640139124_src.jpg" alt="" />
         </div>
         * thumb : https://www.ycdy8.com/upload/1/images/gallery/x/s/18065_src.jpg
         * thumbs : [{"href":"","src":"https://www.ycdy8.com/upload/1/images/gallery/x/s/18065_src.jpg"},{"href":"","src":"https://www.ycdy8.com/upload/1/images/gallery/x/z/18072_src.jpg"},{"href":"","src":"https://www.ycdy8.com/upload/1/images/gallery/x/w/18069_src.jpg"},{"href":"","src":"https://www.ycdy8.com/upload/1/images/gallery/x/v/18068_src.jpg"}]
         * price : 438.00
         * score : 0
         * limit_buy_bumber : 0
         * is_fav : false
         * count : 0
         * desc :
         * sell : 38
         * luck_num : 0
         * start_time : 0
         * end_time : 0
         * team_price : 0.00
         * team_num : 0
         * sales_num : 0
         * time : -1560269452
         * share_content :
         * typeid : 0
         * stock : 1380
         * favourable_price : 39.00
         * gift_free_coin : 0
         * goods_tip : null
         * tj_list : null
         * store : null
         * url : https://www.ycdy8.com/goods/show/227
         */

        public int id;
        public String name;
        public String content;
        public String thumb;
        public String price;
        public String score;
        public String limit_buy_bumber;
        public boolean is_fav;
        public String count;
        public String desc;
        public String sell;
        public String luck_num;
        public String start_time;
        public String end_time;
        public String team_price;
        public String team_num;
        public String sales_num;
        public int time;
        public String share_content;
        public String typeid;
        public String stock;
        public double favourable_price;
        public int favourable_score;
        public String gift_free_coin;
        public Object goods_tip;
        public Object tj_list;
        public Object store;
        public String url;

        public List<ThumbsBean> thumbs;
        public List<SpArrBean> sp_arr;
        public List<GoodsItemBean> goods_item;

        public static class ThumbsBean {
            /**
             * href :
             * src : https://www.ycdy8.com/upload/1/images/gallery/x/s/18065_src.jpg
             */

            public String href;
            public String src;
        }

        /**
         * 产品属性分类
         */
        public static class SpArrBean {
            /**
             * id : 4
             * name : 闪存
             */

            public String id;
            public String name;
            public List<OptionBean> option;
        }

        public static class OptionBean{
            public String id;
            public String value;
            public boolean isSelected;
            public boolean isShortage;//是否缺货
        }

        public static class GoodsItemBean {
            /**
             * id : 13
             * goods_id : 116
             * goods_name : xxxx
             * spec : 8-11-13
             * price : 2.00
             * score : 4
             * cost : 6.00
             * stock : 8
             * serial :
             * thumb :
             * spec_array : [{"spec_key":"颜色","spec_value":"白色","spec_item_id":8},{"spec_key":"闪存","spec_value":"64G","spec_item_id":11},{"spec_key":"模式","spec_value":"4G","spec_item_id":13}]
             */

            public String id;
            public String goods_id;
            public String goods_name;
            public String spec;
            public String price;
            public String score;
            public String cost;
            public String stock;
            public String serial;
            public String thumb;
            public List<SpecArrayBean> spec_array;

            public static class SpecArrayBean {
                /**
                 * spec_key : 颜色
                 * spec_value : 白色
                 * spec_item_id : 8
                 */

                public String spec_key;
                public String spec_value;
                public int spec_item_id;
            }
        }
    }
}
