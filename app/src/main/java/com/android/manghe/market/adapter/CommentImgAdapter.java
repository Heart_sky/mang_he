package com.android.manghe.market.adapter;

import android.content.Context;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/11 15:28
 * desc   :
 */
public class CommentImgAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private Context mContext;

    public CommentImgAdapter(Context context, @Nullable List<String> data) {
        super(R.layout.item_img, data);
        this.mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder holder, String item) {
        GlideHelper.loadWithHolderErr(mContext, item, holder.itemView.findViewById(R.id.id_img_src));
    }
}
