package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.box.model.BoxPrizeModel;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.activity.OpenBoxHistoryActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/28 14:41
 * desc   :
 */
public class POpenBoxHistory extends XPresenter<OpenBoxHistoryActivity> {

    private int currentPage = 1;
    private final String PageSize = "10";
    private final List<BoxPrizeModel.DataBean.ListBean> boxPrizeList = new ArrayList<>();


    public void getBoxPrizeList(boolean isRefresh) {
        //获取开盒纪录
        if (isRefresh) {
            currentPage = 1;
            boxPrizeList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.Box_Prize_List, data)
                .get()
                .execute(new BaseBack<BoxPrizeModel>() {
                    @Override
                    public void onSuccess(Call call, BoxPrizeModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                boxPrizeList.addAll(res.data.list);
                                getV().setCanLoadMore(boxPrizeList.size() != res.data.list_total);
                                getV().updateList(boxPrizeList);
                                if (boxPrizeList.size() != res.data.list_total) {
                                    currentPage++;
                                }

                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateList(boxPrizeList);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                });

    }
}
