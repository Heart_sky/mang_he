package com.android.manghe.market.activity

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.DisplayUtil
import com.android.manghe.R
import com.android.manghe.market.adapter.MarketGoodsGridAdapter
import com.android.manghe.market.model.MarketAuctionRes
import com.android.manghe.market.presenter.PMarketCatGoodList
import com.android.manghe.view.GridAuctionSpacesItemDecoration
import com.eightbitlab.rxbus.Bus
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_market_cat_good_list.*

/**
 * 小分类的商品列表界面
 */
class MarketCatGoodListActivity: MVPActivity<PMarketCatGoodList>(),IRefresh {
    private var mAdapter : MarketGoodsGridAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_market_cat_good_list
    }
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar(intent.getStringExtra("title")).setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        initListener()
        initListView()
        p.setCategoryId(intent.getStringExtra("id"))
        showLoadingDialog()
        p.getAuctionData(true)
    }
    private fun initListener(){
        tvComprehensive.setOnClickListener{
            //综合
            setFilter(0)
        }
        tvSalesVolume.setOnClickListener{
            //销量
            setFilter(1)
        }
        tvDiscount.setOnClickListener{
            //折扣
            setFilter(2)
        }
        layoutPrice.setOnClickListener{
            //价钱
            setFilter(3)
        }

    }

    private fun initListView(){
        recyclerView.layoutManager =
            GridLayoutManager(this, 2)
        recyclerView.addItemDecoration(GridAuctionSpacesItemDecoration(DisplayUtil.dip2px(this, 8f)))
        mAdapter = MarketGoodsGridAdapter(
            this,false,
            ArrayList<MarketAuctionRes.AuctionInfo>()
        )
        recyclerView.adapter = mAdapter
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getAuctionData(true)
    }

    override fun onLoad() {
        p.getAuctionData(false)
    }
    private fun setPriceSortState(type: Int){
        p.mPriceSort = type
        when(type){
            0-> ivSort.setImageResource(R.mipmap.ic_sort)
            1->ivSort.setImageResource(R.mipmap.ic_sort_top)
            2->ivSort.setImageResource(R.mipmap.ic_sort_down)
        }
    }

    private fun setFilter(type: Int){
        if(type != 3){
            setPriceSortState(0)
        }else{
            when(p.mPriceSort){
                0-> setPriceSortState(1)
                1-> setPriceSortState(2)
                2-> setPriceSortState(1)
            }
        }
        tvComprehensive.setTextColor(ContextCompat.getColor(this, if(type == 0) R.color.red700 else R.color.black))
        tvSalesVolume.setTextColor(ContextCompat.getColor(this, if(type == 1) R.color.red700 else R.color.black))
        tvDiscount.setTextColor(ContextCompat.getColor(this, if(type == 2) R.color.red700 else R.color.black))
        tvPrice.setTextColor(ContextCompat.getColor(this, if(type == 3) R.color.red700 else R.color.black))
        if(type != 3 && p.mFilterType == type){
            return
        }
        p.mFilterType = type
        p.getAuctionData(true)
    }
    fun update(dataList : List<MarketAuctionRes.AuctionInfo>){
        mAdapter?.let {
            it.update(dataList)
            if (dataList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
        layoutFilter.visibility = if(dataList.isNotEmpty()) View.VISIBLE else View.GONE
    }


    fun setCanLoadMore(canLoadMore : Boolean){
        refreshLayout.isEnableLoadMore = canLoadMore
    }
    override fun onDestroy() {
        super.onDestroy()
        Bus.unregister(this)
    }

}