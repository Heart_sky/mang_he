package com.android.manghe.market.model;

public class CategoryContentInfo {
    public String id;
    public String name;
    public String url;
}
