package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.fragment.MarketCommentFragment;
import com.android.manghe.market.model.MarketCommentRes;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PMarketCommentDetail extends XPresenter<MarketCommentFragment> {
    public String goodId = "";
    private int mCurrentPage = 1;
    private List<MarketCommentRes.DataBean.ListBean> mCommentList = new ArrayList<>();

    public void setGoodId(String goodId){
        this.goodId = goodId;
    }

    public void loadData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mCommentList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("id", goodId);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV().getActivity()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV().getActivity()).TOKEN);
        new OKHttpUtil(getV().getContext()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MAINLIST_COMMENT, data, headMap)
                .get()
                .execute(new BaseBack<MarketCommentRes>() {

                    @Override
                    public void onSuccess(Call call, MarketCommentRes res) {
                        if (res != null && res.code == 0) {
                            mCommentList.addAll(res.data.list);
                            getV().setCanLoadMore(mCommentList.size() < res.data.list_total);
                            getV().update(mCommentList);
                            mCurrentPage++;
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
