package com.android.manghe.market.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeModel;
import com.android.manghe.market.adapter.BoxHistoryAdapter;
import com.android.manghe.market.adapter.UltraPagerAdapter;
import com.android.manghe.market.presenter.POpenBoxHistory;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.tmall.ultraviewpager.UltraViewPager;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/28 14:41
 * desc   :
 */
public class OpenBoxHistoryActivity extends MVPActivity<POpenBoxHistory> implements IRefresh {


    private RecyclerView mRecyclerView;
    private UltraViewPager mUltraViewPager;
    private UltraPagerAdapter mUltraPagerAdapter;
    private BoxHistoryAdapter mAdapter;


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("开盒记录").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mRecyclerView = findViewById(R.id.recyclerView);
        mUltraViewPager = findViewById(R.id.ultraViewPager);
        initRecyclerView();
        showLoadingDialog();
        getP().getBoxPrizeList(true);

    }

    private void initRecyclerView() {
        LinearLayoutManager boxHistoryLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(boxHistoryLinearLayoutManager);
        mAdapter = new BoxHistoryAdapter(this, R.layout.item_box_history_2, new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
    }

    public void updateList(List<BoxPrizeModel.DataBean.ListBean> list) {
        mUltraPagerAdapter = new UltraPagerAdapter(this, list);
        mUltraViewPager.setAdapter(mUltraPagerAdapter);
        if (mUltraPagerAdapter.getCount() > 0) {
            mUltraViewPager.setScrollMode(UltraViewPager.ScrollMode.VERTICAL);
            mUltraViewPager.setInfiniteLoop(true);
            mUltraViewPager.setAutoScroll(4000);
        } else {
            mUltraViewPager.setInfiniteLoop(false);
        }
        mAdapter.updateDate(list);
        if (list.size() >= 100) {
            setCanLoadMore(false);
            View footView = LayoutInflater.from(this).inflate(R.layout.layout_foot_tip, null, false);
            mAdapter.addFooterView(footView);

        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_open_box_history;
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return findViewById(R.id.id_refreshLayout);
    }

    @Override
    public void onRefresh() {
        getP().getBoxPrizeList(true);
        if (mAdapter != null) {
            mAdapter.removeAllFooterView();
        }
    }


    @Override
    public void onLoad() {

        getP().getBoxPrizeList(false);


    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);

    }

}
