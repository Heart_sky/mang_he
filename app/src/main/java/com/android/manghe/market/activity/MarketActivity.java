package com.android.manghe.market.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.view.MVPActivity;
import com.android.base.view.roundview.RoundTextView;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.market.adapter.MarketCategoryAdapter;
import com.android.manghe.market.adapter.MarketCategoryContentAdapter;
import com.android.manghe.market.model.MarketCatInfo;
import com.android.manghe.market.presenter.PMarket2;
import com.android.manghe.search.activity.SearchBigMarketActivity;
import com.android.manghe.user.activity.LoginActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;

/**
 * 分类页面
 */
public class MarketActivity extends MVPActivity<PMarket2> {
    private MarketCategoryAdapter mCatAdapter;
    private MarketCategoryContentAdapter mCatContentAdapter;

    private RecyclerView mCatRecyclerView;
    private RecyclerView mCatContentRecyclerView;
    private RoundTextView tvSearch;
    private String mGoodscatid = "";



    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("分类").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine().setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
        mCatRecyclerView = findViewById(R.id.recyclerViewCategory);
        mCatContentRecyclerView = findViewById(R.id.recyclerViewContent);
        tvSearch = findViewById(R.id.tvSearch);

        initView();
        initListener();
        getP().getCategories();

        mGoodscatid = getIntent().getStringExtra("goodscatid");
        if(mCatAdapter!=null){
            if(mCatAdapter.getData().isEmpty()){
                chooseOnLevel(mGoodscatid);
            }
        }
    }

    private void initView() {

        mCatRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mCatAdapter = new MarketCategoryAdapter(this);
        mCatRecyclerView.setAdapter(mCatAdapter);

        mCatContentRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        mCatContentAdapter = new MarketCategoryContentAdapter(this);
        mCatContentRecyclerView.setAdapter(mCatContentAdapter);

    }

    private void initListener() {
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //进入搜索页面
                if (UserHolder.getUserInfo(MarketActivity.this) != null) {
                    //进入搜索页面
                    Intent intent = new Intent(MarketActivity.this, SearchBigMarketActivity.class);
                    startActivity(intent);
                } else {
                    open(LoginActivity.class);
                }
            }
        });


        mCatAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                chooseOnLevel(mCatAdapter.getData().get(position).id);
            }
        });

        mCatContentAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                MarketCatInfo.SubBean catInfo = mCatContentAdapter.getData().get(position);
                Intent intent = new Intent(MarketActivity.this, MarketCatGoodListActivity.class);
                intent.putExtra("id", catInfo.id);
                intent.putExtra("title", catInfo.catname);
                startActivity(intent);
            }
        });

    }


    public void updateCatView() {
        if (mCatAdapter != null) {
            mCatAdapter.setNewData(getP().mCategoryList);
            chooseOnLevel(mGoodscatid);
        }

        if (getP().mCategoryList.isEmpty()) {
            getP().getCategoryContent(mGoodscatid);
        }
    }


    public void updateContentView() {
        if (mCatContentAdapter != null) {
            mCatContentAdapter.setNewData(getP().mContentList);
        }
    }


    private void chooseOnLevel(String goodscatid) {
        mCatAdapter.updateSelected(goodscatid);
        getP().getCategoryContent(goodscatid);
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_market;
    }
}
