package com.android.manghe.market.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.ToastUtil;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeListModel;
import com.android.manghe.box.model.MessageEvent;
import com.android.manghe.config.Config;
import com.android.manghe.market.adapter.GoodsAdapter;
import com.android.manghe.market.presenter.PDeliverGoods;
import com.android.manghe.mine.activity.AddressListActivity;
import com.android.manghe.mine.activity.MyOrderActivity;
import com.android.manghe.mine.model.AddressInfo;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/02 14:33
 * desc   :  发货页面
 */
public class DeliverGoodsActivity extends MVPActivity<PDeliverGoods> {


    private RecyclerView mRecyclerView;
    private GoodsAdapter mAdapter;
    private EditText mEditContent;
    private List<BoxPrizeListModel.DataBean.ListBean> mList;
    private List<String> mIds;
    private TextView mTvTotal, mTvTotalPrice, mTvName, mTvTelNum, mTvAddress;
    private String mTotal, mTotalPrice;
    private LinearLayout mLayoutAddAddress;
    private RelativeLayout mLayoutEditAddress;
    private int ReqCode_SelectAddress = 1000;


    @Override
    public int getLayoutId() {
        return R.layout.activity_deliver_goods;
    }


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("确认发货").hideBottomLine().setLeftIcon(R.mipmap.icon_back_black)
                .setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mRecyclerView = findViewById(R.id.id_recycler_view);
        mEditContent = findViewById(R.id.id_edit_deliver_content);
        mTvTotal = findViewById(R.id.tvTotal);
        mTvTotalPrice = findViewById(R.id.tvTotalPrice);
        mTvName = findViewById(R.id.id_tv_name);
        mTvTelNum = findViewById(R.id.id_tv_tle_num);
        mTvAddress = findViewById(R.id.id_tv_address);
        mLayoutAddAddress = findViewById(R.id.ly_add_address);
        mLayoutEditAddress = findViewById(R.id.ly_edit_address);

        mList = (List<BoxPrizeListModel.DataBean.ListBean>) getIntent().getSerializableExtra("SelectGoods");
        mTotal = getIntent().getStringExtra("SelectCount");
        mTotalPrice = getIntent().getStringExtra("SelectMoney");
        mIds = (List<String>) getIntent().getSerializableExtra("SelectIds");

        mTvTotal.setText(mTotal);
        mTvTotalPrice.setText(mTotalPrice);
        initRecyclerView();
        getP().getAddressList();
    }


    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new GoodsAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);


    }

    public void onClickChooseAddress(View view) {
        Intent intent = new Intent(mContext, AddressListActivity.class);
        intent.putExtra("isToSelect", true);
        startActivityForResult(intent, ReqCode_SelectAddress);
    }

    public void onClickAddAddress(View view) {
        Intent intent = new Intent(mContext, AddressListActivity.class);
        intent.putExtra("isToSelect", true);
        startActivityForResult(intent, ReqCode_SelectAddress);
    }

    public void onClickSureSent(View view) {
        String content = mEditContent.getText().toString().intern();
        if (getP().currentAddress != null) {
            showLoadingDialog();
            getP().setGoods(mIds, 1, getP().currentAddress.id, content);
        } else {
            ToastUtil.showLong(mContext, "请填写收货地址");
        }
    }


    public void setGoodsSuccess() {
        finish();
        EventBus.getDefault().post(new MessageEvent(Config.EVENT_BUS_GET_ORDER));
        Intent intent = new Intent(mContext, MyOrderActivity.class);
        startActivity(intent);
    }

    public void showExchangeErrorTip() {
        ToastUtil.showLong(mContext, "发货失败，请重试");
    }

    public void showAddress(AddressInfo addressInfo) {
        getP().setCurrentAddress(addressInfo);
        if (getP().currentAddress != null) {
            mLayoutAddAddress.setVisibility(View.GONE);
            mLayoutEditAddress.setVisibility(View.VISIBLE);
            mTvName.setText(getP().currentAddress.name);
            mTvAddress.setText(getP().currentAddress.address);
            mTvTelNum.setText(getP().currentAddress.mobile);
        } else {
            mLayoutAddAddress.setVisibility(View.VISIBLE);
            mLayoutEditAddress.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ReqCode_SelectAddress) {
                getP().setCurrentAddress((AddressInfo) data.getSerializableExtra("addressInfo"));
                if (getP().currentAddress != null) {
                    mLayoutAddAddress.setVisibility(View.GONE);
                    mLayoutEditAddress.setVisibility(View.VISIBLE);
                    mTvName.setText(getP().currentAddress.name);
                    mTvAddress.setText(getP().currentAddress.address);
                    mTvTelNum.setText(getP().currentAddress.mobile);
                } else {
                    mLayoutAddAddress.setVisibility(View.VISIBLE);
                    mLayoutEditAddress.setVisibility(View.GONE);
                }
            }
        }
    }
}
