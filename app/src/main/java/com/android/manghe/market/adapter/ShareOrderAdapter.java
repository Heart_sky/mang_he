package com.android.manghe.market.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.market.model.ShareOrderModel;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/10 16:15
 * desc   :
 */
public class ShareOrderAdapter extends BaseQuickAdapter<ShareOrderModel.DataBean.ListBean, BaseViewHolder> {

    private Context mContext;
    private View emptyView;

    public ShareOrderAdapter(Context context, @Nullable List<ShareOrderModel.DataBean.ListBean> data) {
        super(R.layout.item_share_order, data);
        this.mContext = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void update(List<ShareOrderModel.DataBean.ListBean> list) {
        replaceData(list);
    }

    @Override
    protected void convert(BaseViewHolder holder, ShareOrderModel.DataBean.ListBean item) {
        GlideHelper.loadRoundTrans(mContext, item.member_avatar,
                holder.itemView.findViewById(R.id.id_img_src), 20);
        holder.setText(R.id.id_tv_name, item.username);
        holder.setText(R.id.id_tv_time, item.c_time);
        holder.setText(R.id.id_tv_desc, item.content);

        RecyclerView recyclerView = holder.itemView.findViewById(R.id.id_child_recycler_view);
        CommentImgAdapter adapter = new CommentImgAdapter(mContext, item.thumbs);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);


    }
}
