package com.android.manghe.market.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.market.adapter.MarketCommentAdapter
import com.android.manghe.market.model.MarketCommentRes
import com.android.manghe.market.presenter.PMarketCommentDetail
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_market_comment.*

class MarketCommentFragment : MVPFragment<PMarketCommentDetail>(), IRefresh {
    private var mAdapter: MarketCommentAdapter? = null
    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun onRefresh() {
        p.loadData(true)
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {
        p.setGoodId(arguments!!.getString("goodId"))

        mAdapter = MarketCommentAdapter(arrayListOf())
        mAdapter!!.emptyView = layoutInflater.inflate(R.layout.no_data, null)
        recyclerView.adapter = mAdapter
        recyclerView.layoutManager =
            LinearLayoutManager(activity)
        recyclerView.addItemDecoration(RecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL))

        refreshLayout.autoRefresh()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_market_comment
    }

    override fun onLoad() {
        p.loadData(false)
    }

    fun update(dataList: List<MarketCommentRes.DataBean.ListBean>) {
        mAdapter?.let {
            it.replaceData(dataList)
        }
    }

    fun setCanLoadMore(canLoadMore: Boolean) {
        refreshLayout.isEnableLoadMore = canLoadMore
    }
}