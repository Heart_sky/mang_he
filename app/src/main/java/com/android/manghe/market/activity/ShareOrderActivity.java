package com.android.manghe.market.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.market.adapter.ShareOrderAdapter;
import com.android.manghe.market.model.ShareOrderModel;
import com.android.manghe.market.presenter.PShareOrder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/28 10:23
 * desc   :
 */
public class ShareOrderActivity extends MVPActivity<PShareOrder> implements IRefresh {

    private RecyclerView mRecyclerView;
    private String manghe_id;
    private BoxModel.DataBean mCurrentListBean;
    private ShareOrderAdapter mAdapter;

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        getTitleBar().setToolbar("晒单分享").setLeftIcon(R.mipmap.icon_back_black).
                hideBottomLine().setBackgroundColor(ContextCompat.getColor(this, R.color.white));

        mRecyclerView = findViewById(R.id.recyclerView);
        initRecyclerView();

        mCurrentListBean = (BoxModel.DataBean) getIntent().getSerializableExtra("BoxListModel");
        if (mCurrentListBean != null) {
            manghe_id = mCurrentListBean.id;
            showLoadingDialog();
            getP().getShareOrderList(true, manghe_id);
        }


    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ShareOrderAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_share_order;
    }

    public void updateDate(List<ShareOrderModel.DataBean.ListBean> listBeans) {
        mAdapter.update(listBeans);
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        getP().getShareOrderList(true, manghe_id);
    }

    @Override
    public void onLoad() {
        getP().getShareOrderList(false, manghe_id);
    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }



}
