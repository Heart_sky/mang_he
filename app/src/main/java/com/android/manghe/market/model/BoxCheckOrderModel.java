package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/19 17:25
 * desc   :
 */


public class BoxCheckOrderModel extends NetBean<BoxCheckOrderModel.DataBean> {
    /**
     * data : {"manghe_cat_id":4,"mid":911730,"c_time":1637649194,"buy_num":1,"total":1,"free_coin":0.4,"coin":0,"cash":0.6,"withhold":0.4}
     * flag : true
     */

    public static class DataBean implements Serializable {
        /**
         * manghe_cat_id : 4
         * mid : 911730
         * c_time : 1637649194
         * buy_num : 1
         * total : 1
         * free_coin : 0.4
         * coin : 0
         * cash : 0.6
         * withhold : 0.4
         * score
         * score_offset
         * deduct_score_cash
         */

        public int manghe_cat_id;
        public int mid;
        public int c_time;
        public int buy_num;
        public double total;
        public double free_coin;
        public double remain_free_coin;//剩余赠币
        public double coin;
        public double remain_coin;//剩余余额
        public double cash;
        public double withhold;
        public int score;
        public double score_offset;
    }


//    public String free_coin;//赠送币抵扣
//    public String coin;//余额抵扣
//    public double cash;//需要支付的现金
//    public double withhold;//抵扣额度

}
