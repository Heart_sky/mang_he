package com.android.manghe.market.adapter.multi;

import com.android.manghe.market.model.CartGoodListRes;
import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

public class CartGoodLevel extends AbstractExpandableItem<CartGoodLevel> implements MultiItemEntity {

    public List<CartGoodListRes.DataBean.CartGoodsBean> goodList;

    public CartGoodLevel(List<CartGoodListRes.DataBean.CartGoodsBean> dataList){
        this.goodList = dataList;
    }
    @Override
    public int getLevel() {
        return 0;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}
