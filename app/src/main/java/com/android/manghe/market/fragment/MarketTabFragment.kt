package com.android.manghe.market.fragment

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.View
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.extend.IStateController
import com.android.base.frame.title.ETitleType
import com.android.base.frame.view.MVPFragment
import com.android.base.frame.view.XStateController
import com.android.base.tools.DisplayUtil
import com.android.manghe.R
import com.android.manghe.market.adapter.MarketGoodsGridAdapter
import com.android.manghe.market.model.MarketAuctionRes
import com.android.manghe.market.presenter.PMarketTab
import com.android.manghe.view.GridAuctionSpacesItemDecoration
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.fragment_market_tab.*

class MarketTabFragment : MVPFragment<PMarketTab>(),IRefresh , IStateController<XStateController> {
    override fun getStateView(): XStateController {
        return mView.findViewById(R.id.xStateController)
    }
    private var mAdapter : MarketGoodsGridAdapter? = null

    override fun showToolBarType(): ETitleType {
        return ETitleType.NO_TITLE
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_market_tab
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListView()
        p.setCategoryId(arguments!!.getString("id"))
        refreshLayout.autoRefresh()
    }

    override fun initData(savedInstanceState: Bundle?, parent: View?) {

    }

    private fun initListView(){
        recyclerView.layoutManager =
            GridLayoutManager(activity, 2)
        recyclerView.addItemDecoration(GridAuctionSpacesItemDecoration(DisplayUtil.dip2px(activity, 8f)))
        mAdapter = MarketGoodsGridAdapter(
            activity,false,
            ArrayList<MarketAuctionRes.AuctionInfo>()
        )
        recyclerView.adapter = mAdapter
    }

    override fun getRefreshView(): SmartRefreshLayout {
        return mView.findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.getAuctionData(true)
    }

    override fun onLoad() {
        p.getAuctionData(false)
    }

    fun update(dataList : List<MarketAuctionRes.AuctionInfo>){
        mAdapter?.let {
            it.update(dataList)
            if (dataList.isEmpty()) {
                xStateController.showEmpty()
            } else {
                xStateController.showContent()
            }
        }
    }


    fun setCanLoadMore(canLoadMore : Boolean){
        refreshLayout.isEnableLoadMore = canLoadMore
    }
}