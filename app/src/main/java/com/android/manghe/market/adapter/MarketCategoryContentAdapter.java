package com.android.manghe.market.adapter;

import android.content.Context;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.market.model.MarketCatInfo;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;

public class MarketCategoryContentAdapter extends BaseQuickAdapter<MarketCatInfo.SubBean, BaseViewHolder> {
    private Context mContext;

    public MarketCategoryContentAdapter(Context context) {
        super(R.layout.item_market_category_content, new ArrayList<>());
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, MarketCatInfo.SubBean item) {
        helper.setText(R.id.tvName, item.catname);
        GlideHelper.load(mContext, item.thumb, helper.getView(R.id.ivPic));
    }
}
