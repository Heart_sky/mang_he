package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class MarketAuctionRes extends NetBean {


    /**
     * list : [{"id":"5","name":"Apple iPad 平板电脑 2018年新款9.7英寸（128G WLAN版/A10 芯片/Retina显示屏/Touch ID MRJP2CH/A）金色","price":"3699.00","score":"0","end_time":"0","team_num":"0","team_price":"0.00","run_time":1553527105,"thumb":"http://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg"},{"id":"4","name":"Apple iPhone 8 Plus (A1864) 64GB 深空灰色 移动联通电信4G手机","price":"6269.00","score":"0","end_time":"0","team_num":"0","team_price":"0.00","run_time":1553527105,"thumb":"http://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg"}]
     * list_total : 2
     * flag : true
     * code : 0
     * msg : 操作成功
     * data : null
     * time : 1553527105
     */

    public int list_total;
    public List<AuctionInfo> list;

    public static class AuctionInfo extends NetBean<List<AuctionInfo>>{
        /**
         * id : 5
         * name : Apple iPad 平板电脑 2018年新款9.7英寸（128G WLAN版/A10 芯片/Retina显示屏/Touch ID MRJP2CH/A）金色
         * price : 3699.00
         * score : 0
         * end_time : 0
         * team_num : 0
         * team_price : 0.00
         * run_time : 1553527105
         * thumb : http://www.ycdy8.com/upload/1/images/gallery/9/e/13299_src.jpg
         */

        public String id;
        public String name;
        public String price;
        public String score;
        public String end_time;
        public String team_num;
        public double team_price;
        public int run_time;
        public String thumb;
        public String img_src;//显示一张图片的时候用这个
        public int favourable_score;
        public String manghe_cat_id;//盲盒id



    }
}
