package com.android.manghe.market.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.market.activity.BoxListActivity;
import com.android.manghe.market.model.MarketAuctionRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class MarketGoodsGridAdapter extends BaseQuickAdapter<MarketAuctionRes.AuctionInfo, BaseViewHolder> {
    private Context mContext;
    private boolean isFromCart;
    private View emptyView;

    public MarketGoodsGridAdapter(Context context, boolean isFromCart, List<MarketAuctionRes.AuctionInfo> list) {
        super(R.layout.item_market_grid_auction, list);
        mContext = context;
        this.isFromCart = isFromCart;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void update(List<MarketAuctionRes.AuctionInfo> list) {
        replaceData(list);
    }

    @Override
    protected void convert(BaseViewHolder holder, MarketAuctionRes.AuctionInfo item) {
        GlideHelper.loadWithHolderErr(mContext, isFromCart ? item.img_src : item.thumb, holder.getView(R.id.ivPic));
        holder.setText(R.id.tvCurrentPrice, "￥" + item.price);
        holder.setText(R.id.tvTitle, item.name);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, BoxListActivity.class);
                intent.putExtra("mangheCatId", item.manghe_cat_id);
                mContext.startActivity(intent);
            }
        });
    }
}
