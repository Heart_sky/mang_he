package com.android.manghe.market.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.utils.WindowUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/10 15:09
 * desc   :
 */
public class ShowBoxGoodsAdapter extends BaseQuickAdapter<BoxModel.DataBean.GoodsListBean, BaseViewHolder> {
    private Context mContext;
    private View emptyView;

    public ShowBoxGoodsAdapter(Context context, @Nullable List<BoxModel.DataBean.GoodsListBean> data) {
        super(R.layout.item_show_goods, data);
        this.mContext = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }


    @Override
    protected void convert(BaseViewHolder holder, BoxModel.DataBean.GoodsListBean item) {
        try {
            GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.itemView.findViewById(R.id.ivPic));
            holder.setText(R.id.id_tv_price_value, "市场价：" + "￥" + item.price);
            holder.setText(R.id.tvTitle, item.name);
            RoundLinearLayout linearLayout = holder.itemView.findViewById(R.id.id_ly_level);
            ImageView imgLevel = holder.itemView.findViewById(R.id.id_img_level);
            TextView tvLevel = holder.itemView.findViewById(R.id.id_tv_level);
            WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, item.probability_level);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
