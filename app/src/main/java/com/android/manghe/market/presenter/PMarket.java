package com.android.manghe.market.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.common.model.RecommendSecondLevelInfo;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.fragment.MarketFragment;
import com.android.manghe.market.model.MarketCatInfo;
import com.android.manghe.utils.StringConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

public class PMarket extends XPresenter<MarketFragment> {
    public List<MarketCatInfo> mCategoryList = new ArrayList<>();
    public List<MarketCatInfo.SubBean> mContentList = new ArrayList();

    public void getCategories() {
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.SORTLISTDATA + "?type=1") //1商城分类，0抢购分类
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        super.onSuccess(call, json);
                        try{
                            GsonBuilder gb = new GsonBuilder();
                            gb.registerTypeAdapter(String.class, new StringConverter());
                            Gson gson = gb.create();
                            MarketCatInfo res = gson.fromJson(json, MarketCatInfo.class);
                            if (res.code == 0 && res.data != null && !res.data.isEmpty()) {
                                mCategoryList.clear();
                                mCategoryList.add(new MarketCatInfo("", "热门推荐", true));
                                mCategoryList.addAll(res.data);
                                getV().updateCatView();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    public void getCategoryContent(String catId) {
        mContentList.clear();
        if(TextUtils.isEmpty(catId)){
            //热门推荐
            new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.RecommendSecondLevel)
                    .get()
                    .execute(new BaseBack<RecommendSecondLevelInfo>() {

                        @Override
                        public void onSuccess(Call call, List<RecommendSecondLevelInfo> infoList) {
                            if (infoList != null && mContentList.isEmpty()) {
                                for(RecommendSecondLevelInfo info : infoList){
                                    mContentList.add(new MarketCatInfo.SubBean(info.id, info.catname, info.thumb));
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            super.onFailure(e);
                        }

                        @Override
                        public void onComplete() {
                            getV().updateContentView();
                        }
                    });
        }else {
            for (MarketCatInfo info : mCategoryList) {
                if (TextUtils.equals(catId, info.id) && info.sub != null) {
                    mContentList.addAll(info.sub);
                    break;
                }
            }
            getV().updateContentView();
        }

    }
}
