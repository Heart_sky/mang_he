package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class MarketCommentRes extends NetBean<MarketCommentRes.DataBean> {


    public static class DataBean {
        /**
         * list_total : 1
         * list : [{"id":"4","mid":"101738","order_id":"416","good_id":"119","goods_spec":"","star":"5","content":"速度","thumbs":["https://www.ycdy8.com:443/upload/1/images/101738/20190611113555178429480_src.jpg"],"state":"1","c_time":"1560267356","u_time":"0","listorder":"0","buy_num":"1","sid":"0","nickname":"妈卖批都是我的","photo":"https://thirdwx.qlogo.cn/mmopen/vi_32/Gk4AZcP2TQdtJBkG8pibXMMG0lDFxfsKnCQxJsTmqibkCgYRmTW2VQnmR58GK59hWDs1teTa41tMaqiafBkyTuicXg/132","goods_name":"100积分兑换30赠币"}]
         */

        public int list_total;
        public List<ListBean> list;

        public static class ListBean {
            /**
             * id : 4
             * mid : 101738
             * order_id : 416
             * good_id : 119
             * goods_spec :
             * star : 5
             * content : 速度
             * thumbs : ["https://www.ycdy8.com:443/upload/1/images/101738/20190611113555178429480_src.jpg"]
             * state : 1
             * c_time : 1560267356
             * u_time : 0
             * listorder : 0
             * buy_num : 1
             * sid : 0
             * nickname : 妈卖批都是我的
             * photo : https://thirdwx.qlogo.cn/mmopen/vi_32/Gk4AZcP2TQdtJBkG8pibXMMG0lDFxfsKnCQxJsTmqibkCgYRmTW2VQnmR58GK59hWDs1teTa41tMaqiafBkyTuicXg/132
             * goods_name : 100积分兑换30赠币
             */

            public String id;
            public String mid;
            public String order_id;
            public String good_id;
            public String goods_spec;
            public String star;
            public String content;
            public String state;
            public String c_time;
            public String u_time;
            public String listorder;
            public String buy_num;
            public String sid;
            public String nickname;
            public String photo;
            public String goods_name;
            public List<String> thumbs;
        }
    }
}
