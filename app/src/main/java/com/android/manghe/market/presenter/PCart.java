package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.activity.CartActivity;
import com.android.manghe.market.model.CartGoodListRes;
import com.android.manghe.market.model.MarketAuctionRes;
import com.android.manghe.mine.model.MemberIndexRes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PCart extends XPresenter<CartActivity> {
    public List<CartGoodListRes.DataBean.CartGoodsBean> goodList = new ArrayList<>();
    public final List<MarketAuctionRes.AuctionInfo> recommendGoodList = new ArrayList<>();
    public int flowUserScore;
    public double couponFavourablePrice;
    public boolean isMember;

    public void loadCardGoodData(boolean isFirstLoad){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.GET_CARD_GOODS, headMap)
                .get()
                .execute(new BaseBack<CartGoodListRes>() {

                    @Override
                    public void onSuccess(Call call, CartGoodListRes res) {
                        getV().hideLoadingDialog();
                        if (res != null) {
                            if(res.code == 0 && res.data != null){
                                couponFavourablePrice = res.data.coupon_favourable_price;
                                flowUserScore = res.data.flow_user_score;
                                goodList.clear();
                                goodList.addAll(res.data.cart_goods);
                            }else{
                                if(isFirstLoad) {
                                    ToastUtil.showLong(getV(), res.msg);
                                }else{
                                    goodList.clear();
                                }
                            }
                            getV().updateCartGoods();
                        }else{
                            ToastUtil.showLong(getV(), "获取购物车商品失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        ToastUtil.showLong(getV(), "获取购物车商品失败");
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

    public void loadRecommendGoods(){
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.GET_CARD_RECOMMEND,headMap)
                .get()
                .execute(new BaseBack<MarketAuctionRes.AuctionInfo>() {
                    @Override
                    public void onSuccess(Call call, List<MarketAuctionRes.AuctionInfo> list) {
                        if (list != null) {
                            recommendGoodList.clear();
                            recommendGoodList.addAll(list);
                        }
                        getV().updateRecommendGoods();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteCartGood(String id){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.DELETE_CARD + "?ids="+id,headMap)
                .get()
                .execute(new BaseBack<NetBean>() {
                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        getV().hideLoadingDialog();
                        if (res != null) {
                            if(res.code == 0) {
                                ToastUtil.showShort(getV(), "删除成功");
                                loadCardGoodData(false);
                            }else{
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        }else{
                            ToastUtil.showShort(getV(), "删除失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        ToastUtil.showShort(getV(), "删除失败");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void modifyCardGood(String goodId , String spec, int qty, boolean isSelected){
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("spec", spec);
        data.put("qty", qty +"");
        data.put("goods_id", goodId);
        data.put("is_selected", isSelected ? "1" : "0");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.EDIT_CARD, headMap)
                .post(data).build()
                .execute(new BaseBack<CartGoodListRes>() {

                    @Override
                    public void onSuccess(Call call, CartGoodListRes res) {
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

    public void getSelectedGoodList(){

    }

    public void getMemberInfo() {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERINDEX, headMap)
                .get()
                .execute(new BaseBack<MemberIndexRes>() {

                    @Override
                    public void onSuccess(Call call, MemberIndexRes res) {
                        if (res.code == 0 && res.data != null) {
                            isMember = res.data.member.level > 0;
                        }
                    }
                });
    }
}
