package com.android.manghe.market.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPActivity;
import com.android.base.tools.DisplayUtil;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.box.model.MessageEvent;
import com.android.manghe.config.Config;
import com.android.manghe.market.adapter.BoxListAdapter;
import com.android.manghe.market.presenter.PBoxList;
import com.android.manghe.view.GridAuctionSpacesItemDecoration;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/25 14:24
 * desc   : 所在盒子的list页面
 */
public class BoxListActivity extends MVPActivity<PBoxList> implements IRefresh {

    private RecyclerView mRecyclerView;
    private BoxListAdapter mAdapter;
    private String mMangHeCatId;
    private List<BoxModel.DataBean> mList = new ArrayList<>();

    @Override
    public int getLayoutId() {
        return R.layout.activity_box_list;
    }

    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {
        EventBus.getDefault().register(this);
        getTitleBar().setToolbar("商品所在盲盒").setLeftIcon(R.mipmap.icon_back_black).hideBottomLine()
                .setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        mRecyclerView = findViewById(R.id.recyclerView);
        mMangHeCatId = getIntent().getStringExtra("mangheCatId");
        initRecyclerView();
        showLoadingDialog();
        getP().getBoxList(mMangHeCatId);
    }

    private void initRecyclerView() {
        setEnableRefresh(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.addItemDecoration(new GridAuctionSpacesItemDecoration(DisplayUtil.dip2px(this, 8f)));
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mAdapter = new BoxListAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);

    }

    public void updateList(BoxModel.DataBean dataBean) {
        mList.clear();
        mList.add(dataBean);
        mAdapter.updateDate(mList);
    }

    public void setEnableRefresh(Boolean canLoadMore) {
        getRefreshView().setEnableRefresh(canLoadMore);
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        getP().getBoxList(mMangHeCatId);
    }

    @Override
    public void onLoad() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
       if(event.getEventType()==Config.EVENT_BUS_CLOSE_ACTIVITY){
            finish();
        }
    }

}
