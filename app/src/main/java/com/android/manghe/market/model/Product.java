package com.android.manghe.market.model;

import android.text.TextUtils;

import com.wuhenzhizao.sku.bean.Sku;
import com.wuhenzhizao.sku.bean.SkuAttribute;

import java.util.ArrayList;
import java.util.List;

public class Product {
    private String id;
    private String name;
    private String mainImage;
    private double sellingPrice;
    private List<Sku> skus;

    public static Product get(MarketGoodDetailRes.DataBean detail) {
        Product product = new Product();
        try{
            product.setId(detail.id + "");
            product.setName(detail.name);
            product.setMainImage(detail.thumbs.get(0).src);
            product.setSellingPrice(Double.parseDouble(detail.price));

            List<MarketGoodDetailRes.DataBean.GoodsItemBean> specGoodList = detail.goods_item;
            List<Sku> skuList = new ArrayList<>();
            for(MarketGoodDetailRes.DataBean.GoodsItemBean goodBean : specGoodList){
                Sku sku = new Sku();
                SkuAttribute skuAttribute;
                List<SkuAttribute> skuAttributeList = new ArrayList<>();
                for(int i = 0;i<goodBean.spec_array.size();i++){
                    MarketGoodDetailRes.DataBean.GoodsItemBean.SpecArrayBean specOption = goodBean.spec_array.get(i);
                    skuAttribute = new SkuAttribute();
                    skuAttribute.setKey(specOption.spec_key);
                    skuAttribute.setValue(specOption.spec_value);
                    skuAttributeList.add(skuAttribute);
                }
                sku.setAttributes(skuAttributeList);
                sku.setId(goodBean.spec);
                sku.setMainImage(TextUtils.isEmpty(goodBean.thumb) ? product.mainImage : goodBean.thumb);
                sku.setSellingPrice(new Double(goodBean.price).longValue());
                sku.setOriginPrice(new Double(goodBean.cost).longValue());
                sku.setStockQuantity(Integer.parseInt(goodBean.stock));
                skuList.add(sku);
            }
            product.setSkus(skuList);
        }catch (Exception e){

        }

        return product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }


    public List<Sku> getSkus() {
        return skus;
    }

    public void setSkus(List<Sku> skus) {
        this.skus = skus;
    }
}
