package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.activity.ShareOrderActivity;
import com.android.manghe.market.model.ShareOrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/28 11:14
 * desc   :
 */
public class PShareOrder extends XPresenter<ShareOrderActivity> {




    private int currentPage = 1;
    private final String PageSize = "10";
    private final List<ShareOrderModel.DataBean.ListBean> shareOrderList = new ArrayList<>();


    public void getShareOrderList(boolean isRefresh,String manghe_id) {
        //获取开盒纪录
        if (isRefresh) {
            currentPage = 1;
            shareOrderList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", currentPage + "");
        data.put("size", PageSize);
        data.put("manghe_id",manghe_id);
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.Box_Comment_List, data)
                .get()
                .execute(new BaseBack<ShareOrderModel>() {
                    @Override
                    public void onSuccess(Call call, ShareOrderModel res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            if (res.data.list != null) {
                                shareOrderList.addAll(res.data.list);
                                getV().setCanLoadMore(shareOrderList.size() != res.data.list_total);
                                getV().updateDate(shareOrderList);

                                if (shareOrderList.size() != res.data.list_total) {
                                    currentPage++;
                                }
                            } else {
                                getV().setCanLoadMore(false);
                                getV().updateDate(shareOrderList);
                            }

                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                    }

                });

    }
}
