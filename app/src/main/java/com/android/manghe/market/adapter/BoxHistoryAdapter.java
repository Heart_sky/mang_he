package com.android.manghe.market.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeModel;
import com.android.manghe.utils.WindowUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/25 16:40
 * desc   :
 */
public class BoxHistoryAdapter extends BaseQuickAdapter<BoxPrizeModel.DataBean.ListBean, BaseViewHolder> {
    private Context mContext;
    private View emptyView;

    public BoxHistoryAdapter(Context context, int layoutResId, @Nullable List<BoxPrizeModel.DataBean.ListBean> data) {
        super(layoutResId, data);
        this.mContext = context;
        emptyView = LayoutInflater.from(context).inflate(R.layout.no_data, null);
        emptyView.setVisibility(data.size() > 0 ? View.GONE : View.VISIBLE);
        setEmptyView(emptyView);
    }

    public void updateDate(List<BoxPrizeModel.DataBean.ListBean> list) {
        replaceData(list);
    }


    @Override
    protected void convert(BaseViewHolder holder, BoxPrizeModel.DataBean.ListBean item) {
        GlideHelper.loadWithHolderErr(mContext, item.goods_thumb, holder.itemView.findViewById(R.id.id_img_src));
        holder.setText(R.id.id_tv_price, "价值：￥" + item.goods_price);
        holder.setText(R.id.id_tv_name, item.goods_name);
        holder.setText(R.id.id_tv_open_history, item.created_at);

        RoundLinearLayout linearLayout = holder.itemView.findViewById(R.id.id_ly_level);
        ImageView imgLevel = holder.itemView.findViewById(R.id.id_img_level);
        TextView tvLevel = holder.itemView.findViewById(R.id.id_tv_level);
        WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, item.goods_probability_level);

    }
}
