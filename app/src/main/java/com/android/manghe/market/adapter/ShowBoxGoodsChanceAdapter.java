package com.android.manghe.market.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.layout.LinearLayoutHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/02/17 09:46
 * desc   :
 */
public class ShowBoxGoodsChanceAdapter extends DelegateAdapter.Adapter<ShowBoxGoodsChanceAdapter.MyViewHolder> {


    private Context mContext;
    private BoxModel.DataBean mCurrentBoxModel;

    public ShowBoxGoodsChanceAdapter(Context mContext, BoxModel.DataBean mCurrentBoxModel) {
        this.mContext = mContext;
        this.mCurrentBoxModel = mCurrentBoxModel;
    }

    @Override
    public LayoutHelper onCreateLayoutHelper() {
        return new LinearLayoutHelper();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_box_chancge, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (mCurrentBoxModel.front_level_ratio_config != null) {
            holder.mTvOne.setText("概率" + mCurrentBoxModel.front_level_ratio_config._$1);
            holder.mTvTwo.setText("概率" + mCurrentBoxModel.front_level_ratio_config._$2);
            holder.mTvThree.setText("概率" + mCurrentBoxModel.front_level_ratio_config._$3);
            holder.mTvFour.setText("概率" + mCurrentBoxModel.front_level_ratio_config._$4);
        }
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 2;
    }

   static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvOne, mTvTwo, mTvThree, mTvFour;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvOne = itemView.findViewById(R.id.id_tv_1);
            mTvTwo = itemView.findViewById(R.id.id_tv_2);
            mTvThree = itemView.findViewById(R.id.id_tv_3);
            mTvFour = itemView.findViewById(R.id.id_tv_4);
        }
    }
}
