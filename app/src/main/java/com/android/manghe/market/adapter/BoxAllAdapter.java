package com.android.manghe.market.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.utils.WindowUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/25 16:27
 * desc   :
 */
public class BoxAllAdapter extends BaseQuickAdapter<BoxModel.DataBean.GoodsListBean, BaseViewHolder> {

    private Context mContext;

    public BoxAllAdapter(int layoutResId, Context context, @Nullable List<BoxModel.DataBean.GoodsListBean> data) {
        super(layoutResId, data);
        this.mContext = context;
    }

    public BoxAllAdapter(Context context, @Nullable List<BoxModel.DataBean.GoodsListBean> data) {
        super(R.layout.item_box_all, data);
        this.mContext = context;
    }

    public void update(List<BoxModel.DataBean.GoodsListBean> data) {
        replaceData(data);
    }


    @Override
    protected void convert(BaseViewHolder holder, BoxModel.DataBean.GoodsListBean item) {
        GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.itemView.findViewById(R.id.id_img_src));
        holder.setText(R.id.id_tv_price, "￥" + item.price);
        RoundLinearLayout linearLayout = holder.itemView.findViewById(R.id.id_ly_level);
        ImageView imgLevel = holder.itemView.findViewById(R.id.id_img_level);
        TextView tvLevel = holder.itemView.findViewById(R.id.id_tv_level);
        WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, item.probability_level);

    }
}
