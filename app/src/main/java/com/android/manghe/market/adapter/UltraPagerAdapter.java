package com.android.manghe.market.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/28 15:51
 * desc   :
 */
public class UltraPagerAdapter extends PagerAdapter {
    private String[] noticeItemHtmlArr;
    private Context mContext;
    private List<BoxPrizeModel.DataBean.ListBean> mIndexList;

    public UltraPagerAdapter(Context context, List<BoxPrizeModel.DataBean.ListBean> list) {
        this.mContext = context;
        noticeItemHtmlArr = new String[]{};
        mIndexList = new ArrayList<>();

        mIndexList.clear();
        mIndexList.addAll(list);
        noticeItemHtmlArr = new String[mIndexList.size()];
        for (int i = 0; i < mIndexList.size(); i++) {
            BoxPrizeModel.DataBean.ListBean bean = mIndexList.get(i);
            noticeItemHtmlArr[i] = "<strong><font color='#999999'>恭喜</font>"
                    + "<font color='#999999'>#" + bean.nickname + "#</font>"
                    + "<font color='#e84e40'>" + bean.buy_way + "</font>"
                    + "<font color='#999999'>获得</font>"
                    + "<font color='#999999'>" + bean.goods_name + "</font>";
        }
    }

    public void updateDate(List<BoxPrizeModel.DataBean.ListBean> list) {
        mIndexList.clear();
        mIndexList.addAll(list);
        noticeItemHtmlArr = new String[mIndexList.size()];
        for (int i = 0; i < mIndexList.size(); i++) {
            BoxPrizeModel.DataBean.ListBean bean = mIndexList.get(i);
            noticeItemHtmlArr[i] = "<strong><font color='#999999'>恭喜</font>"
                    + "<font color='#999999'>#" + bean.nickname + "#</font>"
                    + "<font color='#e84e40'>" + bean.buy_way + "</font>"
                    + "<font color='#999999'>获得</font>"
                    + "<font color='#999999'>" + bean.goods_name + "</font>"
                    + "</strong>";

        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return noticeItemHtmlArr.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        TextView tv = (TextView) LayoutInflater.from(container.getContext()).inflate(R.layout.layout_pager_tv_item, null);
        tv.setText(Html.fromHtml(noticeItemHtmlArr[position]));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(noticeItemHtmlArr[position], Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(noticeItemHtmlArr[position]));
        }
        container.addView(tv);
        return tv;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        TextView view = (TextView) object;
        container.removeView(view);
    }
}
