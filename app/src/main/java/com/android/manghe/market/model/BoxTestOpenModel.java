package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/09 16:25
 * desc   :
 */
public class BoxTestOpenModel extends NetBean<BoxTestOpenModel.DataBean> {


    /**
     * data : {"goods_id":"2133","goods_name":"三只松鼠巨型零食大礼包/内含30包 送礼送女友礼物芒果干网红薯片饼干锅巴辣条肉干肉脯/6斤装","goods_price":"228.00","thumb":"https://www.mxmd88.com/upload/1/images/gallery/i/a/29171_src.jpg"}
     * flag : true
     */


    public static class DataBean implements Serializable {
        /**
         * goods_id : 2133
         * goods_name : 三只松鼠巨型零食大礼包/内含30包 送礼送女友礼物芒果干网红薯片饼干锅巴辣条肉干肉脯/6斤装
         * goods_price : 228.00
         * thumb : https://www.mxmd88.com/upload/1/images/gallery/i/a/29171_src.jpg
         */

        public String goods_id;
        public String goods_name;
        public String goods_price;
        public String thumb;
        public String probability_level;
    }
}
