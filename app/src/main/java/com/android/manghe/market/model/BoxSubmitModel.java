package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/22 16:38
 * desc   :
 */
public class BoxSubmitModel extends NetBean<BoxSubmitModel.DataBean> {


    /**
     * data : {"order_id":10}
     * flag : true
     */


    public static class DataBean implements Serializable {
        /**
         * order_id : 10
         */

        public int order_id;
        public boolean need_pay;
    }
}
