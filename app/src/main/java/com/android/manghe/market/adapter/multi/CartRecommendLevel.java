package com.android.manghe.market.adapter.multi;

import com.android.manghe.market.model.MarketAuctionRes;
import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.util.List;

public class CartRecommendLevel extends AbstractExpandableItem<CartRecommendLevel> implements MultiItemEntity {
    public List<MarketAuctionRes.AuctionInfo> goodList;

    public CartRecommendLevel(List<MarketAuctionRes.AuctionInfo> goodList){
        this.goodList = goodList;
    }
    @Override
    public int getLevel() {
        return 1;
    }

    @Override
    public int getItemType() {
        return 1;
    }
}
