package com.android.manghe.market.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;
import java.util.List;

/**
 * 确认订单的数据
 */
public class CartGoodListRes extends NetBean<CartGoodListRes.DataBean> {

    public static class DataBean implements Serializable{

        /**
         * cart_goods : [{"id":10,"goods_name":"商品名称","spec":"","goods_price":10,"qty":10,"subtotal":10,"stock":10,"goods_id":10,"goods_cid":"商品分类","goods_thumb":"商品缩略图"}]
         * total : {"goods_amount":100}
         */

        public TotalBean total;//总价
        public List<CartGoodsBean> cart_goods;//订单包含的商品信息

        public int flow_user_score;
        public double coupon_favourable_price;

        public static class TotalBean {
            /**
             * goods_amount : 100.0
             */

            public double goods_amount;
        }


        public static class CartGoodsBean implements Serializable {
            /**
             * id : 110900
             * mid : 427273
             * goods_id : 308
             * spec : 白色 32G 5G
             * goods_name : Apple iPhone 11 Pro  64GB全网通（颜色备注）
             * cost_price : 8699.00
             * goods_price : null
             * goods_score : 0
             * qty : 2
             * subtotal : 0
             * subtotal_score : 0
             * type : 0
             * obj_id : 0
             * is_selected : 1
             * goods_cid : 159
             * goods_stock : 1200
             * goods_is_sale : 1
             * goods_weight : 0
             * goods_weight_unit : kg
             * goods_sp_val : {"1":{"2":"黑色","3":"白色","1":"黄色"},"2":{"5":"32G","6":"64G"},"3":{"8":"4G","9":"5G"}}
             * goods_favourable_price : 1100.00
             * goods_spec :
             * thumb : [{"path":"\/upload\/1\/images\/gallery\/h\/f\/17476_src.jpg","title":""}]
             * img_src : http://new.qmjp8.com/upload/1/images/gallery/h/f/17476_src.jpg
             * url : /goods/show/308
             */

            public String id;
            public String mid;
            public String goods_id;
            public String spec;
            public String goods_name;
            public double cost_price;
            public double goods_price;
            public int goods_score;
            public int qty;
            public double subtotal;
            public int subtotal_score;
            public String type;
            public String obj_id;
            public String is_selected;//这个暂时不用。因为初次进入购物车后商品是都不勾选的
            public String goods_cid;
            public int goods_stock;
            public String goods_is_sale;
            public String goods_weight;
            public String goods_weight_unit;
            public double goods_favourable_price;
            public String goods_spec;
            public String thumb;
            public String img_src;
            public String url;

            public boolean isSelected;
        }
    }
}
