package com.android.manghe.market.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.market.activity.MarketCatGoodListActivity;
import com.android.manghe.market.model.MarketAuctionRes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;


public class PMarketCatGoodList extends XPresenter<MarketCatGoodListActivity> {

    private int mCurrentPage = 1;
    private final String PageSize = "10";
    private final List<MarketAuctionRes.AuctionInfo> mAuctionList = new ArrayList<>();
    public int mPriceSort; //0无排序 1升序 2降序
    public int mFilterType; // 0综合 1销量 2折扣 3价格

    private String mId;

    public void setCategoryId(String id) {
        mId = id;
    }

    public void getAuctionData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mAuctionList.clear();
        }
        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("size", PageSize);
        data.put("id", mId);

        if(mPriceSort != 0){
            data.put("order", "price");
            data.put("sort", mPriceSort == 1 ? "asc" : "desc");
        }else if(mFilterType != 0){
            //除了 综合 。销量或折扣
            data.put("order", mFilterType == 1 ? "sales" : "discount");
        }

        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.DIRECTBUY_GOODSLISTSDATA, data)
                .get()
                .execute(new BaseBack<MarketAuctionRes>() {

                    @Override
                    public void onSuccess(Call call, MarketAuctionRes res) {
                        if (res != null && res.code == 0 && res.list != null) {
                            mAuctionList.addAll(res.list);
                            getV().setCanLoadMore(mAuctionList.size() != res.list_total);
                            getV().update(mAuctionList);
                            if (mAuctionList.size() != res.list_total) {
                                mCurrentPage++;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

}
