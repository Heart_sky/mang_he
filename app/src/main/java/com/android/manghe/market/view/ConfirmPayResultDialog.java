package com.android.manghe.market.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.android.base.view.roundview.RoundTextView;
import com.android.manghe.R;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/18 14:54
 * desc   :
 */
public class ConfirmPayResultDialog extends Dialog {


    private String mOrderId;
    private CallBack mCallBack;
    private Context mContext;
    private RoundTextView mTvOk, mTvNotOk;


    public ConfirmPayResultDialog(@NonNull Context context, String orderId, CallBack callBack) {
        super(context);
        this.mContext = context;
        this.mOrderId = orderId;
        this.mCallBack = callBack;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_confirm_pay_result);
        mTvOk = findViewById(R.id.tvOk);
        mTvNotOk = findViewById(R.id.tvNotOk);

        mTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCallBack!=null){
                    mCallBack.callBack(mOrderId);
                }
            }
        });
        mTvNotOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }


    public interface CallBack {
        void callBack(String orderId);
    }
}
