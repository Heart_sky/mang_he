package com.android.manghe.view.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.android.manghe.R;

public class DefaultOfferPop extends FrameLayout {
    private View containerView;
    private Context context;
    private IDefaultOfferPopListener listener;
    private String currentTimes;
    private View view;
    private TextView tvOfferTimes, tv1, tv2, tv3, tv4;

    public DefaultOfferPop(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(context);
    }
    private void init(final Context context) {
        view = LayoutInflater.from(context).inflate(R.layout.pop_offer_default, null);
        addView(view);
        setVisibility(View.GONE);
        tvOfferTimes = view.findViewById(R.id.tvOfferTimes);
        tv1 = view.findViewById(R.id.tv1);
        tv2 = view.findViewById(R.id.tv2);
        tv3 = view.findViewById(R.id.tv3);
        tv4 = view.findViewById(R.id.tv4);

        tv1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedView(1);
                listener.clickItem(20);
            }
        });
        tv2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedView(2);
                listener.clickItem(50);
            }
        });
        tv3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedView(3);
                listener.clickItem(100);
            }
        });
        tv4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedView(4);
                listener.clickItem(200);
            }
        });
    }

    public void setParam(String currentTimes,IDefaultOfferPopListener listener){
        this.currentTimes = currentTimes;
        this.listener = listener;
    }

    private void setSelectedView(int index){
        tv1.setBackgroundResource(R.drawable.corner_grayborder_whitebg);
        tv2.setBackgroundResource(R.drawable.corner_grayborder_whitebg);
        tv3.setBackgroundResource(R.drawable.corner_grayborder_whitebg);
        tv4.setBackgroundResource(R.drawable.corner_grayborder_whitebg);

        if(index == 1) {
            tv1.setBackgroundResource(R.drawable.corner_redborder_whitebg);
        }else if(index == 2) {
            tv2.setBackgroundResource(R.drawable.corner_redborder_whitebg);
        }else if(index == 3) {
            tv3.setBackgroundResource(R.drawable.corner_redborder_whitebg);
        }else if(index == 4) {
            tv4.setBackgroundResource(R.drawable.corner_redborder_whitebg);
        }
    }

    public void setTimes(String times){
        if(tvOfferTimes != null) {
            if (TextUtils.isEmpty(times)) {
                tvOfferTimes.setText("0");
            }else{
                tvOfferTimes.setText(Integer.parseInt(times) + "");
            }
        }
    }

    public void show() {
        setVisibility(View.VISIBLE);
    }

    public void dismiss() {
        setVisibility(View.GONE);
    }

    public interface IDefaultOfferPopListener{
        void clickItem(int times);
    }
}
