package com.android.manghe.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxPrizeModel;
import com.android.manghe.utils.WindowUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/10 16:13
 * desc   :
 */
public class BoxGiftsUpView extends RelativeLayout {
    private List<BoxPrizeModel.DataBean.ListBean> drawableList = new ArrayList<>();

    private int viewWidth = dp2pix(180), viewHeight = dp2pix(30);
//    private int viewWidth = dp2pix(60), viewHeight = dp2pix(120);

    private int maxHeartNum = 8;
    private int minHeartNum = 2;

    private int riseDuration = 4000;

    private int bottomPadding = 200;
    private int originsOffset = 60;

    private float maxScale = 1.0f;
    private float minScale = 1.0f;

    private int innerDelay = 20;
    private ImageView mGiftImageView;
    private Context mContext;
    private ValueAnimator valueAnimator;
    private Disposable disposable;
    private int count = 0;


    public BoxGiftsUpView(Context context) {
        super(context);
        this.mContext = context;
    }

    public BoxGiftsUpView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public BoxGiftsUpView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    public BoxGiftsUpView setDrawableList(List<BoxPrizeModel.DataBean.ListBean> drawableList) {
        this.drawableList = drawableList;
        return this;
    }

    public BoxGiftsUpView setRiseDuration(int riseDuration) {
        this.riseDuration = riseDuration;
        return this;
    }

    public BoxGiftsUpView setBottomPadding(int px) {
        this.bottomPadding = px;
        return this;
    }

    public BoxGiftsUpView setOriginsOffset(int px) {
        this.originsOffset = px;
        return this;
    }

    public BoxGiftsUpView setScaleAnimation(float maxScale, float minScale) {
        this.maxScale = maxScale;
        this.minScale = minScale;
        return this;
    }

    public BoxGiftsUpView setAnimationDelay(int delay) {
        this.innerDelay = delay;
        return this;
    }

    public BoxGiftsUpView setMaxHeartNum(int maxHeartNum) {
        this.maxHeartNum = maxHeartNum;
        return this;
    }

    public BoxGiftsUpView setMinHeartNum(int minHeartNum) {
        this.minHeartNum = minHeartNum;
        return this;
    }

    public BoxGiftsUpView setItemViewWH(int viewWidth, int viewHeight) {
        this.viewHeight = viewHeight;
        this.viewWidth = viewWidth;
        return this;
    }

    @SuppressLint("CheckResult")
    public void startAnimation(int count) {
        disposable = Observable.timer(2000, TimeUnit.MILLISECONDS)
                .repeat(count)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        bubbleAnimation();
                    }
                });

    }

    private void bubbleAnimation() {
        try {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(viewWidth, viewHeight);
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_box_gift_up, null, false);
            ImageView imageView = view.findViewById(R.id.id_img_head);
            TextView tvView = view.findViewById(R.id.id_text);
            TextView tvUser = view.findViewById(R.id.id_tv_user);
            ImageView imgViewSrc = view.findViewById(R.id.id_img_desc);
            if (drawableList.size() > 0) {
                setImgGift(imageView, imgViewSrc, tvView, tvUser, drawableList.get(count));
                addView(view, layoutParams);
                count++;
                if (count == drawableList.size()) {
                    count = 0;
                }
                view.setPivotX(DisplayUtil.dip2px(mContext, 15));
                ObjectAnimator riseAlphaAnimator = ObjectAnimator.ofFloat(view, "alpha", 1.0f, 0.0f);
                riseAlphaAnimator.setDuration(riseDuration);

                ObjectAnimator riseScaleXAnimator = ObjectAnimator.ofFloat(view, "scaleX", minScale, maxScale);
                riseScaleXAnimator.setDuration(riseDuration);

                ObjectAnimator riseScaleYAnimator = ObjectAnimator.ofFloat(view, "scaleY", minScale, maxScale);
                riseScaleYAnimator.setDuration(riseDuration);
                ObjectAnimator moveScaleYAnimator = ObjectAnimator.
                        ofFloat(view, "translationY", DisplayUtil.dip2px(mContext, WindowUtils.getRawScreenH(mContext) > 2000 ? 90 : 75), 0);
                moveScaleYAnimator.setDuration(riseDuration);

                moveScaleYAnimator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                removeView(view);
                            }
                        },1500);

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.play(moveScaleYAnimator).with(riseScaleXAnimator).with(riseScaleYAnimator);
                animatorSet.start();
            }

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

    }


    public void stop() {
        count = 0;
        //取消注册
        if (disposable != null) {
            disposable.dispose();
            removeAllViews();
        }
    }

    private void setImgGift(ImageView img, ImageView imgsrc, TextView tv, TextView tvUser, BoxPrizeModel.DataBean.ListBean item) {
        GlideHelper.loadRoundTrans(mContext, item.member_avatar, img, 16);
        GlideHelper.loadRoundTrans(mContext, item.goods_thumb, imgsrc, 16);
        tv.setText("开到" + item.goods_name);
        tvUser.setText(item.nickname);
    }


    private int dp2pix(int dp) {
        float scale = getResources().getDisplayMetrics().density;
        int pix = (int) (dp * scale + 0.5f);
        return pix;
    }

    private int pix2dp(int pix) {
        float scale = getResources().getDisplayMetrics().density;
        int dp = (int) (pix / scale + 0.5f);
        return dp;
    }

}


