package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.android.base.tools.ActivityManager
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_agreement.*


class CancellationAccountDialog(context: Context) : Dialog(context, R.style.AppDialogTheme) {
    private var mContext : Context? = null
    init {
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_agreement)
        setCancelable(false)
        setCanceledOnTouchOutside(false)


        tvOk.setOnClickListener{
            dismiss()
        }
        tvCancel.setOnClickListener{
            dismiss()
            ActivityManager.finishAllActivity();
        }

    }
}