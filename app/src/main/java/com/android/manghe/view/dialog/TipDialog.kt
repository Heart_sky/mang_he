package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Html
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_tip.*
import kotlinx.android.synthetic.main.dialog_tip.tvContent

class TipDialog(context: Context, title : String,content : String) : Dialog(context,R.style.AppDialogTheme) {
    private var mContext : Context? = null
    private var mContent : String = ""
    private var mTitle : String? = ""
    init {
        mTitle = title
        mContext = context
        mContent = content
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_tip)

        tvOk.setOnClickListener{
            dismiss()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.text = Html.fromHtml(mContent, Html.FROM_HTML_MODE_COMPACT)
        }else{
            tvContent.text = Html.fromHtml(mContent)
        }
        tvTitle.text = mTitle
    }
}