package com.android.manghe.view.popWindow;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.manghe.R;
import com.android.manghe.utils.WindowUtils;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/22 17:07
 * desc   : 置换
 */
public class ReplaceRulePopWindow extends PopupWindow {


    private Activity mContext;
    private TextView mTvTip, mTvDesc;
    private IReplaceRuleListener mListener;
    private boolean isDismiss = false;
    private ImageView mImgClose;
    private Button btnReplace, btnIntegral, btnSent;
    private String mDesc;

    public ReplaceRulePopWindow(Activity activity, String desc) {
        super(activity);
        mContext = activity;
        mDesc = desc;
        init();
    }

    public void setListener(IReplaceRuleListener listener) {
        this.mListener = listener;
    }

    private void init() {
        final View popupView = LayoutInflater.from(mContext).inflate(R.layout.layout_replace_rule_pop_window, null, false);
        mTvTip = popupView.findViewById(R.id.id_tv_tip);
        mImgClose = popupView.findViewById(R.id.id_img_close);
        btnReplace = popupView.findViewById(R.id.btnReplace);
        btnIntegral = popupView.findViewById(R.id.btnIntegral);
        btnSent = popupView.findViewById(R.id.btnSent);
        mTvDesc = popupView.findViewById(R.id.id_tv_desc);
        setText(mDesc);
        popupView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        this.setContentView(popupView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(new ColorDrawable());
        this.setOnDismissListener(() -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
        });

        mImgClose.setOnClickListener(v -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
        });


        btnSent.setOnClickListener(v -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
            if (null != mListener)
                mListener.onSent();

        });

        btnIntegral.setOnClickListener(v -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
            if (null != mListener)
                mListener.onIntegral();
        });
        btnReplace.setOnClickListener(v -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
            if (null != mListener)
                mListener.onReplace();
        });

    }

    public void showPopWindow(View view) {
        if (this.isShowing()) {
            this.dismiss();
        } else {
            this.showAtLocation(view, Gravity.CENTER, 0, 0);
            WindowUtils.backgroundAlpha(mContext, 0.5f);
        }
    }

    public void setText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mTvDesc.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            mTvDesc.setText(Html.fromHtml(text));
        }
    }


    public void close() {
        super.dismiss();
    }

    public interface IReplaceRuleListener {
        void onIntegral();

        void onSent();

        void onReplace();


    }
}
