package com.android.manghe.view.preview.photoview;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.android.base.tools.Glide.GlideHelper;

import java.util.List;

public class PreviewViewPagerAdapter extends PagerAdapter {
	private List<String> mLocalPicPathList;

	private Context mContext;
	public PreviewViewPagerAdapter(Context context, List<String> localPicPathList) {
		mContext = context;
		mLocalPicPathList = localPicPathList;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public void finishUpdate(View container) {
	}

	@Override
	public int getCount() {
		return mLocalPicPathList.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		PhotoView photoView = new PhotoView(container.getContext());
		container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		GlideHelper.load(mContext,mLocalPicPathList.get(position),photoView);
		return photoView;
	}
}
