package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_exist_buy_member.*


class ExistBuyMemberDialog(context: Context, callback : IExistBuyMemberDialogCallback) : Dialog(context, R.style.AppDialogTheme) {
    private var mContext : Context? = null
    private var mCallBack : IExistBuyMemberDialogCallback? =null
    init {
        mContext = context
        mCallBack =callback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_exist_buy_member)
        setCancelable(false)
        setCanceledOnTouchOutside(false)


        tvContinue.setOnClickListener{
            dismiss()
            mCallBack?.continuePay()
        }
        tvCancel.setOnClickListener{
            dismiss()
            mCallBack?.giveUp()
        }

    }

    interface IExistBuyMemberDialogCallback {
        fun giveUp()
        fun continuePay()
    }
}