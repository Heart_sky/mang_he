package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_modify_nick.*

class InputCouponMoneyDialog(context: Context, listener: IInputListener) : Dialog(context) {
    private var mContent : Context? = null
    private var mListener : IInputListener? = null
    init {
        mContent = context
        mListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_modify_money)

        layoutConfirm.setOnClickListener{
            val money = etNick.text.toString().trim()
            if(TextUtils.isEmpty(etNick.text.toString())){
                ToastUtil.showShort(mContent, "请输入金额")
                return@setOnClickListener
            }
            if (etNick.text.toString().toInt() < 5) {
                ToastUtil.showShort(mContent, "最少充值5元")
                return@setOnClickListener
            }

            mListener?.let {
                it.confirm(etNick.text.toString().toInt())
                dismiss()
            }
        }
        layoutCancel.setOnClickListener {
            this.dismiss()
        }
    }
    interface IInputListener{
        fun confirm(num :Int)
    }
}