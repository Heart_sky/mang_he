package com.android.manghe.view.dialog;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.android.manghe.R;
import com.lxj.xpopup.core.BasePopupView;

public class CancelllationAccountPopupView extends BasePopupView {

    public ICallBack callBack;
    public CancelllationAccountPopupView(@NonNull Context context, ICallBack callBack) {
        super(context);
        this.callBack = callBack;
    }

    @Override
    protected int getPopupLayoutId() {
        return R.layout.cancellation_account_popup_view;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        findViewById(R.id.ivClose).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.btnConfirm).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(callBack != null){
                    callBack.onConfirm();
                }
            }
        });
    }
    public interface ICallBack{
        void onConfirm();
    }
}
