package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_input_qty.*
import kotlinx.android.synthetic.main.dialog_modify_nick.layoutCancel
import kotlinx.android.synthetic.main.dialog_modify_nick.layoutConfirm

class InputGoodQtyDialog(context: Context, initQyt: Int, listener :IInputGoodQtyListener) : Dialog(context) {
    private var mContent : Context? = null
    private var mInitQyt = 1
    private var mListener : IInputGoodQtyListener? = null
    init {
        mContent = context
        mInitQyt = initQyt
        mListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_input_qty)

        etQty.setText(mInitQyt.toString())
        layoutConfirm.setOnClickListener{
            val qty = etQty.text.toString().trim()
            if(qty.isEmpty()){
                ToastUtil.showLong(mContent, "请输入购买数量")
                return@setOnClickListener
            }
            val qtyNum = qty.toInt()
            if(qtyNum<1){
                ToastUtil.showLong(mContent, "请正确输入购买数量")
                return@setOnClickListener
            }
            mListener?.let {
                it.confirm(qtyNum)
                dismiss()
            }
        }
        layoutCancel.setOnClickListener {
            this.dismiss()
        }
    }
    interface IInputGoodQtyListener{
        fun confirm(qty :Int);
    }
}