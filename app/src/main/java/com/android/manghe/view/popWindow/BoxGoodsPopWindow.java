package com.android.manghe.view.popWindow;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.vlayout.DelegateAdapter;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.android.base.tools.DisplayUtil;
import com.android.manghe.R;
import com.android.manghe.box.activity.BoxGoodDetailActivity;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.market.adapter.ShowBoxGoodsAdapter;
import com.android.manghe.market.adapter.ShowBoxGoodsChanceAdapter;
import com.android.manghe.market.adapter.ShowBoxGoodsDelegateAdapter;
import com.android.manghe.utils.WindowUtils;
import com.android.manghe.view.GridAuctionSpacesItemDecoration;
import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/10 14:37
 * desc   :
 */
public class BoxGoodsPopWindow extends PopupWindow {


    private Activity mContext;
    private RecyclerView mRecyclerView;
    private ImageView mImgDelete;
    private ShowBoxGoodsDelegateAdapter showBoxGoodsDelegateAdapter;
    private ShowBoxGoodsChanceAdapter showBoxGoodsChanceAdapter;
    private List<BoxModel.DataBean.GoodsListBean> mGoods_list;
    private BoxModel.DataBean mCurrentBoxModel;

    public BoxGoodsPopWindow(Activity activity, BoxModel.DataBean boxModel, List<BoxModel.DataBean.GoodsListBean> goods_list) {
        super(activity);
        mContext = activity;
        this.mGoods_list = goods_list;
        this.mCurrentBoxModel = boxModel;
        init();
    }


    private void init() {
        final View popupView = LayoutInflater.from(mContext).inflate(R.layout.layout_box_goods_pop_winodw, null, false);
        mRecyclerView = popupView.findViewById(R.id.id_recycler_view);
        mImgDelete = popupView.findViewById(R.id.id_img_delete);


        VirtualLayoutManager layoutManager = new VirtualLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);
        DelegateAdapter delegateAdapter = new DelegateAdapter(layoutManager, true);
        delegateAdapter.setAdapters(getAdapters());
        mRecyclerView.setAdapter(delegateAdapter);

        popupView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        this.setContentView(popupView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(new ColorDrawable());
        this.setOnDismissListener(() -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
        });


        mImgDelete.setOnClickListener(v -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
        });


    }

    private List<DelegateAdapter.Adapter> getAdapters() {
        List<DelegateAdapter.Adapter> adapters = new ArrayList<>();

        showBoxGoodsDelegateAdapter = new ShowBoxGoodsDelegateAdapter(mContext, mGoods_list);
        showBoxGoodsChanceAdapter = new ShowBoxGoodsChanceAdapter(mContext, mCurrentBoxModel);

        adapters.add(showBoxGoodsDelegateAdapter);
        adapters.add(showBoxGoodsChanceAdapter);
        return adapters;

    }

    public void showPopWindow(View view) {
        if (this.isShowing()) {
            this.dismiss();
        } else {
            this.showAtLocation(view, Gravity.BOTTOM, 0, 0);
            WindowUtils.backgroundAlpha(mContext, 0.5f);
        }
    }


    public void close() {
        super.dismiss();
    }

}
