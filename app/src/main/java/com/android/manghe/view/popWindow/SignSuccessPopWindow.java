package com.android.manghe.view.popWindow;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.manghe.R;
import com.android.manghe.utils.WindowUtils;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/28 13:55
 * desc   :
 */
public class SignSuccessPopWindow extends PopupWindow {


    private Activity mContext;
    private TextView mTvCoin, mTvCoinDesc, mTvOk;
    private String mMoney;


    public SignSuccessPopWindow(Activity activity, String money) {
        super(activity);
        mContext = activity;
        this.mMoney = money;
        init();
    }


    private void init() {
        final View popupView = LayoutInflater.from(mContext).inflate(R.layout.layout_singn_success_pop_window, null, false);
        mTvCoin = popupView.findViewById(R.id.id_tv_coin);
        mTvCoinDesc = popupView.findViewById(R.id.id_tv_coin_desc);
        mTvOk = popupView.findViewById(R.id.id_tv_ok);

        mTvCoin.setText(mMoney + "币");
        mTvCoinDesc.setText("恭喜你获得" + mMoney + "币");
        popupView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        this.setContentView(popupView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(new ColorDrawable());
        this.setOnDismissListener(() -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
        });

        mTvOk.setOnClickListener(v -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
        });

    }

    public void showPopWindow(View view) {
        if (this.isShowing()) {
            this.dismiss();
        } else {
            this.showAtLocation(view, Gravity.CENTER, 0, 0);
            WindowUtils.backgroundAlpha(mContext, 0.5f);
        }
    }


    public void close() {
        super.dismiss();
    }

    public interface IReplaceListener {
        void ok();
    }
}
