package com.android.manghe.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.android.manghe.R;

public class NoDataView extends FrameLayout {

    public NoDataView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public void initView(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_no_data, null);
        addView(view);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.NoDataView);
        String tip = ta.getString(R.styleable.NoDataView_tip);

        if(!TextUtils.isEmpty(tip)){
            TextView tv = view.findViewById(R.id.tvTip);
            tv.setText(tip);
        }
    }
}
