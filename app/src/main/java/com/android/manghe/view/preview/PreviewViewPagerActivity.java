package com.android.manghe.view.preview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.widget.TextView;
import com.android.base.frame.presenter.IView;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.frame.view.MVPActivity;
import com.android.manghe.R;
import com.android.manghe.view.preview.photoview.HackyViewPager;
import com.android.manghe.view.preview.photoview.PreviewViewPagerAdapter;

import java.io.Serializable;
import java.util.List;

public class PreviewViewPagerActivity extends MVPActivity<XPresenter<IView>> {

    private TextView mTvCurrentPageRecord;
    private HackyViewPager mViewPager;
    private PreviewViewPagerAdapter mPagerAdapter;
    private int mCurrentIndex;


    private List<String> imageUrls;
    private int initPosition;

    public static void showActivity(Context context, List<String> imageUrls, int initPosition) {
        if (imageUrls != null && imageUrls.size() != 0) {
            Intent intent = new Intent(context, PreviewViewPagerActivity.class);
            intent.putExtra("imageUrls", (Serializable) imageUrls);
            intent.putExtra("initPosition", initPosition);
            context.startActivity(intent);
        }
    }


    @Override
    protected void initData(@Nullable Bundle savedInstanceState) {

        getTitleBar().setToolbar("图片预览").setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black);

        Intent intent = getIntent();
        imageUrls = (List<String>) intent.getSerializableExtra("imageUrls");
        initPosition = intent.getIntExtra("initPosition", 0);
        mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
        mTvCurrentPageRecord = findViewById(R.id.tvPageCount);
        mCurrentIndex = getIntent().getIntExtra("position", 0);
        mPagerAdapter = new PreviewViewPagerAdapter(this, imageUrls);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setCurrentItem(mCurrentIndex);
        setImgIndexView(mCurrentIndex);
        mViewPager.setCurrentItem(initPosition);
        mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                setImgIndexView(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_preview_view_pager;
    }

    private void setImgIndexView(int position) {
        String pageRecord = (position + 1) + "/" + imageUrls.size();
        mTvCurrentPageRecord.setText(pageRecord);
    }

}
