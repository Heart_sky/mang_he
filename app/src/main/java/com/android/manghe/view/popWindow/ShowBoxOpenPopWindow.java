package com.android.manghe.view.popWindow;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.box.adapter.ShowOpenBoxAdapter;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.box.model.OpenBoxWayModel;
import com.android.manghe.utils.WindowUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/06 11:00
 * desc   :
 */
public class ShowBoxOpenPopWindow extends PopupWindow {

    private Activity mContext;
    private RecyclerView mRecyclerView;

    private BoxModel.DataBean mCurrentBoxModel;
    private ShowOpenBoxAdapter mAdapter;
    private IShowBoxOpenListener mListener;
    private ImageView mImgGift;


    public ShowBoxOpenPopWindow(Activity activity, BoxModel.DataBean boxModel) {
        super(activity);
        mContext = activity;
        this.mCurrentBoxModel = boxModel;
        init();
    }

    public void setListener(IShowBoxOpenListener listener) {
        this.mListener = listener;
    }


    private void init() {
        final View popupView = LayoutInflater.from(mContext).inflate(R.layout.layout_box_one, null, false);
        mRecyclerView = popupView.findViewById(R.id.id_recycler_view);
        mImgGift = popupView.findViewById(R.id.id_img_gift);
        setDate();
        setGift();
        popupView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        this.setContentView(popupView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(new ColorDrawable());
        this.setOnDismissListener(() -> {
            dismiss();
            WindowUtils.backgroundAlpha(mContext, 1f);
        });


    }

    private void setGift() {
        GlideHelper.load(mContext, mCurrentBoxModel.cover_pic, mImgGift);
    }

    private void setDate() {
        try {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 2);
            mAdapter = new ShowOpenBoxAdapter(mContext, getBoxWayList());
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (getBoxWayList().size() == 1) {
                        return 2;
                    } else if (getBoxWayList().size() == 2) {
                        return 2;
                    } else if (getBoxWayList().size() == 3) {
                        if (position == 0) {
                            return 2;
                        } else {
                            return 1;
                        }
                    } else if (getBoxWayList().size() == 4) {
                        return 1;
                    } else if (getBoxWayList().size() == 5) {
                        if (position == 0) {
                            return 2;
                        } else {
                            return 1;
                        }
                    } else {
                        if (position == 0) {
                            return 2;
                        } else {
                            return 1;
                        }
                    }
                }
            });
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(gridLayoutManager);
            mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    if (mListener != null) {
                        dismiss();
                        mListener.onClick(mAdapter.getItem(position));
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<OpenBoxWayModel> getBoxWayList() {
        List<OpenBoxWayModel> list = new ArrayList<>();
        if (mCurrentBoxModel.prices._$1 != null) {
            OpenBoxWayModel openBoxWayModel = new OpenBoxWayModel();
            openBoxWayModel.setDiscount(mCurrentBoxModel.prices._$1.discount);
            openBoxWayModel.setLabel(mCurrentBoxModel.prices._$1.label);
            openBoxWayModel.setNum(mCurrentBoxModel.prices._$1.num);
            openBoxWayModel.setOrigin_price(mCurrentBoxModel.prices._$1.origin_price);
            openBoxWayModel.setSell_price(mCurrentBoxModel.prices._$1.sell_price);
            list.add(openBoxWayModel);
        }
        if (mCurrentBoxModel.prices._$2 != null) {
            OpenBoxWayModel openBoxWayModel = new OpenBoxWayModel();
            openBoxWayModel.setDiscount(mCurrentBoxModel.prices._$2.discount);
            openBoxWayModel.setLabel(mCurrentBoxModel.prices._$2.label);
            openBoxWayModel.setNum(mCurrentBoxModel.prices._$2.num);
            openBoxWayModel.setOrigin_price(mCurrentBoxModel.prices._$2.origin_price);
            openBoxWayModel.setSell_price(mCurrentBoxModel.prices._$2.sell_price);
            list.add(openBoxWayModel);
        }
        if (mCurrentBoxModel.prices._$3 != null) {
            OpenBoxWayModel openBoxWayModel = new OpenBoxWayModel();
            openBoxWayModel.setDiscount(mCurrentBoxModel.prices._$3.discount);
            openBoxWayModel.setLabel(mCurrentBoxModel.prices._$3.label);
            openBoxWayModel.setNum(mCurrentBoxModel.prices._$3.num);
            openBoxWayModel.setOrigin_price(mCurrentBoxModel.prices._$3.origin_price);
            openBoxWayModel.setSell_price(mCurrentBoxModel.prices._$3.sell_price);
            list.add(openBoxWayModel);
        }
        if (mCurrentBoxModel.prices._$5 != null) {
            OpenBoxWayModel openBoxWayModel = new OpenBoxWayModel();
            openBoxWayModel.setDiscount(mCurrentBoxModel.prices._$5.discount);
            openBoxWayModel.setLabel(mCurrentBoxModel.prices._$5.label);
            openBoxWayModel.setNum(mCurrentBoxModel.prices._$5.num);
            openBoxWayModel.setOrigin_price(mCurrentBoxModel.prices._$5.origin_price);
            openBoxWayModel.setSell_price(mCurrentBoxModel.prices._$5.sell_price);
            list.add(openBoxWayModel);
        }
        if (mCurrentBoxModel.prices._$10 != null) {
            OpenBoxWayModel openBoxWayModel = new OpenBoxWayModel();
            openBoxWayModel.setDiscount(mCurrentBoxModel.prices._$10.discount);
            openBoxWayModel.setLabel(mCurrentBoxModel.prices._$10.label);
            openBoxWayModel.setNum(mCurrentBoxModel.prices._$10.num);
            openBoxWayModel.setOrigin_price(mCurrentBoxModel.prices._$10.origin_price);
            openBoxWayModel.setSell_price(mCurrentBoxModel.prices._$10.sell_price);
            list.add(openBoxWayModel);
        }

        return list;
    }


    public void showPopWindow(View view) {
        if (this.isShowing()) {
            this.dismiss();
        } else {
            this.showAtLocation(view, Gravity.CENTER, 0, 0);
            WindowUtils.backgroundAlpha(mContext, 0.5f);
        }
    }

    public void close() {
        super.dismiss();
    }

    public interface IShowBoxOpenListener {
        void onClick(OpenBoxWayModel model);
    }

}
