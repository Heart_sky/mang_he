package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.android.base.tools.ActivityManager
import com.android.base.tools.SPUtil
import com.android.manghe.R
import com.android.manghe.config.SPTag
import kotlinx.android.synthetic.main.dialog_ask_continue_to_read_agreement.*

class AskContinueToReadAgreementDialog(context: Context, listener: IAskContinueToReadAgreement) :
    Dialog(context, R.style.AppDialogTheme) {
    private var mContext: Context? = null
    private var mListener: IAskContinueToReadAgreement? = null

    init {
        mContext = context
        mListener = listener
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_ask_continue_to_read_agreement)
        setCancelable(false)
        setCanceledOnTouchOutside(false)

        tvCancel.setOnClickListener {
            dismiss()
            ActivityManager.finishAllActivity();
        }
        tvReadAgain.setOnClickListener {
            dismiss()
            AgreementDialog(mContext!!, object : AgreementDialog.IAgreementListener {
                override fun ok() {
                    mListener?.ok()

                }

                override fun noOk() {
                    AskContinueToReadAgreementDialog(mContext!!,
                        object : IAskContinueToReadAgreement {
                            override fun ok() {
                                mListener?.ok()
                            }
                        }).show()
                }
            }).show()
        }
    }

    interface IAskContinueToReadAgreement {
        fun ok()
    }

}