package com.android.manghe.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.base.tools.Glide.GlideHelper;
import com.android.base.view.roundview.RoundLinearLayout;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxGiftModel;
import com.android.manghe.utils.WindowUtils;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/16 14:06
 * desc   :
 */
public class FiveGiftLayout extends FrameLayout {

    private Context mContext;
    private TextView mTvNameOne, mTvNameTwo, mTvNameThree, mTvNameFour, mTvNameFive;
    private ImageView mImgOne, mImgTwo, mImgThree, mImgFour, mImgFive;
    private TextView mTvPriceOne, mTvPriceTwo, mTvPriceThree, mTvPriceFour, mTvPriceFive;
    private RoundLinearLayout mLayoutOne,mLayoutTwo,mLayoutThree,mLayoutFour,mLayoutFive;
    private TextView mTvLevel1,mTvLevel2,mTvLevel3,mTvLevel4,mTvLevel5;
    private ImageView mImgLevel1,mImgLevel2,mImgLevel3,mImgLevel4,mImgLevel5;


    public FiveGiftLayout(Context context) {
        super(context);
        initView(context);
    }

    public FiveGiftLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }


    private void initView(Context context) {
        mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.layout_five_gift, null);
        addView(view);
        mTvNameOne = view.findViewById(R.id.id_tv_name_1);
        mTvNameTwo = view.findViewById(R.id.id_tv_name_2);
        mTvNameThree = view.findViewById(R.id.id_tv_name_3);
        mTvNameFour = view.findViewById(R.id.id_tv_name_4);
        mTvNameFive = view.findViewById(R.id.id_tv_name_5);

        mTvPriceOne = view.findViewById(R.id.id_tv_price_value_1);
        mTvPriceTwo = view.findViewById(R.id.id_tv_price_value_2);
        mTvPriceThree = view.findViewById(R.id.id_tv_price_value_3);
        mTvPriceFour = view.findViewById(R.id.id_tv_price_value_4);
        mTvPriceFive = view.findViewById(R.id.id_tv_price_value_5);

        mImgOne = view.findViewById(R.id.id_img_gift_1);
        mImgTwo = view.findViewById(R.id.id_img_gift_2);
        mImgThree = view.findViewById(R.id.id_img_gift_3);
        mImgFour = view.findViewById(R.id.id_img_gift_4);
        mImgFive = view.findViewById(R.id.id_img_gift_5);

        mLayoutOne=view.findViewById(R.id.id_ly_level_1);
        mLayoutTwo=view.findViewById(R.id.id_ly_level_2);
        mLayoutThree=view.findViewById(R.id.id_ly_level_3);
        mLayoutFour=view.findViewById(R.id.id_ly_level_4);
        mLayoutFive=view.findViewById(R.id.id_ly_level_5);

        mImgLevel1=view.findViewById(R.id.id_img_level_1);
        mImgLevel2=view.findViewById(R.id.id_img_level_2);
        mImgLevel3 =view.findViewById(R.id.id_img_level_3);
        mImgLevel4=view.findViewById(R.id.id_img_level_4);
        mImgLevel5=view.findViewById(R.id.id_img_level_5);

        mTvLevel1=view.findViewById(R.id.id_tv_level_1);
        mTvLevel2=view.findViewById(R.id.id_tv_level_2);
        mTvLevel3=view.findViewById(R.id.id_tv_level_3);
        mTvLevel4=view.findViewById(R.id.id_tv_level_4);
        mTvLevel5=view.findViewById(R.id.id_tv_level_5);

    }

    public void update(List<BoxGiftModel.ListBean> list) {
        try {
            for (int i = 0; i < list.size(); i++) {
                if (i == 0) {
                    setDate(mTvNameOne, mTvPriceOne, mImgOne,mLayoutOne,mImgLevel1,mTvLevel1, list.get(i));
                } else if (i == 1) {
                    setDate(mTvNameTwo, mTvPriceTwo, mImgTwo, mLayoutTwo,mImgLevel2,mTvLevel2,list.get(i));
                } else if (i == 2) {
                    setDate(mTvNameThree, mTvPriceThree, mImgThree,mLayoutThree,mImgLevel3,mTvLevel3, list.get(i));
                } else if (i == 3) {
                    setDate(mTvNameFour, mTvPriceFour, mImgFour,mLayoutFour,mImgLevel4,mTvLevel4, list.get(i));
                } else if (i == 4) {
                    setDate(mTvNameFive, mTvPriceFive, mImgFive,mLayoutFive,mImgLevel5,mTvLevel5, list.get(i));
                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDate(TextView tv, TextView tvPrice, ImageView img,
                         RoundLinearLayout linearLayout, ImageView imgLevel, TextView tvLevel, BoxGiftModel.ListBean giftModel) {
        tv.setText(giftModel.goods_name);
        tvPrice.setText("￥"+giftModel.goods_price);
        GlideHelper.loadWithHolderErr(mContext, giftModel.goods_pic, img);
        WindowUtils.setBoxLevel(mContext, linearLayout, imgLevel, tvLevel, giftModel.probability_level);
    }


}
