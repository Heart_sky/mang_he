package com.android.manghe.view;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.tools.ScreenUtil;
import com.android.manghe.R;
import com.android.manghe.box.model.BoxModel;
import com.android.manghe.utils.WindowUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class BoxGiftBubbleView extends RelativeLayout {
    private List<List<BoxModel.DataBean.GoodsListBean>> drawableList = new ArrayList<>();

    private int viewWidth = LinearLayout.LayoutParams.MATCH_PARENT, viewHeight = LinearLayout.LayoutParams.WRAP_CONTENT;

    private int riseDuration = 4000;

    private int bottomPadding = 200;
    private int originsOffset = 60;

    private float maxScale = 1.0f;
    private float minScale = 1.0f;

    private int innerDelay = 20;
    private ImageView mGiftImageView;
    private Context mContext;
    private Disposable disposable;
    private int count = 0;


    public BoxGiftBubbleView(Context context) {
        super(context);
        this.mContext = context;
    }

    public BoxGiftBubbleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public BoxGiftBubbleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
    }

    public BoxGiftBubbleView setDrawableList(List<List<BoxModel.DataBean.GoodsListBean>> drawableList) {
        this.drawableList = drawableList;
        return this;
    }

    public BoxGiftBubbleView setRiseDuration(int riseDuration) {
        this.riseDuration = riseDuration;
        return this;
    }

    public BoxGiftBubbleView setBottomPadding(int px) {
        this.bottomPadding = px;
        return this;
    }

    public BoxGiftBubbleView setOriginsOffset(int px) {
        this.originsOffset = px;
        return this;
    }

    public BoxGiftBubbleView setScaleAnimation(float maxScale, float minScale) {
        this.maxScale = maxScale;
        this.minScale = minScale;
        return this;
    }

    public BoxGiftBubbleView setAnimationDelay(int delay) {
        this.innerDelay = delay;
        return this;
    }


    public BoxGiftBubbleView setItemViewWH(int viewWidth, int viewHeight) {
        this.viewHeight = viewHeight;
        this.viewWidth = viewWidth;
        return this;
    }

    @SuppressLint("CheckResult")
    public void startAnimation(int count) {
        disposable = Observable.timer(2000, TimeUnit.MILLISECONDS)
                .repeat(count)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        bubbleAnimation();
                    }
                });

    }

    private void bubbleAnimation() {
        try {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(viewWidth, viewHeight);
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_box_gift_up_bubble, null, false);
            ImageView imgOne = view.findViewById(R.id.id_img_1);
            ImageView imgTwo = view.findViewById(R.id.id_img_2);
            ImageView imgThree = view.findViewById(R.id.id_img_3);

            if (drawableList.size() > 0) {
                if(drawableList.get(count).size()==1){
                    setImgGift(imgOne,drawableList.get(count).get(0));
                    setImgGift(imgTwo,drawableList.get(0).get(0));
                    setImgGift(imgThree,drawableList.get(0).get(1));
                }else if(drawableList.get(count).size()==2){
                    setImgGift(imgOne,drawableList.get(count).get(0));
                    setImgGift(imgTwo,drawableList.get(count).get(1));
                    setImgGift(imgThree,drawableList.get(0).get(0));
                }else if(drawableList.get(count).size()==3) {
                    setImgGift(imgOne,drawableList.get(count).get(0));
                    setImgGift(imgTwo,drawableList.get(count).get(1));
                    setImgGift(imgThree,drawableList.get(count).get(2));
                }
                addView(view, layoutParams);
                count++;
                if (count == drawableList.size()) {
                    count = 0;
                }
                view.setPivotX(ScreenUtil.getScreenWidth(mContext)/2);

                ObjectAnimator riseAlphaAnimator = ObjectAnimator.ofFloat(view, "alpha", 1.0f, 1.0f,1.0f,1.0f,1.0f,0.0f);
                riseAlphaAnimator.setDuration(riseDuration);

                ObjectAnimator riseScaleXAnimator = ObjectAnimator.ofFloat(view, "scaleX", 0.4f,0.6f,0.8f, 1f,1.2f,1.3f);
                riseScaleXAnimator.setDuration(riseDuration);

                ObjectAnimator riseScaleYAnimator = ObjectAnimator.ofFloat(view, "scaleY",0.4f,0.6f,0.8f, 1f,1.2f,1.3f);
                riseScaleYAnimator.setDuration(riseDuration);

                ObjectAnimator moveScaleYAnimator = ObjectAnimator.
                        ofFloat(view, "translationY", DisplayUtil.dip2px(mContext, WindowUtils.getRawScreenH(mContext) > 2000 ? 320 : 150), 0);
                moveScaleYAnimator.setDuration(riseDuration);

                moveScaleYAnimator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        removeView(view);

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.play(moveScaleYAnimator).with(riseAlphaAnimator).with(riseScaleXAnimator).with(riseScaleYAnimator);
                animatorSet.start();
            }

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void stop() {
        count = 0;
        //取消注册
        if (disposable != null) {
            disposable.dispose();
            removeAllViews();
        }
    }

    private void setImgGift(ImageView img, BoxModel.DataBean.GoodsListBean item) {
        GlideHelper.loadCropRoundTrans(mContext, item.thumb, img,22);
    }


    private int dp2pix(int dp) {
        float scale = getResources().getDisplayMetrics().density;
        int pix = (int) (dp * scale + 0.5f);
        return pix;
    }

    private int pix2dp(int pix) {
        float scale = getResources().getDisplayMetrics().density;
        int dp = (int) (pix / scale + 0.5f);
        return dp;
    }
}
