package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import com.android.base.tools.SPUtil
import com.android.manghe.R
import com.android.manghe.common.webview.SimpleWebViewActivity
import com.android.manghe.config.SPTag
import kotlinx.android.synthetic.main.dialog_agreement.*


class AgreementDialog(context: Context,listener:IAgreementListener) : Dialog(context, R.style.AppDialogTheme) {
    private var mContext : Context? = null
    private var mListener :IAgreementListener? = null
    init {
        mContext = context
        mListener=listener
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_agreement)
        setCancelable(false)
        setCanceledOnTouchOutside(false)


        tvOk.setOnClickListener{
            dismiss()
            mListener?.ok()


        }
        tvCancel.setOnClickListener{
            dismiss()
            mListener?.noOk()

        }

        val str = StringBuilder("欢迎您使用“芒铺”软件及相关服务。\n" +
                "我们依据法律法规收集、使用个人信息。在使用芒铺软件及相关服务前，请您务必仔细阅读并理解我们的《用户协议》及《隐私政策》。" +
                "您一旦选择“同意”，即意味着您授权我们收集、保存、使用、共享、披露及保护您的信息。点击查看《用户协议》和《隐私政策》")
        val spannableBuilder = SpannableStringBuilder(str)

        val colorSpan = ForegroundColorSpan(ContextCompat.getColor(context, R.color.red400));
        spannableBuilder.setSpan(colorSpan, 58, 64, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableBuilder.setSpan(object : ClickableSpan(){
            override fun onClick(widget: View) {
                SimpleWebViewActivity.showActivity(
                    mContext!!,
                    "https://www.manghe98.com/protocol/user_service.html",
                    "用户服务协议",
                    false
                )
            }
        }, 123, 129, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        spannableBuilder.setSpan(colorSpan, 130, 136, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableBuilder.setSpan(object : ClickableSpan(){
            override fun onClick(widget: View) {
                SimpleWebViewActivity.showActivity(
                    mContext!!,
                    "https://www.manghe98.com/protocol/privacy.html",
                    "隐私政策",
                    false
                )
            }
        }, 130, 136, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        tvContent.movementMethod = LinkMovementMethod.getInstance()
        tvContent.text = spannableBuilder
    }

    interface IAgreementListener {
        fun ok()
        fun noOk()
    }
}