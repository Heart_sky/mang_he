package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_confirm_pay_result.*
import kotlinx.android.synthetic.main.dialog_score_rule.tvOk

/**
 * 询问用户是否微信支付已完成了
 */
class ConfirmPayResultDialog(context: Context, orderId : String, callback: CallBack) : Dialog(context,R.style.AppDialogTheme) {
    private var orderId : String = ""
    private var callback : CallBack? = null
    init {
        this.orderId = orderId
        this.callback = callback
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_confirm_pay_result)

        tvOk.setOnClickListener{
            dismiss()
            callback?.let {
                it.callBack(this.orderId)
            }
        }
        tvNotOk.setOnClickListener {
            dismiss()
        }
    }

    interface CallBack{
        fun callBack(orderId : String)
    }
}