package com.android.manghe.view;

import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.base.tools.ScreenUtil;
import com.android.manghe.R;
import com.android.manghe.index.model.OldNoticeBean;

import java.util.List;

public class DanMuScrollerView extends LinearLayout {
    private Context mContext;
    private LinearLayout mLayoutContainer;
    private ScrollViewExt mScrollView;
    private IDanMuScorllerViewListener mListener;

    public DanMuScrollerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setListener(IDanMuScorllerViewListener listener) {
        this.mListener = listener;
    }

    private void init(Context context) {
        mContext = context;
        View view = LayoutInflater.from(context).inflate(R.layout.layout_dan_mu, null, false);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(lp);
        addView(view);
        mLayoutContainer = view.findViewById(R.id.layout_dan_mu_container);
        mLayoutContainer.setPadding(ScreenUtil.getScreenWidth(mContext), 0, DisplayUtil.dip2px(mContext, 10), 0);
        mScrollView = view.findViewById(R.id.scrollView);

        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                //不能滑动
                return true;
                //可以滑动
                //return false;
            }
        });
    }

    public void add(List<OldNoticeBean.DataBean.ListBean> danmu) {
        if(mLayoutContainer.getVisibility() == View.GONE) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startScroll();
                }
            },200);
        }
        mLayoutContainer.setVisibility(View.VISIBLE);
        for (OldNoticeBean.DataBean.ListBean bean : danmu) {
            final View view = View.inflate(getContext(), R.layout.item_last_notice_dan_mu, null);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT);
            lp.rightMargin = DisplayUtil.dip2px(mContext, 10);
            view.setLayoutParams(lp);
            mLayoutContainer.addView(view);
            ((TextView) view.findViewById(R.id.tvContent)).setText(Html.fromHtml(bean.username
                    + "#</font>以"
                    + "<font color='#333333'>" + "¥" + bean.price + "</font>获得"
                    + "<font color='#e84e40'>" + bean.title + "</font>"));
            GlideHelper.loadAvatar(mContext, bean.thumb, view.findViewById(R.id.ivAvatar));
        }
        mScrollView.setScrollViewListener(new ScrollViewExt.IScrollChangedListener() {
            @Override
            public void onScrolledToBottom() {
                try {
//                    mListener.finished();
//                    mLayoutContainer.removeAllViews();
//                    mLayoutContainer.setVisibility(View.GONE);
//                    current = 0;
//                    mScrollView.scrollBy(0,0);
                    add(danmu);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onScrolledToTop() {
                System.out.println("xxxeeqq--1");

            }
        });

    }

    private int current=0;
    public void startScroll() {
        try {
            mScrollView.smoothScrollTo(current, 0);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (mLayoutContainer.getVisibility() != View.GONE) {
                            current++;
                            startScroll();
                        }
                    } catch (Exception e) {

                    }
                }
            }, 5);
        } catch (Exception e) {

        }
    }
    public void onResume(){

    }

    public void clear(){
        try{
            mLayoutContainer.removeAllViews();
        }catch (Exception e){}
    }

    public interface IDanMuScorllerViewListener {
        void finished();
    }
}
