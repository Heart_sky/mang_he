package com.android.manghe.view.multiselectorview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.android.base.tools.DisplayUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;

import java.util.ArrayList;
import java.util.List;


/**
 * 多图选择显示控件
 */
public class MultiImageSelectorView extends LinearLayout {
    private static final int DEFAULT_IMAGE_SPACING = 8;//默认图片之间间隔
    private static final int DEFAULT_PER_ROW_COUNT = 4;//默认每行显示的数量
    private static final int DEFAULT_IMAGE_MAX_COUNT = 9;//默认最多选择图片的数量
    private static final int DEFAULT_ADD_IMAGE_RES_ID = R.mipmap.icon_camer;//默认显示的添加图片图标
    private static final int DEFAULT_DELETE_IMAGE_RES_ID = R.mipmap.icon_del;//默认的删除图标

    private int maxWidth;//最大宽度
    private float imageSpacing = DEFAULT_IMAGE_SPACING;//图片之间间隔
    private List<String> imageList;//图片的Url列表
    private int imageMaxCount = DEFAULT_IMAGE_MAX_COUNT;//图片的允许选择数量
    private int addImageResId = DEFAULT_ADD_IMAGE_RES_ID;//选择图片的图标
    private boolean showAddImageOnMaximum = true;//当达到最大图片选择数量时是否显示添加图标，默认显示
    private int deleteImageResId = DEFAULT_DELETE_IMAGE_RES_ID;//删除图片的图标
    private int perRowCount = DEFAULT_PER_ROW_COUNT;// 每行显示最大数

    private int deleteImageSize;

    //单位为Pixel
    private int pxMoreWH = 0;// 每张图的宽高
    private int pxImagePadding = DisplayUtil.dip2px(getContext(), imageSpacing);// 图片间的间距


    private LayoutParams morePicParam, moreColumnFirstParam;//多图的布局参数和第一列的布局参数
    private LayoutParams rowParam;//行布局参数(多图时)

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public MultiImageSelectorView(Context context) {
        super(context);
        initAttrs(context, null);
        init();
    }

    public MultiImageSelectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
        init();
    }

    private void init() {
        ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getViewTreeObserver().removeGlobalOnLayoutListener(this);
                maxWidth = getWidth();
                setList(new ArrayList<>());
            }
        });
    }

    private void initAttrs(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MultiImageSelectorView);
            imageSpacing = typedArray.getDimension(R.styleable.MultiImageSelectorView_imageSpacing, DEFAULT_IMAGE_SPACING);//图片之间间隔
            imageMaxCount = typedArray.getInt(R.styleable.MultiImageSelectorView_imageMaxCount, DEFAULT_IMAGE_MAX_COUNT);//图片的允许选择数量
            addImageResId = typedArray.getResourceId(R.styleable.MultiImageSelectorView_addImage, DEFAULT_ADD_IMAGE_RES_ID);//选择图片的图标
            perRowCount = typedArray.getInt(R.styleable.MultiImageSelectorView_numColumns, DEFAULT_PER_ROW_COUNT);// 每行显示最大数
            deleteImageResId = typedArray.getResourceId(R.styleable.MultiImageSelectorView_deleteImage, DEFAULT_DELETE_IMAGE_RES_ID);//删除图标
            deleteImageSize = (int) typedArray.getDimension(R.styleable.MultiImageSelectorView_deleteImageSize, DisplayUtil.dip2px(context, 15));//删除图标大小
            typedArray.recycle();
        } else {
            deleteImageSize = DisplayUtil.dip2px(context, 3);
        }
    }

    public void setList(List<String> lists) throws IllegalArgumentException {
        if (lists == null) {
            throw new IllegalArgumentException("imageList is null...");
        }
        imageList = lists;

        if (maxWidth > 0) {
            pxMoreWH = (maxWidth - pxImagePadding * (perRowCount - 1)) / perRowCount; //解决右侧图片和内容对不齐问题
            initImageLayoutParams();
        }
        initView();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (maxWidth == 0) {
            int width = measureWidth(widthMeasureSpec);
            if (width > 0) {
                maxWidth = width;
                if (imageList != null && imageList.size() > 0) {
                    setList(imageList);
                }
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    /**
     * 初始化各种布局参数
     */
    private void initImageLayoutParams() {
        int wrap = LayoutParams.WRAP_CONTENT;
        int match = LayoutParams.MATCH_PARENT;
        moreColumnFirstParam = new LayoutParams(pxMoreWH, pxMoreWH);

        morePicParam = new LayoutParams(pxMoreWH, pxMoreWH);
        morePicParam.setMargins(pxImagePadding, 0, 0, 0);

        rowParam = new LayoutParams(match, wrap);
    }

    /**
     * 根据ImageView的数量初始化不同的View布局,还要为每一个View作点击效果
     */
    private void initView() {
        this.setOrientation(VERTICAL);
        this.removeAllViews();
        if (maxWidth == 0) {
            //为了触发onMeasure()来测量MultiImageView的最大宽度，MultiImageView的宽设置为match_parent
            addView(new View(getContext()));
            return;
        }

        int allCount = imageList.size() + 1;//图片数量
        int rowCount = allCount / perRowCount
                + (allCount % perRowCount > 0 ? 1 : 0);// 行数
        for (int rowCursor = 0; rowCursor < rowCount; rowCursor++) {
            //每行是一个单独的水平线性布局
            LinearLayout rowLayout = new LinearLayout(getContext());
            rowLayout.setOrientation(LinearLayout.HORIZONTAL);

            rowLayout.setLayoutParams(rowParam);
            if (rowCursor != 0) {
                rowLayout.setPadding(0, pxImagePadding, 0, 0);
            }

            int columnCount = allCount % perRowCount == 0 ? perRowCount
                    : allCount % perRowCount;//每行的列数
            //如果不是最后一行，有可能出现不够3张图片的情况
            if (rowCursor != rowCount - 1) {
                columnCount = perRowCount;
            }
            addView(rowLayout);

            int rowOffset = rowCursor * perRowCount;// 行偏移
            for (int columnCursor = 0; columnCursor < columnCount; columnCursor++) {
                int position = columnCursor + rowOffset;
                if (position == allCount - 1) {
                    //添加图标
                    if (imageList.size() < imageMaxCount || showAddImageOnMaximum) {
                        rowLayout.addView(createAddImageView(position));
                    }
                } else {
                    rowLayout.addView(createImageView(position));
                }
            }
        }
    }

    /**
     * 创建多图对应的ImageView
     */
    private View createImageView(int position) {
        String url = imageList.get(position);

        OnClickImageListener listener = new OnClickImageListener(position);
        RelativeLayout container = new RelativeLayout(getContext());
        container.setLayoutParams(position % perRowCount == 0 ? moreColumnFirstParam : morePicParam);

        ColorFilterImageView imageView = new ColorFilterImageView(getContext());
        imageView.setScaleType(ScaleType.CENTER_CROP);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        lp.leftMargin = lp.topMargin = lp.rightMargin = lp.bottomMargin = deleteImageSize / 2;
        imageView.setLayoutParams(lp);
        container.addView(imageView);

        ImageView deleteImageView = new ImageView(getContext());
        lp = new RelativeLayout.LayoutParams(deleteImageSize, deleteImageSize);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP | RelativeLayout.ALIGN_PARENT_RIGHT);
        deleteImageView.setLayoutParams(lp);
        deleteImageView.setImageResource(deleteImageResId);
        deleteImageView.setOnClickListener(listener);
        deleteImageView.setTag("delete");
        container.addView(deleteImageView);

        container.setId(url.hashCode());
        container.setTag(String.valueOf(url.hashCode()));
        container.setOnClickListener(listener);
        GlideHelper.loadRoundTrans(getContext(),url, imageView ,DisplayUtil.dip2px(getContext(), 4));
        return container;
    }

    /**
     * 创建添加图标的ImageView
     */
    private View createAddImageView(int position) {
        RelativeLayout container = new RelativeLayout(getContext());
        container.setLayoutParams(position % perRowCount == 0 ? moreColumnFirstParam : morePicParam);
        ImageView imageView = new ImageView(getContext());
        imageView.setScaleType(ScaleType.CENTER_CROP);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        lp.leftMargin = lp.topMargin = lp.rightMargin = lp.bottomMargin = deleteImageSize / 2;
        imageView.setLayoutParams(lp);
        container.addView(imageView);
        container.setTag("add");
        container.setOnClickListener(new OnClickImageListener(position));
        imageView.setImageResource(addImageResId);
        return container;
    }

    private class OnClickImageListener implements OnClickListener {
        private int position;

        OnClickImageListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                String tag = (String) view.getTag();
                if (TextUtils.equals("delete", tag)) {
                    onItemClickListener.onDelete(view, position);
                } else if (TextUtils.equals("add", tag)) {
                    //if (position == imageList.size()) {
                    if (imageList.size() >= imageMaxCount) {
                        onItemClickListener.onReachTheMaximum();
                    } else {
                        onItemClickListener.onAdd(view);
                    }
                    //}
                } else {
                    onItemClickListener.onItemClick(view, position);
                }
                if(view instanceof ColorFilterImageView) {
                    ((ColorFilterImageView)view).setColorFilter(Color.TRANSPARENT);
                } else {
                    if(view instanceof ViewGroup) {
                        ViewGroup parent = (ViewGroup) view;
                        int count = parent.getChildCount();
                        for(int i = 0; i < count; i++) {
                            View child = parent.getChildAt(i);
                            if(child instanceof ColorFilterImageView) {
                                ColorFilterImageView colorFilterImageView = (ColorFilterImageView) child;
                                colorFilterImageView.setColorFilter(Color.TRANSPARENT);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 指定位置是否为添加图标
     */
    public boolean isClickOnAddImageIcon(int position) {
        return position == imageList.size();
    }

    /**
     * 设置在达到最大图片数量限制后是否显示添加图标
     *
     * @param show true为显示，默认为显示
     */
    public void setShowAddImageOnMaximum(boolean show) {
        this.showAddImageOnMaximum = show;
    }

    public void setImageMaxCount(int count) {
        this.imageMaxCount=count;
    }




    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onDelete(View view, int position);

        void onAdd(View view);

        void onReachTheMaximum();
    }
}