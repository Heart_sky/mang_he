package com.android.manghe.view;

import android.content.Context;
import android.util.AttributeSet;
import com.android.base.frame.view.XStateController;

public class EmptyView extends XStateController {
    public EmptyView(Context context) {
        super(context);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
