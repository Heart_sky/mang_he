package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.android.base.tools.ToastUtil
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_modify_nick.*

class ModifyNickDialog(context: Context, listener :IModifyNickNameListener) : Dialog(context) {
    private var mContent : Context? = null
    private var mListener : IModifyNickNameListener? = null
    init {
        mContent = context
        mListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_modify_nick)

        layoutConfirm.setOnClickListener{
            val nick = etNick.text.toString().trim()
            if(nick.isEmpty()){
                ToastUtil.showShort(mContent, "请输入昵称")
                return@setOnClickListener
            }
            mListener?.let {
                it.confirm(nick)
                dismiss()
            }
        }
        layoutCancel.setOnClickListener {
            this.dismiss()
        }
    }
    interface IModifyNickNameListener{
        fun confirm(nick :String);
    }
}