package com.android.manghe.view.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.android.manghe.R
import kotlinx.android.synthetic.main.dialog_update.*

class UpdateDialog: Dialog{

    private var mVersionName = ""
    private var mContent = ""
    private var mUrl = ""
    private var mListener : IUpdateDialogListener? = null
//    private var context:Context? = null

    constructor(context: Context, themeResId: Int = R.style.AppDialogTheme, versionName :String,content:String,url:String,listener: IUpdateDialogListener) : super(context, themeResId) {
        mVersionName = versionName
        mContent = content
        mUrl = url
        mListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_update)
        tvTitle.text = "发现新版本：$mVersionName"
        tvContent.text = mContent
        tvIgnore.setOnClickListener {
            dismiss()
        }
        tvUpdate.setOnClickListener {
            dismiss()
            mListener?.clickUpdate(mUrl)
        }
    }

    companion object {
        fun showDialog(context: Context, versionName :String,content:String,url:String,listener: IUpdateDialogListener){
            UpdateDialog(context,versionName = versionName,content = content,url = url,listener = listener).show()
        }

        interface IUpdateDialogListener{
            fun clickUpdate(url: String)
        }
    }
}