package com.android.manghe.view.popWindow;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.tools.BigDecimalUtils;
import com.android.base.tools.DisplayUtil;
import com.android.manghe.R;
import com.android.manghe.box.adapter.ReplaceAdapter;
import com.android.manghe.box.model.BoxPrizeListModel;
import com.android.manghe.utils.WindowUtils;

import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/10/22 14:59
 * desc   :
 */
public class ReplacePopWindow extends PopupWindow {


    private Activity mContext;
    private List<BoxPrizeListModel.DataBean.ListBean> mList;
    private IReplaceListener mListener;
    private boolean isDismiss = false;
    private boolean mIsShowTip;
    private RecyclerView mRecyclerView;
    private ImageView mImgClose;
    private TextView mTvCancel, mTvOk, mTvCount, mTvCoin, mTvDesc;
    private ReplaceAdapter mAdapter;
    private int mReplaceCode;
    private Double mCoin, mScore;
    private String mDesc;


    public ReplacePopWindow(Activity activity, List<BoxPrizeListModel.DataBean.ListBean> list,
                            double coin, double score, String desc, int code) {
        super(activity);
        mContext = activity;
        this.mList = list;
        this.mReplaceCode = code;
        this.mCoin = coin;
        this.mScore = score;
        this.mDesc = desc;
        init();
    }

    public void setListener(IReplaceListener listener) {
        this.mListener = listener;
    }

    private void init() {
        final View popupView = LayoutInflater.from(mContext).inflate(R.layout.layout_replace_pop_window, null, false);

        mTvCancel = popupView.findViewById(R.id.id_tv_cancel);
        mTvOk = popupView.findViewById(R.id.id_tv_ok);
        mImgClose = popupView.findViewById(R.id.id_img_close);
        mRecyclerView = popupView.findViewById(R.id.id_recycler_view);
        mTvCount = popupView.findViewById(R.id.id_tv_count);
        mTvCoin = popupView.findViewById(R.id.id_tv_coin);
        mTvDesc = popupView.findViewById(R.id.id_tv_desc);
        mTvCount.setText(mList.size() + "");
        if (mReplaceCode == ReplaceAdapter.REPLACE_CODE_COIN) {
            String temCoin = BigDecimalUtils.round(mCoin);
            if (temCoin.contains(".00")) {
                temCoin = temCoin.replace(".00", "");
            }

            mTvCoin.setText(temCoin + "币");
        } else if (mReplaceCode == ReplaceAdapter.REPLACE_CODE_SCORE) {
            String temSore = BigDecimalUtils.round(mScore);
            if (temSore.contains(".00")) {
                temSore = temSore.replace(".00", "");
            }
            mTvCoin.setText(temSore + "积分");
        }
        setText(mDesc);
        LinearLayout.LayoutParams linearParams = (LinearLayout.LayoutParams) mRecyclerView.getLayoutParams(); //取控件textView当前的布局参数
        if (mList.size() == 1) {
            linearParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
            linearParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        } else if (mList.size() == 2) {
            linearParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
            linearParams.height = DisplayUtil.dip2px(mContext, 250f);
        } else {
            linearParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
            linearParams.height = DisplayUtil.dip2px(mContext, 320f);
        }
        mRecyclerView.setLayoutParams(linearParams);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new ReplaceAdapter(mContext, mList, mReplaceCode);

        mRecyclerView.setAdapter(mAdapter);

        popupView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        this.setContentView(popupView);
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setOutsideTouchable(true);
        this.setBackgroundDrawable(new ColorDrawable());
        this.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                dismiss();
                WindowUtils.backgroundAlpha(mContext, 1f);
            }
        });

        mTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != mListener)
                    mListener.ok();
                WindowUtils.backgroundAlpha(mContext, 1f);
            }
        });

        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mImgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                WindowUtils.backgroundAlpha(mContext, 1f);
            }
        });


    }

    public void setText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mTvDesc.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            mTvDesc.setText(Html.fromHtml(text));
        }
    }


    public void showPopWindow(View view) {
        if (this.isShowing()) {
            this.dismiss();
        } else {
            this.showAtLocation(view, Gravity.CENTER, 0, 0);
            WindowUtils.backgroundAlpha(mContext, 0.5f);
        }
    }


    public void close() {
        super.dismiss();
    }

    public interface IReplaceListener {
        void ok();
    }
}
