package com.android.manghe.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.android.manghe.R;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2021/11/26 16:45
 * desc   :
 */
public class NewTipDialog extends Dialog implements View.OnClickListener {
    private String mContent = "";
    private ITipDialogListener mListener;
    private TextView mTvLeft, mTvRight, mTvTitle;
    private TextView mSLine;
    private TextView mTxtContent;
    private Context mContext;


    public NewTipDialog(Context context, String content) {
        super(context, R.style.CustomDialogStyle);
        this.mContext = context;
        this.mContent = content;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_dialog_tip);
        mTxtContent = findViewById(R.id.dialog_tv);
        mTxtContent.setText(mContent);
        mSLine = findViewById(R.id.line_s);
        mTvTitle = findViewById(R.id.dialog_hint);

        this.setCancelable(true);
        mTvLeft = findViewById(R.id.dialog_left);
        mTvRight = findViewById(R.id.dialog_right);
        mTvLeft.setOnClickListener(this);
        mTvRight.setOnClickListener(this);
    }


    public void setTitle(String text) {
        mTvTitle.setText(text);
    }

    public void setCanCancelAble(boolean flag) {
        setCancelable(flag);
        setCanceledOnTouchOutside(flag);
    }

    public void setLeftTextColor(int colors) {
        mTvLeft.setTextColor(ContextCompat.getColor(mContext, colors));
    }

    public void setRightTextColor(int colors) {
        mTvRight.setTextColor(ContextCompat.getColor(mContext, colors));
    }

    public void setContentSizeColors(float size, int colors) {
        mTxtContent.setTextSize(size);
        mTxtContent.setTextColor(ContextCompat.getColor(mContext, colors));
    }

    public interface ITipDialogListener {
        void clickLeft();

        void clickRight();
    }

    public void setSureMode() {
        mTvLeft.setVisibility(View.GONE);
        mSLine.setVisibility(View.GONE);
    }


    public void setListener(ITipDialogListener listener) {
        mListener = listener;
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_left:
                dismiss();
                if (null != mListener)
                    mListener.clickLeft();
                break;
            case R.id.dialog_right:
                dismiss();
                if (null != mListener)
                    mListener.clickRight();
                break;
            default:
                break;
        }
    }

    public void setLeftText(String text) {
        mTvLeft.setText(text);
    }

    public void setRightText(String text) {
        mTvRight.setText(text);
    }


}
