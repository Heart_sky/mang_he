package com.android.manghe.category.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.AuctionSocketRes;
import com.android.manghe.detail.activity.GoodDetailActivity;
import com.android.manghe.user.activity.LoginActivity;

import java.util.List;

public class CategoryAuctionGridAdapter extends RecyclerView.Adapter<CategoryAuctionHolder> {
    private Context mContext;
    private boolean isUseFromSearch;
    public List<AuctionSocketRes.ListBean> mAuctionList;

    private AsyncListDiffer<AuctionSocketRes.ListBean> mDiffer;

    private DiffUtil.ItemCallback<AuctionSocketRes.ListBean> diffCallback = new DiffUtil.ItemCallback<AuctionSocketRes.ListBean>() {
        @Override
        public boolean areItemsTheSame(AuctionSocketRes.ListBean oldItem, AuctionSocketRes.ListBean newItem) {
            return TextUtils.equals(oldItem.id, newItem.id);
        }

        @Override
        public boolean areContentsTheSame(AuctionSocketRes.ListBean oldItem, AuctionSocketRes.ListBean newItem) {
            return oldItem.time == newItem.time;
        }
    };

    public AuctionSocketRes.ListBean getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public CategoryAuctionGridAdapter(Context context, List<AuctionSocketRes.ListBean> list) {
//        super(R.layout.item_category_grid_auction, list);
        mAuctionList = list;
        mContext = context;
        mDiffer = new AsyncListDiffer<>(this, diffCallback);
    }

    public void setUseFromSearch() {
        isUseFromSearch = true;
    }


    public void update(List<AuctionSocketRes.ListBean> list) {
        mDiffer.submitList(list);
        mAuctionList = list;
    }

    @NonNull
    @Override
    public CategoryAuctionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_grid_auction, parent, false);
        return new CategoryAuctionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAuctionHolder holder, int position) {
        if (mAuctionList.size() != 0) {
            AuctionSocketRes.ListBean item =getItem(position);
            GlideHelper.loadWithHolderErr(mContext, item.thumb, holder.ivPic);
            holder.tvCurrentPrice.setText("￥" + item.getPrice());
            holder.tvTitle.setText(item.title);
            holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserHolder.getUserInfo(mContext) == null) {
                        mContext.startActivity(new Intent(mContext, LoginActivity.class));
                    } else {
                        Intent intent = new Intent(mContext, GoodDetailActivity.class);
                        intent.putExtra("auctionId", item.id);
                        mContext.startActivity(intent);
                    }
                }
            });
            if (isUseFromSearch) {
                if (item.time > 0) {
                    setCountDown(holder.tvTime, item.time);
                } else {
                    holder.tvTime.setText("00：00：00");
                }
            } else {
                setCountDown(holder.tvTime, item.time);
            }

            if(item.start>0){
                holder.tvKan.setText("即将开始");
            }else{
                holder.tvKan.setText("立即购买");
            }
        }
    }

    private void setCountDown(TextView tvTime, int time) {
        int day = 60 * 60 * 24;
        int hour = 60 * 60;
        int min = 60;
        double d1 = time * 1.0 / day;
        if (time < 0) {
            time = 0;
        }
        if (d1 > 1) {
            int dayNum = time / day;
            int hourNum = time / hour % 24;
            int minuteNum = time / min % 60;
            tvTime.setText(dayNum + "天 " + hourNum + "时 " + minuteNum + "分");
        } else {
            String hour1 = String.format("%02d", time / 3600);
            String minute1 = String.format("%02d", time % 3600 / 60);
            String second1 = String.format("%02d", time % 60);
            tvTime.setText(hour1 + "：" + minute1 + "：" + second1);
        }
    }

    private int result = -1;

    /**
     * 倒数，实时更新每个item的显示时间
     *
     * @return
     */
    public int countDownTime() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    //如果goodsList为null或者result等于goodsList.size+1，则表示已经倒数完，此时需要停止倒数
                    if (mAuctionList == null || result == mAuctionList.size() + 1) {
                        result = 0;
                        return;
                    }
                    result = 1;
                    for (AuctionSocketRes.ListBean goods : mAuctionList) {
                        if (goods.time == 0) {
                            goods.time = 0;
                            result++;   //每当一个对象倒数完或者当前对象已经处于进行中，则让result加1，用于后续所有对象倒数完后，停止倒数
                        } else {
                            goods.time = goods.time - 1;
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return result;
    }
}
class CategoryAuctionHolder extends RecyclerView.ViewHolder {
    public ImageView ivPic;
    public TextView tvCurrentPrice,tvTitle, tvTime,tvKan;
    public View layoutContent;

    public CategoryAuctionHolder(View itemView) {
        super(itemView);
        ivPic = itemView.findViewById(R.id.ivPic);
        tvCurrentPrice = itemView.findViewById(R.id.tvCurrentPrice);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        layoutContent = itemView.findViewById(R.id.layoutContent);
        tvTime = itemView.findViewById(R.id.tvTime);
        tvKan = itemView.findViewById(R.id.tvKan);
    }
}
