package com.android.manghe.category.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class CategoryInfo extends NetBean<List<CategoryInfo>> {

        public String id;
        public String catname;
        public String parentid;
        public String arrparentid;
        public String arrchildid;
        public String thumb;
        public String child;

        public boolean isSelected;
}
