package com.android.manghe.category.presenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.tools.EmptyUtil;
import com.android.manghe.category.model.CategoryInfo;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.index.fragment.CategoryFragment;
import com.android.manghe.common.model.AuctionSocketReq;
import com.android.manghe.common.model.AuctionSocketRes;
import com.google.gson.Gson;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PCategory extends XPresenter<CategoryFragment> {

    private List<CategoryInfo> mCategoryList = new ArrayList<>();

    public void getCategories() {
        new OKHttpUtil(getV().getContext()).url(ConstantsUrl.domain + ConstantsUrl.MAIN_GOODSCATEGORYDATA + "?type=0")
                .get()
                .execute(new BaseBack<CategoryInfo>() {

                    @Override
                    public void onSuccess(Call call, List<CategoryInfo> categoryList) {
                        if (categoryList != null && categoryList.size() != 0) {
                            CategoryInfo allCategory = new CategoryInfo();
                            allCategory.catname = "全部商品";
                            allCategory.id = "0";
                            mCategoryList.clear();
                            mCategoryList.add(allCategory);
                            mCategoryList.addAll(categoryList);
                            getV().updateTabs(mCategoryList);
                            loadGridData();
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private int mCurrentTabPosition;
    public void setTabPosition(int position){
        mCurrentTabPosition = position;
    }

    private Timer timer;
    public void loadGridData(){
        if(mCategoryList == null || mCategoryList.size() == 0){
            return;
        }
        if(timer == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    AuctionSocketReq auction = new AuctionSocketReq();
                    auction.type = "get_second_auctions";
                    auction.page = 1;
                    auction.per_page = 300;
                    auction.cid = Integer.parseInt(mCategoryList.get(mCurrentTabPosition).id);
                    String sendJson = new Gson().toJson(auction);
                    getV().sendText(sendJson);
                }
            },1000,1000);
        }
    }

    public void stopSocketTimer(){
        if(timer != null){
            timer.cancel();
            timer = null;
        }
    }

    public void dealSocketData(String json){
        if(!EmptyUtil.check(json)){
            if(json.contains("get_second_auctions")){
                try{
                    AuctionSocketRes socketRes = new Gson().fromJson(json, AuctionSocketRes.class);
                    if(!EmptyUtil.check(socketRes)){
                        getV().updateGridData(socketRes.list != null ? socketRes.list : new ArrayList<>());
                    }
                }catch (Exception e){}
            }
        }
    }
}
