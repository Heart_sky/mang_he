package com.android.manghe.push;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;
import com.android.manghe.R;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.webview.ERefreshWebType;
import com.android.manghe.common.webview.WebViewActivity;
import com.android.manghe.common.webview.WebViewParameter;
import com.android.manghe.detail.activity.GoodDetailActivity;
import com.android.manghe.main.MainActivity;
import com.android.manghe.push.model.PushInfo;
import com.android.manghe.user.activity.LoginActivity;
import com.google.gson.Gson;
import com.umeng.message.UmengNotifyClickActivity;
import org.android.agoo.common.AgooConstants;

public class PushDialogActivity extends UmengNotifyClickActivity {
    private TextView pushTextView;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_push);
        pushTextView = findViewById(R.id.mipushTextView);
    }

    @Override
    public void onMessage(Intent intent) {
        super.onMessage(intent);  //此方法必须调用，否则无法统计打开数
        String body = intent.getStringExtra(AgooConstants.MESSAGE_BODY);
        if (!TextUtils.isEmpty(body)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //解析body的json
//                    pushTextView.setText(body);
                    try {
                        PushInfo info = new Gson().fromJson(body, PushInfo.class);
                        if (info != null) {
                            PushInfo.ExtraBean extra = info.extra;
                            if (extra != null) {
                                if (extra.jumpType != null) {
                                    if (UserHolder.getUserInfo(PushDialogActivity.this) == null){
                                        LoginActivity.Companion.showNewTaskActivity(PushDialogActivity.this);
                                    }else {
                                        if (TextUtils.equals("URL", extra.jumpType)) {
                                            WebViewActivity.showNewTaskActivity(true,
                                                    PushDialogActivity.this, new WebViewParameter.WebParameterBuilder()
                                                            .setUrl(extra.urlStr)
                                                            .setShowProgress(true)
                                                            .setCanHistoryGoBackOrForward(true)
                                                            .setReloadable(true)
                                                            .setReloadType(ERefreshWebType.ClickRefresh)
                                                            .setCustomTitle(extra.urlTitle)
                                                            .build());
                                        } else if (TextUtils.equals("Auction", extra.jumpType)) {
                                            Intent intent = new Intent(PushDialogActivity.this, GoodDetailActivity.class);
                                            intent.putExtra("auctionId", extra.auctionId);
                                            intent.putExtra("isFromPush", true);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        } else if (TextUtils.equals("MySetting", extra.jumpType)) {
                                            Intent intent = new Intent(PushDialogActivity.this, MainActivity.class);
                                            intent.putExtra("isToChangeTab", true);
                                            intent.putExtra("position", 4);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    finish();
                }
            });
        }
    }
}
