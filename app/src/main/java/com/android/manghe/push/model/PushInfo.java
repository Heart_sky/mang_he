package com.android.manghe.push.model;

public class PushInfo {


    /**
     * display_type : notification
     * extra : {"jumpType":"URL","urlTitle":"我是标题","urlStr":"https://www.baidu.com"}
     * msg_id : uuhrhzk156691339094010
     * body : {"after_open":"go_app","play_lights":"false","ticker":"开发测试标题","play_vibrate":"false","text":"开发测试内容  开发测试内容","title":"开发测试标题","play_sound":"true"}
     * random_min : 0
     */

    public String display_type;
    public ExtraBean extra;
    public String msg_id;
    public BodyBean body;
    public int random_min;

    public static class ExtraBean {
        /**
         * jumpType : URL
         * urlTitle : 我是标题
         * urlStr : https://www.baidu.com
         */

        public String jumpType;
        public String urlTitle;
        public String urlStr;
        public String auctionId;
    }

    public static class BodyBean {
        /**
         * after_open : go_app
         * play_lights : false
         * ticker : 开发测试标题
         * play_vibrate : false
         * text : 开发测试内容  开发测试内容
         * title : 开发测试标题
         * play_sound : true
         */

        public String after_open;
        public String play_lights;
        public String ticker;
        public String play_vibrate;
        public String text;
        public String title;
        public String play_sound;
    }
}
