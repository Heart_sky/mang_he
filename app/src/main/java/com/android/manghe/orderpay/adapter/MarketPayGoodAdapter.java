package com.android.manghe.orderpay.adapter;

import android.content.Context;

import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.market.model.CartGoodListRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class MarketPayGoodAdapter extends BaseQuickAdapter<CartGoodListRes.DataBean.CartGoodsBean, BaseViewHolder> {
    private Context mContext;

    public MarketPayGoodAdapter(Context context, List<CartGoodListRes.DataBean.CartGoodsBean> dataList) {
        super(R.layout.item_pay_good, dataList);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, CartGoodListRes.DataBean.CartGoodsBean goodInfo) {
        GlideHelper.loadWithHolderErr(mContext, goodInfo.img_src, helper.getView(R.id.ivGoodPic));
        helper.setText(R.id.tvGoodTitle, goodInfo.goods_name);
        helper.setText(R.id.tvPrice, "￥"+goodInfo.goods_price);
        helper.setText(R.id.tvSpecName, goodInfo.goods_spec);
        helper.setText(R.id.tvQyt, "x" + goodInfo.qty);
    }
}
