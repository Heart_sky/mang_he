package com.android.manghe.orderpay.model;

import com.android.base.okhttp.okUtil.base.NetBean;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MarketOrderInfoRes extends NetBean<MarketOrderInfoRes.DataBean> {

    public static class DataBean {
        /**
         * cart_type : 0
         * cart_goods : [{"id":"1019","mid":"101731","goods_id":"230","spec":"","goods_name":"今古楼 小叶紫檀手串","cost_price":"1022.00","goods_price":"1022.00","goods_score":0,"qty":"1","subtotal":1022,"subtotal_score":0,"type":"0","obj_id":"0","is_selected":"1","goods_cid":"144","goods_stock":"459","goods_is_sale":"1","goods_weight":"0","goods_weight_unit":"kg","goods_sp_val":"","goods_favourable_price":"152.00","thumb":"[{\"path\":\"\\/upload\\/1\\/images\\/gallery\\/y\\/h\\/18090_src.jpg\",\"title\":\"\"}]","img_src":"https://www.ycdy8.com/upload/1/images/gallery/y/h/18090_src.jpg","url":"/goods/show/230"}]
         * address_list : [{"id":"41100","mid":"101731","username":"15989027318","name":"噜啦噜啦","mobile":"55222","zone":"1191","area":"河北  廊坊  大城县","address":"太累了哦啦咯","zip":"","is_default":"1"}]
         * address : {"id":"41100","mid":"101731","username":"15989027318","name":"噜啦噜啦","mobile":"55222","zone":"1191","area":"河北  廊坊  大城县","address":"太累了哦啦咯","zip":"","is_default":"1"}
         * weight : 0
         * fright : {"17":{"id":"17","name":"顺丰","pinyin":"sf","listorder":"0","status":"1","free":"0","sid":"6","title":"顺丰","is_default":1,"freight_free":"0.00","shipping_fee":"0.00"},"20":{"id":"20","name":"京东快递","pinyin":"jd","listorder":"0","status":"1","free":"1","sid":"0","title":"京东快递","freight_free":"0.00","shipping_fee":"0.00"},"21":{"id":"21","name":"自动充值","pinyin":"自动充值","listorder":"0","status":"1","free":"1","sid":"0","title":"自动充值","freight_free":"0.00","shipping_fee":"0.00"},"10":{"id":"10","name":"顺丰速递","pinyin":"shunfeng","listorder":"1","status":"1","free":"1","sid":"0","title":"顺丰速递","freight_free":"0.00","shipping_fee":"0.00"},"12":{"id":"12","name":"申通快递","pinyin":"shentong","listorder":"4","status":"1","free":"1","sid":"0","title":"申通快递","freight_free":"0.00","shipping_fee":"0.00"}}
         * flow_user_money : false
         * flow_user_score : 0
         * total : {"score_amout":0,"goods_amount":"1022.00"}
         * token : 5bde495de228af42b7494d0407efedfb
         */

        public int cart_type;
        public int weight;
        public FrightBean fright;
        public boolean flow_user_money;
        public int flow_user_score;
        public double coupon_favourable_price;
        public TotalBean total;
        public String token;
        public List<CartGoodsBean> cart_goods;
        public List<AddressListBean> address_list;

        public static class AddressBean {
            /**
             * id : 41100
             * mid : 101731
             * username : 15989027318
             * name : 噜啦噜啦
             * mobile : 55222
             * zone : 1191
             * area : 河北  廊坊  大城县
             * address : 太累了哦啦咯
             * zip :
             * is_default : 1
             */

            public String id;
            public String mid;
            public String username;
            public String name;
            public String mobile;
            public String zone;
            public String area;
            public String address;
            public String zip;
            public String is_default;
        }

        public static class FrightBean {
            /**
             * 17 : {"id":"17","name":"顺丰","pinyin":"sf","listorder":"0","status":"1","free":"0","sid":"6","title":"顺丰","is_default":1,"freight_free":"0.00","shipping_fee":"0.00"}
             * 20 : {"id":"20","name":"京东快递","pinyin":"jd","listorder":"0","status":"1","free":"1","sid":"0","title":"京东快递","freight_free":"0.00","shipping_fee":"0.00"}
             * 21 : {"id":"21","name":"自动充值","pinyin":"自动充值","listorder":"0","status":"1","free":"1","sid":"0","title":"自动充值","freight_free":"0.00","shipping_fee":"0.00"}
             * 10 : {"id":"10","name":"顺丰速递","pinyin":"shunfeng","listorder":"1","status":"1","free":"1","sid":"0","title":"顺丰速递","freight_free":"0.00","shipping_fee":"0.00"}
             * 12 : {"id":"12","name":"申通快递","pinyin":"shentong","listorder":"4","status":"1","free":"1","sid":"0","title":"申通快递","freight_free":"0.00","shipping_fee":"0.00"}
             */

            @SerializedName("17")
            public _$17Bean _$17;
            @SerializedName("20")
            public _$20Bean _$20;
            @SerializedName("21")
            public _$21Bean _$21;
            @SerializedName("10")
            public _$10Bean _$10;
            @SerializedName("12")
            public _$12Bean _$12;

            public static class _$17Bean {
                /**
                 * id : 17
                 * name : 顺丰
                 * pinyin : sf
                 * listorder : 0
                 * status : 1
                 * free : 0
                 * sid : 6
                 * title : 顺丰
                 * is_default : 1
                 * freight_free : 0.00
                 * shipping_fee : 0.00
                 */

                public String id;
                public String name;
                public String pinyin;
                public String listorder;
                public String status;
                public String free;
                public String sid;
                public String title;
                public int is_default;
                public String freight_free;
                public String shipping_fee;
            }

            public static class _$20Bean {
                /**
                 * id : 20
                 * name : 京东快递
                 * pinyin : jd
                 * listorder : 0
                 * status : 1
                 * free : 1
                 * sid : 0
                 * title : 京东快递
                 * freight_free : 0.00
                 * shipping_fee : 0.00
                 */

                public String id;
                public String name;
                public String pinyin;
                public String listorder;
                public String status;
                public String free;
                public String sid;
                public String title;
                public String freight_free;
                public String shipping_fee;
            }

            public static class _$21Bean {
                /**
                 * id : 21
                 * name : 自动充值
                 * pinyin : 自动充值
                 * listorder : 0
                 * status : 1
                 * free : 1
                 * sid : 0
                 * title : 自动充值
                 * freight_free : 0.00
                 * shipping_fee : 0.00
                 */

                public String id;
                public String name;
                public String pinyin;
                public String listorder;
                public String status;
                public String free;
                public String sid;
                public String title;
                public String freight_free;
                public String shipping_fee;
            }

            public static class _$10Bean {
                /**
                 * id : 10
                 * name : 顺丰速递
                 * pinyin : shunfeng
                 * listorder : 1
                 * status : 1
                 * free : 1
                 * sid : 0
                 * title : 顺丰速递
                 * freight_free : 0.00
                 * shipping_fee : 0.00
                 */

                public String id;
                public String name;
                public String pinyin;
                public String listorder;
                public String status;
                public String free;
                public String sid;
                public String title;
                public String freight_free;
                public String shipping_fee;
            }

            public static class _$12Bean {
                /**
                 * id : 12
                 * name : 申通快递
                 * pinyin : shentong
                 * listorder : 4
                 * status : 1
                 * free : 1
                 * sid : 0
                 * title : 申通快递
                 * freight_free : 0.00
                 * shipping_fee : 0.00
                 */

                public String id;
                public String name;
                public String pinyin;
                public String listorder;
                public String status;
                public String free;
                public String sid;
                public String title;
                public String freight_free;
                public String shipping_fee;
            }
        }

        public static class TotalBean {
            /**
             * score_amout : 0
             * goods_amount : 1022.00
             */

            public int score_amout;
            public String goods_amount;
        }

        public static class CartGoodsBean {
            /**
             * id : 1019
             * mid : 101731
             * goods_id : 230
             * spec :
             * goods_name : 今古楼 小叶紫檀手串
             * cost_price : 1022.00
             * goods_price : 1022.00
             * goods_score : 0
             * qty : 1
             * subtotal : 1022
             * subtotal_score : 0
             * type : 0
             * obj_id : 0
             * is_selected : 1
             * goods_cid : 144
             * goods_stock : 459
             * goods_is_sale : 1
             * goods_weight : 0
             * goods_weight_unit : kg
             * goods_sp_val :
             * goods_favourable_price : 152.00
             * thumb : [{"path":"\/upload\/1\/images\/gallery\/y\/h\/18090_src.jpg","title":""}]
             * img_src : https://www.ycdy8.com/upload/1/images/gallery/y/h/18090_src.jpg
             * url : /goods/show/230
             */

            public String id;
            public String mid;
            public String goods_id;
            public String spec;
            public String goods_name;
            public double cost_price;
            public double goods_price;
            public int goods_score;
            public int qty;
            public double subtotal;
            public int subtotal_score;
            public String type;
            public String obj_id;
            public String is_selected;
            public String goods_cid;
            public String goods_stock;
            public String goods_is_sale;
            public String goods_weight;
            public String goods_spec;
            public String goods_weight_unit;
            public String goods_favourable_price;
            public String thumb;
            public String img_src;
            public String url;
        }

        public static class AddressListBean {
            /**
             * id : 41100
             * mid : 101731
             * username : 15989027318
             * name : 噜啦噜啦
             * mobile : 55222
             * zone : 1191
             * area : 河北  廊坊  大城县
             * address : 太累了哦啦咯
             * zip :
             * is_default : 1
             */

            public String id;
            public String mid;
            public String username;
            public String name;
            public String mobile;
            public String zone;
            public String area;
            public String address;
            public String zip;
            public String is_default;
        }
    }
}
