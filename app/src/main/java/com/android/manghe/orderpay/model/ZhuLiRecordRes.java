package com.android.manghe.orderpay.model;


import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class ZhuLiRecordRes extends NetBean<ZhuLiRecordRes.DataBean> {
    public static class DataBean {
        public List<ZhuLiRecordRes.DataBean.RecordInfo> list;

        public static class RecordInfo {
            public String nickname;
            public String avatar;
            public float distance_amount;
        }
    }
}
