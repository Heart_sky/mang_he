package com.android.manghe.orderpay.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.StatusHolder;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshCartEvent;
import com.android.manghe.mine.model.AddressInfo;
import com.android.manghe.mine.model.MemberIndexRes;
import com.android.manghe.orderpay.activity.NoPriceMarketOrderPayActivity;
import com.android.manghe.orderpay.model.MarketOrderInfoRes;
import com.android.manghe.orderpay.model.MarketSubmitOrderRes;
import com.eightbitlab.rxbus.Bus;

import java.util.HashMap;

import okhttp3.Call;

public class PNoPriceMarketOrder extends XPresenter<NoPriceMarketOrderPayActivity> {

    public String goodId = "";
    public String spec = "";
    public String perPrice = "";
    public int qty;
    public MarketOrderInfoRes.DataBean detailInfo;
    public AddressInfo currentAddress;
    public MemberIndexRes.DataBean.MemberBean memberBean;
    public MarketOrderInfoRes.DataBean orderInfo;

    public void setCurrentAddress(AddressInfo currentAddress) {
        this.currentAddress = currentAddress;
    }

    public void loadOrderInfo() {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.DIRECTBUY_FIRMORDER + "?id=" + goodId + "&qty=" + qty + "&spec=" + spec + "&is_exempt=1", headMap)
                .get()
                .execute(new BaseBack<MarketOrderInfoRes>() {

                    @Override
                    public void onSuccess(Call call, MarketOrderInfoRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0 && res.data != null) {
                            orderInfo = res.data;
                            detailInfo = res.data;
                            getV().showGoodDetail(detailInfo);
                        } else {
                            getV().showGoodDetail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        e.printStackTrace();
                        getV().showGoodDetail(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void loadAddressList() {
        HashMap<String, String> data = new HashMap<>();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERADDRESSLIST, data, headMap)
                .get()
                .execute(new GsonBaseBack<AddressInfo>() {

                    @Override
                    public void onSuccess(Call call, AddressInfo addressInfo) {
                        if (addressInfo != null && addressInfo.code == 0 && addressInfo.data != null && !addressInfo.data.isEmpty()) {
                            for (AddressInfo addr : addressInfo.data) {
                                if (TextUtils.equals(addr.is_default, "1")) {
                                    getV().showAddress(addr);
                                    return;
                                }
                            }
                            getV().showAddress(addressInfo.data.get(0));
                        } else {
                            getV().showAddress(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().showAddress(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void toPay(String mark) {
        if (orderInfo == null) {
            return;
        }
        StatusHolder.mCurrentPayType = 2;
        getV().showLoadingDialog();
        //-------------开始生成订单
        HashMap<String, String> data = new HashMap<>();
        data.put("address_id", currentAddress.id);
        data.put("ajax", "1");
        data.put("order_tip", mark);
        data.put("is_exempt", "1");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.DIRECTBUY_SUBMITORDER, headMap)
                .post(data).build()
                .execute(new BaseBack<MarketSubmitOrderRes>() {
                    @Override
                    public void onSuccess(Call call, MarketSubmitOrderRes res) {
                        if (res != null) {
                            if (res.code == 0) {
                                if (res.data != null) {
                                    //-------------生成订单完成
                                    Bus.INSTANCE.send(new RefreshCartEvent());
                                    getV().buildOrderDone(res.data.order_sn, orderInfo.cart_goods.get(0).img_src, orderInfo.cart_goods.get(0).goods_name);
                                } else {
                                    ToastUtil.showLong(getV(), "生成订单失败");
                                }
                            } else {
                                ToastUtil.showLong(getV(), res.msg);
                            }
                        }
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

    public void getMemberInfo() {
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("UID", UserHolder.getUID(getV()));
        data.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBERINDEX, data)
                .get()
                .execute(new BaseBack<MemberIndexRes>() {

                    @Override
                    public void onSuccess(Call call, MemberIndexRes res) {
                        if (res.code == 0 && res.data != null) {
                            memberBean = res.data.member;
                            loadOrderInfo();
                        }else{
                            getV().hideLoadingDialog();
                            getV().getInfoFail();
                        }
                    }
                });
    }
}
