package com.android.manghe.orderpay.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class MarketSubmitOrderRes extends NetBean<MarketSubmitOrderRes.DataBean> {
    public static class DataBean {
        /**
         * order_sn : 2017091516590052495210
         */

        public String order_sn;
        public String id;
    }
}
