package com.android.manghe.orderpay.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class SignInfoRes extends NetBean<SignInfoRes.DataBean> {

    public static class DataBean {
        /**
         * total : 0
         * signin_count : 1
         * sign_list : {"id":"2","mid":"463620","amount":"1.60","type":"income","cause_note":"signin","c_time":"1605626134"}
         */

        public float total;
        public String signin_count;
        public boolean isTodaySign;
        public List<SignListBean> sign_list;

        public static class SignListBean {
            /**
             * id : 2
             * mid : 463620
             * amount : 1.60
             * type : income
             * cause_note : signin
             * c_time : 1605626134
             */

            public String id;
            public String mid;
            public String amount;
            public String type;
            public String cause_note;
            public String c_time;
        }
    }
}
