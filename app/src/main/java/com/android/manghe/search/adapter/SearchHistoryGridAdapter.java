package com.android.manghe.search.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.android.base.view.recycler.BaseRecyclerViewAdapter;
import com.android.manghe.R;

import java.util.List;

public class SearchHistoryGridAdapter extends BaseRecyclerViewAdapter<String, SearchHistoryGridAdapter.SearchHistoryHolder> {


    public SearchHistoryGridAdapter(List<String> list) {
        super(list);
    }

    private ISearchHistoryListener mListener;

    public void update(List<String> list) {
        super.update(list);
    }


    public void setListener(ISearchHistoryListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void bindDataToItemView(SearchHistoryHolder holder, String item) {
        holder.setText(R.id.tvText, item);
        holder.setVisible(R.id.verticalLine, getData().indexOf(item) % 3 != 2);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(item);
                }
            }
        });
    }

    @Override
    public SearchHistoryHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return new SearchHistoryHolder(inflateItemView(viewGroup, R.layout.item_search_history));
    }

    public class SearchHistoryHolder extends BaseRecyclerViewAdapter.SparseArrayViewHolder {

        public SearchHistoryHolder(View itemView) {
            super(itemView);
        }
    }

    public interface ISearchHistoryListener {
        void onClick(String content);
    }
}
