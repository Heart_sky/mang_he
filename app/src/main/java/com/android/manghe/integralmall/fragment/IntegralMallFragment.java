package com.android.manghe.integralmall.fragment;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.frame.extend.IRefresh;
import com.android.base.frame.view.MVPFragment;
import com.android.base.tools.DisplayUtil;
import com.android.manghe.R;
import com.android.manghe.box.model.MessageEvent;
import com.android.manghe.config.Config;
import com.android.manghe.index.model.MainTopBannerBean;
import com.android.manghe.integralmall.presenter.PIntegral;
import com.android.manghe.mine.adapter.IntegralAdapter;
import com.android.manghe.mine.adapter.IntegralCoverPagerAdapter;
import com.android.manghe.mine.model.IntegralModel;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.tmall.ultraviewpager.UltraViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * author : liuwen
 * e-mail : liuwen370494581@163.com
 * time   : 2022/01/05 09:05
 * desc   : 积分商城
 */
public class IntegralMallFragment extends MVPFragment<PIntegral> implements IRefresh {

    private RecyclerView mRecyclerView;
    private IntegralAdapter mAdapter;
    private TextView tvComprehensive, tvSalesVolume, tvPrice, tvIntegral;
    private int mSort = 1;
    private ImageView mImgSort;
    private String mOrder = "asc";//默认是从低到高排序
    private UltraViewPager mViewpager;
    private IntegralCoverPagerAdapter mPagerAdapter;
    private LinearLayout mLayoutPrice;

    @Override
    protected void initData(@Nullable Bundle savedInstanceState, View parent) {
        EventBus.getDefault().register(this);
        getTitleBar().setToolbar("商城").hideBottomLine()
                .setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        mRecyclerView = parent.findViewById(R.id.id_recycler_view);
        tvComprehensive = parent.findViewById(R.id.tvComprehensive);
        tvSalesVolume = parent.findViewById(R.id.tvSalesVolume);
        tvPrice = parent.findViewById(R.id.tvPrice);
        tvIntegral = parent.findViewById(R.id.tvIntegral);
        mImgSort = parent.findViewById(R.id.ivSort);
        mViewpager = parent.findViewById(R.id.id_view_pager);
        mLayoutPrice = parent.findViewById(R.id.id_ly_price);
        setListener();
        initRecyclerView();
        showLoadingDialog();
        getP().getIntegralList(true, 1, "");
        getP().getBanner();

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_integral_mall;
    }

    private void setListener() {
        tvComprehensive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFilter(1, "");

            }
        });
        tvSalesVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFilter(2, "");

            }
        });

        mLayoutPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOrder.equals("asc")) {
                    setFilter(3, "desc");
                } else {
                    setFilter(3, "asc");
                }
            }
        });

    }


    private void setFilter(int type, String order) {
        mSort = type;
        this.mOrder = order;
        tvComprehensive.setTextColor(ContextCompat.getColor(getContext(), type == 1 ? R.color.comm_yellow : R.color.black));
        tvSalesVolume.setTextColor(ContextCompat.getColor(getContext(), type == 2 ? R.color.comm_yellow : R.color.black));
        tvPrice.setTextColor(ContextCompat.getColor(getContext(), type == 3 ? R.color.comm_yellow : R.color.black));

        if (order.equals("asc")) {
            mImgSort.setImageResource(R.mipmap.ic_sort_down);
        } else if (order.equals("desc")) {
            mImgSort.setImageResource(R.mipmap.ic_sort_top);
        } else {
            mImgSort.setImageResource(R.mipmap.ic_sort);
        }
        getP().getIntegralList(true, type, order);
    }


    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mAdapter = new IntegralAdapter(getContext(), new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
    }

    public void updateList(List<IntegralModel.DataBean.ListBean> listBeans) {
        mAdapter.replaceData(listBeans);

    }

    public void updateIntegral(String integral) {
        tvIntegral.setText(integral);
    }

    public void updateBanner(List<MainTopBannerBean> bannerList) {
        mPagerAdapter = new IntegralCoverPagerAdapter(getContext(), bannerList);
        mViewpager.setAdapter(mPagerAdapter);
        if (bannerList.size() > 1) {
            mViewpager.setInfiniteLoop(true);
            mViewpager.setAutoScroll(5000);
            mViewpager.setCurrentItem(0);
            mViewpager.initIndicator()
                    .setOrientation(UltraViewPager.Orientation.HORIZONTAL)
                    .setFocusColor(ContextCompat.getColor(mContext, R.color.orange300))
                    .setNormalColor(ContextCompat.getColor(mContext, R.color.grey200))
                    .setRadius(DisplayUtil.dip2px(mContext, 4f)).setMargin(0, 0, 0, DisplayUtil.dip2px(mContext, 20))
                    .setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM)
                    .build();
        } else {
            mViewpager.disableIndicator();
            mViewpager.setInfiniteLoop(false);
        }

    }

    public void setCanLoadMore(Boolean canLoadMore) {
        getRefreshView().setEnableLoadMore(canLoadMore);
    }

    @Override
    public SmartRefreshLayout getRefreshView() {
        return mView.findViewById(R.id.refreshLayout);
    }

    @Override
    public void onRefresh() {
        setFilter(1, "");
        getP().getBanner();
    }

    @Override
    public void onLoad() {
        getP().getIntegralList(false, mSort, "");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getEventType() == Config.EVENT_BUS_REPLACE_INTEGRAL_SUCCESS) {
            //积分去刷新
            setFilter(1, "");
        }
    }

}
