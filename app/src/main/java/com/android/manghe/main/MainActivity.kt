package com.android.manghe.main

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.base.frame.activity.FragmentActivity
import com.android.base.okhttp.okUtil.OKHttpUtil
import com.android.base.okhttp.okUtil.base.NetBean
import com.android.base.tools.*
import com.android.base.tools.Glide.GlideHelper
import com.android.manghe.R
import com.android.manghe.box.fragment.BoxFragment
import com.android.manghe.box.fragment.OpenBoxFragment
import com.android.manghe.box.model.MessageEvent
import com.android.manghe.cache.ConfigHolder
import com.android.manghe.cache.StatusHolder
import com.android.manghe.cache.UserHolder
import com.android.manghe.common.MyApplication
import com.android.manghe.common.model.*
import com.android.manghe.config.Config
import com.android.manghe.config.ConstantsUrl
import com.android.manghe.config.SPTag
import com.android.manghe.config.events.*
import com.android.manghe.index.fragment.IndexFragment
import com.android.manghe.integralmall.fragment.IntegralMallFragment
import com.android.manghe.main.model.RegCouponAmount
import com.android.manghe.main.view.TabEntity
import com.android.manghe.market.fragment.MarketFragment
import com.android.manghe.mine.fragment.MineFragment
import com.android.manghe.mine.model.UserDetailRes
import com.android.manghe.service.DownloadService
import com.android.manghe.toutiao.TouTiaoUtils
import com.android.manghe.user.activity.LoginActivity
import com.android.manghe.utils.DeviceIdUtil
import com.android.manghe.utils.VoicePlayer
import com.android.manghe.view.luckdraw.LuckPanLayout
import com.android.manghe.zhuli.activity.WebTextActivity
import com.eightbitlab.rxbus.Bus
import com.eightbitlab.rxbus.registerInBus
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import com.tencent.bugly.Bugly
import com.tencent.bugly.beta.Beta
import com.zhangke.websocket.WebSocketService
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_first_tip.*
import okhttp3.Call
import org.greenrobot.eventbus.EventBus
import java.util.*
import kotlin.collections.set

class MainActivity : FragmentActivity() {
    private val mTabEntities = ArrayList<CustomTabEntity>()
    private val mTitles = arrayOf("首页", "盒柜", "", "商城", "我的")

    // private val mTitles = arrayOf("首页", "开盒", "分类", "盒柜", "我的")
    private val mIconUnselectIds = intArrayOf(
        R.mipmap.home_normal,
        R.mipmap.box_normal,
        R.mipmap.tran,
        R.mipmap.shop_normal,
        R.mipmap.me_normal
    )
    private val mIconSelectIds = intArrayOf(
        R.mipmap.home_click,
        R.mipmap.box_click,
        R.mipmap.tran,
        R.mipmap.shop_click,
        R.mipmap.me_click
    )

    private var mIndexFragment: IndexFragment? = null
    private var mBoxFragment: BoxFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyApplication.initUemng(application)
        //MyApplication.initMeiQia(application)
        MyApplication.initOpenInstall(applicationInfo, application)


        SharedPreferencesUtil.setIntPreferences(
            this@MainActivity,
            SPTag.TAG_IsSecondTokenInvalidate,
            0
        )
        Beta.upgradeDialogLayoutId = R.layout.dialog_update_bugly
        Bugly.init(applicationContext, ConstantsUrl.Bugly_APPID, false)

        ivMsgDot.setPadding(
            (ScreenUtil.getScreenWidth(this) / 5 * 1.5 + DisplayUtil.dip2px(
                this,
                4f
            )).toInt(), DisplayUtil.dip2px(this, 12f), 0, 0
        )
    }


    private fun initTouTiao() {
        if (!SPUtil.contains(this, SPTag.TAG_TouTiao)) {
            TouTiaoUtils.touTiaoCallBack(this, 0)
        }

        if (!SPUtil.contains(this, SPTag.TAG_TentCent)) {
            TouTiaoUtils.TentCentCallBack(this)

        }
        if (!SPUtil.contains(this, SPTag.TAG_TuiA)) {
            TouTiaoUtils.TuiACallBack(this)

        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let {
            if (it.getBooleanExtra("isToChangeTab", false)) {
                val position = it.getIntExtra("position", 0)
                showFragment(position)
                tabLayout.currentTab = position
                EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, position))
                return
            }

            if (it.extras != null && TextUtils.isEmpty(it.getStringExtra("MySetting"))) {
                showFragment(4)
                EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 4))
                tabLayout.currentTab = 4
                return
            }
//            if (it.data != null && it.data.getQueryParameter("payCode") != null) {
//                EventBus.getDefault().post(
//                    MessageEvent(
//                        Config.EVENT_BUS_SAD_PAY_RESULT_ALPAY,
//                        it.data.getQueryParameter("payCode")
//                    )
//                )
//                return
//            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (UserHolder.getUserInfo(this) == null && tabLayout.currentTab == 4) {
            showFragment(0)
            EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 0))
            tabLayout.currentTab = 0
        }
    }


    private var canShowLogin = true
    override fun initData(savedInstanceState: Bundle?) {
        StatusHolder.mCanUpdateApp = true
        initView()
//        Bus.observe<ShowTabMsgRedDotEvent>().subscribe {
//            ivMsgDot.visibility = if (it.isShow) View.VISIBLE else View.GONE
//        }.registerInBus(this)
        Bus.observe<ReloginEvent>().subscribe {
            val isSecondTokenInvalidate = SharedPreferencesUtil.getIntPreferences(
                this,
                SPTag.TAG_IsSecondTokenInvalidate,
                0
            ) + 1
            SharedPreferencesUtil.setIntPreferences(
                this,
                SPTag.TAG_IsSecondTokenInvalidate,
                isSecondTokenInvalidate
            )
            println(
                "xxxxx---${
                    SharedPreferencesUtil.getIntPreferences(
                        this,
                        SPTag.TAG_IsSecondTokenInvalidate,
                        0
                    )
                }    $isSecondTokenInvalidate"
            )
            if (isSecondTokenInvalidate < 10) {
                //token还不是第二次失效
                checkTokenValidate()
            } else {
                //token第二次失效,清理数据退出登录
                if (canShowLogin) {
                    canShowLogin = false
                    startActivity(Intent(this, MainActivity::class.java))
                    LoginActivity.showActivity(this)
                }
                Handler().postDelayed({
                    SharedPreferencesUtil.setIntPreferences(
                        this,
                        SPTag.TAG_IsSecondTokenInvalidate,
                        0
                    )
                    canShowLogin = true
                }, 1000)
            }
        }.registerInBus(this)
        Bus.observe<ShowLuckDrawDialog>().subscribe {
            showFragment(0)
            tabLayout.currentTab = 0
            EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 0))
            SPUtil.remove(this, SPTag.TAG_MainPageDirty)
            showLuckDrawDialog(false)
        }.registerInBus(this)

        Bus.observe<ShowIndexEvent>().subscribe {
            showFragment(0)
            tabLayout.currentTab = 0
            EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 0))
        }.registerInBus(this)
        Bus.observe<ShowBoxEvent>().subscribe {
            showFragment(1)
            tabLayout.currentTab = 1
            EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 1))
        }.registerInBus(this)
//        Bus.observe<ShowBigMarketEvent>().subscribe {
//            showFragment(1)
//            tabLayout.currentTab = 1
//            EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 1))
//            mMarketFragment?.setWillShowTitle(it.goodscatid)
//        }.registerInBus(this)
        Bus.observe<ShowMineEvent>().subscribe {
            showFragment(4)
            EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 4))
            tabLayout.currentTab = 4
        }.registerInBus(this)
        Bus.observe<ShowGuideEvent>().subscribe {
            mTypeface = Typeface.createFromAsset(assets, "zcool_happy.ttf")
            mScreenWidth = ScreenUtil.getScreenWidth(this)
            VoicePlayer.getInstance().init(application)
            showGuide(1)
        }.registerInBus(this)
        initTouTiao()
//        PermissionUtil.requestPermissions(
//            this, OnPermissionResultListener() {
//                initTouTiao()
//            },
//            Permission.READ_PHONE_STATE
//        )

        // loadConfig()
    }

    override fun onDestroy() {
        super.onDestroy()
        Bus.unregister(this)
        stopService(Intent(this, WebSocketService::class.java))
        DownloadService.stop(this)
        VoicePlayer.getInstance().stopPlay()
    }

    private fun initView() {
        //content page
        addFragment(
            R.id.container,
            IndexFragment::class.java,
            BoxFragment::class.java,
            OpenBoxFragment::class.java,
            IntegralMallFragment::class.java,
            MineFragment::class.java
        )
        mIndexFragment = getFragment(0) as IndexFragment
        mBoxFragment = getFragment(1) as BoxFragment
        //tab
        for (i in mTitles.indices) {
            mTabEntities.add(TabEntity(mTitles[i], mIconSelectIds[i], mIconUnselectIds[i]))
        }
        tabLayout.setTabData(mTabEntities)
        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabReselect(position: Int) {
            }

            override fun onTabSelect(position: Int) {
                if (position == 0) {
                    tabLayout.currentTab = 0
                    showFragment(0)
                    EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, position))
                    return
                }
                if (UserHolder.getUserInfo(this@MainActivity) == null) {
                    if (position == 1 || position == 4) {
                        tabLayout.currentTab = 0
                        showFragment(0)
                        EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, 0))
                        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                        return
                    }
                }
                if (position == 2) {
                    EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_OPEN_MUSIC, position))
                } else {
                    EventBus.getDefault().post(MessageEvent(Config.EVENT_BUS_CLOSE_MUSIC, position))
                }
                showFragment(position)

            }
        })
        showFragment(0)
        showFragment(2);
        tabLayout.currentTab = 2
    }


    private var mTypeface: Typeface? = null
    private var mScreenWidth = 0
    private fun showLuckDrawDialog(isFirstLogin: Boolean) {
        val isAppDirty = SPUtil.contains(this, SPTag.TAG_MainPageDirty)
        if (isFirstLogin) {
            //没有打开过APP，显示弹出框
            showLoadingDialog()
            val headMap = HashMap<String, String>()
            headMap["PLATFORM"] = "android"
            headMap["DEVICEID"] = DeviceIdUtil.getUniqueId(this@MainActivity)
            val data = HashMap<String, String>()
            val userInfo = UserHolder.getUserInfo(applicationContext)
            if (userInfo != null) {
                data["mid"] = userInfo.UID
            }
            OKHttpUtil(this@MainActivity).urlByHeadData(
                ConstantsUrl.domain + ConstantsUrl.REG_COUPON_AMOUNT,
                data, headMap
            )
                .get()
                .execute(object : BaseBack<RegCouponAmount>() {

                    override fun onSuccess(call: Call, res: RegCouponAmount?) {
                        if (res != null && res.code == 0 && res.data != null) {
                            val names = arrayOfNulls<String>(res.data.list.size)
                            val namesBottom = arrayOfNulls<String>(res.data.list.size)
                            val panImages = arrayOfNulls<Int>(res.data.list.size)
                            var zhongJiangIndex = 0
                            for ((index, item) in res.data.list.withIndex()) {
                                names[index] =
                                    if (TextUtils.equals("0", item.number)) "谢谢参与" else item.number
                                namesBottom[index] =
                                    if (TextUtils.equals("0", item.number)) "" else "抵用金"
                                panImages[index] = if (TextUtils.equals(
                                        "0",
                                        item.number
                                    )
                                ) R.mipmap.luck_draw_cry else R.mipmap.luck_draw_redpackage
                                if (item.number == res.data.gift_amount) {
                                    zhongJiangIndex = index
                                }
                            }
                            rotatePan.setStr(*names)
                            rotatePan.setStrBottom(*namesBottom)
                            rotatePan.setImages(*panImages)
                            tvGotMoney.text = res.data.gift_amount
                            dialogFirstTip.visibility = View.VISIBLE
                            layoutLuckDraw.visibility = View.VISIBLE
                            layoutRedPackage.visibility = View.GONE
                            dialogFirstTip.setOnClickListener { }
                            go.setOnClickListener {
                                luckPanLayout.rotate(zhongJiangIndex, 100)
                            }
                            ivHelp.setOnClickListener {
                                WebTextActivity.showActivity(
                                    this@MainActivity,
                                    "大转盘说明",
                                    ConstantsUrl.domain + ConstantsUrl.BigLuckDrawHelp
                                )
                            }
                            luckPanLayout.animationEndListener =
                                LuckPanLayout.AnimationEndListener {
                                    Handler().postDelayed({
                                        if (res.data.gift_amount.toDouble() != 0.00) {
                                            ivHelp.visibility = View.GONE
                                            layoutLuckDraw.visibility = View.GONE
                                            layoutRedPackage.visibility = View.VISIBLE
                                        } else {
                                            dialogFirstTip.visibility = View.GONE
                                            ToastUtil.showLong(this@MainActivity, "谢谢参与！")
                                        }
                                    }, 400)
                                }

                            btnCheckNow.setOnClickListener {
                                SPUtil.put(this@MainActivity, SPTag.TAG_MainPageDirty, true)
                                dialogFirstTip.visibility = View.GONE
                                if (isFirstLogin) {
                                    //首次已登录的情况，直接领取
                                    Bus.send(ShowMineEvent())
//                                    platePresent(res.data.gift_amount.toInt())
                                } else {
                                    //app首次打开的情况，跳转到登录界面
                                    LoginActivity.showActivity(
                                        this@MainActivity,
                                        res.data.gift_amount
                                    )
                                }
                            }
                        }

                    }

                    override fun onFailure(e: Exception) {
                        super.onFailure(e)
                    }

                    override fun onComplete() {
                        hideLoadingDialog()
                    }
                })
        }
    }

    private fun showGuide(page: Int) {
        layoutGuide.setOnClickListener { }
        layoutGuide.visibility = View.VISIBLE
        layoutGuide.removeAllViews()
        when (page) {
            1 -> {
                VoicePlayer.getInstance().startPlay(R.raw.guide_1)
                val guideView1 = LayoutInflater.from(this).inflate(R.layout.layout_guide_1, null)
                layoutGuide.addView(guideView1)
                guideView1.findViewById<View>(R.id.guide1Jump).setOnClickListener {
                    layoutGuide.visibility = View.GONE
                    VoicePlayer.getInstance().stopPlay()
                    showLuckDrawDialog(false)
                }
                guideView1.findViewById<View>(R.id.ivGuide1ToStudy).setOnClickListener {
                    showGuide(2)
                }
                guideView1.findViewById<TextView>(R.id.guide1Jump).typeface = mTypeface
                guideView1.findViewById<TextView>(R.id.tvTip1).typeface = mTypeface
            }
            2 -> {
                VoicePlayer.getInstance().startPlay(R.raw.guide_2)
                val guideView2 = LayoutInflater.from(this).inflate(R.layout.layout_guide_2, null)
                layoutGuide.addView(guideView2)
                guideView2.findViewById<View>(R.id.guide2Jump).setOnClickListener {
                    layoutGuide.visibility = View.GONE
                    VoicePlayer.getInstance().stopPlay()
                    showLuckDrawDialog(false)
                }
                guideView2.findViewById<View>(R.id.ivCam).setOnClickListener {
                    showGuide(3)
                }
                guideView2.findViewById<TextView>(R.id.guide2Jump).typeface = mTypeface
                guideView2.findViewById<TextView>(R.id.tvTip2).typeface = mTypeface

                //tabbar
                val tabHeight = mScreenWidth * 164 / 1080
                val lp1 = RelativeLayout.LayoutParams(mScreenWidth, tabHeight)
                lp1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                guideView2.findViewById<View>(R.id.ivGuide2Pic3).layoutParams = lp1
                //底部图片
                val pic2Height = mScreenWidth * 2754 / 1080
                val lp2 = RelativeLayout.LayoutParams(mScreenWidth, pic2Height)
                lp2.addRule(RelativeLayout.ABOVE, R.id.ivGuide2Pic3)
                lp2.topMargin = -(pic2Height - tabHeight)
                guideView2.findViewById<View>(R.id.ivGuide2Pic2).layoutParams = lp2
                //商品图
                val lp3 = RelativeLayout.LayoutParams(mScreenWidth / 3 + 5, mScreenWidth / 3 + 10)
                lp3.addRule(RelativeLayout.ABOVE, R.id.ivGuide2Pic3)
                lp3.bottomMargin = DisplayUtil.dip2px(this, 20f)
                guideView2.findViewById<View>(R.id.ivCam).layoutParams = lp3
            }
            3 -> {
                VoicePlayer.getInstance().startPlay(R.raw.guide_3)
                val guideView3 = LayoutInflater.from(this).inflate(R.layout.layout_guide_3, null)
                layoutGuide.addView(guideView3)
                guideView3.findViewById<View>(R.id.guide3Jump).setOnClickListener {
                    layoutGuide.visibility = View.GONE
                    VoicePlayer.getInstance().stopPlay()
                    showLuckDrawDialog(false)
                }

                guideView3.findViewById<View>(R.id.ivGuide3Pic3).setOnClickListener {
                    showGuide(4)
                }
                guideView3.findViewById<View>(R.id.zheZhao3).setOnClickListener {}
                guideView3.findViewById<TextView>(R.id.guide3Jump).typeface = mTypeface
                guideView3.findViewById<TextView>(R.id.tvTip3).typeface = mTypeface

                //tabbar
                val tabHeight = mScreenWidth * 137 / 1080
                val lp1 = RelativeLayout.LayoutParams(mScreenWidth, tabHeight)
                lp1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                guideView3.findViewById<View>(R.id.ivGuid3Pic2).layoutParams = lp1

                val height1 = mScreenWidth * 979 / 1080
                val height2 = mScreenWidth * 180 / 1080
                val height3 = mScreenWidth * 230 / 1080
                val lp11 = LinearLayout.LayoutParams(mScreenWidth, height1)
                guideView3.findViewById<View>(R.id.ivGuid3Pic1).layoutParams = lp11
                val lp22 = LinearLayout.LayoutParams(mScreenWidth, height2)
                guideView3.findViewById<View>(R.id.ivGuid3Pic22).layoutParams = lp22
                val lp33 = LinearLayout.LayoutParams(mScreenWidth, height3)
                guideView3.findViewById<View>(R.id.ivGuid3Pic3).layoutParams = lp33
                val lp44 = LinearLayout.LayoutParams(mScreenWidth, mScreenWidth * 152 / 1080)
                guideView3.findViewById<View>(R.id.ivGuid3Pic4).layoutParams = lp44
                val lp55 = LinearLayout.LayoutParams(mScreenWidth, mScreenWidth * 1526 / 1080)
                guideView3.findViewById<View>(R.id.ivGuid3Pic5).layoutParams = lp55

                val lp3 = RelativeLayout.LayoutParams(mScreenWidth / 3, tabHeight)
                lp3.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                lp3.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                guideView3.findViewById<View>(R.id.ivGuide3Pic3).layoutParams = lp3

            }
            4 -> {
                VoicePlayer.getInstance().startPlay(R.raw.guide_4)
                val guideView4 = LayoutInflater.from(this).inflate(R.layout.layout_guide_4, null)
                layoutGuide.addView(guideView4)
                guideView4.findViewById<View>(R.id.guide4Jump).setOnClickListener {
                    layoutGuide.visibility = View.GONE
                    VoicePlayer.getInstance().stopPlay()
                    showLuckDrawDialog(false)
                }
                guideView4.findViewById<View>(R.id.zheZhao4).setOnClickListener {}
                GlideHelper.load(
                    this,
                    R.mipmap.count_down,
                    guideView4.findViewById<ImageView>(R.id.ivCountDown)
                )
                Handler().postDelayed({
                    if (layoutGuide.visibility == View.VISIBLE)
                        showGuide(5)
                }, 10000)
                guideView4.findViewById<TextView>(R.id.guide4Jump).typeface = mTypeface
                guideView4.findViewById<TextView>(R.id.tvTip4).typeface = mTypeface

                //tabbar
                val tabHeight = mScreenWidth * 137 / 1080
                val lp0 = RelativeLayout.LayoutParams(mScreenWidth, tabHeight)
                lp0.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                guideView4.findViewById<View>(R.id.ivGuid4Pic0).layoutParams = lp0


                val height1 = mScreenWidth * 979 / 1080
                val height2 = mScreenWidth * 180 / 1080
                val height3 = mScreenWidth * 230 / 1080
                val lp1 = LinearLayout.LayoutParams(mScreenWidth, height1)
                guideView4.findViewById<View>(R.id.ivGuid4Pic1).layoutParams = lp1
                val lp11 = RelativeLayout.LayoutParams(mScreenWidth, height1)
                guideView4.findViewById<View>(R.id.guide4TempView).layoutParams = lp11
                val lp2 = LinearLayout.LayoutParams(mScreenWidth, height2)
                guideView4.findViewById<View>(R.id.ivGuid4Pic2).layoutParams = lp2
                val lp3 = LinearLayout.LayoutParams(mScreenWidth, height3)
                guideView4.findViewById<View>(R.id.ivGuid4Pic3).layoutParams = lp3
                val lp4 = LinearLayout.LayoutParams(mScreenWidth, mScreenWidth * 152 / 1080)
                guideView4.findViewById<View>(R.id.ivGuid4Pic4).layoutParams = lp4
                val lp5 = LinearLayout.LayoutParams(mScreenWidth, mScreenWidth * 1526 / 1080)
                guideView4.findViewById<View>(R.id.ivGuid4Pic5).layoutParams = lp5

                val lp6 = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
                lp6.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                lp6.topMargin = height1
                guideView4.findViewById<View>(R.id.layoutCountDown).layoutParams = lp6

                val lp7 = RelativeLayout.LayoutParams(
                    mScreenWidth, mScreenWidth * 165 / 1080
                )
                lp7.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
                lp7.topMargin = height1 + height2 + height3
                guideView4.findViewById<View>(R.id.ivHeng).layoutParams = lp7

                val lp8 = RelativeLayout.LayoutParams(
                    mScreenWidth, RelativeLayout.LayoutParams.WRAP_CONTENT
                )
                lp8.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.guide4TempView)
                guideView4.findViewById<View>(R.id.guide4TopLayout).layoutParams = lp8

            }
            5 -> {
                VoicePlayer.getInstance().startPlay(R.raw.guide_5)
                val guideView5 = LayoutInflater.from(this).inflate(R.layout.layout_guide_5, null)
                layoutGuide.addView(guideView5)
                guideView5.findViewById<View>(R.id.guide5Jump).setOnClickListener {
                    layoutGuide.visibility = View.GONE
                    VoicePlayer.getInstance().stopPlay()
                    showLuckDrawDialog(false)
                }
                guideView5.findViewById<View>(R.id.ivStart).setOnClickListener {
                    layoutGuide.visibility = View.GONE
                    VoicePlayer.getInstance().stopPlay()
                    showLuckDrawDialog(false)
                }
                guideView5.findViewById<TextView>(R.id.guide5Jump).typeface = mTypeface
                guideView5.findViewById<TextView>(R.id.tvTip5).typeface = mTypeface

                //tabbar
                val tabHeight = mScreenWidth * 137 / 1080
                val lp1 = RelativeLayout.LayoutParams(mScreenWidth, tabHeight)
                lp1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                guideView5.findViewById<View>(R.id.ivGuid5Pic2).layoutParams = lp1

                val height1 = mScreenWidth * 979 / 1080
                val height2 = mScreenWidth * 180 / 1080
                val height3 = mScreenWidth * 230 / 1080
                val lp11 = LinearLayout.LayoutParams(mScreenWidth, height1)
                guideView5.findViewById<View>(R.id.ivGuid5Pic1).layoutParams = lp11
                val lp22 = LinearLayout.LayoutParams(mScreenWidth, height2)
                guideView5.findViewById<View>(R.id.ivGuid5Pic22).layoutParams = lp22
                val lp33 = LinearLayout.LayoutParams(mScreenWidth, height3)
                guideView5.findViewById<View>(R.id.ivGuid5Pic3).layoutParams = lp33
                val lp44 = LinearLayout.LayoutParams(mScreenWidth, mScreenWidth * 152 / 1080)
                guideView5.findViewById<View>(R.id.ivGuid5Pic4).layoutParams = lp44
                val lp55 = LinearLayout.LayoutParams(mScreenWidth, mScreenWidth * 1526 / 1080)
                guideView5.findViewById<View>(R.id.ivGuid5Pic5).layoutParams = lp55
            }
        }
    }

    override fun onBackPressed() {
        when (dialogFirstTip.visibility) {
            View.VISIBLE -> {
                return
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

    private fun loadConfig() {
        OKHttpUtil(this).url(ConstantsUrl.domain + ConstantsUrl.ACUTIONCONFIG)
            .get()
            .execute(object : BaseBack<AuctionConfig>() {

                override fun onSuccess(call: Call, res: AuctionConfig) {
                    if (res.code == 0 && res.data != null) {
                        ConfigHolder.setConfigInfo(this@MainActivity, res.data)

                        if (TextUtils.equals(
                                "0",
                                res.data.switch_boot_animation
                            ) && !SPUtil.contains(
                                this@MainActivity,
                                SPTag.TAG_MainShowGuideDirty
                            )
                        ) {
                            SPUtil.put(this@MainActivity, SPTag.TAG_MainShowGuideDirty, true)
                            mTypeface = Typeface.createFromAsset(assets, "zcool_happy.ttf")
                            mScreenWidth = ScreenUtil.getScreenWidth(this@MainActivity)
                            VoicePlayer.getInstance().init(application)
                            Handler().postDelayed({
                                showGuide(1)
                            }, 2000)
                            return
                        }
                        showLuckDrawDialog(false)
                    }
                }

                override fun onFailure(e: Exception) {
                    super.onFailure(e)
                    showLuckDrawDialog(false)
                }

                override fun onComplete() {
                }
            })
    }


    private fun checkTokenValidate() {
        val data = HashMap<String, String>()
        data["UID"] = UserHolder.getUID(this)
        data["TOKEN"] = UserHolder.getUserInfo(this).TOKEN
        OKHttpUtil(this).urlByHeadData(
            ConstantsUrl.domain + ConstantsUrl.MEMBERORDINGDANGNUM,
            data
        )
            .get()
            .execute(object : BaseBack<UserDetailRes?>() {
                fun onSuccess(call: Call, userDetailRes: UserDetailRes) {
                    SPUtil.put(this@MainActivity, SPTag.TAG_IsSecondTokenInvalidate, 0)
                }

                override fun onFailure(e: java.lang.Exception) {
                    super.onFailure(e)
                }

                override fun onComplete() {}
            })
    }

    private fun platePresent(giftAmount: Int) {
        val header = HashMap<String, String>()
        header["UID"] = UserHolder.getUID(this)
        header["TOKEN"] = UserHolder.getUserInfo(this).TOKEN
        val data = HashMap<String, String>()
        data["gift_amount"] = "$giftAmount"
        OKHttpUtil(this).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.PlaceGift, header)
            .post(data).build()
            .execute(object : BaseBack<NetBean<*>?>() {
                override fun onSuccess(call: Call?, json: String) {
                    super.onSuccess(call, json)
                    if (json.contains("\"code\":0")) {
                        Bus.send(RefreshMemberInfoEvent())
                        ToastUtil.showLong(this@MainActivity, "领取奖励成功")
                    }
                }
            })
    }

//    fun getMsgCount() {
//        if (UserHolder.getUserInfo(this) == null)
//            return
//        val data = HashMap<String, String>()
//        data["UID"] = UserHolder.getUID(this)
//        data["TOKEN"] = UserHolder.getUserInfo(this).TOKEN
//        OKHttpUtil(this).urlByHeadData(
//            ConstantsUrl.domain + ConstantsUrl.MEMBERMESSAGENUM,
//            data
//        )
//            .get()
//            .execute(object : BaseBack<MsgCountRes?>() {
//                override fun onSuccess(call: Call?, msgCountRes: MsgCountRes?) {
//                    if (msgCountRes?.code == 0 && msgCountRes.data != null) {
//                        Bus.send(ShowTabMsgRedDotEvent(msgCountRes.data.message_unread_num + msgCountRes.data.notice_unread_num > 0))
//                    } else {
//                        Bus.send(ShowTabMsgRedDotEvent(false))
//                    }
//                }
//
//                override fun onFailure(e: java.lang.Exception) {
//                    super.onFailure(e)
//                }
//
//                override fun onComplete() {}
//            })
//    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EventBus.getDefault()
            .post(MessageEvent(Config.EVENT_BUS_SAD_PAY_RESULT_UPAY, requestCode, resultCode, data))
    }

}
