package com.android.manghe.main.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class RegCouponAmount extends NetBean<RegCouponAmount> {


    /**
     * list : [{"title":"一","number":"5000"},{"title":"二","number":"1000"},{"title":"三","number":"500"},{"title":"四","number":"100"},{"title":"五","number":"50"},{"title":"六","number":"10"},{"title":"七","number":"5"},{"title":"八","number":"0"}]
     * gift_amount : 5000
     */

    public String gift_amount;
    public List<ListBean> list;

    public static class ListBean {
        /**
         * title : 一
         * number : 5000
         */

        public String title;
        public String number;
    }
}
