package com.android.manghe.main.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class FreeCoinRes extends NetBean<FreeCoinRes.DataBean> {

    public static  class DataBean{
        public int free_coin;
    }
}
