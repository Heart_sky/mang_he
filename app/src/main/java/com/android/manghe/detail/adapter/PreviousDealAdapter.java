package com.android.manghe.detail.adapter;

import com.android.base.tools.BigDecimalUtils;
import com.android.base.tools.DateTimeUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.detail.model.PreviousDealRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class PreviousDealAdapter extends BaseQuickAdapter<PreviousDealRes.ListBean, BaseViewHolder> {
    public PreviousDealAdapter(List<PreviousDealRes.ListBean> list) {
        super(R.layout.item_previous_deal,list);
    }

    @Override
    protected void convert(BaseViewHolder holder, PreviousDealRes.ListBean item) {
        GlideHelper.loadCircle(mContext,item.photo,holder.getView(R.id.ivAvatar));
        holder.setText(R.id.tvName, item.safe_end != 0 ? "检测到作弊行为": item.username);
        holder.setText(R.id.tvTime, DateTimeUtil.longToString(Long.parseLong(item.c_time),"yyyy-MM-dd HH:mm:ss"));
        holder.setText(R.id.tvMarketPrice, "￥"+item.market_price);
        holder.setText(R.id.tvPrice, item.safe_end != 0 ? "参与费用已退":"￥"+item.price);
        double percent = BigDecimalUtils.round((Double.parseDouble(item.market_price) - Double.parseDouble(item.price)) / Double.parseDouble(item.market_price) * 100.00, 2);
        holder.setText(R.id.tvSave, percent >= 100 ? "99.99" : percent + "");

        holder.setGone(R.id.tvSave, item.safe_end == 0);
        holder.setGone(R.id.tvTime, item.safe_end == 0);
        holder.setGone(R.id.tvCheckDetail, item.safe_end != 0);

    }
}