package com.android.manghe.detail.activity

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.base.frame.extend.IRefresh
import com.android.base.frame.view.MVPActivity
import com.android.base.view.recycler.RecyclerViewDivider
import com.android.manghe.R
import com.android.manghe.detail.adapter.PayHistoryAdapter
import com.android.manghe.detail.model.PeopleGotRes
import com.android.manghe.detail.precenter.PPayHistory
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import kotlinx.android.synthetic.main.activity_pay_history.*

/**
 * 出价记录
 * @author ZhangWeiJun
 * @date 2019/6/8
 */

class PayHistoryActivity  :MVPActivity<PPayHistory>(),IRefresh{

    private var mAdapter : PayHistoryAdapter? = null

    override fun getRefreshView(): SmartRefreshLayout {
        return findViewById(R.id.refreshLayout)
    }

    override fun onRefresh() {
        p.loadData(true)
    }

    override fun onLoad() {
        p.loadData(false)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_pay_history
    }
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("出价记录").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)

        mAdapter = PayHistoryAdapter(arrayListOf())
        recyclerView.adapter = mAdapter
        recyclerView.layoutManager =
            LinearLayoutManager(this)
        recyclerView.addItemDecoration(RecyclerViewDivider(mContext, LinearLayoutManager.HORIZONTAL,
            1, ContextCompat.getColor(mContext, R.color.colorDivider)))

        p.setOrderSN(intent.getStringExtra("order_sn"))
        p.setDeal(intent.getBooleanExtra("isDeal",false))
        refreshLayout.autoRefresh()
    }

    fun update(dataList: MutableList<PeopleGotRes.ListBean>) {
        mAdapter?.let {
            if (dataList != null && dataList.isNotEmpty()) {
                dataList[0].isTop = true
                dataList.add(PeopleGotRes.ListBean(true))
            }
            it.setNewData(dataList)
        }
    }
}