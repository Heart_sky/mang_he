package com.android.manghe.detail.activity

import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebView
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.detail.precenter.PGoodDetailPic
import kotlinx.android.synthetic.main.activity_good_detail_pic.*

class GoodDetailPicActivity :MVPActivity<PGoodDetailPic>() {
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("商品详情").setLeftIcon(R.mipmap.icon_back_black).setTitleAndStatusBgColor(R.color.white)
        p.loadData(intent.getStringExtra("auctionId"))
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_good_detail_pic
    }

    fun showWeb(htmlString: String){
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                if (newProgress == 100) {
                    hideLoadingDialog()
                }
            }
        }
        //宽度适应屏幕
        val js = "<style>\n" +
                "img{\n" +
                " max-width:100%;\n" +
                "height:auto\n" +
                "}\n" +
                "</style>" + htmlString

        webView.loadDataWithBaseURL(null, js, "text/html", "utf-8", null)
    }
}