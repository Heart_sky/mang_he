package com.android.manghe.detail.adapter;

import androidx.core.content.ContextCompat;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.detail.model.PeopleGotRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class PayHistoryAdapter extends BaseQuickAdapter<PeopleGotRes.ListBean, BaseViewHolder> {
    public PayHistoryAdapter(List<PeopleGotRes.ListBean> list) {
        super(R.layout.item_pay_history, list);
    }

    @Override
    protected void convert(BaseViewHolder holder, PeopleGotRes.ListBean item) {
        holder.setGone(R.id.tvOnly100, item.isFoot);
        holder.setGone(R.id.layoutContent, !item.isFoot);
        GlideHelper.loadCircle(mContext, item.photo, holder.getView(R.id.ivAvatar));
        holder.setText(R.id.tvName, item.username);
        holder.setText(R.id.tvRegion, item.city);
        holder.setText(R.id.tvPrice, item.price);
        holder.setText(R.id.tvTag, item.isTop ? "领先" : "出局");
        holder.setTextColor(R.id.tvTag, ContextCompat.getColor(mContext, item.isTop ? R.color.colorPrimary : R.color.colorFont66));
        holder.setVisible(R.id.ivTag, item.isTop);
    }
}