package com.android.manghe.detail.adapter;

import android.view.View;
import com.android.base.tools.DateTimeUtil;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.detail.model.ShareOrderRes;
import com.android.manghe.view.preview.PreviewViewPagerActivity;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class ShareOrderAdapter extends BaseQuickAdapter<ShareOrderRes.ListBean, BaseViewHolder> {
    public ShareOrderAdapter(List<ShareOrderRes.ListBean> list) {
        super(R.layout.item_comment_list,list);
    }

    @Override
    protected void convert(BaseViewHolder holder, ShareOrderRes.ListBean item) {
        GlideHelper.loadCircle(mContext,item.photo,holder.getView(R.id.ivAvatar));
        holder.setText(R.id.tvName, item.username);
        holder.setText(R.id.tvTime, DateTimeUtil.longToString(Long.parseLong(item.c_time),"yyyy-MM-dd HH:mm:ss"));
        holder.setText(R.id.tvTitle, item.content);
        GlideHelper.loadCropRoundTrans(mContext,item.thumbs.size()>=1 ? item.thumbs.get(0):null,holder.getView(R.id.ivPic0),16);
        GlideHelper.loadCropRoundTrans(mContext,item.thumbs.size()>=2 ? item.thumbs.get(1):null,holder.getView(R.id.ivPic1),16);
        GlideHelper.loadCropRoundTrans(mContext,item.thumbs.size()>=3 ? item.thumbs.get(2):null,holder.getView(R.id.ivPic2),16);
        GlideHelper.loadCropRoundTrans(mContext,item.thumbs.size()>=4 ? item.thumbs.get(3):null,holder.getView(R.id.ivPic3),16);

        if(item.thumbs != null && item.thumbs.size() > 0) {
            holder.setGone(R.id.layoutPic, true);
            holder.setVisible(R.id.ivPic0, item.thumbs.size()>=1);
            holder.setVisible(R.id.ivPic1, item.thumbs.size()>=2);
            holder.setVisible(R.id.ivPic2, item.thumbs.size()>=3);
            holder.setVisible(R.id.ivPic3, item.thumbs.size()>=4);
        }else{
            holder.setGone(R.id.layoutPic, false);
            holder.setGone(R.id.ivPic0, false);
            holder.setGone(R.id.ivPic1, false);
            holder.setGone(R.id.ivPic2, false);
            holder.setGone(R.id.ivPic3, false);
        }
        holder.setOnClickListener(R.id.layoutPic, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreviewViewPagerActivity.showActivity(mContext, item.thumbs , 0);
            }
        });
    }
}