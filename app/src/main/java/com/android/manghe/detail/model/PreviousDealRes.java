package com.android.manghe.detail.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class PreviousDealRes extends NetBean<PreviousDealRes.DataBean> {
    public static  class DataBean {
        public List<ListBean> list;
    }

    public static class ListBean{
        public String id;
        public String title;
        public String c_time;
        public String thumb;
        public String mid;
        public String photo;
        public String pay_time;
        public String market_price;
        public String price;
        public String mid_coin_count;
        public String username;
        public String returns_score;
        public String mid_count;
        public String safe_price;
        public String safe_rate;
        public int safe_end;
        public String top_price;
        public String top_end;
    }
}
