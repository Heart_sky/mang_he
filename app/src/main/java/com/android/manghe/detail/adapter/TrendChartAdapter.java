package com.android.manghe.detail.adapter;

import androidx.core.content.ContextCompat;

import com.android.base.tools.DateTimeUtil;
import com.android.manghe.R;
import com.android.manghe.detail.model.AuctionHistoryRes;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

public class TrendChartAdapter extends BaseQuickAdapter<AuctionHistoryRes.ListBean, BaseViewHolder> {
    public TrendChartAdapter(List<AuctionHistoryRes.ListBean> list) {
        super(R.layout.item_frend_chart, list);
    }

    @Override
    protected void convert(BaseViewHolder holder, AuctionHistoryRes.ListBean item) {
        holder.setText(R.id.tvTime, DateTimeUtil.longToString(Long.parseLong(item.c_time), "yyyy-MM-dd\nHH:mm:ss"));
        holder.setText(R.id.tvName, item.nickname);
        holder.setText(R.id.tvPrice, "￥" + item.price);
        holder.setText(R.id.tvCha, item.diff);
        holder.setTextColor(R.id.tvChaIv, ContextCompat.getColor(mContext, Double.parseDouble(item.diff) < 0 ? R.color.red400 : R.color.green400));
        holder.setText(R.id.tvChaIv, Double.parseDouble(item.diff) < 0 ? "↓":"↑");
    }
}