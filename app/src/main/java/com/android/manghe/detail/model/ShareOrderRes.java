package com.android.manghe.detail.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.ArrayList;
import java.util.List;

public class ShareOrderRes extends NetBean<ShareOrderRes.DataBean>{

    public static class  DataBean {
        public List<ListBean> list;
    }
    public static class  ListBean{
        public String id;
        public ArrayList<String> thumbs;
        public String mid;
        public String content;
        public  String star;
        public String c_time;
        public String username;
        public String title;
        public String photo;
    }
}
