package com.android.manghe.detail.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class AuctionHistoryRes extends NetBean<String> {

    public int list_total;
    public AuctionBean auction;
    public List<ListBean> list;

    public  static  class  AuctionBean {
        public String title;
        public String thumb;
        public String avg_price;
        public String market_price;
        public String cost_price;
    }

    public static class  ListBean{
        public String id;
        public String order_sn;
        public String mid;
        public String username;
        public String c_time;
        public float price;
        public String mobile;
        public String nickname;
        public String c_time_format;
        public String photo;
        public String diff;
        public float period;
    }
}
