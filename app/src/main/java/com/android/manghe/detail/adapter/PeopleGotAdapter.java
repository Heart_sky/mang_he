package com.android.manghe.detail.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.base.tools.Glide.GlideHelper;
import com.android.manghe.R;
import com.android.manghe.detail.activity.QuestionActivity;
import com.android.manghe.detail.model.PeopleGotRes;

import java.util.List;

public class PeopleGotAdapter extends RecyclerView.Adapter<PeopleGotHolder> {

    private Context mContext;

    public PeopleGotAdapter(Context context, @Nullable List<PeopleGotRes.ListBean> data) {
        mContext = context;
        mDiffer = new AsyncListDiffer<>(this, diffCallback);
    }

    private AsyncListDiffer<PeopleGotRes.ListBean> mDiffer;

    private DiffUtil.ItemCallback<PeopleGotRes.ListBean> diffCallback = new DiffUtil.ItemCallback<PeopleGotRes.ListBean>() {
        @Override
        public boolean areItemsTheSame(PeopleGotRes.ListBean oldItem, PeopleGotRes.ListBean newItem) {
            return TextUtils.equals(oldItem.id, newItem.id);
        }

        @Override
        public boolean areContentsTheSame(PeopleGotRes.ListBean oldItem, PeopleGotRes.ListBean newItem) {
            return false;
        }
    };

    public PeopleGotRes.ListBean getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }


    public void update(List<PeopleGotRes.ListBean> list) {
        mDiffer.submitList(list);
    }

    @Override
    public PeopleGotHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PeopleGotHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_people_got, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleGotHolder helper, int position) {
        PeopleGotRes.ListBean item = getItem(position);
        GlideHelper.loadAvatar(mContext,item.photo, helper.ivAvatar);
        helper.tvName.setText(TextUtils.isEmpty(item.nickname) ? item.username : item.nickname);
        helper.tvRegion.setText(item.city);
        helper.tvPrice.setText("￥"+item.price);
        helper.layoutContainer.setBackgroundResource(item.isTop ? R.mipmap.winner_bg:R.mipmap.loser_bg);
        helper.tvTag.setText(item.isTop ? "领先":"出局");
        helper.tvTag.setBackgroundResource(item.isTop ? R.mipmap.tag_bg: R.mipmap.tag_bg_gray);



        helper.layoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, QuestionActivity.class);
                intent.putExtra("memberId",item.mid);
                intent.putExtra("name",TextUtils.isEmpty(item.nickname) ? item.username : item.nickname);
                intent.putExtra("region",item.city);
                intent.putExtra("price",item.price);
                intent.putExtra("avatar",item.photo);
                mContext.startActivity(intent);
            }
        });
    }
}

class PeopleGotHolder extends RecyclerView.ViewHolder {
    public ImageView ivAvatar;
    public TextView tvName,tvRegion, tvPrice,tvTag, tvTime;
    public RelativeLayout layoutContainer;

    public PeopleGotHolder(View itemView) {
        super(itemView);
        ivAvatar = itemView.findViewById(R.id.ivAvatar);
        tvName = itemView.findViewById(R.id.tvName);
        tvRegion = itemView.findViewById(R.id.tvRegion);
        tvPrice = itemView.findViewById(R.id.tvPrice);
        layoutContainer = itemView.findViewById(R.id.layoutContainer);
        tvTag = itemView.findViewById(R.id.tvTag);
    }
}