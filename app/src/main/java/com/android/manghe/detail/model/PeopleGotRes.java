package com.android.manghe.detail.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.ArrayList;

public class PeopleGotRes extends NetBean {
    public ArrayList<ListBean> list ;
    public static class ListBean{

        public ListBean(boolean isFoot) {
            this.isFoot = isFoot;
        }

        public boolean isTop;
        public String id;
        public String order_id;
        public String mid;
        public String type;
        public String price;
        public String c_time;
        public String username;
        public String mobile;
        public String nickname;
        public String photo;
        public String city;
        public boolean isFoot;
    }
}
