package com.android.manghe.detail.precenter;

import android.text.TextUtils;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.detail.activity.QuestionActivity;
import com.android.manghe.mine.model.UploadFileRes;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;

public class PQuestion extends XPresenter<QuestionActivity> {

    private String memberId = "";
    private ArrayList<String> mPicLocalPathList = new ArrayList<>();
    private ArrayList<String> mPicUrlList = new ArrayList<>();

    public ArrayList<String> getPicList() {
        return mPicLocalPathList;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public void submit(String content) {
        if (mPicLocalPathList.size() == 0) {
            //文字
            submitAllContent(content);
        } else {
            //文字+图片
            for (String path : mPicLocalPathList) {
                uploadFile(content, path);
            }
        }
    }

    private void uploadFile(String content, String path) {
        new OKHttpUtil(getV()).url(ConstantsUrl.domain + ConstantsUrl.MEMBER_UPLOAD)
                .uploadFile("upFile", path)
                .addHead("UID", UserHolder.getUID(getV()))
                .addHead("TOKEN", UserHolder.getUserInfo(getV()).TOKEN)
                .build()
                .execute(new BaseBack<UploadFileRes>() {
                    @Override
                    public void onSuccess(Call call, UploadFileRes res) {
                        super.onSuccess(call, res);
                        if (res.code == 0 && !TextUtils.isEmpty(res.data.toString())) {
                            mPicUrlList.add(res.data.toString());
                            if (mPicUrlList.size() == mPicLocalPathList.size()) {
                                submitAllContent(content);
                            }
                        } else {
                            mPicUrlList.clear();
                            getV().hideLoadingDialog();
                            getV().submitFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        mPicUrlList.clear();
                        getV().hideLoadingDialog();
                        getV().submitFail(null);
                    }
                });

    }

    private void submitAllContent(String content) {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("content", content);
        data.put("ans_mid", memberId);

        if (mPicUrlList.size() != 0) {
            data.put("thumb", mPicUrlList.get(0));
        }
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.Question,data,headMap)
                .post(data)
                .build()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean bean) {
                        if (bean != null) {
                            if (bean.code == 0) {
                                mPicLocalPathList.clear();
                                mPicUrlList.clear();
                                getV().submitOk(bean.msg);
                            } else {
                                getV().submitFail(bean.msg);
                            }
                        } else {
                            getV().submitFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().submitFail(null);
                    }

                    @Override
                    public void onComplete() {
                        mPicUrlList.clear();
                        getV().hideLoadingDialog();
                    }
                });
    }
}
