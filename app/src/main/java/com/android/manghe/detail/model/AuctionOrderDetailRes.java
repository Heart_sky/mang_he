package com.android.manghe.detail.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class AuctionOrderDetailRes extends NetBean<AuctionOrderDetailRes.DataBean> {

    public static  class  DataBean {
        public String id;
        public String order_sn;
        public String mid;
        public String username;
        public String auction_id;
        public String title;
        public String content;
        public String cost_price;
        public String market_price;
        public String cid;
        public String c_time;
        public String thumb;
        public String[] thumbs;
        public String is_virtual;
        public String period;
        public String start_price;
        public String per_price;
        public String brokerage;
        public String first_countdown;
        public String per_countdown;
        public String price;
        public String amount;
        public String returns_rate;
        public String send_time;
        public String comment_time;
        public String status;
        public String coin_type;
        public String for_new;
        public String join_count;
        public String coin_count;
        public String mid_coin_count;
        public String returns_score;
        public String mid_count;
        public String photo;
        public String safe_price;

    }
}
