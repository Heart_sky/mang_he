package com.android.manghe.detail.model;

import com.android.base.okhttp.okUtil.base.NetBean;

public class CheckFavoriteRes extends NetBean<CheckFavoriteRes.DataBean> {

    public static class DataBean {
        /**
         * is_favorite : 1
         */

        public int is_favorite;
    }
}
