package com.android.manghe.detail.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.android.base.frame.view.MVPActivity
import com.android.base.tools.Glide.GlideHelper
import com.android.base.tools.MediaUtil
import com.android.base.tools.ToastUtil
import com.android.base.tools.permission.OnPermissionResultListener
import com.android.base.tools.permission.PermissionUtil
import com.android.manghe.R
import com.android.manghe.detail.precenter.PQuestion
import com.android.manghe.view.crop.ImageCropHelper
import com.android.manghe.view.multiselectorview.MultiImageSelectorView
import com.isseiaoki.simplecropview.CropImageView
import com.yanzhenjie.permission.runtime.Permission
import kotlinx.android.synthetic.main.activity_question.*
import me.nereo.multi_image_selector.MultiImageSelector
import me.nereo.multi_image_selector.MultiImageSelectorActivity


class QuestionActivity : MVPActivity<PQuestion>(), OnPermissionResultListener, MultiImageSelectorView.OnItemClickListener {


    private val CODE_IMAGE_SELECT = 100
    private val CODE_IMAGE_CROP = 101

    override fun getLayoutId(): Int {
        return R.layout.activity_question
    }

    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar("提问").setTitleAndStatusBgColor(R.color.white).setLeftIcon(R.mipmap.icon_back_black)

        p.setMemberId(intent.getStringExtra("memberId"))
        tvName.text = intent.getStringExtra("name")
        tvRegion.text = intent.getStringExtra("region")
        tvPrice.text = intent.getStringExtra("price")
        GlideHelper.loadAvatar(this , intent.getStringExtra("avatar"),ivAvatar)

        multiImageSelectorView.setShowAddImageOnMaximum(false)
        initListener()
    }


    private fun initListener() {
        multiImageSelectorView.setOnItemClickListener(this)
        layoutPicTip.setOnClickListener {
            //添加第一个图片
            PermissionUtil.requestPermissions(
                this, this,
                Permission.WRITE_EXTERNAL_STORAGE, Permission.CAMERA
            )
        }
        btnSubmit.setOnClickListener {
            val content = etContent.text.toString().trim()
            if(TextUtils.isEmpty(content)){
                ToastUtil.showShort(this@QuestionActivity, "请输入内容")
                return@setOnClickListener
            }
            showLoadingDialog()
            p.submit(content)
        }
    }

    fun submitOk(text: String?){
        ToastUtil.showShort(this, text)
        finish()
    }
    fun submitFail(text : String?){
        ToastUtil.showShort(this, if(TextUtils.isEmpty(text)) "提交失败" else text)
    }

    override fun onGranted(permissions: MutableList<String>?) {
        MultiImageSelector.create()
            .showCamera(true) // show camera or not. true by default
            .count(1) // max select image size, 9 by default. used width #.multi()
            .single() // multi mode, default mode;
            .origin(p.picList) // original select data set, used width #.multi()
            .start(this, CODE_IMAGE_SELECT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == CODE_IMAGE_SELECT){
                val arrayList = data?.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT)
                if (arrayList != null && !arrayList.isEmpty()) {
                    ImageCropHelper.startCropFree(this, arrayList.get(0),
                        ImageCropHelper.ImageCropParamsBuilder
                        .create()
                        .compressQuality(90)
                        .cropMode(CropImageView.CropMode.FREE)
                        .outputMaxSize(1280, 1280)
                        .build(), CODE_IMAGE_CROP)
                }

            }else if(requestCode == CODE_IMAGE_CROP){
                val resultUri = ImageCropHelper.onActivityResult(resultCode, data)
                if (resultUri != null) {
                    val imagePath = MediaUtil.getPath(this, resultUri)
                    p.picList.add(imagePath)
                    layoutPicTip.visibility = View.GONE
                    multiImageSelectorView.visibility = View.VISIBLE
                    multiImageSelectorView.setList(p.picList)
                }
            }
        }
    }



    override fun onDelete(view: View?, position: Int) {
        if (position >= 0 && position < p.picList.size) {
            p.picList.removeAt(position)
            multiImageSelectorView.setList(p.picList)
        }
    }

    override fun onAdd(view: View?) {
        PermissionUtil.requestPermissions(
            this, this,
            Permission.WRITE_EXTERNAL_STORAGE, Permission.CAMERA
        )
    }
    override fun onItemClick(view: View?, position: Int) {
    }
    override fun onReachTheMaximum() {
    }
}