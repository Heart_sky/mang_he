package com.android.manghe.detail.precenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.detail.activity.TrendChartActivity;
import com.android.manghe.detail.model.AuctionHistoryRes;

import okhttp3.Call;

import java.util.HashMap;

public class PTrendChart extends XPresenter<TrendChartActivity> {


    public String mAuctionId = "";

    public void loadData(){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("size","20");
        dataMap.put("auction_id",mAuctionId);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ACUTIONREHISTORY, dataMap, headMap)
                .get()
                .execute(new BaseBack<AuctionHistoryRes>() {

                    @Override
                    public void onSuccess(Call call, AuctionHistoryRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.code == 0) {
                            getV().updateView(res);
                        } else {
                            ToastUtil.showShort(getV(), "数据出错，请稍后重试");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        e.printStackTrace();
                        getV().hideLoadingDialog();
                        ToastUtil.showShort(getV(), "数据出错，请稍后重试");
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
