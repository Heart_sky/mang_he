package com.android.manghe.detail.precenter;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.detail.activity.PayHistoryActivity;
import com.android.manghe.detail.model.PeopleGotRes;
import okhttp3.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PPayHistory extends XPresenter<PayHistoryActivity> {

    private String orderSN = "";
    private int mCurrentPage = 1;
    private boolean isDeal;
    private List<PeopleGotRes.ListBean> mDataList = new ArrayList<>();

    public void setOrderSN(String orderSN){
        this.orderSN = orderSN;
    }
    public void setDeal(boolean deal) {
        isDeal = deal;
    }

    public void loadData(boolean isRefresh){
        if (isRefresh) {
            mCurrentPage = 1;
            mDataList.clear();
        }
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("page", mCurrentPage+"");
        dataMap.put("order_sn", orderSN);
        dataMap.put(isDeal?"size":"per_page", "100");

        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + (isDeal ? ConstantsUrl.ACUTIONREJOINLOG : ConstantsUrl.ACUTIONREJOINLOG_ING), dataMap, null)
                .get()
                .execute(new BaseBack<PeopleGotRes>() {

                    @Override
                    public void onSuccess(Call call, PeopleGotRes res) {
                        if (res != null && res.code == 0 && res.list != null) {
                            mDataList.addAll(res.list);
                            getV().update(mDataList);
                            mCurrentPage++;
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
