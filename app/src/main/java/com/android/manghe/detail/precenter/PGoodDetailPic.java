package com.android.manghe.detail.precenter;

import android.text.TextUtils;
import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.detail.activity.GoodDetailPicActivity;
import com.google.gson.Gson;
import okhttp3.Call;

import java.util.HashMap;

public class PGoodDetailPic extends XPresenter<GoodDetailPicActivity> {

    public void loadData(String auctionId){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
headMap.put("PLATFORM", "android");
        HashMap<String, String> dataMap = new HashMap<>();
        dataMap.put("id", auctionId);
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ACUTIONCONTENT, dataMap, headMap)
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        if(!TextUtils.isEmpty(json)){
                            NetBean<String> res = new Gson().fromJson(json, NetBean.class);
                            if (res != null && res.code == 0 && !TextUtils.isEmpty(res.data)) {
                                getV().showWeb(res.data);
                                return;
                            }else{
                                ToastUtil.showShort(getV(), "数据出错，请稍后重试");
                            }
                        }
                        getV().hideLoadingDialog();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        ToastUtil.showShort(getV(), "数据出错，请稍后重试");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
