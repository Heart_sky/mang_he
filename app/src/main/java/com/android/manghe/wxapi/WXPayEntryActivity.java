package com.android.manghe.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.android.base.tools.ToastUtil;
import com.android.manghe.common.activity.PaySuccessActivity;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshMemberInfoEvent;
import com.eightbitlab.rxbus.Bus;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.Locale;


public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
    private IWXAPI api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, ConstantsUrl.PAY_WX_APPID);
        api.handleIntent(getIntent(), this);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
    }

    @Override
    public void onResp(BaseResp resp) {
        if (resp.getType() != ConstantsAPI.COMMAND_PAY_BY_WX) {
            finish();
            return;
        }

        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                Bus.INSTANCE.send(new RefreshMemberInfoEvent());
                startActivity(new Intent(this, PaySuccessActivity.class));
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                ToastUtil.showShort(this, "您取消了支付");
                break;
            default:
                String errorMsg = String.format(Locale.CHINA, "errCode:%d;errStr::%s", resp.errCode, resp.errStr);
//                ToastUtil.showLong(this,errorMsg+"-");
                ToastUtil.showShort(this, "支付出错");
                break;
        }
        finish();
    }
}