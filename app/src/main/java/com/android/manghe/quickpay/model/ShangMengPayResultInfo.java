package com.android.manghe.quickpay.model;
/**
 * 快捷支付 付款结果信息
 */
public class ShangMengPayResultInfo {


    /**
     * offset_amount : 0
     * order_no : 20210219230401_247308
     * order_time : 20210219230418
     * paid_amount : 6.9
     * pay_product_code : 90101
     * serial_no : T102081380
     * status : 1
     * success_time : 20210219230433
     * trade_amt : 6.9
     * trade_code : T0002
     */

    public String offset_amount;
    public String order_no;
    public String order_time;
    public String paid_amount;
    public String pay_product_code;
    public String serial_no;
    public String status;
    public String success_time;
    public double trade_amt;
    public String trade_code;
}
