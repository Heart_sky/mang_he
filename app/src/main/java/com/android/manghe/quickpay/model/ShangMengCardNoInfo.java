package com.android.manghe.quickpay.model;

import java.io.Serializable;

/**
 * 快捷支付 获取支付卡的详细信息
 */
public class ShangMengCardNoInfo implements Serializable {


    /**
     * bank_code : ICBC
     * bank_name : 工商银行
     * business_code : 03
     * card_type : 0
     * day_amount_limit : 200000
     * month_amount_limit : 500000
     * single_amount_limit : 10000
     */

    public String bank_code;
    public String bank_name;
    public String business_code;
    public String card_type;
    public String day_amount_limit;
    public String month_amount_limit;
    public String single_amount_limit;


    public String bind_card_id;
    public String card_no;

    public ShangMengCardNoInfo(String bank_name, String bind_card_id, String card_no) {
        this.bank_name = bank_name;
        this.bind_card_id = bind_card_id;
        this.card_no = card_no;
    }
}
