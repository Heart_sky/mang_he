package com.android.manghe.quickpay.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.common.model.BuyGoodShangMengZhiFuRes;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.quickpay.activity.QuickPayDetailActivity;
import com.android.manghe.quickpay.model.ShangMengCardInfoRes;
import com.android.manghe.quickpay.model.CardTypeMode;
import com.android.manghe.quickpay.model.ShangMengCardNoInfo;
import com.android.manghe.common.model.BuyMemberShangMengZhiFuRes;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PQuickPayDetail extends XPresenter<QuickPayDetailActivity> {
    public BuyMemberShangMengZhiFuRes.DataBean buyMemberShangMengData;
    public BuyGoodShangMengZhiFuRes.DataBean buyGoodShangMengData;
    public List<ShangMengCardNoInfo> cardList = new ArrayList<>();
    public ShangMengCardNoInfo currentCardInfo;
    public boolean isSecondPay;
    public boolean isBuyGood;

    public void checkCardType(String cardNo){
        getV().showLoadingDialog();
        String url = "https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardBinCheck=true&cardNo=" + cardNo;
        new OKHttpUtil(getV()).url(url)
                .get()
                .execute(new GsonBaseBack<CardTypeMode>() {

                    @Override
                    public void onSuccess(Call call, CardTypeMode res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.validated) {
                            if(TextUtils.equals("CC",res.cardType)){
                                //信用卡

                            }else if(TextUtils.equals("DC",res.cardType)){
                                //储蓄卡

                            }
                        } else {
                            ToastUtil.showLong(getV(), "请检查您的卡号是否输入有误");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        ToastUtil.showLong(getV(), "检查卡号失败，请重试。");
                    }
                });

    }

    public void next(String cardNo){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ShangMengCardInfo, headMap)
                .post("card_no",cardNo)
                .build().execute(new BaseBack<ShangMengCardInfoRes>() {

            @Override
            public void onSuccess(Call call, ShangMengCardInfoRes bean) {
                getV().hideLoadingDialog();
                if (bean.code == 0) {
                    if(TextUtils.equals("000000", bean.data.resp_code)){
                        ShangMengCardNoInfo cardNoRes = new Gson().fromJson(bean.data.sumpay_check_card_no_response, ShangMengCardNoInfo.class);
                        getV().toPayPage(cardNoRes, cardNo);
                    }else{
                        ToastUtil.showLong(getV(), bean.data.resp_msg);
                    }
                }else{
                    ToastUtil.showLong(getV(), bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().hideLoadingDialog();
                ToastUtil.showLong(getV(), "请求失败，请重试！");
            }
        });
    }


    public void deleteCard(ShangMengCardNoInfo cardNoInfo){
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        String uid = UserHolder.getUID(getV());
        headMap.put("UID", uid);
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ShangMengDelCard, headMap)
                .post("user_id",uid)
                .post("bind_card_id",cardNoInfo.bind_card_id)
                .build().execute(new BaseBack<ShangMengCardInfoRes>() {

            @Override
            public void onSuccess(Call call, ShangMengCardInfoRes bean) {
                getV().hideLoadingDialog();
                if (bean.code == 0) {
                    if(TextUtils.equals("000000", bean.data.resp_code)){
                        cardList.remove(cardNoInfo);
                        if(TextUtils.equals(currentCardInfo.bind_card_id, cardNoInfo.bind_card_id)){
                            //变成添加新卡输入框
                            getV().changeToAddOtherCardStatusView();
                        }
                        if(cardList.size() == 0){
                            //关闭弹出框，隐藏已绑定的银行卡
                            getV().closePopupDialog();
                            getV().hideChooseBoundCardEntrance();
                        }else{
                            //更新弹出框中的cardList和currentCardNo
                            getV().refreshChooseCardDialog();
                        }
                    }else{
                        ToastUtil.showLong(getV(), bean.data.resp_msg);
                    }
                }else{
                    ToastUtil.showLong(getV(), bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().hideLoadingDialog();
                ToastUtil.showLong(getV(), "请求失败，请重试！");
            }
        });
    }
}
