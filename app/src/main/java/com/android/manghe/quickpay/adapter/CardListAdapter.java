package com.android.manghe.quickpay.adapter;

import android.content.Context;

import com.android.manghe.R;
import com.android.manghe.quickpay.model.ShangMengCardNoInfo;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.HashMap;
import java.util.List;

public class CardListAdapter extends BaseQuickAdapter<ShangMengCardNoInfo, BaseViewHolder> {
    private Context mContext;
    private HashMap<String, Boolean> mSelectedMap = new HashMap<>();
    private ShangMengCardNoInfo mSelectedItemObject;
    private boolean isEdit;

    public CardListAdapter(Context context, List<ShangMengCardNoInfo> list) {
        super(R.layout.item_card, list);
        mContext = context;
        update(list);
    }

    public void setEditStatus(boolean isEdit){
        this.isEdit = isEdit;
        notifyDataSetChanged();
    }

    public void update(List<ShangMengCardNoInfo> list) {
        mSelectedMap.clear();
        ShangMengCardNoInfo info;
        for (int i = 0; i < list.size(); i++) {
            info = list.get(i);
            mSelectedMap.put(info.bind_card_id, i == 0);
            if (i == 0) {
                list.set(0, info);
                mSelectedItemObject = info;
            }
        }
        replaceData(list);
    }

    public void selectedItem(ShangMengCardNoInfo info) {

        for (int i = 0; i < getData().size(); i++) {
            mSelectedMap.put(getData().get(i).bind_card_id, false);
        }
        if(info != null) {
            mSelectedMap.put(info.bind_card_id, true);
            mSelectedItemObject = info;
        }
        notifyDataSetChanged();
    }

    public ShangMengCardNoInfo getSelectedPayment() {
        return mSelectedItemObject;
    }

    @Override
    protected void convert(BaseViewHolder holder, ShangMengCardNoInfo item) {
        holder.setText(R.id.tvBankName, item.bank_name);
        holder.setText(R.id.tvCardNo, "****" + item.card_no);
        holder.setBackgroundRes(R.id.ivCheck, mSelectedMap.get(item.bind_card_id) ? R.mipmap.tick_yellow_normal : R.color.white);
        holder.setGone(R.id.ivCheck, !isEdit);
        holder.setGone(R.id.ivDel, isEdit);

        holder.addOnClickListener(R.id.layoutItemContent, R.id.ivCheck, R.id.ivDel);
    }
}
