package com.android.manghe.quickpay.presenter;

import android.text.TextUtils;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.PayHolder;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.common.model.BuyGoodShangMengZhiFuRes;
import com.android.manghe.common.model.BuyMemberShangMengZhiFuRes;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.quickpay.activity.CCPayActivity;
import com.android.manghe.quickpay.model.ShangMengCardNoInfo;
import com.android.manghe.quickpay.model.ShangMengNewOrderRes;
import com.android.manghe.quickpay.model.ShangMengPayRes;
import com.android.manghe.quickpay.model.ShangMengPayResultInfo;
import com.android.manghe.quickpay.model.ShangMengPayResultRes;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;

public class PCCPay extends XPresenter<CCPayActivity> {
    public ShangMengCardNoInfo cardNoData;
    public BuyMemberShangMengZhiFuRes.DataBean.OrderBean buyMemberOrderDetail;
    public BuyGoodShangMengZhiFuRes.DataBean.OrderBean buyGoodOrderDetail;
    public boolean isBuyGood;
    public boolean isSecondPay;
    public String cardNo = "";

    public int logId = -1;
    public String outTradeNo = "";

    public void toPay(String smsCode) {
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ShangMengPay, headMap)
                .post("log_id", isBuyGood ? buyGoodOrderDetail.log_id + "" : logId + "")
                .post("verify_code", smsCode)
                .build().execute(new BaseBack<ShangMengPayRes>() {

            @Override
            public void onSuccess(Call call, ShangMengPayRes bean) {
                getV().hideLoadingDialog();
                if (bean.code == 0) {
                    if (TextUtils.equals("000000", bean.data.resp_code)) {
                        HashMap<String, String> headMap = new HashMap<>();
                        headMap.put("UID", UserHolder.getUID(getV()));
                        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
                        headMap.put("PLATFORM", "android");
                        Map<String, String> paramMap = new HashMap<>();
                        String orderNo = isBuyGood ? buyGoodOrderDetail.out_trade_no : outTradeNo;
                        paramMap.put("order_no", orderNo);
                        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.CheckQuickPayResult, headMap)
                                .post(paramMap)
                                .build().execute(new BaseBack<ShangMengPayResultRes>() {

                            @Override
                            public void onSuccess(Call call, ShangMengPayResultRes bean1) {
                                getV().hideLoadingDialog();
                                if (bean1.code == 0) {
                                    if(TextUtils.equals("000000", bean1.data.resp_code)){
                                        ShangMengPayResultInfo resultInfo = new Gson().fromJson(bean1.data.sumpay_order_search_merchant_response, ShangMengPayResultInfo.class);
                                        if(TextUtils.equals("1", resultInfo.status)) {
                                            getV().paySuccess();
                                        }else{
                                            getV().payFail();
                                        }
                                    }else{
                                        ToastUtil.showLong(getV(), bean1.data.resp_msg);
                                    }
                                }else{
                                    ToastUtil.showLong(getV(), bean1.msg);
                                }
                            }

                            @Override
                            public void onFailure(Exception e) {
                                super.onFailure(e);
                                getV().payFail();
                                getV().hideLoadingDialog();
                            }
                        });
                        return;
                    } else {
                        ToastUtil.showLong(getV(), bean.data.resp_msg);
                    }
                } else if (bean.code == 100099) {
                    ToastUtil.showLong(getV(), "请勿重复支付！");
                    logId = -1;
                    outTradeNo = "";
                } else if (bean.code == 100098) {
                    getV().payFail();
                } else {
                    getV().sendCodeFail(bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().hideLoadingDialog();
                getV().payFail();
            }
        });
    }

    /**
     * 生成新订单编号
     */
    public void newOrder(String tel, String realname, String certificateNo, String cvv, String validYear, String validMonth) {
        HashMap<String, String> data = new HashMap<>();
        data.put("id", PayHolder.shangMengCouponId);
        data.put("pay_id", PayHolder.shangMengPayid);
        data.put("count", "1");
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        getV().showLoadingDialog();
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.VOUCHERCHECKOUT, headMap)
                .post(data).build()
                .execute(new BaseBack<BuyMemberShangMengZhiFuRes>() {
                    @Override
                    public void onSuccess(Call call, BuyMemberShangMengZhiFuRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.data != null) {
                            logId = res.data.order.log_id;
                            outTradeNo = res.data.order.out_trade_no;
                            sendCode(tel, realname, certificateNo, cvv, validYear, validMonth);
                        } else {
                            getV().sendCodeFail(null);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        getV().hideLoadingDialog();
                        getV().sendCodeFail(null);
                    }

                    @Override
                    public void onComplete() {
                        getV().hideLoadingDialog();
                    }
                });
    }

    public void sendCode(String tel, String realname, String certificateNo, String cvv, String validYear, String validMonth) {
        getV().showLoadingDialog();
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("card_type", "1");
        paramMap.put("log_id", isBuyGood ? buyGoodOrderDetail.log_id + "" : logId + "");
        paramMap.put("mobile_no", tel);
        paramMap.put("id_no", certificateNo);
        paramMap.put("realname", realname);
        paramMap.put("bank_code", cardNoData.bank_code);
        paramMap.put("cvv", cvv);
        paramMap.put("valid_year", validYear);
        paramMap.put("valid_month", validMonth);

        if (!TextUtils.isEmpty(cardNoData.bind_card_id)) {
            //使用已绑定的储蓄卡
            paramMap.put("bind_card_id", cardNoData.bind_card_id);
        } else {
            //使用新的储蓄卡
            paramMap.put("card_no", cardNo);
        }

        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.ShangMengNewOrder, headMap)
                .post(paramMap)
                .build().execute(new BaseBack<ShangMengNewOrderRes>() {

            @Override
            public void onSuccess(Call call, ShangMengNewOrderRes bean) {
                getV().hideLoadingDialog();
                if (bean.code == 0) {
                    if (TextUtils.equals("000000", bean.data.resp_code)) {
                        getV().sendCodeOk();
                    } else {
                        ToastUtil.showLong(getV(), bean.data.resp_msg);
                    }
                } else {
                    getV().sendCodeFail(bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                getV().hideLoadingDialog();
                getV().sendCodeFail(null);
            }
        });
    }
}
