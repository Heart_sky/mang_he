package com.android.manghe.quickpay.view;

import android.content.Context;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.base.view.recycler.RecyclerViewDivider;
import com.android.manghe.R;
import com.android.manghe.quickpay.adapter.CardListAdapter;
import com.android.manghe.quickpay.model.ShangMengCardNoInfo;
import com.lxj.xpopup.core.BottomPopupView;

import java.util.List;

public class CardListPopupView  extends BottomPopupView {
    private ICardListPopupViewListener cardListPopupViewListener;
    private boolean isEdit;
    private TextView tvRightFun, tvOtherCard;
    private Context context;

    private RecyclerView recyclerView;
    private CardListAdapter adapter;

    List<ShangMengCardNoInfo> cardList;
    ShangMengCardNoInfo currentCard;

    public CardListPopupView(@NonNull Context context, List<ShangMengCardNoInfo> cardList, ShangMengCardNoInfo currentCard,
                             ICardListPopupViewListener cardListPopupViewListener) {
        super(context);
        this.context = context;
        this.cardList = cardList;
        this.currentCard = currentCard;
        this.cardListPopupViewListener = cardListPopupViewListener;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.layout_card_list_popup;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        tvRightFun = findViewById(R.id.tvRightFun);
        tvOtherCard = findViewById(R.id.tvOtherCard);
        recyclerView = findViewById(R.id.recyclerView);

        tvRightFun.setOnClickListener(v -> {
            isEdit = !isEdit;
            tvRightFun.setTextColor(ContextCompat.getColor(context, isEdit ? R.color.blue900 : R.color.colorFont66));
            tvRightFun.setText(isEdit ? "完成" : "编辑");
            adapter.setEditStatus(isEdit);
        });
        tvOtherCard.setOnClickListener(v -> {
            cardListPopupViewListener.onClickAddOtherCard();
            dismiss();
        });
        adapter = new CardListAdapter(context, cardList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.addItemDecoration(new RecyclerViewDivider(context,LinearLayoutManager.HORIZONTAL));
        recyclerView.setAdapter(adapter);
        adapter.selectedItem(currentCard);
        adapter.setOnItemChildClickListener((ada, view, position) -> {
            try{
                ShangMengCardNoInfo cardInfo = adapter.getData().get(position);
                if(view.getId() == R.id.ivDel){
                    //删除
                    cardListPopupViewListener.onDelete(cardInfo);
                }else if(view.getId() == R.id.ivCheck || view.getId() == R.id.layoutItemContent){
                    //选择
                    adapter.selectedItem(cardInfo);
                    cardListPopupViewListener.onClickItem(cardInfo);
                    dismiss();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        });
    }


    public void updateData(List<ShangMengCardNoInfo> cardList, ShangMengCardNoInfo currentCard){
        this.cardList = cardList;
        this.currentCard = currentCard;
        adapter.setNewData(cardList);
        adapter.selectedItem(currentCard);
    }

    public interface ICardListPopupViewListener{
        void onClickItem(ShangMengCardNoInfo info);
        void onClickAddOtherCard();
        void onDelete(ShangMengCardNoInfo info);
    }
}
