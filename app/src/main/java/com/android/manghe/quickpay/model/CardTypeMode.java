package com.android.manghe.quickpay.model;

public class CardTypeMode {


    /**
     * messages : [{"errorCodes":"CARD_BIN_NOT_MATCH","name":"cardNo"}]
     * validated : false
     * stat : ok
     * key : 222200202002
     */

    public Boolean validated;
    public String cardType;

}
