package com.android.manghe.quickpay.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.io.Serializable;

/**
 * 快捷支付 支付结果
 */
public class ShangMengPayRes extends NetBean<ShangMengPayRes.DataBean> {

    public static class DataBean implements Serializable {
        /**
         * resp_code : AM999998
         * sign :
         * resp_msg : 银行卡号格式不正确
         * sign_type :
         */

        public String resp_code;
        public String sign;
        public String resp_msg;
        public String sign_type;
        public String sumpay_order_apply_response;
    }
}
