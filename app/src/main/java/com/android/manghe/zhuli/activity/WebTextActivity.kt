package com.android.manghe.zhuli.activity

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import com.android.base.frame.view.MVPActivity
import com.android.manghe.R
import com.android.manghe.zhuli.presenter.PWebText
import kotlinx.android.synthetic.main.activity_article.*

class WebTextActivity : MVPActivity<PWebText>() {
    companion object {
        fun showActivity(context: Context, title: String, url : String) {
            val intent = Intent(context, WebTextActivity::class.java)
            intent.putExtra("title", title)
            intent.putExtra("url", url)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_article
    override fun initData(savedInstanceState: Bundle?) {
        titleBar.setToolbar(intent.getStringExtra("title")).setLeftIcon(R.mipmap.icon_back_black)
            .setTitleAndStatusBgColor(R.color.white)
        p.getRule(intent.getStringExtra("url"))
    }

    fun showText(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvContent.text = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        }else{
            tvContent.text = Html.fromHtml(text)
        }
    }

}