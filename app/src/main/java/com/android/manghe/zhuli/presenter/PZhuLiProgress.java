package com.android.manghe.zhuli.presenter;

import android.view.View;
import android.widget.RelativeLayout;

import com.android.base.frame.presenter.XPresenter;
import com.android.base.okhttp.okUtil.OKHttpUtil;
import com.android.base.okhttp.okUtil.base.NetBean;
import com.android.base.okhttp.okUtil.callback.GsonBaseBack;
import com.android.base.tools.ToastUtil;
import com.android.manghe.cache.UserHolder;
import com.android.manghe.common.model.BaseBack;
import com.android.manghe.config.ConstantsUrl;
import com.android.manghe.config.events.RefreshOrdersEvent;
import com.android.manghe.mine.model.InvitationDataRes;
import com.android.manghe.orderpay.model.ZhuLiRecordRes;
import com.android.manghe.zhuli.activity.ZhuLiProgressActivity;
import com.android.manghe.zhuli.model.RedPackageBean;
import com.android.manghe.zhuli.model.RedPackageRes;
import com.eightbitlab.rxbus.Bus;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

public class PZhuLiProgress extends XPresenter<ZhuLiProgressActivity> {

    public String orderSN = "";
    public String thumb = "";
    public float currentProgress;
    private int mCurrentPage = 1;
    private final List<ZhuLiRecordRes.DataBean.RecordInfo> recordList = new ArrayList<>();
    public final List<RedPackageBean> redPackageList = new ArrayList<>();

    public void getZhuLiRecordList(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            recordList.clear();
        }

        HashMap<String, String> data = new HashMap<>();
        data.put("page", mCurrentPage + "");
        data.put("goods_order_sn", orderSN);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.PinDanRecord, data, headMap)
                .get()
                .execute(new BaseBack<ZhuLiRecordRes>() {
                    @Override
                    public void onSuccess(Call call, ZhuLiRecordRes res) {
                        getV().hideLoadingDialog();
                        if (res != null && res.data != null && res.data.list != null && !res.data.list.isEmpty()) {
                            recordList.addAll(res.data.list);
                            getV().setCanLoadMore(res.data.list.size() == 10);
                            getV().update(recordList);
                            if (recordList.size() == 10) {
                                mCurrentPage++;
                            }
                            return;
                        }
                        getV().update(recordList);
                    }
                });
    }

    public void getStrategy() {
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.PinDanGongLue, headMap)
                .get()
                .execute(new GsonBaseBack() {

                    @Override
                    public void onSuccess(Call call, String json) {
                        if (json != null && !json.isEmpty()) {
                            try {
                                String desc = new JSONObject(json).getString("data");
                                getV().showZhuLiGongLue(desc);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }
                });
    }

    public String shareTitle = "", shareContent = "", shareUrl = "", sharePic = "";

    public void getShareContent() {
        HashMap headMap = new HashMap<String, String>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        HashMap dataMap = new HashMap<String, String>();
        dataMap.put("app_name", "meng_xiang");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.MEMBER_INVITATION, dataMap, headMap)
                .get()
                .execute(new BaseBack<InvitationDataRes>() {

                    @Override
                    public void onSuccess(Call call, InvitationDataRes res) {
                        if (res != null && res.code == 0 && res.data != null) {
                            if (res.data.comment != null) {
                                shareTitle = res.data.comment.site_name;
                                shareContent = res.data.comment.site_name + "-" + res.data.comment.text;
                                shareUrl = res.data.comment.url;
                                sharePic = res.data.comment.pic;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }
                });
    }

    public void getRedPackageList() {
        HashMap headMap = new HashMap<String, String>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.RED_PACKAGE_LIST + "?goods_order_sn=" + orderSN, headMap)
                .get()
                .execute(new BaseBack<RedPackageRes>() {

                    @Override
                    public void onSuccess(Call call, RedPackageRes res) {
                        if (res != null && res.data != null && res.data.list != null && !res.data.list.isEmpty()) {
                            redPackageList.addAll(res.data.list);
                            getV().startFallDown();
//                            getV().updateRedPackage();

                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
//                        getV().hideLoadingDialog();
                    }
                });
    }

    public void getRedPackage(RedPackageBean packageBean, RelativeLayout randomLayout, View deleteView) {
        HashMap headMap = new HashMap<String, String>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.GET_RED_PACKAGE + "?id=" + packageBean.id, headMap)
                .get()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        if (res != null) {
                            if (res.code == 0) {
                                currentProgress = currentProgress + packageBean.distance_amount;
                                BigDecimal bigDecimal = new BigDecimal(currentProgress);
                                currentProgress = bigDecimal.setScale(3, BigDecimal.ROUND_HALF_UP).floatValue();
                                getV().updateProgress();
                                randomLayout.removeView(deleteView);
                                getZhuLiRecordList(true);
                                Bus.INSTANCE.send(new RefreshOrdersEvent());
                            } else {
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        } else {
                            ToastUtil.showShort(getV(), "领取失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                        ToastUtil.showShort(getV(), "领取失败");
                    }
                });
    }

    public void getRedPackage(RedPackageBean packageBean) {
        HashMap headMap = new HashMap<String, String>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.GET_RED_PACKAGE + "?id=" + packageBean.id, headMap)
                .get()
                .execute(new BaseBack<NetBean>() {

                    @Override
                    public void onSuccess(Call call, NetBean res) {
                        if (res != null) {
                            if (res.code == 0) {
                                currentProgress = currentProgress + packageBean.distance_amount;
                                BigDecimal bigDecimal = new BigDecimal(currentProgress);
                                currentProgress = bigDecimal.setScale(3, BigDecimal.ROUND_HALF_UP).floatValue();
                                getV().updateProgress();
                                getZhuLiRecordList(true);
                                Bus.INSTANCE.send(new RefreshOrdersEvent());
                            } else {
                                ToastUtil.showShort(getV(), res.msg);
                            }
                        } else {
                            ToastUtil.showShort(getV(), "领取失败");
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        super.onFailure(e);
                    }
                });
    }

    /**
     * 确认领取
     */
    public void confirmGet(){
        getV().showLoadingDialog();
        HashMap<String, String> data = new HashMap<>();
        data.put("goods_order_sn", orderSN);
        HashMap<String, String> headMap = new HashMap<>();
        headMap.put("UID", UserHolder.getUID(getV()));
        headMap.put("TOKEN", UserHolder.getUserInfo(getV()).TOKEN);
        headMap.put("PLATFORM", "android");
        new OKHttpUtil(getV()).urlByHeadData(ConstantsUrl.domain + ConstantsUrl.GET_MIAN_DAN_GOOD, data, headMap)
                .get().execute(new BaseBack<NetBean>() {
            @Override
            public void onSuccess(Call call, NetBean bean) {
                if (bean.code == 0) {
                    ToastUtil.showLong(getV(), "领取成功");
                    getV().confirmGetGoodSuccess();
                }else{
                    ToastUtil.showLong(getV(), bean.msg);
                }
            }

            @Override
            public void onFailure(Exception e) {
                super.onFailure(e);
                ToastUtil.showLong(getV(), "领取失败");
            }

            @Override
            public void onComplete() {
                getV().hideLoadingDialog();
            }
        });
    }
}
