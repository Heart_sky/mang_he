package com.android.manghe.zhuli.model;

import com.android.base.okhttp.okUtil.base.NetBean;

import java.util.List;

public class RedPackageRes extends NetBean<RedPackageRes.DataBean> {
    public static class DataBean {
        public List<RedPackageBean> list;

    }
}
