package com.android.manghe.zhuli.adapter

import android.content.Context
import com.android.base.tools.Glide.GlideHelper
import com.android.manghe.R
import com.android.manghe.orderpay.model.ZhuLiRecordRes
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

class ZhuLiRecordAdapter(context: Context)  : BaseQuickAdapter<ZhuLiRecordRes.DataBean.RecordInfo, BaseViewHolder>(R.layout.item_zhu_li_record){

    private var context : Context? = null
    init {
        this.context = context
    }
    override fun convert(holder: BaseViewHolder, item: ZhuLiRecordRes.DataBean.RecordInfo) {
        GlideHelper.loadAvatar(context, item.avatar, holder.getView(R.id.ivAvatar))
        holder.setText(R.id.tvName, item.nickname)
        holder.setText(R.id.tvProgress, "${item.distance_amount}%")
    }
}