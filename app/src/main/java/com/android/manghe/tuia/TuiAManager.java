package com.android.manghe.tuia;

import android.content.Context;
import android.os.Build;
import android.webkit.WebSettings;

public class TuiAManager {

    public static void send(Context context, String device, int subType) {
//        Object obj = SerializeUtil.loadFile(Config.SerializeTuiA);
//        if (obj != null) {
//            TuiABean bean = (TuiABean) obj;
//            if ((subType == 1 && bean.is1Ok) || (subType == 2 && bean.is2Ok)
//                    || (subType == 3 && bean.is3Ok) || (subType == 5 && bean.is5Ok)
//                    || (subType == 6 && bean.is6Ok)) {
//                return;
//            }
//        }
//        new OKHttpUtil(context).url(ConstantsUrl.domain + ConstantsUrl.GET_IP)
//                .get()
//                .execute(new BaseBack<GetIpRes>() {
//                    @Override
//                    public void onSuccess(Call call, GetIpRes obj) {
//                        super.onSuccess(call, obj);
//                        if (obj.code == 0 && obj.data != null) {
//                            HashMap<String, String> data = new HashMap<>();
//                            data.put("device", device);
//                            data.put("subType", subType + "");
//                            try {
//                                data.put("ua", URLEncoder.encode(getUserAgent(context), "utf-8"));
//                            }catch (Exception e){
//                                e.printStackTrace();
//                            }
//                            data.put("ip", obj.data.ip);
//                            new OKHttpUtil(context).url(ConstantsUrl.domain + ConstantsUrl.TuiA)
//                                    .post(data)
//                                    .build()
//                                    .execute(new BaseBack<NetBean>() {
//                                        @Override
//                                        public void onSuccess(Call call, NetBean bean) {
//                                            try{
//                                                if(bean.code == 0) {
//                                                    Object obj = SerializeUtil.loadFile(Config.SerializeTuiA);
//                                                    TuiABean beanSave;
//                                                    if (obj != null) {
//                                                        beanSave = (TuiABean) obj;
//                                                    } else {
//                                                        beanSave = new TuiABean();
//                                                    }
//                                                    if (subType == 1) {
//                                                        beanSave.is1Ok = true;
//                                                    } else if (subType == 2) {
//                                                        beanSave.is2Ok = true;
//                                                    } else if (subType == 3) {
//                                                        beanSave.is3Ok = true;
//                                                    } else if (subType == 5) {
//                                                        beanSave.is5Ok = true;
//                                                    } else if (subType == 6) {
//                                                        beanSave.is6Ok = true;
//                                                    }
//                                                    SerializeUtil.saveFile(beanSave, Config.SerializeTuiA);
//                                                }
//                                            }catch (Exception e){
//                                                e.printStackTrace();
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onFailure(Exception e) {
//                                            super.onFailure(e);
//                                        }
//
//                                        @Override
//                                        public void onComplete() {
//                                        }
//                                    });
//
//                        }
//                    }
//                });


    }

    public static String getUserAgent(Context context) {
        String userAgent = "";
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                try {
                    userAgent = WebSettings.getDefaultUserAgent(context);
                } catch (Exception e) {
                    userAgent = System.getProperty("http.agent");
                }
            } else {
                userAgent = System.getProperty("http.agent");
            }
            StringBuffer sb = new StringBuffer();
            for (int i = 0, length = userAgent.length(); i < length; i++) {
                char c = userAgent.charAt(i);
                if (c <= '\u001f' || c >= '\u007f') {
                    sb.append(String.format("\\u%04x", (int) c));
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        } catch (Exception e) {
        }
        return userAgent;
    }
}
