package me.nereo.multi_image_selector.utils;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * 屏幕工具
 * Created by nereo on 15/11/19.
 */
public class ScreenUtils {

    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (wm != null) {
            Display display = wm.getDefaultDisplay();
            Point out = new Point();
            display.getSize(out);
            return out;
        }
        return null;
    }
}
